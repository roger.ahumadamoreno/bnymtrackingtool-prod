(function () {
  function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

  function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

  function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

  function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

  function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

  function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

  function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

  function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

  function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

  function _possibleConstructorReturn(self, call) { if (call && (typeof call === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

  function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

  function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

  function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"], {
    /***/
    "+II3":
    /*!*********************************************************************************!*\
      !*** ./src/app/pages/tid-creation/cameracomponent/cameracomponent.component.ts ***!
      \*********************************************************************************/

    /*! exports provided: CameracomponentComponent */

    /***/
    function II3(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CameracomponentComponent", function () {
        return CameracomponentComponent;
      });
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! rxjs */
      "qCKp");
      /* harmony import */


      var ngx_webcam__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ngx-webcam */
      "QKVY");
      /* harmony import */


      var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! sweetalert2/dist/sweetalert2.js */
      "PdH4");
      /* harmony import */


      var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_2__);
      /* harmony import */


      var src_app_services_tid_tidcreartion_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/app/services/tid/tidcreartion-service.service */
      "eHNn");
      /* harmony import */


      var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/material/dialog */
      "0IaG");
      /* harmony import */


      var _image_manipulation_image_manipulation_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../image-manipulation/image-manipulation.component */
      "xw10");
      /* harmony import */


      var src_app_services_packages_audit_trail_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! src/app/services/packages/audit-trail.service */
      "irGt");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! @angular/material/progress-spinner */
      "Xa2L");

      function CameracomponentComponent_mat_spinner_4_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](0, "mat-spinner", 12);
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("diameter", 20);
        }
      }

      function CameracomponentComponent_webcam_17_Template(rf, ctx) {
        if (rf & 1) {
          var _r6 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "webcam", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("imageCapture", function CameracomponentComponent_webcam_17_Template_webcam_imageCapture_0_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r6);

            var ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"]();

            return ctx_r5.handleImage($event);
          })("cameraSwitched", function CameracomponentComponent_webcam_17_Template_webcam_cameraSwitched_0_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r6);

            var ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"]();

            return ctx_r7.cameraWasSwitched($event);
          })("initError", function CameracomponentComponent_webcam_17_Template_webcam_initError_0_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r6);

            var ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"]();

            return ctx_r8.handleInitError($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("height", 638)("width", 1152)("trigger", ctx_r3.triggerObservable)("allowCameraSwitch", ctx_r3.allowCameraSwitch)("switchCamera", ctx_r3.nextWebcamObservable)("videoOptions", ctx_r3.videoOptions)("imageQuality", 1);
        }
      }

      function CameracomponentComponent_div_18_div_2_Template(rf, ctx) {
        if (rf & 1) {
          var _r13 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](1, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](2, "div", 20);

          _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](3, "button", 21);

          _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function CameracomponentComponent_div_18_div_2_Template_button_click_3_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r13);

            var img_r10 = ctx.$implicit;
            var i_r11 = ctx.index;

            var ctx_r12 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](2);

            return ctx_r12.cardClick(img_r10, i_r11);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](4, "i", 22);

          _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](5, "img", 23);

          _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function CameracomponentComponent_div_18_div_2_Template_img_click_5_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r13);

            var i_r11 = ctx.index;

            var ctx_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](2);

            return ctx_r14.doubleClick($event, i_r11);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var img_r10 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](5);

          _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("src", img_r10.imageAsDataUrl, _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵsanitizeUrl"]);
        }
      }

      function CameracomponentComponent_div_18_Template(rf, ctx) {
        if (rf & 1) {
          var _r16 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](1, "div", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](2, CameracomponentComponent_div_18_div_2_Template, 6, 1, "div", 16);

          _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](3, "div", 17);

          _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](4, "span", 18);

          _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](5, "button", 19);

          _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function CameracomponentComponent_div_18_Template_button_click_5_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r16);

            var ctx_r15 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"]();

            return ctx_r15.saveBulkImages();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](6, "Save Images");

          _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngForOf", ctx_r4.imageArray.reverse());
        }
      }

      var CameracomponentComponent = /*#__PURE__*/function () {
        function CameracomponentComponent(_tidService, _dialogRef, data, _dialog, auditService) {
          _classCallCheck(this, CameracomponentComponent);

          this._tidService = _tidService;
          this._dialogRef = _dialogRef;
          this.data = data;
          this._dialog = _dialog;
          this.auditService = auditService; //   GLOBAL VARAIBLES   //
          // toggle webcam on/off

          this.showWebcam = true;
          this.allowCameraSwitch = true;
          this.multipleWebcamsAvailable = false; //public videoOptions: MediaTrackConstraints = {width: {width:  1080, height:2048 }, height: {min: 720, ideal: 1080}};

          this.videoOptions = {
            width: 3840,
            height: 2160,
            aspectRatio: 1.777777778
          }; //2048 × 1080

          this.imageArray = [];
          this.ss = [];
          this.errors = []; // latest snapshot

          this.webcamImage = null; //images to remove from array

          this.removeArray = []; // webcam snapshot trigger

          this.trigger = new rxjs__WEBPACK_IMPORTED_MODULE_0__["Subject"](); // switch to next / previous / specific webcam; true/false: forward/backwards, string: deviceId

          this.nextWebcam = new rxjs__WEBPACK_IMPORTED_MODULE_0__["Subject"]();
          this.enableZoom = true;
        }

        _createClass(CameracomponentComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this = this;

            console.log("DATAAA", this.data);
            this.userlogged = JSON.parse(localStorage.getItem('user'));

            if (this.userlogged != undefined) {
              console.log('User logged: ' + this.userlogged.User_Login);
            } else {
              sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_2___default()("Not logged in", "Please log back in", "error");
            }

            this.imageCounter = 0;
            this._dialogRef.disableClose = true;
            ngx_webcam__WEBPACK_IMPORTED_MODULE_1__["WebcamUtil"].getAvailableVideoInputs().then(function (mediaDevices) {
              _this.multipleWebcamsAvailable = mediaDevices && mediaDevices.length > 1;
            }); //this.showWebcam = false;

            this.trigger.next(); //this.showWebcam = false;
          }
        }, {
          key: "triggerSnapshot",
          value: function triggerSnapshot() {
            this.trigger.next();
            this._dialogRef.disableClose = false;
            var audit = {
              PID: this.data.Package_Tracking_ID,
              TID: 'N/A',
              ActionName: "Picture Taken",
              ActionDateTime: null,
              BusinessUnit: this.data.Client_Name,
              Sticker: '',
              User_ID: this.userlogged.User_Name
            }; //insert audit record

            this.auditService.saveAudit(audit).subscribe(function (data) {
              console.log(data);
            });
            this.imageCounter++;
          }
        }, {
          key: "saveBulkImages",
          value: function saveBulkImages() {
            var _this2 = this;

            this.showSpinner = true;
            var numberOfImages = this.imageArray.length;
            var counter = this.imageArray.length;
            var fileArray = [];
            this.imageArray.forEach(function (element) {
              // console.info('saving webcam image', img);
              _this2.webcamImage = element;
              var base64 = _this2.webcamImage.imageAsBase64;
              var fileData = _this2.webcamImage.imageAsBase64;
              var imageName = "PID-" + _this2.data.Package_Tracking_ID + '.tif';

              var imageBlob = _this2.dataURItoBlob(base64);

              var imageFile = new File([imageBlob], imageName, {
                type: 'image/tif'
              });
              fileArray.push(imageFile); // console.log(zip);      
            });
            var audit = {
              PID: this.data.Package_Tracking_ID,
              TID: 'N/A',
              ActionName: "Save Image",
              ActionDateTime: null,
              BusinessUnit: this.data.Client_Name,
              Sticker: '',
              User_ID: this.userlogged.User_Name
            }; //insert audit record

            this.auditService.saveAudit(audit).subscribe(function (data) {
              console.log(data);
            });

            this._tidService.uploadBulk("tid", "UploadMultiple", fileArray).subscribe(function (response) {
              console.log("success", response);
              counter++;

              if (_this2.imageArray.length >= 1) {
                sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                  position: 'top-end',
                  type: 'success',
                  title: 'Images saved successfully',
                  showConfirmButton: true,
                  timer: 800
                });
                _this2.imageArray = [];

                _this2._dialogRef.close();
              } else {
                if (_this2.imageArray.length < 1) {
                  sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                    position: 'top-end',
                    type: 'error',
                    title: 'Take an Image',
                    timer: 800
                  });
                } else {
                  sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                    position: 'top-end',
                    type: 'error',
                    title: 'Error saving',
                    timer: 800
                  });
                }
              }

              _this2.showSpinner = false;
            }, function (error) {
              counter--;
              _this2.showSpinner = false;
              console.log("Error saving images", error);
              sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                position: 'top-end',
                type: 'error',
                title: 'Image error, please retry saving',
                showConfirmButton: true
              });
            });
          }
        }, {
          key: "toggleWebcam",
          value: function toggleWebcam() {
            this.showWebcam = !this.showWebcam;
          }
        }, {
          key: "spliceValue",
          value: function spliceValue(a, index) {
            for (var i = 0, j = a.length; i < a.length; i++) {
              a.splice(i, 0, a[j - 1]);
              console.log('index:', i);
              console.log('copy:', a.join(','));
              a.splice(j, 1);
              console.log('trim:', a.join(','));
            }
          }
        }, {
          key: "handleInitError",
          value: function handleInitError(error) {
            this.errors.push(error);
          }
        }, {
          key: "cardClick",
          value: function cardClick(img, index) {
            var _this3 = this;

            console.log(this.imageArray);
            console.log(img, index);
            sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
              title: 'Are you sure?',
              text: "Do you wish to remove this image?",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, remove it'
            }).then(function (result) {
              if (result.value == true) {
                var audit = {
                  PID: _this3.data.Package_Tracking_ID,
                  TID: 'N/A',
                  ActionName: "Picture Deleted",
                  ActionDateTime: null,
                  BusinessUnit: _this3.data.Client_Name,
                  Sticker: '',
                  User_ID: _this3.userlogged.User_Name
                }; //insert audit record

                _this3.auditService.saveAudit(audit).subscribe(function (data) {
                  console.log(data);
                }); //search for image


                for (var _index = 0; _index < _this3.imageArray.length; _index++) {
                  var element = _this3.imageArray[_index];

                  if (element.imageAsBase64 == img.imageAsBase64) {
                    var picture = _this3.imageArray.indexOf(element);

                    _this3.imageArray.splice(picture, 1);
                  }
                }

                _this3.imageCounter--;
              }
            }); //end swal fire
          }
        }, {
          key: "showNextWebcam",
          value: function showNextWebcam(directionOrDeviceId) {
            // true => move forward through devices
            // false => move backwards through devices
            // string => move to device with given deviceId
            this.nextWebcam.next(directionOrDeviceId);
          }
        }, {
          key: "handleImage",
          value: function handleImage(webcamImage) {
            console.info('received webcam image', webcamImage);
            this.webcamImage = webcamImage;
            this.imageArray.push(webcamImage);
            var base64 = this.webcamImage.imageAsBase64;
            var imageName = 'name.png';
            var imageBlob = this.dataURItoBlob(base64);
            var imageFile = new File([imageBlob], imageName, {
              type: 'image/png'
            });
            this.imageTosend = imageFile;
          }
        }, {
          key: "dataURItoBlob",
          value: function dataURItoBlob(dataURI) {
            var byteString = window.atob(dataURI);
            var arrayBuffer = new ArrayBuffer(byteString.length);
            var int8Array = new Uint8Array(arrayBuffer);

            for (var i = 0; i < byteString.length; i++) {
              int8Array[i] = byteString.charCodeAt(i);
            }

            var blob = new Blob([int8Array], {
              type: 'image/png'
            });
            return blob;
          }
        }, {
          key: "cameraWasSwitched",
          value: function cameraWasSwitched(deviceId) {
            console.log('active device: ' + deviceId);
            this.deviceId = deviceId;
          }
        }, {
          key: "triggerObservable",
          get: function get() {
            return this.trigger.asObservable();
          }
        }, {
          key: "removeImages",
          value: function removeImages() {
            var _this4 = this;

            console.log(this.removeArray);
            this.removeArray.forEach(function (element) {
              console.log(element); //console.log(this.removeArray.indexOf(element));

              _this4.imageArray.splice(element, 1);
            });
            this.removeArray = [];
          }
        }, {
          key: "nextWebcamObservable",
          get: function get() {
            return this.nextWebcam.asObservable();
          }
        }, {
          key: "doubleClick",
          value: function doubleClick(event, index) {
            var _this5 = this;

            var dialogConfig = new _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__["MatDialogConfig"]();
            dialogConfig.panelClass = 'custom-dialog-container';
            dialogConfig.data = event.target.src;

            var dialogManipulation = this._dialog.open(_image_manipulation_image_manipulation_component__WEBPACK_IMPORTED_MODULE_5__["ImageManipulationComponent"], dialogConfig);

            dialogManipulation.afterClosed().subscribe(function (res) {
              //console.log("DATA FROM IMAGE MANIPULATION", res.data);
              //console.log(index, "index");
              //this.imageArray.reverse();
              console.log(_this5.imageArray[index]);
              _this5.imageArray[index] = new ngx_webcam__WEBPACK_IMPORTED_MODULE_1__["WebcamImage"](res.data, "jpeg", res.data); //this.imageArray.reverse();
            });
          } //end clickImage

        }]);

        return CameracomponentComponent;
      }();

      CameracomponentComponent.ɵfac = function CameracomponentComponent_Factory(t) {
        return new (t || CameracomponentComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵdirectiveInject"](src_app_services_tid_tidcreartion_service_service__WEBPACK_IMPORTED_MODULE_3__["TidcreartionServiceService"]), _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵdirectiveInject"](_angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__["MatDialogRef"]), _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵdirectiveInject"](_angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__["MAT_DIALOG_DATA"]), _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵdirectiveInject"](_angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__["MatDialog"]), _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵdirectiveInject"](src_app_services_packages_audit_trail_service__WEBPACK_IMPORTED_MODULE_6__["AuditTrailService"]));
      };

      CameracomponentComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵdefineComponent"]({
        type: CameracomponentComponent,
        selectors: [["app-cameracomponent"]],
        decls: 19,
        vars: 4,
        consts: [[2, "max-height", "950px"], [1, "card-header"], [2, "float", "right"], ["style", "display: inline; margin-right: 30px;", 3, "diameter", 4, "ngIf"], ["align", "center", 2, "position", "relative"], [2, "position", "absolute", "top", "-30px", "width", "30%", "left", "22%", "z-index", "1"], [1, "btn", "btn-success", 2, "border-radius", "10px", "border", "5px white", 3, "click"], [2, "position", "absolute", "top", "-30px", "width", "30%", "left", "45%", "z-index", "1"], ["centered", ""], ["inside", ""], [3, "height", "width", "trigger", "allowCameraSwitch", "switchCamera", "videoOptions", "imageQuality", "imageCapture", "cameraSwitched", "initError", 4, "ngIf"], ["class", "snapshot", 4, "ngIf"], [2, "display", "inline", "margin-right", "30px", 3, "diameter"], [3, "height", "width", "trigger", "allowCameraSwitch", "switchCamera", "videoOptions", "imageQuality", "imageCapture", "cameraSwitched", "initError"], [1, "snapshot"], [2, "display", "inline-block", "flex-wrap", "wrap"], [4, "ngFor", "ngForOf"], [1, "col-10", 2, "padding", "2em"], [2, "margin-left", "52%"], [1, "btn", "btn-success", 3, "click"], [1, "container", 2, "display", "block"], [1, "btn", "btn-success", 2, "position", "relative", "top", "5em", "left", "-2.5em", 3, "click"], [1, "fas", "fa-times-circle"], [2, "display", "block", "clear", "left", "max-height", "100%", "max-width", "100%", 3, "src", "click"]],
        template: function CameracomponentComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "mat-dialog-content", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](1, "h5", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](2, "Image Capture ");

            _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](3, "span", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](4, CameracomponentComponent_mat_spinner_4_Template, 1, 1, "mat-spinner", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](5);

            _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](6, "div", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](7, "span", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](8, "button", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function CameracomponentComponent_Template_button_click_8_listener() {
              return ctx.triggerSnapshot();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](9, "Take Pic");

            _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](10, "span", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](11, "button", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function CameracomponentComponent_Template_button_click_11_listener() {
              return ctx.saveBulkImages();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](12, "Save Images");

            _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](13, "div", null, 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](15, "div", null, 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](17, CameracomponentComponent_webcam_17_Template, 1, 7, "webcam", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](18, CameracomponentComponent_div_18_Template, 7, 1, "div", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](4);

            _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", ctx.showSpinner);

            _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate1"](" TOTAL IMAGES TAKEN: ", ctx.imageArray.length, " ");

            _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](12);

            _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", ctx.showWebcam);

            _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", ctx.webcamImage);
          }
        },
        directives: [_angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__["MatDialogContent"], _angular_common__WEBPACK_IMPORTED_MODULE_8__["NgIf"], _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_9__["MatSpinner"], ngx_webcam__WEBPACK_IMPORTED_MODULE_1__["WebcamComponent"], _angular_common__WEBPACK_IMPORTED_MODULE_8__["NgForOf"]],
        styles: [".card-header[_ngcontent-%COMP%] {\r\n  background-color: #031d44;\r\n  color: white;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNhbWVyYWNvbXBvbmVudC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UseUJBQXlCO0VBQ3pCLFlBQVk7QUFDZCIsImZpbGUiOiJjYW1lcmFjb21wb25lbnQuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jYXJkLWhlYWRlciB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzAzMWQ0NDtcclxuICBjb2xvcjogd2hpdGU7XHJcbn1cclxuIl19 */"]
      });
      /***/
    },

    /***/
    "+ngi":
    /*!***********************************************************!*\
      !*** ./src/app/pages/imagelookup/image-lookup-routing.ts ***!
      \***********************************************************/

    /*! exports provided: ImageLookupRouting */

    /***/
    function ngi(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ImageLookupRouting", function () {
        return ImageLookupRouting;
      });
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _alfresco_lookup_alfresco_lookup_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./alfresco-lookup/alfresco-lookup.component */
      "x9+C");
      /* harmony import */


      var src_app_services_packages_add_packages_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! src/app/services/packages/add-packages.service */
      "wlS1");
      /* harmony import */


      var src_app_services_tid_tidcreartion_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/app/services/tid/tidcreartion-service.service */
      "eHNn");
      /* harmony import */


      var src_app_services_tid_pdf_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! src/app/services/tid/pdf.service */
      "c1hE");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/core */
      "fXoL"); //import { HeroesComponent } from './heroes/heroes.component';


      var routes = [{
        path: 'alfrescoLookup',
        component: _alfresco_lookup_alfresco_lookup_component__WEBPACK_IMPORTED_MODULE_1__["AlfrescoLookupComponent"],
        data: {
          title: 'Add'
        }
      }];

      var ImageLookupRouting = function ImageLookupRouting() {
        _classCallCheck(this, ImageLookupRouting);
      };

      ImageLookupRouting.ɵfac = function ImageLookupRouting_Factory(t) {
        return new (t || ImageLookupRouting)();
      };

      ImageLookupRouting.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineNgModule"]({
        type: ImageLookupRouting
      });
      ImageLookupRouting.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineInjector"]({
        providers: [src_app_services_packages_add_packages_service__WEBPACK_IMPORTED_MODULE_2__["AddPackagesService"], src_app_services_tid_tidcreartion_service_service__WEBPACK_IMPORTED_MODULE_3__["TidcreartionServiceService"], src_app_services_tid_pdf_service__WEBPACK_IMPORTED_MODULE_4__["PdfService"]],
        imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"].forChild(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵsetNgModuleScope"](ImageLookupRouting, {
          imports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]],
          exports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]]
        });
      })();
      /***/

    },

    /***/
    "/a8Q":
    /*!*************************************************************!*\
      !*** ./src/app/services/packages/worflow-status.service.ts ***!
      \*************************************************************/

    /*! exports provided: WorflowStatusService */

    /***/
    function a8Q(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "WorflowStatusService", function () {
        return WorflowStatusService;
      });
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");
      /* harmony import */


      var _shared_http_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../shared/http-rest.service */
      "vulr");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var WorflowStatusService = /*#__PURE__*/function (_shared_http_rest_ser) {
        _inherits(WorflowStatusService, _shared_http_rest_ser);

        var _super = _createSuper(WorflowStatusService);

        function WorflowStatusService(http, router) {
          var _this6;

          _classCallCheck(this, WorflowStatusService);

          _this6 = _super.call(this, http);
          _this6.router = router;
          _this6.rootURL = "http://localhost:53420/api/workflow_status";
          return _this6;
        }

        _createClass(WorflowStatusService, [{
          key: "getTaskbyWF",
          value: function getTaskbyWF(wfid) {
            return this.findAllById(wfid, "workflow_task", "FindAllwithTaskID");
          }
        }, {
          key: "getTransformStatus",
          value: function getTransformStatus(name, id) {
            return this.findUniqueByIdStatus(name, id, "workflow_status", "FindAllwithTaskID");
          }
        }, {
          key: "FindByTaskID2",
          value: function FindByTaskID2(task) {
            return this.findAllById(task, "workflow_status", "FindByTaskID2");
          }
        }, {
          key: "findById",
          value: function findById(wfid) {
            return this.findAllById(wfid, "workflow_status", "findById");
          }
        }, {
          key: "getAll",
          value: function getAll() {
            return this.findAll("workflow_status", "FindAll");
          }
        }]);

        return WorflowStatusService;
      }(_shared_http_rest_service__WEBPACK_IMPORTED_MODULE_2__["HttpRestService"]);

      WorflowStatusService.ɵfac = function WorflowStatusService_Factory(t) {
        return new (t || WorflowStatusService)(_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵinject"](_angular_router__WEBPACK_IMPORTED_MODULE_0__["Router"]));
      };

      WorflowStatusService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineInjectable"]({
        token: WorflowStatusService,
        factory: WorflowStatusService.ɵfac,
        providedIn: 'root'
      });
      /***/
    },

    /***/
    0:
    /*!***************************!*\
      !*** multi ./src/main.ts ***!
      \***************************/

    /*! no static exports found */

    /***/
    function _(module, exports, __webpack_require__) {
      module.exports = __webpack_require__(
      /*! C:\IronMountain\BNYM Front End\BNYMTrackingTool\src\main.ts */
      "zUnb");
      /***/
    },

    /***/
    "0Cwu":
    /*!********************************************!*\
      !*** ./src/app/services/service.module.ts ***!
      \********************************************/

    /*! exports provided: ServiceModule */

    /***/
    function Cwu(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ServiceModule", function () {
        return ServiceModule;
      });
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");
      /* harmony import */


      var _service_index__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./service.index */
      "vWu4");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var ServiceModule = function ServiceModule() {
        _classCallCheck(this, ServiceModule);
      };

      ServiceModule.ɵfac = function ServiceModule_Factory(t) {
        return new (t || ServiceModule)();
      };

      ServiceModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineNgModule"]({
        type: ServiceModule
      });
      ServiceModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineInjector"]({
        providers: [_service_index__WEBPACK_IMPORTED_MODULE_2__["SidebarService"], _service_index__WEBPACK_IMPORTED_MODULE_2__["UsersService"], _service_index__WEBPACK_IMPORTED_MODULE_2__["LoginGuardGuard"]],
        imports: [[_angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClientModule"]]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵsetNgModuleScope"](ServiceModule, {
          imports: [_angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClientModule"]]
        });
      })();
      /***/

    },

    /***/
    "320Y":
    /*!***************************************************!*\
      !*** ./src/app/shared/header/header.component.ts ***!
      \***************************************************/

    /*! exports provided: HeaderComponent */

    /***/
    function Y(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HeaderComponent", function () {
        return HeaderComponent;
      });
      /* harmony import */


      var _services_users_users_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ../../services/users/users.service */
      "j7lE");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var _c0 = function _c0() {
        return [""];
      };

      var HeaderComponent = /*#__PURE__*/function () {
        function HeaderComponent(_usersService, router) {
          _classCallCheck(this, HeaderComponent);

          this._usersService = _usersService;
          this.router = router;
        }

        _createClass(HeaderComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.userlogged = JSON.parse(localStorage.getItem('user'));
          }
        }, {
          key: "logout",
          value: function logout() {
            localStorage.removeItem('user');
            localStorage.clear();
            this.router.navigate(['/login']);
          }
        }]);

        return HeaderComponent;
      }();

      HeaderComponent.ɵfac = function HeaderComponent_Factory(t) {
        return new (t || HeaderComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_services_users_users_service__WEBPACK_IMPORTED_MODULE_0__["UsersService"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]));
      };

      HeaderComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineComponent"]({
        type: HeaderComponent,
        selectors: [["app-header"]],
        decls: 33,
        vars: 4,
        consts: [[1, "topbar"], [1, "topbar-left"], [1, "logo", 3, "routerLink"], [1, "label", "label-default"], ["src", "assets/images/logo.png", "alt", "", "height", "18"], [1, "navbar-custom"], [1, "list-inline", "float-right", "mb-0"], [1, "list-inline-item", "dropdown", "notification-list"], ["data-toggle", "dropdown", "href", "#", "role", "button", "aria-haspopup", "false", "aria-expanded", "false", 1, "nav-link", "dropdown-toggle", "nav-user"], [1, "noti-icon"], ["src", "assets/images/users/avatar-1.jpg", "alt", "user", 1, "img-fluid", "rounded-circle"], [1, "profile-username", "ml-2", "text-dark"], [1, "mdi", "mdi-menu-down", "text-dark"], [1, "dropdown-menu", "dropdown-menu-animated", "dropdown-menu-right", "profile-dropdown"], [1, "dropdown-item", "noti-title", "d-inline-flex", "p-2"], [1, "text-overflow"], [1, "dropdown-item", "notify-item", 3, "click"], [1, "mdi", "mdi-power"], [1, "list-inline", "menu-left", "mb-0"], [1, "float-left"], [1, "button-menu-mobile", "open-left", "waves-light", "waves-effect"], [1, "mdi", "mdi-menu"], [1, "display-5", 2, "margin-top", "1.6%"]],
        template: function HeaderComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](2, "a", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](3, "span");

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](4, "span", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](5, "img", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](6, "IRON MOUNTAIN");

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](7, "i");

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](8, "nav", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](9, "ul", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](10, "li", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](11, "a", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](12, "i", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](13, "img", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](14, "span", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](15);

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](16, "span", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](17, "div", 13);

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](18, "div", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](19, "h5", 15);

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](20, "small");

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](21);

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](22, "a", 16);

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function HeaderComponent_Template_a_click_22_listener() {
              return ctx.logout();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](23, "i", 17);

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](24, "span");

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](25, "Logout");

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](26, "ul", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](27, "li", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](28, "button", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](29, "i", 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](30, "li");

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](31, "h3", 22);

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](32, "Bank of New York Mellon");

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵpureFunction0"](3, _c0));

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](13);

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtextInterpolate1"](" Log out ! ", ctx.userlogged.User_Name, " ");

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](6);

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtextInterpolate"](ctx.userlogged.User_Name);
          }
        },
        directives: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterLinkWithHref"]],
        encapsulation: 2
      });
      /***/
    },

    /***/
    "3OKj":
    /*!*************************************************************!*\
      !*** ./src/app/shared/nopagefound/nopagefound.component.ts ***!
      \*************************************************************/

    /*! exports provided: NopagefoundComponent */

    /***/
    function OKj(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "NopagefoundComponent", function () {
        return NopagefoundComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");

      var _c0 = function _c0() {
        return ["/admin"];
      };

      var NopagefoundComponent = /*#__PURE__*/function () {
        function NopagefoundComponent() {
          _classCallCheck(this, NopagefoundComponent);
        }

        _createClass(NopagefoundComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            init_plugins();
          }
        }]);

        return NopagefoundComponent;
      }();

      NopagefoundComponent.ɵfac = function NopagefoundComponent_Factory(t) {
        return new (t || NopagefoundComponent)();
      };

      NopagefoundComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: NopagefoundComponent,
        selectors: [["app-nopagefound"]],
        decls: 15,
        vars: 2,
        consts: [[1, "wrapper-page"], [1, "account-pages"], [1, "account-box"], [1, "account-logo-box", "bg-primary", "p-4"], [1, "m-0", "text-center", "text-white"], [1, "account-content", "text-center"], [1, "text-error", "display-4", "font-weight-bold"], [1, "text-uppercase", "font-weight-bold", "text-danger", "m-t-30"], [1, "text-muted", "mt-3"], [1, "btn", "btn-md", "btn-block", "btn-primary", "waves-effect", "waves-light", "m-t-20", 3, "routerLink"]],
        template: function NopagefoundComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h3", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Template");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "h1", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "404");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "h3", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Page Not Found");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "p", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "It's looking like you may have taken a wrong turn. Don't worry... it happens to the best of us.");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "a", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, " Return Home");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](13);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](1, _c0));
          }
        },
        directives: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterLinkWithHref"]],
        encapsulation: 2
      });
      /***/
    },

    /***/
    "3sU2":
    /*!*************************************************************************!*\
      !*** ./src/app/pages/manifest/sort-packages/sort-packages.component.ts ***!
      \*************************************************************************/

    /*! exports provided: SortPackagesComponent */

    /***/
    function sU2(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SortPackagesComponent", function () {
        return SortPackagesComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! sweetalert2/dist/sweetalert2.js */
      "PdH4");
      /* harmony import */


      var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1__);
      /* harmony import */


      var src_app_services_packages_client_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! src/app/services/packages/client.service */
      "SagX");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var src_app_services_packages_add_packages_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! src/app/services/packages/add-packages.service */
      "wlS1");
      /* harmony import */


      var src_app_services_packages_audit_trail_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! src/app/services/packages/audit-trail.service */
      "irGt");
      /* harmony import */


      var src_app_services_tid_tidcreartion_service_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! src/app/services/tid/tidcreartion-service.service */
      "eHNn");
      /* harmony import */


      var src_app_models_package_model__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/models/package.model */
      "JYu2");
      /* harmony import */


      var src_app_models_audit_trail_model__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! src/app/models/audit-trail.model */
      "VAPG");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");

      var _c0 = ["cboCarrier"];
      var _c1 = ["businessUnitInput"];
      var _c2 = ["binNumberInput"];
      var _c3 = ["trackingInput"];

      function SortPackagesComponent_tr_108_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tr");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "td");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "td", 25);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "td");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "td");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "td");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var row_r5 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](row_r5.Package_Tracking_BC);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](row_r5.Package_Tracking_ID);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](row_r5.Client_Name);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](row_r5.Carrier);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](row_r5.Bin_Number);
        }
      }

      var _c4 = function _c4() {
        return {
          standalone: true
        };
      }; // import { Subscription } from "rxjs";
      // import { TransportItem } from "src/app/models/transport-item/transport-item.model";
      // import { CheckInService, UsersService } from "src/app/services/service.index";


      var SortPackagesComponent = /*#__PURE__*/function () {
        function SortPackagesComponent(_packageService, auditService, clientService, _tidService, router) {
          _classCallCheck(this, SortPackagesComponent);

          this._packageService = _packageService;
          this.auditService = auditService;
          this.clientService = clientService;
          this._tidService = _tidService;
          this.router = router; //BNYM Sort Module Variables

          this.binNumber = '';
          this.businessUnit = '';
          this.trackingNumber = '';
          this.showCarrier = false;
          this.carrier = '';
          this.trackingPid = ''; //Object variables

          this.packFound = new src_app_models_package_model__WEBPACK_IMPORTED_MODULE_7__["Package"]();
          this.audit = new src_app_models_audit_trail_model__WEBPACK_IMPORTED_MODULE_8__["AuditTrail"]();
          this.packageArray = new Array();
        }

        _createClass(SortPackagesComponent, [{
          key: "ngAfterViewInit",
          value: function ngAfterViewInit() {
            this.txtBusinessUnit.nativeElement.focus();
          }
        }, {
          key: "ngOnInit",
          value: function ngOnInit() {
            this.userlogged = JSON.parse(localStorage.getItem('user'));

            if (this.userlogged != null) {
              console.log('User logged: ' + this.userlogged.User_Login);
            } else {
              this.router.navigate(['/login']);
            }
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {// if(this.subscription !== undefined) {
            //   this.subscription.unsubscribe();
            // }
          } //*EVENTS*//
          //Business Unit

        }, {
          key: "onEnterBusinessUnit",
          value: function onEnterBusinessUnit(event) {
            if (this.businessUnit != '') {
              this.txtBinNumber.nativeElement.focus();
            }
          } //onChange BU

        }, {
          key: "onChangeBusinessUnit",
          value: function onChangeBusinessUnit(event) {
            if (this.businessUnit != '') {
              this.txtBinNumber.nativeElement.focus();
            }
          } //on Enter Bin Number

        }, {
          key: "onEnterBinNumber",
          value: function onEnterBinNumber(event) {
            if (this.binNumber != '') {
              this.txtTrackingInput.nativeElement.focus();
            }
          } //onChangeBinNumber
          //on Enter Tracking

        }, {
          key: "onEnterTracking",
          value: function onEnterTracking(event) {
            var _this7 = this;

            if (this.businessUnit != '') {
              setTimeout(function () {
                _this7.trackingNumber = _this7.trackingNumber.toLowerCase();
                _this7.trackingNumber = _this7.trackingNumber.replace(/\s/g, "");

                if (_this7.trackingNumber.startsWith("pid-")) {
                  _this7.trackingNumber = _this7.trackingNumber.replace("pid-", ""); //this.usingPID(this.trackingNumber);
                } else if (_this7.trackingNumber.startsWith("*save*")) {
                  _this7.saveAndValidate();
                } else {
                  _this7.trackingNumber = _this7.trackingNumber.toLowerCase();

                  _this7.usingTracking(_this7.trackingNumber);
                }
              }, 100); //end set timeout
            } else {
              //error empty business unit
              setTimeout(function () {
                sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
                  position: 'top-end',
                  type: 'error',
                  title: 'Empty Business Unit',
                  text: 'Select a business unit',
                  // showConfirmButton:true,
                  footer: '',
                  timer: 1500
                });
              }, 100); //end set timeout
            }
          } //onchangeCarrier

        }, {
          key: "onChangeCarrier",
          value: function onChangeCarrier(event) {
            if (this.carrier != '') {
              this.createPackage();
            }
          } //onEnterCarrier input

        }, {
          key: "onEnterCarrier",
          value: function onEnterCarrier(event) {
            if (this.carrier != '') {
              this.createPackage();
            }
          } //***BackEnd Calls***//

        }, {
          key: "usingTracking",
          value: function usingTracking(trackingNumber) {
            var _this8 = this;

            console.log("using Tracking", this.businessUnit);

            this._packageService.findOneByIdBC(trackingNumber).subscribe(function (data) {
              _this8.packFound = data; //Si packate recibido no tiene business unit 
              //Asigna el business unit seleccionado 

              if (data.Client_Name == '' && _this8.businessUnit != "") {
                //asigna valor a business unit porque venia vacio
                _this8.packFound.Client_Name = _this8.businessUnit;
              } else if (data.Client_Name != "" && _this8.businessUnit != "") {
                _this8.packFound.Client_Name = _this8.businessUnit;
              } //BIN NUMBER VALIDATIONS
              //Si el bin number recibido es diferente de EMPTY y se tiene un BIN NUMBER EN EL DOM
              //asigna el BIN NUMBER DEL DOM


              if (data.Bin_Number != "" && _this8.binNumber != '') {
                _this8.packFound.Bin_Number = _this8.binNumber;
              } else if (data.Bin_Number == '' && _this8.binNumber != '') {
                _this8.packFound.Bin_Number = _this8.binNumber;
              } //Add to table view.`


              _this8.packageArray.push(_this8.packFound);

              console.log(_this8.packFound, "Package about to be saved"); //Save Package to DB

              _this8.savePackage();
            }, function (error) {
              //DID NOT FIND ANYTHING ON API//  
              //Si SHOW CARRIES ES FALSO 
              //MUESTRA SHOW CARRIER COMBO
              if (_this8.showCarrier != true) {
                _this8.showCarrier = true;
                setTimeout(function () {
                  _this8.cboCarrier.nativeElement.focus();
                }, 100);
              } else {
                if (_this8.carrier == '') {
                  //Execute Sound ERROR
                  var sound = new Audio();
                  sound.src = '/BNYMTrackingTool/assets/Windows 10 Error Sound.mp3';
                  sound.load();
                  var promise = sound.play();

                  if (promise !== undefined) {
                    // On older browsers play() does not return anything, so the value would be undefined.
                    promise.then(function () {// Audio is playing.
                    })["catch"](function (error) {
                      console.log(error);
                    });
                  } //end promise


                  setTimeout(function () {
                    sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
                      position: 'top-end',
                      type: 'error',
                      title: 'Empty Carrier ',
                      text: 'Select a carrier',
                      // showConfirmButton:true,
                      footer: '',
                      timer: 1500
                    }); //this.carrierInput.nativeElement.focus();
                  }, 110);
                } else {
                  //CREATE PACKAGE WITH INPUT INFORMATION
                  _this8.createPackage();
                }
              }
            }); //End Observable

          } //END USING TRACKING
          //API CALLS
          //Save Package To API

        }, {
          key: "savePackage",
          value: function savePackage() {
            var _this9 = this;

            console.log("saving packages");
            console.log("BU", this.packFound.Client_Name);
            console.log(this.businessUnit, "BU SELECTED"); //VALIDATE If business unit is not empty

            if (this.packFound.Client_Name == undefined || this.packFound.Client_Name == null || this.packFound.Client_Name == "") {
              //check if nothing is empty or not validated
              sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default()("Error Saving", "Package contains Business unit empty, please select again.", "error");
            } else {
              //API UPDATE SORT
              this._packageService.updateSort(this.packFound).subscribe(function (data) {
                console.log(data, "Update made"); //CLEAR BIND Variables

                _this9.trackingNumber = '';
              }, function (error) {
                //catch error message
                if (error == "Package not updated because package doesnt exist.") {
                  sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default()("Access Denied", "Package contains Business unit empty, saving failed.", "error");
                } else {
                  sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default()("Access Denied", "Error saving", "error");
                }
              }); //end if clientname undefined

            } //END UPDATE SORT
            //AUDIT TRAIL OBJECT ASIGNATION


            this.audit = {
              PID: this.packFound.Package_Tracking_ID,
              TID: '',
              ActionName: "Sort Packages",
              ActionDateTime: null,
              BusinessUnit: this.packFound.Client_Name,
              Sticker: '',
              User_ID: this.userlogged.User_Name
            }; //Audit Trail Save

            this.auditService.saveAudit(this.audit).subscribe(function (data) {
              console.log(data);
            });
          } //END SAVE PACKAGE
          //Create Package
          //When a package was searched for but no package was found

        }, {
          key: "createPackage",
          value: function createPackage() {
            var _this10 = this;

            this.packFound = new src_app_models_package_model__WEBPACK_IMPORTED_MODULE_7__["Package"](); //
            //asigna valores a packfound 

            this.packFound.Bin_Number = this.binNumber;
            this.packFound.Package_Tracking_BC = this.trackingNumber;
            this.packFound.Client_Name = this.businessUnit;
            this.packFound.Carrier = this.carrier;
            var audit2 = {
              PID: this.trackingPid,
              TID: 'N/A',
              ActionName: "Sort Packages",
              ActionDateTime: null,
              BusinessUnit: this.packFound.Client_Name,
              Sticker: '',
              User_ID: this.userlogged.User_Name
            };
            var audit = {
              PID: this.trackingPid,
              TID: 'N/A',
              ActionName: "Add Packages",
              ActionDateTime: null,
              BusinessUnit: this.packFound.Client_Name,
              Sticker: '',
              User_ID: this.userlogged.User_Name
            };

            this._packageService.savePackageSortModule(this.packFound).subscribe(function (data) {
              _this10.packageArray.push(data);

              audit.PID = data.Package_Tracking_ID;
              audit2.PID = data.Package_Tracking_ID;
              _this10.trackingPid = data.Package_Tracking_ID;
              sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
                position: 'top-end',
                type: 'success',
                title: 'Package Created',
                text: 'Package' + ' Created with PID-' + data.Package_Tracking_ID,
                footer: '',
                timer: 1500
              });
            }, function (error) {
              sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default()("Error saving in audit trail", String(error.Message), "error");
            }); //insert audit record


            this.auditService.saveAudit(audit).subscribe(function (data) {
              console.log(data);
            });
            this.auditService.saveAudit(audit2).subscribe(function (data) {
              console.log(data);
            }, function (error) {
              sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default()("Error saving in audit trail", String(error.Message), "error");
            });
            setTimeout(function () {
              //Reset for next package
              _this10.packFound = new src_app_models_package_model__WEBPACK_IMPORTED_MODULE_7__["Package"]();
              _this10.trackingNumber = ''; //Change focus

              _this10.txtTrackingInput.nativeElement.focus();
            }, 100);
          } // end create package method
          //Save and Validate From list

        }, {
          key: "saveAndValidate",
          value: function saveAndValidate() {
            var _this11 = this;

            console.log("save and validate");
            var errorFound = false;
            var errorCounter = 0;
            this.packageArray.forEach(function (element) {
              _this11._packageService.findOneByPID(element.Package_Tracking_ID).subscribe(function (data) {
                console.log("Package sent to find from db", element.Client_Name, " Current Business unit", data.Client_Name);

                if (element.Client_Name == data.Client_Name) {
                  errorFound = false;

                  _this11._packageService.updatePack(element).subscribe(function (data) {
                    console.log("saving in the observable");
                  });
                } else {
                  errorFound = true;
                  console.log("error found"); //update object

                  _this11._packageService.updateSort(element).subscribe(function (data) {// console.log("saving in the observable");              
                  }); //update errorTrail


                  errorCounter++;
                }
              }); //end find by PID each package

            }); //End foreach package in table
            //let the user know he has found issues

            if (errorFound) {
              console.log("Errors where found");
            }

            this.packageArray = new Array();
            this.businessUnit = '';
            this.cboCarrier.nativeElement.value = '';
            this.binNumber = '';
            this.trackingNumber = '';
          } //end Save and validate
          //Cancel BUTTON

        }, {
          key: "clearPackage",
          value: function clearPackage() {
            var _this12 = this;

            this.packFound = new src_app_models_package_model__WEBPACK_IMPORTED_MODULE_7__["Package"]();
            this.packageArray = new Array();
            this.binNumber = '';
            this.businessUnit = '';
            this.carrier = '';
            this.showCarrier = false;
            this.trackingNumber = ''; //set focus to business unit

            setTimeout(function () {
              _this12.txtBusinessUnit.nativeElement.focus();
            }, 100);
          }
        }, {
          key: "usingPID",
          value: function usingPID(pid) {
            var _this13 = this;

            this._packageService.findOneByPID(+pid).subscribe(function (data) {
              //console.log("package brought with pid ", data);
              _this13.packFound = data;

              if (data.Client_Name != '' && _this13.businessUnit == "") {
                console.log("clientselected empty and client found"); //smallPack = data;

                _this13.packFound.Client_Name = data.Client_Name;
                _this13.packFound.Bin_Number = data.Bin_Number;
              } else if (data.Client_Name == "" && _this13.businessUnit != "") {
                console.log("selected empty client brought ", data.Client_Name);
                console.log("client selected", _this13.businessUnit);
                _this13.packFound.Client_Name = _this13.businessUnit;
              }

              setTimeout(function () {
                //    if package brought empty asign from input
                if (data.Bin_Number != "" && _this13.binNumber != '') {
                  _this13.packFound.Bin_Number = _this13.binNumber;
                } else if (data.Bin_Number == '' && _this13.binNumber != '') {
                  _this13.packFound.Bin_Number = _this13.binNumber;
                }

                if (data.Client_Name == '') {
                  _this13.packFound.Client_Name = _this13.businessUnit;
                }

                _this13.packageArray.push(_this13.packFound);
              }, 500);
              _this13.showCarrier = false;

              _this13.savePackagePID();
            }, function (error) {
              setTimeout(function () {
                console.log("error finding package with PID-" + pid); // this.businessUnitneeded = true;
              }, 100); //end set timeout

              var sound = new Audio();
              sound.src = '/BNYMTrackingTool/assets/Windows 10 Error Sound.mp3';
              sound.load();
              var promise = sound.play();

              if (promise !== undefined) {
                // On older browsers play() does not return anything, so the value would be undefined.
                promise.then(function () {// Audio is playing.
                })["catch"](function (error) {
                  console.log(error);
                });
              } //this.createPackage(tracking);


              setTimeout(function () {
                if (_this13.showCarrier != true) {
                  //this.createPackage(tracking);
                  _this13.showCarrier = true;
                  setTimeout(function () {
                    _this13.cboCarrier.nativeElement.focus();
                  }, 100); //Swal("create package here ", "lmaoss" ,"error");
                } else {
                  if (_this13.carrier == '') {
                    var sound = new Audio();
                    sound.src = '/BNYMTrackingTool/assets/Windows 10 Error Sound.mp3';
                    sound.load();

                    var _promise = sound.play();

                    if (_promise !== undefined) {
                      // On older browsers play() does not return anything, so the value would be undefined.
                      _promise.then(function () {// Audio is playing.
                      })["catch"](function (error) {
                        console.log(error);
                      });
                    } //end promise


                    setTimeout(function () {
                      sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
                        position: 'top-end',
                        type: 'error',
                        title: 'Empty Carrier ',
                        text: 'Select a carrier',
                        // showConfirmButton:true,
                        footer: '',
                        timer: 1500
                      });

                      _this13.cboCarrier.nativeElement.focus();
                    }, 110);
                  }
                }
              }, 100); //end set timeout
            }); //this.mainInput.nativeElement.value = '';

          }
        }, {
          key: "savePackagePID",
          value: function savePackagePID() {
            var _this14 = this;

            console.log("saving packages");
            console.log("BU ", this.packFound.Client_Name);
            console.log(this.businessUnit, "BU SELECTED");

            if (this.packFound.Client_Name == undefined || this.packFound.Client_Name == null || this.packFound.Client_Name == "") {
              //check if nothing is empty or not validated
              sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default()("Error Saving", "Package contains Business unit empty, please select again.", "error");
            } else {
              //API UPDATE SORT
              this._packageService.updateSort(this.packFound).subscribe(function (data) {
                console.log(data, "Update made"); //CLEAR BIND Variables

                _this14.trackingNumber = '';
              }, function (error) {
                //catch error message
                if (error == "Package not updated because package doesnt exist.") {
                  sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default()("Access Denied", "Package contains Business unit empty, saving failed.", "error");
                } else {
                  sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default()("Access Denied", "Error saving", "error");
                }
              }); //end if clientname undefined

            } //END UPDATE SORT
            //AUDIT TRAIL OBJECT ASIGNATION


            this.audit = {
              PID: this.packFound.Package_Tracking_ID,
              TID: '',
              ActionName: "Sort Packages",
              ActionDateTime: null,
              Sticker: '',
              BusinessUnit: this.packFound.Client_Name,
              User_ID: this.userlogged.User_Name
            }; //Audit Trail Save

            this.auditService.saveAudit(this.audit).subscribe(function (data) {
              console.log(data);
            });
          }
        }]);

        return SortPackagesComponent;
      }();

      SortPackagesComponent.ɵfac = function SortPackagesComponent_Factory(t) {
        return new (t || SortPackagesComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_packages_add_packages_service__WEBPACK_IMPORTED_MODULE_4__["AddPackagesService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_packages_audit_trail_service__WEBPACK_IMPORTED_MODULE_5__["AuditTrailService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_packages_client_service__WEBPACK_IMPORTED_MODULE_2__["ClientService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_tid_tidcreartion_service_service__WEBPACK_IMPORTED_MODULE_6__["TidcreartionServiceService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]));
      };

      SortPackagesComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: SortPackagesComponent,
        selectors: [["app-sort-packages"]],
        viewQuery: function SortPackagesComponent_Query(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c0, 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c1, 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c2, 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c3, 3);
          }

          if (rf & 2) {
            var _t;

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.cboCarrier = _t.first);
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.txtBusinessUnit = _t.first);
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.txtBinNumber = _t.first);
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.txtTrackingInput = _t.first);
          }
        },
        decls: 109,
        vars: 9,
        consts: [[1, "row"], [1, "col-md-3"], [1, "card"], [1, "card-header"], [1, "card-body"], [1, "form-horizontal"], [1, "form-group"], [1, "col-md-6", "control-label"], [1, "col-md-12"], [1, "form-control", 3, "ngModelOptions", "ngModel", "change", "keydown.enter", "ngModelChange"], ["businessUnitInput", ""], ["type", "text", 1, "form-control", 3, "ngModel", "keydown.enter", "ngModelChange"], ["binNumberInput", ""], ["type", "text", 1, "form-control", 3, "ngModel", "ngModelChange", "keydown.enter"], ["trackingInput", ""], [1, "form-group", 3, "hidden"], [1, "form-control", 3, "ngModel", "change", "keydown.enter", "ngModelChange"], ["cboCarrier", ""], ["id", "btnCancel", 1, "btn", "btn-secondary", 2, "margin", "10px", 3, "click"], [1, "col-md-9"], [2, "float", "right"], [1, "table", "table-hover"], [1, "thead-dark"], [1, "w-20"], [4, "ngFor", "ngForOf"], [1, "w-15"]],
        template: function SortPackagesComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h5", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Sort Packages");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "label", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Business Unit-");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "select", 9, 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("change", function SortPackagesComponent_Template_select_change_12_listener($event) {
              return ctx.onChangeBusinessUnit($event);
            })("keydown.enter", function SortPackagesComponent_Template_select_keydown_enter_12_listener($event) {
              return ctx.onEnterBusinessUnit($event);
            })("ngModelChange", function SortPackagesComponent_Template_select_ngModelChange_12_listener($event) {
              return ctx.businessUnit = $event;
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](14, "option");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "option");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "Fannie");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "option");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "Freddie");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "option");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, "Non Agency New Collateral");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "option");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, "Non Agency T-Docs");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "option");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](24, "NIGO");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "option");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26, "GNMA T-docs");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "option");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](28, "Fannie T-docs");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "option");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](30, "Freddie T-docs");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "option");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](32, "Non-production");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "option");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](34, "Mail Intake");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "option");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](36, "NOS");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "option");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](38, "CMC");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "option");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](40, "SLS(SL03)");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "option");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](42, "Pingora");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "option");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](44, "Reinstatements");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "option");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](46, "Resolution Team");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "option");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](48, "Recertification");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "option");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](50, "NRZM");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "div", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "label", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](53, "Bin Number");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "div", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "input", 11, 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("keydown.enter", function SortPackagesComponent_Template_input_keydown_enter_55_listener($event) {
              return ctx.onEnterBinNumber($event);
            })("ngModelChange", function SortPackagesComponent_Template_input_ngModelChange_55_listener($event) {
              return ctx.binNumber = $event;
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](57, "hr");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "div", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](59, "label", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](60, "Tracking # or PID");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](61, "div", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "input", 13, 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function SortPackagesComponent_Template_input_ngModelChange_62_listener($event) {
              return ctx.trackingNumber = $event;
            })("keydown.enter", function SortPackagesComponent_Template_input_keydown_enter_62_listener($event) {
              return ctx.onEnterTracking($event);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](64, "div", 15);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "label", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](66, "Carrier");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](67, "div", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "select", 16, 17);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("change", function SortPackagesComponent_Template_select_change_68_listener($event) {
              return ctx.onChangeCarrier($event);
            })("keydown.enter", function SortPackagesComponent_Template_select_keydown_enter_68_listener($event) {
              return ctx.onEnterCarrier($event);
            })("ngModelChange", function SortPackagesComponent_Template_select_ngModelChange_68_listener($event) {
              return ctx.carrier = $event;
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](70, "option");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](71, "option");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](72, "FEDEX");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](73, "option");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](74, "USPS");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](75, "option");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](76, "UPS");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](77, "option");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](78, "DHL");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](79, "option");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](80, "FedEx Ground");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](81, "option");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](82, "Hand");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](83, "option");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](84, "IM UPS");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](85, "option");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](86, "IM FedEx");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](87, "button", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SortPackagesComponent_Template_button_click_87_listener() {
              return ctx.clearPackage();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](88, " CANCEL ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](89, "div", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](90, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](91, "h5", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](92, "span", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](93);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](94, "div", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](95, "table", 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](96, "thead", 22);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](97, "tr");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](98, "th", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](99, "Tracking Number");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](100, "th", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](101, "PID");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](102, "th", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](103, "Business Unit");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](104, "th", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](105, "Carrier");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](106, "th", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](107, "Bin Number");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](108, SortPackagesComponent_tr_108_Template, 11, 5, "tr", 24);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModelOptions", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](8, _c4))("ngModel", ctx.businessUnit);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](43);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.binNumber);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.trackingNumber);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("hidden", !ctx.showCarrier);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.carrier);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](25);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" TOTAL PACKAGES: ", ctx.packageArray.length, " ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](15);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.packageArray.slice().reverse());
          }
        },
        directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_9__["SelectControlValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_9__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_9__["NgModel"], _angular_forms__WEBPACK_IMPORTED_MODULE_9__["NgSelectOption"], _angular_forms__WEBPACK_IMPORTED_MODULE_9__["ɵangular_packages_forms_forms_z"], _angular_forms__WEBPACK_IMPORTED_MODULE_9__["DefaultValueAccessor"], _angular_common__WEBPACK_IMPORTED_MODULE_10__["NgForOf"]],
        styles: ["hr[_ngcontent-%COMP%] {\r\n  border: 0;\r\n  height: 1px;\r\n  background-image: linear-gradient(\r\n    to right,\r\n    rgba(0, 0, 0, 0),\r\n    rgba(0, 0, 0, 0.75),\r\n    rgba(0, 0, 0, 0)\r\n  );\r\n}\r\n\r\ninput[_ngcontent-%COMP%]:focus {\r\n  border: 5px solid green;\r\n}\r\n\r\nselect[_ngcontent-%COMP%]:focus {\r\n  border: 5px solid green;\r\n}\r\n\r\nth[_ngcontent-%COMP%], td[_ngcontent-%COMP%] {\r\n  text-align: center;\r\n  vertical-align: middle;\r\n}\r\n\r\n.card-header[_ngcontent-%COMP%] {\r\n  background-color: #031d44;\r\n  color: white;\r\n}\r\n\r\n#btnCancel[_ngcontent-%COMP%], #btnSave[_ngcontent-%COMP%] {\r\n  background-color: #031d44;\r\n  color: white;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNvcnQtcGFja2FnZXMuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLFNBQVM7RUFDVCxXQUFXO0VBQ1g7Ozs7O0dBS0M7QUFDSDs7QUFFQTtFQUNFLHVCQUF1QjtBQUN6Qjs7QUFDQTtFQUNFLHVCQUF1QjtBQUN6Qjs7QUFDQTs7RUFFRSxrQkFBa0I7RUFDbEIsc0JBQXNCO0FBQ3hCOztBQUNBO0VBQ0UseUJBQXlCO0VBQ3pCLFlBQVk7QUFDZDs7QUFDQTs7RUFFRSx5QkFBeUI7RUFDekIsWUFBWTtBQUNkIiwiZmlsZSI6InNvcnQtcGFja2FnZXMuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbImhyIHtcclxuICBib3JkZXI6IDA7XHJcbiAgaGVpZ2h0OiAxcHg7XHJcbiAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KFxyXG4gICAgdG8gcmlnaHQsXHJcbiAgICByZ2JhKDAsIDAsIDAsIDApLFxyXG4gICAgcmdiYSgwLCAwLCAwLCAwLjc1KSxcclxuICAgIHJnYmEoMCwgMCwgMCwgMClcclxuICApO1xyXG59XHJcblxyXG5pbnB1dDpmb2N1cyB7XHJcbiAgYm9yZGVyOiA1cHggc29saWQgZ3JlZW47XHJcbn1cclxuc2VsZWN0OmZvY3VzIHtcclxuICBib3JkZXI6IDVweCBzb2xpZCBncmVlbjtcclxufVxyXG50aCxcclxudGQge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xyXG59XHJcbi5jYXJkLWhlYWRlciB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzAzMWQ0NDtcclxuICBjb2xvcjogd2hpdGU7XHJcbn1cclxuI2J0bkNhbmNlbCxcclxuI2J0blNhdmUge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMwMzFkNDQ7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG59XHJcbiJdfQ== */"]
      });
      /***/
    },

    /***/
    "44oB":
    /*!******************************************************************!*\
      !*** ./src/app/services/coversheet/create-coversheet.service.ts ***!
      \******************************************************************/

    /*! exports provided: CreateCoversheetService */

    /***/
    function oB(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CreateCoversheetService", function () {
        return CreateCoversheetService;
      });
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");
      /* harmony import */


      var _shared_http_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../shared/http-rest.service */
      "vulr");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var CreateCoversheetService = /*#__PURE__*/function (_shared_http_rest_ser2) {
        _inherits(CreateCoversheetService, _shared_http_rest_ser2);

        var _super2 = _createSuper(CreateCoversheetService);

        function CreateCoversheetService(http, router) {
          var _this15;

          _classCallCheck(this, CreateCoversheetService);

          _this15 = _super2.call(this, http);
          _this15.router = router;
          _this15.rootURL = "http://localhost:53420/api/package";
          return _this15;
        }

        _createClass(CreateCoversheetService, [{
          key: "createCoversheetManifest",
          value: function createCoversheetManifest(items) {
            return this.httpRequestBufferResponse("package", "CreateCoverSheetTID", null, items);
          }
        }, {
          key: "createCoversheet",
          value: function createCoversheet(items) {
            return this.httpRequestBufferResponse("package", "CreateCoverSheetManifest", null, items);
          }
        }, {
          key: "createCoversheetP",
          value: function createCoversheetP(items) {
            return this.httpRequestBufferResponse("package", "CreateCoverSheetManifest", null, items);
          }
        }]);

        return CreateCoversheetService;
      }(_shared_http_rest_service__WEBPACK_IMPORTED_MODULE_2__["HttpRestService"]);

      CreateCoversheetService.ɵfac = function CreateCoversheetService_Factory(t) {
        return new (t || CreateCoversheetService)(_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵinject"](_angular_router__WEBPACK_IMPORTED_MODULE_0__["Router"]));
      };

      CreateCoversheetService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineInjectable"]({
        token: CreateCoversheetService,
        factory: CreateCoversheetService.ɵfac
      });
      /***/
    },

    /***/
    "6v43":
    /*!***********************************************************************!*\
      !*** ./src/app/pages/manifest/add-packages/add-packages.component.ts ***!
      \***********************************************************************/

    /*! exports provided: AddPackagesComponent */

    /***/
    function v43(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AddPackagesComponent", function () {
        return AddPackagesComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var src_app_services_packages_add_packages_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! src/app/services/packages/add-packages.service */
      "wlS1");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! sweetalert2/dist/sweetalert2.js */
      "PdH4");
      /* harmony import */


      var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_3__);
      /* harmony import */


      var src_app_services_packages_audit_trail_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! src/app/services/packages/audit-trail.service */
      "irGt");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");

      var _c0 = ["mainSearch"];
      var _c1 = ["carrierInput"];

      function AddPackagesComponent_option_21_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "option");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ex_r6 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ex_r6);
        }
      }

      function AddPackagesComponent_th_39_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "th");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var column_r7 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](column_r7);
        }
      }

      function AddPackagesComponent_tr_40_Template(rf, ctx) {
        if (rf & 1) {
          var _r10 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tr");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "td");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "td");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "td");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "td");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](9, "date");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "button", 21);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AddPackagesComponent_tr_40_Template_button_click_10_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r10);

            var row_r8 = ctx.$implicit;

            var ctx_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r9.deleteRecord(row_r8);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "X");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var row_r8 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", row_r8.Package_Tracking_BC, " ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", row_r8.Package_Tracking_ID, " ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", row_r8.Carrier, " ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind2"](9, 4, row_r8.Creation_Date_Time, "dd/MM/yyyy hh:mm:ss"), " ");
        }
      }

      var AddPackagesComponent = /*#__PURE__*/function () {
        function AddPackagesComponent(packageService, auditService, renderer, router) {
          _classCallCheck(this, AddPackagesComponent);

          this.packageService = packageService;
          this.auditService = auditService;
          this.renderer = renderer;
          this.router = router;
          this.carrierArray = ["", "FEDEX", "USPS", "UPS", "DHL", "FedEx Ground", "Hand", "IM UPS", "IM FedEx"];
          this.clientArray = ["FANNIE", "FREDDIE", "GINNIE", "NON-AGENCY", "OTHER"];
          this.dataTable = [];
          this.headers = ["Tracking Number", "Assigned PID", "Carrier", "Date Time"];
          this.rows = [{
            "Tracking Number": "1",
            "Assigned PID": "Example",
            "Date Time": "20/20/1994"
          }];
        }

        _createClass(AddPackagesComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.carrierInput.nativeElement.focus();
            this.initPackFound();
            this.resetForm();
            this.userlogged = JSON.parse(localStorage.getItem("user"));
            this.getAllPackagebyUser(); // in the js code unmute the audio once the event happened

            if (this.userlogged != null) {
              console.log("User logged: " + this.userlogged.User_Login);
            } else {
              this.router.navigate(["/login"]);
            }
          }
        }, {
          key: "getAllPackagebyUser",
          value: function getAllPackagebyUser() {
            var _this16 = this;

            console.log(this.userlogged);
            var newUser = {
              Admin_Flag: this.userlogged.Admin_Flag,
              User_Login: this.userlogged.User_Login,
              Available: true,
              FirstName: this.userlogged.FirstName,
              LastName: this.userlogged.LastName,
              User_Name: this.userlogged.User_Name,
              User_Password: this.userlogged.User_Password
            };
            this.packageService.getByUserDate(newUser.User_Name, newUser.User_Password).subscribe(function (data) {
              console.log(data);

              if (data != null) {
                data.forEach(function (element) {
                  //asign to local table array
                  if (element.clearPackagesFlag == false) {
                    _this16.dataTable.push(element);

                    _this16.totalItems++;
                  }
                });
              }
            });
          }
        }, {
          key: "sendToNotesField",
          value: function sendToNotesField(event) {
            //console.log("select event " , event.target.value);
            // this.packFound.Client_Name = event.target.value;
            // console.log(this.packFound.Client_Name);
            this.clientSelected = event.target.value;
            console.log(this.clientSelected);
            this.mainSearch.nativeElement.focus();
          }
        }, {
          key: "sendToCarrier",
          value: function sendToCarrier(event) {
            this.carrierSelected = event.target.value;
            this.mainSearch.nativeElement.focus();
            console.log(this.carrierSelected);
          }
        }, {
          key: "initPackFound",
          value: function initPackFound() {
            this.totalItems = 0;
            this.packFound = {
              Package_Tracking_BC: "",
              multipleLenders: false,
              Package_Tracking_ID: "",
              LoanNumber: 0,
              Sticker_ID: "",
              Status_ID: 0,
              Location_Name: "",
              Client_Name: "",
              totalLenders: 0,
              WorkFlow_ID: 0,
              Task_ID: 0,
              Creation_Date_Time: "",
              Available: false,
              Exception_Flag: false,
              Exception_Name: "",
              clearPackagesFlag: false,
              Package_Finished: false,
              ManifestInBox: false,
              BaileeLetter: false,
              Carrier: "",
              Package_Notes: "",
              Bin_Number: "",
              clearPackagesDateTime: "",
              lastModule: "",
              lastFunction: "",
              loanTotal: 0,
              loansExpected: 0
            };
          }
        }, {
          key: "resetForm",
          value: function resetForm(form) {
            if (form != null) form.resetForm();
            this.packageService.formData = {
              Package_Tracking_BC: "",
              Package_Tracking_ID: "",
              multipleLenders: false,
              LoanNumber: 0,
              Sticker_ID: "",
              Location_Name: "",
              Client_Name: "",
              WorkFlow_ID: 0,
              Status_ID: 0,
              totalLenders: 0,
              Task_ID: 0,
              Creation_Date_Time: "",
              Available: false,
              Exception_Flag: false,
              Exception_Name: "",
              Package_Finished: false,
              clearPackagesFlag: false,
              ManifestInBox: false,
              BaileeLetter: false,
              Carrier: "",
              Package_Notes: "",
              Bin_Number: "",
              clearPackagesDateTime: "",
              lastModule: "",
              lastFunction: "",
              loanTotal: 0,
              loansExpected: 0
            };
          }
        }, {
          key: "saveAndValidate",
          value: function saveAndValidate() {
            var _this17 = this;

            if (this.dataTable.length > 1) {
              this.dataTable.forEach(function (element) {
                _this17.packageService.updatePack(element).subscribe(function (data) {
                  console.log(data, "updated");
                });
              }); // end foreach

              this.clearTable();
              sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                position: "top-end",
                type: "success",
                title: "",
                text: "Package saved",
                showConfirmButton: true,
                footer: "",
                timer: 1100
              });
            } else {
              sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_3___default()("Error saving packages", "Empty records", "error");
            }
          } //

        }, {
          key: "insertRecord",
          value: function insertRecord(event) {
            var _this18 = this;

            //this.clientSelected = this.selectedInput.nativeElement.value;
            this.carrierSelected = this.carrierInput.nativeElement.value;
            var specialCharacters = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/; //this.mainSearch.nativeElement.focus();

            this.packFound.Package_Tracking_BC = event.target.value;
            this.packFound.Package_Tracking_BC = this.packFound.Package_Tracking_BC.replace(/\s/g, ""); //this.packFound.Client_Name = this.clientSelected;

            this.packFound.Carrier = this.carrierSelected; // empty validation

            if (this.carrierSelected == "") {
              setTimeout(function () {
                sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_3___default()("Error creating package", "Empty carrier selected", "error");
              }, 0);
              return;
            } else if (this.mainSearch.nativeElement.value == "") {
              setTimeout(function () {
                sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_3___default()("Value Blank", "Tracking number cannot be blank.", "error");
              }, 0);
              return;
            } //especial characters validation


            if (specialCharacters.test(this.packFound.Package_Tracking_BC)) {
              setTimeout(function () {
                sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_3___default()("Error creating package", "Special Characters found", "error");
              }, 0);
              return;
            } else if (this.mainSearch.nativeElement.value == "") {
              setTimeout(function () {
                sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_3___default()("Value Blank", "Tracking number cannot be blank.", "error");
              }, 0);
              return;
            } // do as normal
            else {
                this.packageService.createPackage(this.packFound).subscribe(function (data) {
                  _this18.holdingPID = data.Package_Tracking_ID;
                  console.log(data); //create table element
                  //asign to local table array

                  _this18.dataTable.push(data);

                  console.log(_this18.dataTable);

                  _this18.dataTable.sort(function (a, b) {
                    return a.Creation_Date_Time > b.Creation_Date_Time ? -1 : b.Creation_Date_Time < a.Creation_Date_Time ? 1 : 0;
                  });

                  _this18.totalItems = _this18.dataTable.length; //this.resetForm(form);

                  var audit = {
                    PID: _this18.holdingPID,
                    TID: "N/A",
                    ActionName: "Add Packages",
                    ActionDateTime: null,
                    BusinessUnit: "",
                    Sticker: "",
                    User_ID: _this18.userlogged.User_Name
                  }; //insert audit record

                  _this18.auditService.saveAudit(audit).subscribe(function (data) {
                    console.log(data);
                  });
                }, function (error) {
                  sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_3___default()("Error creating package", String(error.Message), "error");
                });
              } //setfocus
            // var element = this.renderer.selectRootElement("#Package_Tracking_BC");


            this.mainSearch.nativeElement.value = "";
            this.mainSearch.nativeElement.focus();
          } //

        }, {
          key: "findByID",
          value: function findByID(id) {
            var _this19 = this;

            console.log("Trying to fecth data for PID " + id);
            this.packageService.findOneByPID(id).subscribe(function (packF) {
              _this19.packFound = packF;
              return _this19.packFound;
            });
            return;
          }
        }, {
          key: "deleteRecord",
          value: function deleteRecord(id) {
            var _this20 = this;

            this.packageService.asyncDELETERequest(id).subscribe(function (res) {
              console.log("Delete response object: ", res);

              if (res == "Package deleted successful.") {
                _this20.deleteFromTable(id);
              }
            });
          }
        }, {
          key: "deleteFromTable",
          value: function deleteFromTable(id) {
            console.log("datatable info " + this.dataTable);
            var index = this.dataTable.indexOf(id);
            console.log("index number " + index);
            this.dataTable.splice(index, 1);
          }
        }, {
          key: "clearTable",
          value: function clearTable() {
            var _this21 = this;

            this.dataTable.forEach(function (element) {
              element.clearPackagesFlag = true;

              _this21.packageService.clearPackagesFlag(element).subscribe(function (pack) {
                console.log("package saved successfully", pack);
              });
            });
            this.dataTable = [];
            this.totalItems = 0; //this.selectedInput.nativeElement.value= "OTHER";

            this.carrierInput.nativeElement.value = "OTHER";
            this.mainSearch.nativeElement.value = "";
            this.mainSearch.nativeElement.focus();
          } // end clear table

        }]);

        return AddPackagesComponent;
      }();

      AddPackagesComponent.ɵfac = function AddPackagesComponent_Factory(t) {
        return new (t || AddPackagesComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_packages_add_packages_service__WEBPACK_IMPORTED_MODULE_1__["AddPackagesService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_packages_audit_trail_service__WEBPACK_IMPORTED_MODULE_4__["AuditTrailService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]));
      };

      AddPackagesComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: AddPackagesComponent,
        selectors: [["app-add-packages"]],
        viewQuery: function AddPackagesComponent_Query(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c0, 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c1, 3);
          }

          if (rf & 2) {
            var _t;

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.mainSearch = _t.first);
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.carrierInput = _t.first);
          }
        },
        decls: 41,
        vars: 4,
        consts: [[1, "container"], [1, "card"], [1, "card-header"], [2, "float", "right"], [1, "card-body"], ["id", "notification", "src", "src\\assets\\Windows 10 Error Sound.mp3"], [1, "manifestWraper"], ["autocomplete", "off"], ["form", "ngForm"], [1, "row"], [1, "col-sm-2"], [1, "col-sm-10"], [1, "form-control", 2, "padding-left", "15px", "padding-right", "15px", 3, "change", "keydown.enter"], ["carrierInput", ""], [4, "ngFor", "ngForOf"], ["type", "text", "placeholder", "Tracking number", "required", "", 1, "form-control", 3, "keydown.enter"], ["mainSearch", ""], ["align", "center", 1, "buttonsDiv"], ["type", "button", "id", "btnClear", 1, "btn", "btn-secondary", 3, "click"], [1, "tableWrapper"], ["id", "customers"], [3, "click"]],
        template: function AddPackagesComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "h5", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "Add Packages ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "span", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "audio", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "form", 7, 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "span");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "Carrier");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "select", 12, 13);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("change", function AddPackagesComponent_Template_select_change_19_listener($event) {
              return ctx.sendToCarrier($event);
            })("keydown.enter", function AddPackagesComponent_Template_select_keydown_enter_19_listener($event) {
              return ctx.sendToCarrier($event);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](21, AddPackagesComponent_option_21_Template, 2, 1, "option", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](22, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](23, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "span");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26, "Tracking Number");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "div", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "input", 15, 16);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("keydown.enter", function AddPackagesComponent_Template_input_keydown_enter_28_listener($event) {
              return ctx.insertRecord($event);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](30, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "div", 17);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "button", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AddPackagesComponent_Template_button_click_32_listener() {
              return ctx.clearTable();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33, "Clear");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](34, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](35, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "table", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "tr");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](39, AddPackagesComponent_th_39_Template, 2, 1, "th", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](40, AddPackagesComponent_tr_40_Template, 12, 7, "tr", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" TOTAL PACKAGES ADDED: ", ctx.totalItems, " ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](15);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.carrierArray);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.headers);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.dataTable);
          }
        },
        directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["ɵangular_packages_forms_forms_ba"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["NgForm"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["NgForOf"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["NgSelectOption"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ɵangular_packages_forms_forms_z"]],
        pipes: [_angular_common__WEBPACK_IMPORTED_MODULE_6__["DatePipe"]],
        styles: ["#userInfo[_ngcontent-%COMP%] {\r\n  text-align: right;\r\n  padding-top: 5%;\r\n}\r\n\r\n#btnAdd[_ngcontent-%COMP%] {\r\n  background-color: grey !important;\r\n  color: whitesmoke;\r\n  border: 2px solid #555555;\r\n  margin-right: 2%;\r\n  display: inline-block;\r\n  border-radius: 5px;\r\n  width: 20%;\r\n  padding: 8px 40px;\r\n}\r\n\r\n#btnClear[_ngcontent-%COMP%] {\r\n  background-color: grey !important;\r\n  color: whitesmoke;\r\n  border: 2px solid #555555;\r\n  margin-right: 2%;\r\n  display: inline-block;\r\n  border-radius: 5px;\r\n  width: 20%;\r\n  padding: 8px 40px;\r\n}\r\n\r\n#errorlbl[_ngcontent-%COMP%] {\r\n  padding-left: 3.5%;\r\n  color: red;\r\n}\r\n\r\n.username[_ngcontent-%COMP%] {\r\n  font-size: medium;\r\n  display: inline-block;\r\n}\r\n\r\n.userInfoDiv[_ngcontent-%COMP%] {\r\n  align-content: center;\r\n  text-align: right;\r\n}\r\n\r\n.col-sm-10[_ngcontent-%COMP%] {\r\n  padding-right: 150px;\r\n}\r\n\r\n.col-sm-2[_ngcontent-%COMP%] {\r\n  padding-top: 12px;\r\n  text-align: center;\r\n  font-weight: bold;\r\n}\r\n\r\n#customers[_ngcontent-%COMP%]   tr[_ngcontent-%COMP%]:nth-child(even) {\r\n  background-color: #f2f2f2;\r\n}\r\n\r\n#customers[_ngcontent-%COMP%]   tr[_ngcontent-%COMP%]:hover {\r\n  background-color: #ddd;\r\n}\r\n\r\n#customers[_ngcontent-%COMP%]   th[_ngcontent-%COMP%] {\r\n  padding-top: 12px;\r\n  padding-bottom: 12px;\r\n  text-align: left;\r\n  background-color: #023e7d;\r\n  color: white;\r\n}\r\n\r\n.card-header[_ngcontent-%COMP%] {\r\n  background-color: #031d44;\r\n  color: white;\r\n}\r\n\r\n.ng-dirty.ng-invalid[_ngcontent-%COMP%] {\r\n  border-color: red;\r\n}\r\n\r\ninput[_ngcontent-%COMP%]:focus {\r\n  border: 5px solid green;\r\n}\r\n\r\nselect[_ngcontent-%COMP%]:focus {\r\n  border: 5px solid green;\r\n}\r\n\r\n#customers[_ngcontent-%COMP%] {\r\n  font-family: \"Trebuchet MS\", Arial, Helvetica, sans-serif;\r\n  border-collapse: collapse;\r\n  width: 100%;\r\n}\r\n\r\n#customers[_ngcontent-%COMP%]   td[_ngcontent-%COMP%], #customers[_ngcontent-%COMP%]   th[_ngcontent-%COMP%] {\r\n  border: 1px solid #ddd;\r\n  padding: 8px;\r\n  width: 25%;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFkZC1wYWNrYWdlcy5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsaUJBQWlCO0VBQ2pCLGVBQWU7QUFDakI7O0FBRUE7RUFDRSxpQ0FBaUM7RUFDakMsaUJBQWlCO0VBQ2pCLHlCQUF5QjtFQUN6QixnQkFBZ0I7RUFDaEIscUJBQXFCO0VBQ3JCLGtCQUFrQjtFQUNsQixVQUFVO0VBQ1YsaUJBQWlCO0FBQ25COztBQUVBO0VBQ0UsaUNBQWlDO0VBQ2pDLGlCQUFpQjtFQUNqQix5QkFBeUI7RUFDekIsZ0JBQWdCO0VBQ2hCLHFCQUFxQjtFQUNyQixrQkFBa0I7RUFDbEIsVUFBVTtFQUNWLGlCQUFpQjtBQUNuQjs7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQixVQUFVO0FBQ1o7O0FBRUE7RUFDRSxpQkFBaUI7RUFDakIscUJBQXFCO0FBQ3ZCOztBQUVBO0VBQ0UscUJBQXFCO0VBQ3JCLGlCQUFpQjtBQUNuQjs7QUFDQTtFQUNFLG9CQUFvQjtBQUN0Qjs7QUFDQTtFQUNFLGlCQUFpQjtFQUNqQixrQkFBa0I7RUFDbEIsaUJBQWlCO0FBQ25COztBQUVBO0VBQ0UseUJBQXlCO0FBQzNCOztBQUVBO0VBQ0Usc0JBQXNCO0FBQ3hCOztBQUVBO0VBQ0UsaUJBQWlCO0VBQ2pCLG9CQUFvQjtFQUNwQixnQkFBZ0I7RUFDaEIseUJBQXlCO0VBQ3pCLFlBQVk7QUFDZDs7QUFFQTtFQUNFLHlCQUF5QjtFQUN6QixZQUFZO0FBQ2Q7O0FBRUE7RUFDRSxpQkFBaUI7QUFDbkI7O0FBQ0E7RUFDRSx1QkFBdUI7QUFDekI7O0FBQ0E7RUFDRSx1QkFBdUI7QUFDekI7O0FBQ0E7RUFDRSx5REFBeUQ7RUFDekQseUJBQXlCO0VBQ3pCLFdBQVc7QUFDYjs7QUFFQTs7RUFFRSxzQkFBc0I7RUFDdEIsWUFBWTtFQUNaLFVBQVU7QUFDWiIsImZpbGUiOiJhZGQtcGFja2FnZXMuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIiN1c2VySW5mbyB7XHJcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbiAgcGFkZGluZy10b3A6IDUlO1xyXG59XHJcblxyXG4jYnRuQWRkIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiBncmV5ICFpbXBvcnRhbnQ7XHJcbiAgY29sb3I6IHdoaXRlc21va2U7XHJcbiAgYm9yZGVyOiAycHggc29saWQgIzU1NTU1NTtcclxuICBtYXJnaW4tcmlnaHQ6IDIlO1xyXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgd2lkdGg6IDIwJTtcclxuICBwYWRkaW5nOiA4cHggNDBweDtcclxufVxyXG5cclxuI2J0bkNsZWFyIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiBncmV5ICFpbXBvcnRhbnQ7XHJcbiAgY29sb3I6IHdoaXRlc21va2U7XHJcbiAgYm9yZGVyOiAycHggc29saWQgIzU1NTU1NTtcclxuICBtYXJnaW4tcmlnaHQ6IDIlO1xyXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgd2lkdGg6IDIwJTtcclxuICBwYWRkaW5nOiA4cHggNDBweDtcclxufVxyXG5cclxuI2Vycm9ybGJsIHtcclxuICBwYWRkaW5nLWxlZnQ6IDMuNSU7XHJcbiAgY29sb3I6IHJlZDtcclxufVxyXG5cclxuLnVzZXJuYW1lIHtcclxuICBmb250LXNpemU6IG1lZGl1bTtcclxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbn1cclxuXHJcbi51c2VySW5mb0RpdiB7XHJcbiAgYWxpZ24tY29udGVudDogY2VudGVyO1xyXG4gIHRleHQtYWxpZ246IHJpZ2h0O1xyXG59XHJcbi5jb2wtc20tMTAge1xyXG4gIHBhZGRpbmctcmlnaHQ6IDE1MHB4O1xyXG59XHJcbi5jb2wtc20tMiB7XHJcbiAgcGFkZGluZy10b3A6IDEycHg7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG59XHJcblxyXG4jY3VzdG9tZXJzIHRyOm50aC1jaGlsZChldmVuKSB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2YyZjJmMjtcclxufVxyXG5cclxuI2N1c3RvbWVycyB0cjpob3ZlciB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2RkZDtcclxufVxyXG5cclxuI2N1c3RvbWVycyB0aCB7XHJcbiAgcGFkZGluZy10b3A6IDEycHg7XHJcbiAgcGFkZGluZy1ib3R0b206IDEycHg7XHJcbiAgdGV4dC1hbGlnbjogbGVmdDtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDIzZTdkO1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuLmNhcmQtaGVhZGVyIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDMxZDQ0O1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuLm5nLWRpcnR5Lm5nLWludmFsaWQge1xyXG4gIGJvcmRlci1jb2xvcjogcmVkO1xyXG59XHJcbmlucHV0OmZvY3VzIHtcclxuICBib3JkZXI6IDVweCBzb2xpZCBncmVlbjtcclxufVxyXG5zZWxlY3Q6Zm9jdXMge1xyXG4gIGJvcmRlcjogNXB4IHNvbGlkIGdyZWVuO1xyXG59XHJcbiNjdXN0b21lcnMge1xyXG4gIGZvbnQtZmFtaWx5OiBcIlRyZWJ1Y2hldCBNU1wiLCBBcmlhbCwgSGVsdmV0aWNhLCBzYW5zLXNlcmlmO1xyXG4gIGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbiNjdXN0b21lcnMgdGQsXHJcbiNjdXN0b21lcnMgdGgge1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkICNkZGQ7XHJcbiAgcGFkZGluZzogOHB4O1xyXG4gIHdpZHRoOiAyNSU7XHJcbn1cclxuIl19 */"]
      });
      /***/
    },

    /***/
    "86tH":
    /*!***************************************************!*\
      !*** ./src/app/pages/workflow/workflow.module.ts ***!
      \***************************************************/

    /*! exports provided: WorkflowModule */

    /***/
    function tH(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "WorkflowModule", function () {
        return WorkflowModule;
      });
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _pidcheck_pidcheck_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./pidcheck/pidcheck.component */
      "jaMP");
      /* harmony import */


      var _workflow_routes__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./workflow.routes */
      "adZh");
      /* harmony import */


      var src_app_services_packages_add_packages_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/app/services/packages/add-packages.service */
      "wlS1");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");

      var WorkflowModule = function WorkflowModule() {
        _classCallCheck(this, WorkflowModule);
      };

      WorkflowModule.ɵfac = function WorkflowModule_Factory(t) {
        return new (t || WorkflowModule)();
      };

      WorkflowModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdefineNgModule"]({
        type: WorkflowModule
      });
      WorkflowModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdefineInjector"]({
        providers: [src_app_services_packages_add_packages_service__WEBPACK_IMPORTED_MODULE_3__["AddPackagesService"]],
        imports: [[_angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"], _workflow_routes__WEBPACK_IMPORTED_MODULE_2__["WORKFLOW_ROUTES"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClientModule"]]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵsetNgModuleScope"](WorkflowModule, {
          declarations: [_pidcheck_pidcheck_component__WEBPACK_IMPORTED_MODULE_1__["PidcheckComponent"]],
          imports: [_angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"], _angular_router__WEBPACK_IMPORTED_MODULE_7__["RouterModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClientModule"]]
        });
      })();
      /***/

    },

    /***/
    "8D7W":
    /*!******************************************!*\
      !*** ./src/app/pages/pages.component.ts ***!
      \******************************************/

    /*! exports provided: PagesComponent */

    /***/
    function D7W(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PagesComponent", function () {
        return PagesComponent;
      });
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _shared_header_header_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../shared/header/header.component */
      "320Y");
      /* harmony import */


      var _shared_sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../shared/sidebar/sidebar.component */
      "sRhs");
      /* harmony import */


      var _shared_footer_footer_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../shared/footer/footer.component */
      "jQpT");

      var PagesComponent = /*#__PURE__*/function () {
        function PagesComponent(router) {
          _classCallCheck(this, PagesComponent);

          this.router = router;
        }

        _createClass(PagesComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            init_plugins();
            this.userlogged = JSON.parse(localStorage.getItem("user"));

            if (this.userlogged != null) {
              console.log();
            } else {
              this.router.navigate(["/login"]);
            }
          }
        }]);

        return PagesComponent;
      }();

      PagesComponent.ɵfac = function PagesComponent_Factory(t) {
        return new (t || PagesComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_0__["Router"]));
      };

      PagesComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({
        type: PagesComponent,
        selectors: [["app-pages"]],
        decls: 7,
        vars: 0,
        consts: [[1, "content-page"], [1, "content"], [1, "container-fluid"]],
        template: function PagesComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "app-header");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "app-sidebar");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](5, "router-outlet");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](6, "app-footer");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
          }
        },
        directives: [_shared_header_header_component__WEBPACK_IMPORTED_MODULE_2__["HeaderComponent"], _shared_sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_3__["SidebarComponent"], _angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterOutlet"], _shared_footer_footer_component__WEBPACK_IMPORTED_MODULE_4__["FooterComponent"]],
        encapsulation: 2
      });
      /***/
    },

    /***/
    "AytR":
    /*!*****************************************!*\
      !*** ./src/environments/environment.ts ***!
      \*****************************************/

    /*! exports provided: environment */

    /***/
    function AytR(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "environment", function () {
        return environment;
      }); // This file can be replaced during build by using the `fileReplacements` array.
      // `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
      // The list of file replacements can be found in `angular.json`.


      var environment = {
        production: false
      };
      /*
       * For easier debugging in development mode, you can import the following file
       * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
       *
       * This import should be commented out in production mode because it will have a negative impact
       * on performance if an error is thrown.
       */
      // import 'zone.js/dist/zone-error';  // Included with Angular CLI.

      /***/
    },

    /***/
    "Bd4x":
    /*!**********************************************************************************************!*\
      !*** ./src/app/pages/tid-creation/dialogComponents/lender-dialog/lender-dialog.component.ts ***!
      \**********************************************************************************************/

    /*! exports provided: LenderDialogComponent */

    /***/
    function Bd4x(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LenderDialogComponent", function () {
        return LenderDialogComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/material/dialog */
      "0IaG");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");

      var _c0 = ["select"];

      function LenderDialogComponent_option_10_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "option");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var number_r2 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](number_r2);
        }
      }

      var LenderDialogComponent = /*#__PURE__*/function () {
        function LenderDialogComponent(_dialogRef) {
          _classCallCheck(this, LenderDialogComponent);

          this._dialogRef = _dialogRef; //Variables

          this.optionArray = ["", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"];
        }

        _createClass(LenderDialogComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {// this.selectCheckbox.nativeElement.focus();
          }
        }, {
          key: "ngAfterViewInit",
          value: function ngAfterViewInit() {
            this.selectCheckbox.nativeElement.focus();
          }
        }, {
          key: "onEditClick",
          value: function onEditClick(skill) {
            console.log("skill name", skill);

            this._dialogRef.close(skill);
          }
        }]);

        return LenderDialogComponent;
      }();

      LenderDialogComponent.ɵfac = function LenderDialogComponent_Factory(t) {
        return new (t || LenderDialogComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"]));
      };

      LenderDialogComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: LenderDialogComponent,
        selectors: [["app-lender-dialog"]],
        viewQuery: function LenderDialogComponent_Query(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c0, 1);
          }

          if (rf & 2) {
            var _t;

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.selectCheckbox = _t.first);
          }
        },
        decls: 11,
        vars: 1,
        consts: [[1, "card"], [1, "card-header"], [1, "card-body"], ["for", "values", 2, "margin-left", "32%"], ["name", "values", 1, "form-select", 2, "margin-left", "10%", 3, "change"], ["select", ""], [4, "ngFor", "ngForOf"]],
        template: function LenderDialogComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "h5", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "Multiple Lenders");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "span", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "How many lenders present");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "select", 4, 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("change", function LenderDialogComponent_Template_select_change_8_listener($event) {
              return ctx.onEditClick($event.target.value);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](10, LenderDialogComponent_option_10_Template, 2, 1, "option", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.optionArray);
          }
        },
        directives: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["NgForOf"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgSelectOption"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ɵangular_packages_forms_forms_z"]],
        styles: [".card-header[_ngcontent-%COMP%] {\r\n  background-color: #031d44;\r\n  color: white;\r\n}\r\n.card[_ngcontent-%COMP%] {\r\n  padding-bottom: 5%;\r\n  height: 9.2em;\r\n  width: 35em;\r\n  border: solid 2px #04395e;\r\n  font-family: Arial;\r\n  font-size: 18px;\r\n}\r\nselect[_ngcontent-%COMP%] {\r\n  border: 2px;\r\n  padding: 0 1em 0 0;\r\n  margin: 0;\r\n  width: 80%;\r\n  font-family: inherit;\r\n  font-size: inherit;\r\n  cursor: inherit;\r\n  line-height: inherit;\r\n}\r\nselect[_ngcontent-%COMP%]:focus {\r\n  border: 5px solid green;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImxlbmRlci1kaWFsb2cuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLHlCQUF5QjtFQUN6QixZQUFZO0FBQ2Q7QUFDQTtFQUNFLGtCQUFrQjtFQUNsQixhQUFhO0VBQ2IsV0FBVztFQUNYLHlCQUF5QjtFQUN6QixrQkFBa0I7RUFDbEIsZUFBZTtBQUNqQjtBQUVBO0VBQ0UsV0FBVztFQUNYLGtCQUFrQjtFQUNsQixTQUFTO0VBQ1QsVUFBVTtFQUNWLG9CQUFvQjtFQUNwQixrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLG9CQUFvQjtBQUN0QjtBQUVBO0VBQ0UsdUJBQXVCO0FBQ3pCIiwiZmlsZSI6ImxlbmRlci1kaWFsb2cuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jYXJkLWhlYWRlciB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzAzMWQ0NDtcclxuICBjb2xvcjogd2hpdGU7XHJcbn1cclxuLmNhcmQge1xyXG4gIHBhZGRpbmctYm90dG9tOiA1JTtcclxuICBoZWlnaHQ6IDkuMmVtO1xyXG4gIHdpZHRoOiAzNWVtO1xyXG4gIGJvcmRlcjogc29saWQgMnB4ICMwNDM5NWU7XHJcbiAgZm9udC1mYW1pbHk6IEFyaWFsO1xyXG4gIGZvbnQtc2l6ZTogMThweDtcclxufVxyXG5cclxuc2VsZWN0IHtcclxuICBib3JkZXI6IDJweDtcclxuICBwYWRkaW5nOiAwIDFlbSAwIDA7XHJcbiAgbWFyZ2luOiAwO1xyXG4gIHdpZHRoOiA4MCU7XHJcbiAgZm9udC1mYW1pbHk6IGluaGVyaXQ7XHJcbiAgZm9udC1zaXplOiBpbmhlcml0O1xyXG4gIGN1cnNvcjogaW5oZXJpdDtcclxuICBsaW5lLWhlaWdodDogaW5oZXJpdDtcclxufVxyXG5cclxuc2VsZWN0OmZvY3VzIHtcclxuICBib3JkZXI6IDVweCBzb2xpZCBncmVlbjtcclxufVxyXG4iXX0= */"]
      });
      /***/
    },

    /***/
    "BoU+":
    /*!***************************************************!*\
      !*** ./src/app/pages/manifest/manifest.module.ts ***!
      \***************************************************/

    /*! exports provided: ManifestModule */

    /***/
    function BoU(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ManifestModule", function () {
        return ManifestModule;
      });
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _manifest_routing_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./manifest-routing.module */
      "LTO5");
      /* harmony import */


      var _add_packages_add_packages_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./add-packages/add-packages.component */
      "6v43");
      /* harmony import */


      var _edit_remove_edit_remove_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./edit-remove/edit-remove.component */
      "bark");
      /* harmony import */


      var src_app_services_packages_add_packages_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! src/app/services/packages/add-packages.service */
      "wlS1");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");
      /* harmony import */


      var src_app_services_packages_audit_trail_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/services/packages/audit-trail.service */
      "irGt");
      /* harmony import */


      var src_app_services_tid_tidcreartion_service_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! src/app/services/tid/tidcreartion-service.service */
      "eHNn");
      /* harmony import */


      var _sort_packages_sort_packages_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! ./sort-packages/sort-packages.component */
      "3sU2");
      /* harmony import */


      var _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! @angular/material/progress-spinner */
      "Xa2L");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! @angular/core */
      "fXoL"); // <== add the imports!


      var ManifestModule = function ManifestModule() {
        _classCallCheck(this, ManifestModule);
      };

      ManifestModule.ɵfac = function ManifestModule_Factory(t) {
        return new (t || ManifestModule)();
      };

      ManifestModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_11__["ɵɵdefineNgModule"]({
        type: ManifestModule
      });
      ManifestModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_11__["ɵɵdefineInjector"]({
        providers: [src_app_services_packages_add_packages_service__WEBPACK_IMPORTED_MODULE_4__["AddPackagesService"], src_app_services_packages_audit_trail_service__WEBPACK_IMPORTED_MODULE_7__["AuditTrailService"], src_app_services_tid_tidcreartion_service_service__WEBPACK_IMPORTED_MODULE_8__["TidcreartionServiceService"]],
        imports: [[_angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"], _manifest_routing_module__WEBPACK_IMPORTED_MODULE_1__["ManifestRoutingModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClientModule"], _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_10__["MatProgressSpinnerModule"]]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_11__["ɵɵsetNgModuleScope"](ManifestModule, {
          declarations: [_add_packages_add_packages_component__WEBPACK_IMPORTED_MODULE_2__["AddPackagesComponent"], _edit_remove_edit_remove_component__WEBPACK_IMPORTED_MODULE_3__["EditRemoveComponent"], _sort_packages_sort_packages_component__WEBPACK_IMPORTED_MODULE_9__["SortPackagesComponent"]],
          imports: [_angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"], _manifest_routing_module__WEBPACK_IMPORTED_MODULE_1__["ManifestRoutingModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClientModule"], _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_10__["MatProgressSpinnerModule"]]
        });
      })();
      /***/

    },

    /***/
    "F/wz":
    /*!****************************************************!*\
      !*** ./src/app/services/shared/sidebar.service.ts ***!
      \****************************************************/

    /*! exports provided: SidebarService */

    /***/
    function FWz(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SidebarService", function () {
        return SidebarService;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var SidebarService = function SidebarService() {
        _classCallCheck(this, SidebarService);

        this.menu = [{
          title: 'Admin',
          icon: 'ion-md-person',
          isRedirect: false,
          hasSubmenu: true,
          ariaexpanded: false,
          url: 'javascript: void(0);',
          submenu: [{
            title: 'Users',
            icon: 'ion-md-person-add',
            url: '/admin/users'
          }]
        }];
      };

      SidebarService.ɵfac = function SidebarService_Factory(t) {
        return new (t || SidebarService)();
      };

      SidebarService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
        token: SidebarService,
        factory: SidebarService.ɵfac
      });
      /***/
    },

    /***/
    "GCp2":
    /*!*********************************************!*\
      !*** ./src/app/pages/admin/admin.module.ts ***!
      \*********************************************/

    /*! exports provided: AdminModule */

    /***/
    function GCp2(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AdminModule", function () {
        return AdminModule;
      });
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _users_users_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./users/users.component */
      "eTcO");
      /* harmony import */


      var _shared_shared_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../../shared/shared.module */
      "PCNd");
      /* harmony import */


      var _admin_routes__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./admin.routes */
      "Gwfp");
      /* harmony import */


      var _reprint_reprint_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./reprint/reprint.component */
      "paas");
      /* harmony import */


      var src_app_services_packages_add_packages_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! src/app/services/packages/add-packages.service */
      "wlS1");
      /* harmony import */


      var src_app_services_tid_tidcreartion_service_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/services/tid/tidcreartion-service.service */
      "eHNn");
      /* harmony import */


      var src_app_services_coversheet_create_coversheet_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! src/app/services/coversheet/create-coversheet.service */
      "44oB");
      /* harmony import */


      var _add_user_add_user_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! ./add-user/add-user.component */
      "dPAj");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! @angular/router */
      "tyNb"); // Pages Admin
      // Module
      // Routes


      var AdminModule = function AdminModule() {
        _classCallCheck(this, AdminModule);
      };

      AdminModule.ɵfac = function AdminModule_Factory(t) {
        return new (t || AdminModule)();
      };

      AdminModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵdefineNgModule"]({
        type: AdminModule
      });
      AdminModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵdefineInjector"]({
        providers: [src_app_services_packages_add_packages_service__WEBPACK_IMPORTED_MODULE_6__["AddPackagesService"], src_app_services_tid_tidcreartion_service_service__WEBPACK_IMPORTED_MODULE_7__["TidcreartionServiceService"], src_app_services_coversheet_create_coversheet_service__WEBPACK_IMPORTED_MODULE_8__["CreateCoversheetService"]],
        imports: [[_angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_3__["SharedModule"], _admin_routes__WEBPACK_IMPORTED_MODULE_4__["ADMIN_ROUTES"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ReactiveFormsModule"]]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵsetNgModuleScope"](AdminModule, {
          declarations: [_users_users_component__WEBPACK_IMPORTED_MODULE_2__["UsersComponent"], _reprint_reprint_component__WEBPACK_IMPORTED_MODULE_5__["ReprintComponent"], _add_user_add_user_component__WEBPACK_IMPORTED_MODULE_9__["AddUserComponent"]],
          imports: [_angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_3__["SharedModule"], _angular_router__WEBPACK_IMPORTED_MODULE_11__["RouterModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ReactiveFormsModule"]]
        });
      })();
      /***/

    },

    /***/
    "Gwfp":
    /*!*********************************************!*\
      !*** ./src/app/pages/admin/admin.routes.ts ***!
      \*********************************************/

    /*! exports provided: ADMIN_ROUTES */

    /***/
    function Gwfp(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ADMIN_ROUTES", function () {
        return ADMIN_ROUTES;
      });
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _users_users_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./users/users.component */
      "eTcO");
      /* harmony import */


      var _reprint_reprint_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./reprint/reprint.component */
      "paas");
      /* harmony import */


      var _add_user_add_user_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./add-user/add-user.component */
      "dPAj"); // Pages Admin Module


      var routes = [{
        path: 'users',
        component: _users_users_component__WEBPACK_IMPORTED_MODULE_1__["UsersComponent"],
        data: {
          title: 'Operators'
        }
      }, {
        path: 'addUser',
        component: _add_user_add_user_component__WEBPACK_IMPORTED_MODULE_3__["AddUserComponent"],
        data: {
          title: 'Add User'
        }
      }, {
        path: 'reprint',
        component: _reprint_reprint_component__WEBPACK_IMPORTED_MODULE_2__["ReprintComponent"],
        data: {
          title: 'Reprint'
        }
      }];

      var ADMIN_ROUTES = _angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"].forChild(routes);
      /***/

    },

    /***/
    "H7Gl":
    /*!**********************************************************!*\
      !*** ./src/app/services/packages/worflow-rel.service.ts ***!
      \**********************************************************/

    /*! exports provided: WorflowRelService */

    /***/
    function H7Gl(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "WorflowRelService", function () {
        return WorflowRelService;
      });
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");
      /* harmony import */


      var _shared_http_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../shared/http-rest.service */
      "vulr");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var WorflowRelService = /*#__PURE__*/function (_shared_http_rest_ser3) {
        _inherits(WorflowRelService, _shared_http_rest_ser3);

        var _super3 = _createSuper(WorflowRelService);

        function WorflowRelService(http, router) {
          var _this22;

          _classCallCheck(this, WorflowRelService);

          _this22 = _super3.call(this, http);
          _this22.router = router;
          _this22.rootURL = "http://localhost:53420/api/workflow_rel";
          return _this22;
        }

        _createClass(WorflowRelService, [{
          key: "getAll",
          value: function getAll() {
            return this.findAll("workflow_rel", "FindAll");
          }
        }, {
          key: "getbyId",
          value: function getbyId(id) {
            return this.findAllById(id, "workflow_rel", "FindById");
          }
        }]);

        return WorflowRelService;
      }(_shared_http_rest_service__WEBPACK_IMPORTED_MODULE_2__["HttpRestService"]);

      WorflowRelService.ɵfac = function WorflowRelService_Factory(t) {
        return new (t || WorflowRelService)(_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵinject"](_angular_router__WEBPACK_IMPORTED_MODULE_0__["Router"]));
      };

      WorflowRelService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineInjectable"]({
        token: WorflowRelService,
        factory: WorflowRelService.ɵfac,
        providedIn: 'root'
      });
      /***/
    },

    /***/
    "Hkzo":
    /*!******************************************************************************************!*\
      !*** ./src/app/pages/tid-creation/dialogComponents/loan-dialog/loan-dialog.component.ts ***!
      \******************************************************************************************/

    /*! exports provided: LoanDialogComponent */

    /***/
    function Hkzo(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoanDialogComponent", function () {
        return LoanDialogComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/material/dialog */
      "0IaG");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");

      var _c0 = ["select"];

      function LoanDialogComponent_option_10_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "option");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var number_r2 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](number_r2);
        }
      }

      var LoanDialogComponent = /*#__PURE__*/function () {
        function LoanDialogComponent(_dialogRef) {
          _classCallCheck(this, LoanDialogComponent);

          this._dialogRef = _dialogRef; //Variables

          this.optionArray = ["", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"];
        }

        _createClass(LoanDialogComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {//this.selectCheckbox.nativeElement.focus();
          }
        }, {
          key: "ngAfterViewInit",
          value: function ngAfterViewInit() {
            this.selectCheckbox.nativeElement.focus();
          }
        }, {
          key: "onEditClick",
          value: function onEditClick(skill) {
            console.log("skill name", skill);

            this._dialogRef.close(skill);
          }
        }]);

        return LoanDialogComponent;
      }();

      LoanDialogComponent.ɵfac = function LoanDialogComponent_Factory(t) {
        return new (t || LoanDialogComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"]));
      };

      LoanDialogComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: LoanDialogComponent,
        selectors: [["app-loan-dialog"]],
        viewQuery: function LoanDialogComponent_Query(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c0, 1);
          }

          if (rf & 2) {
            var _t;

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.selectCheckbox = _t.first);
          }
        },
        decls: 11,
        vars: 1,
        consts: [[1, "card"], [1, "card-header"], [1, "card-body"], ["for", "values", 2, "margin-left", "32%"], ["name", "values", 1, "form-select", 2, "margin-left", "10%", 3, "change"], ["select", ""], [4, "ngFor", "ngForOf"]],
        template: function LoanDialogComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "h5", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "Lender Loan Count");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "span", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "How many loans present");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "select", 4, 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("change", function LoanDialogComponent_Template_select_change_8_listener($event) {
              return ctx.onEditClick($event.target.value);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](10, LoanDialogComponent_option_10_Template, 2, 1, "option", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.optionArray);
          }
        },
        directives: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["NgForOf"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgSelectOption"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ɵangular_packages_forms_forms_z"]],
        styles: [".card-header[_ngcontent-%COMP%] {\r\n  background-color: #031d44;\r\n  color: white;\r\n}\r\n.card[_ngcontent-%COMP%] {\r\n  padding-bottom: 5%;\r\n  height: 9.2em;\r\n  width: 35em;\r\n  border: solid 2px #04395e;\r\n  font-family: Arial;\r\n  font-size: 18px;\r\n}\r\nselect[_ngcontent-%COMP%] {\r\n  border: 2px;\r\n  padding: 0 1em 0 0;\r\n  margin: 0;\r\n  width: 80%;\r\n  font-family: inherit;\r\n  font-size: inherit;\r\n  cursor: inherit;\r\n  line-height: inherit;\r\n}\r\nselect[_ngcontent-%COMP%]:focus {\r\n  border: 5px solid green;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImxvYW4tZGlhbG9nLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSx5QkFBeUI7RUFDekIsWUFBWTtBQUNkO0FBQ0E7RUFDRSxrQkFBa0I7RUFDbEIsYUFBYTtFQUNiLFdBQVc7RUFDWCx5QkFBeUI7RUFDekIsa0JBQWtCO0VBQ2xCLGVBQWU7QUFDakI7QUFFQTtFQUNFLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsU0FBUztFQUNULFVBQVU7RUFDVixvQkFBb0I7RUFDcEIsa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZixvQkFBb0I7QUFDdEI7QUFFQTtFQUNFLHVCQUF1QjtBQUN6QiIsImZpbGUiOiJsb2FuLWRpYWxvZy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNhcmQtaGVhZGVyIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDMxZDQ0O1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxufVxyXG4uY2FyZCB7XHJcbiAgcGFkZGluZy1ib3R0b206IDUlO1xyXG4gIGhlaWdodDogOS4yZW07XHJcbiAgd2lkdGg6IDM1ZW07XHJcbiAgYm9yZGVyOiBzb2xpZCAycHggIzA0Mzk1ZTtcclxuICBmb250LWZhbWlseTogQXJpYWw7XHJcbiAgZm9udC1zaXplOiAxOHB4O1xyXG59XHJcblxyXG5zZWxlY3Qge1xyXG4gIGJvcmRlcjogMnB4O1xyXG4gIHBhZGRpbmc6IDAgMWVtIDAgMDtcclxuICBtYXJnaW46IDA7XHJcbiAgd2lkdGg6IDgwJTtcclxuICBmb250LWZhbWlseTogaW5oZXJpdDtcclxuICBmb250LXNpemU6IGluaGVyaXQ7XHJcbiAgY3Vyc29yOiBpbmhlcml0O1xyXG4gIGxpbmUtaGVpZ2h0OiBpbmhlcml0O1xyXG59XHJcblxyXG5zZWxlY3Q6Zm9jdXMge1xyXG4gIGJvcmRlcjogNXB4IHNvbGlkIGdyZWVuO1xyXG59XHJcbiJdfQ== */"]
      });
      /***/
    },

    /***/
    "ITgV":
    /*!******************************************************!*\
      !*** ./src/app/services/guards/login-guard.guard.ts ***!
      \******************************************************/

    /*! exports provided: LoginGuardGuard */

    /***/
    function ITgV(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginGuardGuard", function () {
        return LoginGuardGuard;
      });
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _users_users_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ../users/users.service */
      "j7lE");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var LoginGuardGuard = /*#__PURE__*/function () {
        function LoginGuardGuard(_usersService, router) {
          _classCallCheck(this, LoginGuardGuard);

          this._usersService = _usersService;
          this.router = router;
        }

        _createClass(LoginGuardGuard, [{
          key: "canActivate",
          value: function canActivate() {
            if (this._usersService.isLogged()) {
              return true;
            } else {
              this.router.navigate(['/login']);
              return false;
            }
          }
        }]);

        return LoginGuardGuard;
      }();

      LoginGuardGuard.ɵfac = function LoginGuardGuard_Factory(t) {
        return new (t || LoginGuardGuard)(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵinject"](_users_users_service__WEBPACK_IMPORTED_MODULE_1__["UsersService"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵinject"](_angular_router__WEBPACK_IMPORTED_MODULE_0__["Router"]));
      };

      LoginGuardGuard.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjectable"]({
        token: LoginGuardGuard,
        factory: LoginGuardGuard.ɵfac
      });
      /***/
    },

    /***/
    "JYu2":
    /*!*****************************************!*\
      !*** ./src/app/models/package.model.ts ***!
      \*****************************************/

    /*! exports provided: Package */

    /***/
    function JYu2(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Package", function () {
        return Package;
      });

      var Package = function Package() {
        _classCallCheck(this, Package);
      };
      /***/

    },

    /***/
    "LTO5":
    /*!***********************************************************!*\
      !*** ./src/app/pages/manifest/manifest-routing.module.ts ***!
      \***********************************************************/

    /*! exports provided: ManifestRoutingModule */

    /***/
    function LTO5(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ManifestRoutingModule", function () {
        return ManifestRoutingModule;
      });
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _add_packages_add_packages_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./add-packages/add-packages.component */
      "6v43");
      /* harmony import */


      var _edit_remove_edit_remove_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./edit-remove/edit-remove.component */
      "bark");
      /* harmony import */


      var _sort_packages_sort_packages_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./sort-packages/sort-packages.component */
      "3sU2");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var routes = [{
        path: 'addPackages',
        component: _add_packages_add_packages_component__WEBPACK_IMPORTED_MODULE_1__["AddPackagesComponent"],
        data: {
          title: 'Add'
        }
      }, {
        path: 'editRemove',
        component: _edit_remove_edit_remove_component__WEBPACK_IMPORTED_MODULE_2__["EditRemoveComponent"]
      }, {
        path: 'sortPackages',
        component: _sort_packages_sort_packages_component__WEBPACK_IMPORTED_MODULE_3__["SortPackagesComponent"]
      }];

      var ManifestRoutingModule = function ManifestRoutingModule() {
        _classCallCheck(this, ManifestRoutingModule);
      };

      ManifestRoutingModule.ɵfac = function ManifestRoutingModule_Factory(t) {
        return new (t || ManifestRoutingModule)();
      };

      ManifestRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineNgModule"]({
        type: ManifestRoutingModule
      });
      ManifestRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineInjector"]({
        imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"].forChild(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵsetNgModuleScope"](ManifestRoutingModule, {
          imports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]],
          exports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]]
        });
      })();
      /***/

    },

    /***/
    "NlK3":
    /*!****************************************************************************!*\
      !*** ./src/app/pages/tid-creation/generate-tid/bailee-transformer.pipe.ts ***!
      \****************************************************************************/

    /*! exports provided: BaileeTransformerPipe */

    /***/
    function NlK3(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "BaileeTransformerPipe", function () {
        return BaileeTransformerPipe;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var BaileeTransformerPipe = /*#__PURE__*/function () {
        function BaileeTransformerPipe() {
          _classCallCheck(this, BaileeTransformerPipe);
        }

        _createClass(BaileeTransformerPipe, [{
          key: "transform",
          value: function transform(value) {
            var number = this.ConvertStringToNumber(value); //80909

            if (number > 0 && number < 100) {
              number = number + 100;
            }

            if (number == 0) {
              value = '';
              return value;
            } //console.log(number);


            return number;
          }
        }, {
          key: "ConvertStringToNumber",
          value: function ConvertStringToNumber(input) {
            var numeric = Number(input);
            return numeric;
          }
        }]);

        return BaileeTransformerPipe;
      }();

      BaileeTransformerPipe.ɵfac = function BaileeTransformerPipe_Factory(t) {
        return new (t || BaileeTransformerPipe)();
      };

      BaileeTransformerPipe.ɵpipe = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefinePipe"]({
        name: "baileeTransformer",
        type: BaileeTransformerPipe,
        pure: true
      });
      /***/
    },

    /***/
    "OP5Q":
    /*!**************************************************************************************!*\
      !*** ./src/app/pages/certification/tid-certification/tid-certification.component.ts ***!
      \**************************************************************************************/

    /*! exports provided: TidCertificationComponent */

    /***/
    function OP5Q(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "TidCertificationComponent", function () {
        return TidCertificationComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! sweetalert2/dist/sweetalert2.js */
      "PdH4");
      /* harmony import */


      var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1__);
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var src_app_services_packages_add_packages_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/app/services/packages/add-packages.service */
      "wlS1");
      /* harmony import */


      var src_app_services_coversheet_create_coversheet_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! src/app/services/coversheet/create-coversheet.service */
      "44oB");
      /* harmony import */


      var src_app_services_tid_tidcreartion_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! src/app/services/tid/tidcreartion-service.service */
      "eHNn");
      /* harmony import */


      var src_app_services_packages_audit_trail_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! src/app/services/packages/audit-trail.service */
      "irGt");
      /* harmony import */


      var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @ng-bootstrap/ng-bootstrap */
      "1kSV");
      /* harmony import */


      var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! @angular/platform-browser */
      "jhN1");
      /* harmony import */


      var _tid_creation_image_display_image_display_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! ../../tid-creation/image-display/image-display.component */
      "ev3G");
      /* harmony import */


      var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! @angular/material/dialog */
      "0IaG");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
      /*! @angular/material/progress-spinner */
      "Xa2L");

      var _c0 = ["mainSearch"];
      var _c1 = ["barcodeSticker"];
      var _c2 = ["selectSearch"];
      var _c3 = ["notes"];
      var _c4 = ["transbody"];
      var _c5 = ["CertifiedSelector"];
      var _c6 = ["txtExceptionBarcode"];

      function TidCertificationComponent_mat_spinner_6_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "mat-spinner", 31);
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("diameter", 22);
        }
      }

      function TidCertificationComponent_div_27_option_6_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "option");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ex_r9 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ex_r9);
        }
      }

      function TidCertificationComponent_div_27_Template(rf, ctx) {
        if (rf & 1) {
          var _r11 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 32);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "span");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Certified In");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "select", 33, 34);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("change", function TidCertificationComponent_div_27_Template_select_change_4_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r11);

            var ctx_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r10.getCertifiedInValue($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](6, TidCertificationComponent_div_27_option_6_Template, 2, 1, "option", 29);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r2.CertifiedArray);
        }
      }

      function TidCertificationComponent_div_45_option_10_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "option", 45);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ex_r16 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngValue", ex_r16);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ex_r16);
        }
      }

      function TidCertificationComponent_div_45_Template(rf, ctx) {
        if (rf & 1) {
          var _r18 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 35);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 36);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "input", 37, 38);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function TidCertificationComponent_div_45_Template_input_ngModelChange_4_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r18);

            var ctx_r17 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r17.excBarcode = $event;
          })("keydown.enter", function TidCertificationComponent_div_45_Template_input_keydown_enter_4_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r18);

            var ctx_r19 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r19.onExceptionReason();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "br");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 39);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "select", 33, 40);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("change", function TidCertificationComponent_div_45_Template_select_change_8_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r18);

            var ctx_r20 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r20.applyChange($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](10, TidCertificationComponent_div_45_option_10_Template, 2, 2, "option", 41);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "br");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](12, "br");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 42);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "span", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "Exception Notes");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](18, "br");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "input", 43, 44);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("keydown.enter", function TidCertificationComponent_div_45_Template_input_keydown_enter_20_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r18);

            var ctx_r21 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r21.notesEnter($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](22, "br");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx_r4.excBarcode);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r4.exceptionArray);
        }
      }

      function TidCertificationComponent_th_71_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "th");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var row_r22 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](row_r22);
        }
      }

      var _c7 = function _c7(a0, a1) {
        return {
          "table-danger": a0,
          "table-success": a1
        };
      };

      function TidCertificationComponent_tbody_72_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tbody", 46);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "tr", 47);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "td");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "td");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "td");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "td");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var row_r23 = ctx.$implicit;
          var i_r24 = ctx.index;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("id", "chkPrint" + i_r24);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction2"](6, _c7, row_r23.ExceptionReason != "", row_r23.Certification === true));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("TID-", row_r23.TID_ID, " ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", row_r23.BarcodeSticker, " ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", row_r23.LoanOnManifest ? "Available" : "NA", " ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", row_r23.ExceptionReason, " ");
        }
      }

      var TidCertificationComponent = /*#__PURE__*/function () {
        function TidCertificationComponent(_packageService, _createTIDCS, auditService, _tidService, router, _dialog, modalService, _sanitizer) {
          _classCallCheck(this, TidCertificationComponent);

          this._packageService = _packageService;
          this._createTIDCS = _createTIDCS;
          this.auditService = auditService;
          this._tidService = _tidService;
          this.router = router;
          this._dialog = _dialog;
          this.modalService = modalService;
          this._sanitizer = _sanitizer; //static arrays

          this.headers = [
          /*"Tracking number", "PID", */
          "TID", "Barcode Sticker", "Loan on Manifest", "Exception Reason"];
          this.exceptionArray = [" ", "DND", "FAIL", "NIGO", "NOS", "REJECTS", "REROUTE", "RESEARCH", "RETURN TO CLIENT", "REVISIONS", "MISSING NOTE", "Proposed Change"];
          this.CertifiedArray = ["MDC", "Freddie", "DDC", "Guild -  Certified", "Guild -  Failed"]; //array intances

          this.tidsFound = [];
          this.tableCreationElements = [];
          this.excBarcode = "";
        }

        _createClass(TidCertificationComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.initPackages();
            this.userlogged = JSON.parse(localStorage.getItem("user"));
          } //main searchEOF" (

        }, {
          key: "search",
          value: function search(event) {
            this.inputReader = event.target.value;
            this.inputReader = this.inputReader.toLowerCase();
            this.tableCreationElements = [];

            if (this.inputReader.startsWith("pid-")) {
              // console.log("trying to print with pid");
              this.inputReader = this.inputReader.replace("pid-", "");
              this.findOneByPID(+this.inputReader);
              this.getAllTIDbyPid(this.inputReader);
              this.sortArray();
            } else if (this.inputReader != "") {
              this.findPackage(event.target.value);
              this.getTIDbyBC(this.inputReader);
              this.sortArray();
            }
          } // end search

        }, {
          key: "searchPackage",
          value: function searchPackage(e) {
            //See notes about 'which' and 'key'
            if (e.keyCode == 13) {
              //console.log(event.target.value);
              this.inputReader = this.mainSearch.nativeElement.value;
              this.inputReader = this.inputReader.toLowerCase();
              this.inputReader = this.inputReader.replace(/\s/g, "");
              this.tidList = [];

              if (this.inputReader.startsWith("pid-")) {
                // console.log("trying to print with pid");
                this.inputReader = this.inputReader.replace("pid-", "");
                this.findOneByPID(+this.inputReader);
                this.getAllTIDbyPid(this.inputReader);
                this.sortArray();
              } else if (this.inputReader != "") {
                this.findPackage(this.inputReader);
                this.getTIDbyBC(this.inputReader);
                this.sortArray();
              }

              return false;
            }
          } //tid input

        }, {
          key: "barcodeStickerFind",
          value: function barcodeStickerFind(e) {
            var _this23 = this;

            //See notes about 'which' and 'key'
            //console.log(e.target.value);
            //console.log(this.tidSearch.nativeElement.value);
            if (e.keyCode == 13) {
              //console.log("ENTERRR");
              this.tidReader = this.tidSearch.nativeElement.value;
              this.tidReader = this.tidReader.trim(); //Sthis.tidReader = this.tidReader.toLowerCase();

              var found = false;

              if (this.tidReader != "") {
                if (this.tidReader === "*MDC*") {
                  this.certifiedSelector.nativeElement.value = "MDC";
                  this.tidSearch.nativeElement.value = "";
                  this.tidSearch.nativeElement.focus();
                  return;
                } else if (this.tidReader === "*FRD*") {
                  this.certifiedSelector.nativeElement.value = "Freddie";
                  this.tidSearch.nativeElement.value = "";
                  this.tidSearch.nativeElement.focus();
                  return;
                } else if (this.tidReader === "*DDC*") {
                  this.certifiedSelector.nativeElement.value = "DDC";
                  this.tidSearch.nativeElement.value = "";
                  this.tidSearch.nativeElement.focus();
                  return;
                } else if (this.tidReader === "*CLP*") {
                  this.closePackage();
                  this.tidSearch.nativeElement.value = "";
                  this.tidSearch.nativeElement.focus();
                  return;
                } else if (this.tidReader === "*CNC*") {
                  this.cancelProcess();
                } else if (this.tidReader === "*EXC*") {
                  if (this.elonmusk) {
                    this.elonmusk = false;
                  } else {
                    this.elonmusk = true;
                  }

                  this.tidSearch.nativeElement.focus();
                  this.tidSearch.nativeElement.value = "";
                  return;
                } //** START COMPARING ***


                this.tidList.forEach(function (element) {
                  //console.log(element.BarcodeSticker);
                  console.log(" ************ COMPARING DATA: ", _this23.tidReader.toUpperCase(), element.BarcodeSticker.toString().toUpperCase());

                  if (_this23.tidReader.toUpperCase() === element.BarcodeSticker.toString().toUpperCase()) {
                    found = true; //CHECK IF MATCHED TID HAS EXCEPTION

                    if (element.ExceptionReason != "") {
                      sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
                        title: "TID in Exception",
                        text: "This TID is currently in Exception: " + element.ExceptionReason,
                        icon: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#3085d6",
                        cancelButtonColor: "#d33",
                        confirmButtonText: "Remove Exception"
                      }).then(function (result) {
                        if (result) {
                          // let index = this.tableCreationElements.findIndex(x => x.BarcodeSticker.toString() === this.tidReader);
                          if (_this23.elonmusk) {
                            _this23.selectSearch.nativeElement.focus();
                          } else {
                            _this23.closeTidRemoveException(element, true);
                          }
                        } else {
                          sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default.a.fire("Exception kept", "This TID remains with", "success");
                        }
                      });
                    } else {
                      //MEANING NO EXCEPTION BUT DID MATCH
                      //element.Certification = true;
                      console.log("TRYING TO COMPARE WITH THIS TIDREADER ", _this23.tidReader.toUpperCase());

                      var tid = _this23.tidList.find(function (x) {
                        return x.BarcodeSticker.toUpperCase() == _this23.tidReader.toUpperCase();
                      }); //console.log("this elemnt table was matched " , this.tableCreationElements[index]);
                      //this.tableCreationElements[index].Certification = true;


                      if (_this23.elonmusk) {
                        _this23.txtExceptionBarcode.nativeElement.focus();

                        _this23.txtExceptionBarcode.nativeElement.value = "";
                      } else {
                        console.log("******SENDING TO CLOSE TID ", tid);

                        _this23.closeTid(tid); //this.tableCreationElements = this.compare(this.tableCreationElements);

                      }
                    }
                  }
                }); ///end comparing

                if (!found) {
                  this.tidSearch.nativeElement.value = "";
                  sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
                    position: "top-end",
                    type: "error",
                    title: "Not found",
                    showConfirmButton: false,
                    timer: 1000
                  });
                }
              }

              return false;
            }
          }
        }, {
          key: "getCertifiedInValue",
          value: function getCertifiedInValue(event) {
            this.certified_value = this.certifiedSelector.nativeElement.value;
            console.log(this.certified_value);
          }
        }, {
          key: "retrieveImages",
          value: function retrieveImages() {
            var _this24 = this;

            this.showSpinner = true;
            var heynow; //console.log("LMAOOOOOOOOO", this.packageFound.Package_Tracking_ID);

            var audit = {
              PID: this.packageFound.Package_Tracking_ID,
              TID: "N/A",
              ActionName: "Retrieve Image / Certification",
              ActionDateTime: null,
              BusinessUnit: this.packageFound.Client_Name,
              Sticker: "",
              User_ID: this.userlogged.User_Name
            }; //insert audit record

            this.auditService.saveAudit(audit).subscribe(function (data) {
              console.log(data);
            });

            this._tidService.retrieveDoc(this.packageFound.Package_Tracking_ID).subscribe(function (e) {
              //console.log("geting images", e);
              _this24.imagesFound = e;
              e.forEach(function (element) {
                //console.log(element);
                var imageo = _this24._sanitizer.bypassSecurityTrustResourceUrl("data:image/tif;base64," + element);

                while (!element == undefined) {
                  _this24.imagesFound.push(element);
                }
              });

              _this24._dialog.open(_tid_creation_image_display_image_display_component__WEBPACK_IMPORTED_MODULE_9__["ImageDisplayComponent"], {
                data: {
                  images: _this24.imagesFound,
                  pidSent: _this24.packageFound.Package_Tracking_ID
                },
                panelClass: "custom-dialog-container"
              });

              _this24.showSpinner = false;
            }, function (error) {
              _this24.showSpinner = false;
              console.log(error);
            }); //end observable

          } //end retrieve images

        }, {
          key: "getAllTIDbyPid",
          value: function getAllTIDbyPid(trackingNumber) {
            var _this25 = this;

            this._tidService.getAllbyPid(trackingNumber).subscribe(function (data) {
              _this25.tidList = data;

              _this25.sortArray();
            }, function (error) {
              sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
                position: "top-end",
                type: "error",
                title: "",
                text: "Tids not found for this package",
                showConfirmButton: true,
                footer: "",
                timer: 1100
              });
            }); //end subscribe

          }
        }, {
          key: "getTIDbyBC",
          value: function getTIDbyBC(trackingNumber) {
            var _this26 = this;

            this._tidService.findTIDsById(trackingNumber).subscribe(function (data) {
              _this26.tidList = data;

              _this26.sortArray();
            }, function (error) {
              sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
                position: "top-end",
                type: "error",
                title: "",
                text: "Tids not found for this package",
                showConfirmButton: true,
                footer: "",
                timer: 1100
              });
            }); //end subscribe

          }
        }, {
          key: "findOneByPID",
          value: function findOneByPID(id) {
            var _this27 = this;

            this._packageService.findOneByPID(id).subscribe(function (packF) {
              //console.log(packF);
              _this27.packageFound = packF;

              _this27.tidSearch.nativeElement.focus();

              if (packF.Client_Name == "Freddie" || packF.Client_Name == "FREDDIE") {
                _this27.businessUnitDetector = true;
              } else {
                _this27.businessUnitDetector = false;
              }

              sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
                position: "top-end",
                type: "success",
                title: "",
                text: "Package loaded!",
                showConfirmButton: false,
                footer: "",
                timer: 600
              }); // this.secondSearch.nativeElement.value = '';
              // this.secondSearch.nativeElement.focus();
            }, function (error) {
              sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
                position: "top-end",
                type: "error",
                title: "",
                text: "Package not found!",
                showConfirmButton: true,
                footer: "",
                timer: 1100
              });
            });
          }
        }, {
          key: "findPackage",
          value: function findPackage(bcstring) {
            var _this28 = this;

            this._packageService.findOneByIdBC(bcstring).subscribe(function (data) {
              _this28.packageFound = data;

              _this28.tidSearch.nativeElement.focus();

              if (data.Client_Name == "Freddie" || data.Client_Name == "FREDDIE") {
                _this28.businessUnitDetector = true;
              } else {
                _this28.businessUnitDetector = false;
              }

              sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
                position: "top-end",
                type: "success",
                title: "",
                text: "Package found!",
                showConfirmButton: false,
                footer: "",
                timer: 700
              });
            }, function (error) {
              _this28.mainSearch.nativeElement.value = "";
              sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
                position: "top-end",
                type: "error",
                title: "",
                text: "Package not found!",
                showConfirmButton: true,
                footer: "",
                timer: 1100
              });
            } //end error
            );
          }
        }, {
          key: "aplyChange",
          value: function aplyChange(event) {
            console.log(event.target.value);
            this.notesSearch.nativeElement.value = "";
            this.notesSearch.nativeElement.focus();
          }
        }, {
          key: "grabNotes",
          value: function grabNotes(event) {
            this.tidNotes = event.target.value; //this.elementTid.no
          }
        }, {
          key: "initPackages",
          value: function initPackages() {
            this.mainSearch.nativeElement.focus();
            this.tidList = [];
            this.totalItems = 0;
            this.elonmusk = false;
            this.businessUnitDetector = false;
            this.totalFail = 0;
            this.totalCertified = 0;
            this.totalPending = 0;
            this.tidsFound = [];
            this.tableCreationElements = [];
            this.tidNumberList = [];
            this.safeToClosePackage = false;
            this.certified_value = "";
            this.packageFound = {
              Package_Tracking_BC: "",
              Package_Tracking_ID: "",
              LoanNumber: 0,
              Sticker_ID: "",
              multipleLenders: false,
              totalLenders: 0,
              Status_ID: 0,
              Location_Name: "",
              Client_Name: "",
              WorkFlow_ID: 0,
              Task_ID: 0,
              Creation_Date_Time: "",
              Available: false,
              Exception_Flag: false,
              Exception_Name: "",
              Package_Finished: false,
              ManifestInBox: false,
              BaileeLetter: false,
              clearPackagesFlag: false,
              Carrier: "",
              Package_Notes: "",
              Bin_Number: "",
              clearPackagesDateTime: "",
              lastModule: "",
              lastFunction: "",
              loanTotal: 0,
              loansExpected: 0
            };
            this.elementTid = {
              Package_Tracking_BC: "",
              Package_Tracking_ID: 0,
              TID_ID: 0,
              BaileeNumber: "",
              LenderName: "",
              LoanNumber: "",
              BarcodeSticker: "",
              BaileeName: "",
              BaileeLetter: false,
              SRC_SYSTM_CD: "",
              DCMNT_BAR_CD: "",
              DCMNT_TYP_CD: "",
              COLLATERAL_KEY: "",
              COLLATERAL_BAR_CODE: "",
              POOL_SAK: "",
              ACCOUNT_SAK: "",
              CUSTOMER_CODE: "",
              CUSTOMER_NAME: "",
              CNTRLLER_NUM: "",
              TRACK_LOCATION_CODE: "",
              ScanDate: null,
              DCMNT_CNDTN_CD: "",
              FILE_NAME: "",
              LoanOnManifest: false,
              Certification: false,
              ExceptionNotes: "",
              ExceptionReason: "",
              BorrowerName: "",
              UserId: "",
              Certified_In: ""
            };
          }
        }, {
          key: "closeTidRemoveException",
          value: function closeTidRemoveException(tid, remove) {
            var _this29 = this;

            //et verify = this.tidList[index];
            tid.ExceptionReason = "";
            tid.ExceptionNotes = "";
            tid.Certification = true; //verify.ExceptionNotes =  this.notesSearch.nativeElement.value;
            //verify.ExceptionReason = this.selectSearch.nativeElement.value;

            if (tid.Certification == true) {
              this.tidReader = "";

              if (this.businessUnitDetector == true) {
                tid.Certified_In = this.certified_value;
              } else {
                tid.Certified_In = "";
              }

              console.log("checking TID certidication", this.tidList);

              this._tidService.Update(tid).subscribe(function (data) {
                //this.tidsFound.push(verify);
                _this29.audit = {
                  PID: tid.Package_Tracking_ID.toString(),
                  TID: tid.TID_ID.toString(),
                  ActionName: "CERTIFICATION",
                  ActionDateTime: null,
                  BusinessUnit: "",
                  Sticker: tid.BarcodeSticker,
                  User_ID: _this29.userlogged.User_Name
                }; //insert audit record

                _this29.auditService.saveAudit(_this29.audit).subscribe(function (data) {
                  console.log(data);
                });

                _this29._packageService.findOneByIdBC(_this29.packageFound.Package_Tracking_BC).subscribe(function (data) {
                  _this29.packageFound = data;
                  _this29.elonmusk = false;
                  _this29.tableCreationElements = [];

                  _this29.getTIDbyBC(_this29.packageFound.Package_Tracking_BC);
                }); //End Package service


                sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
                  position: "top-end",
                  type: "success",
                  title: "TID certified",
                  showConfirmButton: false,
                  timer: 1300
                });
              }); //SORT ARRAY
              //   this.compare(this.tableCreationElements);

            } else {
              sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
                position: "top-end",
                type: "error",
                title: "Tid not certified",
                showConfirmButton: false,
                timer: 1300
              });
            }

            this.tidSearch.nativeElement.value = "";
            this.tidSearch.nativeElement.focus(); //this.notesSearch.nativeElement.value='';
          } //end close tID

        }, {
          key: "closeTid",
          value: function closeTid(tid) {
            var _this30 = this;

            //alert(tid.TID_ID);
            //let verify = this.tidList[index];
            tid.Certification = true; //verify.ExceptionNotes =  this.notesSearch.nativeElement.value;
            //verify.ExceptionReason = this.selectSearch.nativeElement.value;

            if (tid.Certification == true) {
              this.tidReader = "";
              console.log("checking TID certidication", this.tidList);
              console.log("Business UNIT DETECtOR ", this.certified_value);

              if (this.businessUnitDetector == true) {
                tid.Certified_In = this.certifiedSelector.nativeElement.value;
              } else {
                tid.Certified_In = "";
              }

              this._tidService.Update(tid).subscribe(function (data) {
                //this.tidsFound.push(verify);
                _this30.audit = {
                  PID: tid.Package_Tracking_ID.toString(),
                  TID: tid.TID_ID.toString(),
                  BusinessUnit: _this30.packageFound.Client_Name,
                  ActionName: "CERTIFICATION",
                  ActionDateTime: null,
                  Sticker: tid.BarcodeSticker,
                  User_ID: _this30.userlogged.User_Name
                }; //insert audit record

                _this30.auditService.saveAudit(_this30.audit).subscribe(function (data) {
                  console.log(data);
                });

                _this30._packageService.findOneByIdBC(_this30.packageFound.Package_Tracking_BC).subscribe(function (data) {
                  _this30.packageFound = data;
                  _this30.elonmusk = false;
                  _this30.tableCreationElements = [];

                  _this30.getTIDbyBC(_this30.packageFound.Package_Tracking_BC);
                });

                sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
                  position: "top-end",
                  type: "success",
                  title: "TID updated",
                  showConfirmButton: false,
                  timer: 1300
                });
              });
            } else {
              sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
                position: "top-end",
                type: "error",
                title: "Tid not certified",
                showConfirmButton: false,
                timer: 1300
              });
            } //this.compare(this.tableCreationElements);


            this.sortArray();
            this.tidSearch.nativeElement.value = "";
            this.tidSearch.nativeElement.focus(); //this.notesSearch.nativeElement.value='';
          } //end close tID

        }, {
          key: "closeTidException",
          value: function closeTidException(tid) {
            var _this31 = this;

            if (this.businessUnitDetector == true) {
              tid.Certified_In = this.certified_value;
            } else {
              tid.Certified_In = "";
            } //let verify = this.tidList[index];


            console.log("verify", tid);
            tid.Certification = false; //verify.ExceptionNotes =  this.notesSearch.nativeElement.value;
            //verify.ExceptionReason = this.selectSearch.nativeElement.value;

            if (tid.Certification == false) {
              this.tidReader = "";

              this._tidService.Update(tid).subscribe(function (data) {
                //this.tidsFound.push(verify);
                _this31.audit = {
                  PID: tid.Package_Tracking_ID.toString(),
                  TID: tid.TID_ID.toString(),
                  ActionName: "CERTIFICATION",
                  ActionDateTime: null,
                  BusinessUnit: "",
                  Sticker: tid.BarcodeSticker,
                  User_ID: _this31.userlogged.User_Name
                }; //insert audit record

                _this31.auditService.saveAudit(_this31.audit).subscribe(function (data) {
                  console.log(data);
                });

                _this31._packageService.findOneByIdBC(_this31.packageFound.Package_Tracking_BC).subscribe(function (data) {
                  _this31.packageFound = data;
                  _this31.elonmusk = false;
                  _this31.tableCreationElements = [];

                  _this31.getTIDbyBC(_this31.packageFound.Package_Tracking_BC);
                });

                sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
                  position: "top-end",
                  type: "success",
                  title: "TID updated",
                  showConfirmButton: false,
                  timer: 1300
                });
              });
            } else {
              sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
                position: "top-end",
                type: "error",
                title: "TID ERROR CERTIFYING",
                showConfirmButton: false,
                timer: 1300
              });
            } //let index2 = this.tableCreationElements.findIndex(x => x.BarcodeSticker.toString() === index.toString());
            //  console.log("this elemnt table was matched IN TABLE " , this.tableCreationElements[index2]);
            //
            //this.tableCreationElements = this.compare(this.tableCreationElements);


            this.sortArray();
            this.tidSearch.nativeElement.value = "";
            this.tidSearch.nativeElement.focus(); //this.notesSearch.nativeElement.value='';
          } //end close tID

        }, {
          key: "compare",
          value: function compare(filterArray) {
            // console.log("Before sort " , filterArray);
            filterArray.sort(function (a, b) {
              return a.Certification === false ? -1 : b.Certification === true ? 1 : 0;
            }); // console.log("After sort " , filterArray);

            return filterArray;
          }
        }, {
          key: "closePackage",
          value: function closePackage() {
            var _this32 = this;

            //Error counter
            var unfinishedCounter = 0; //Check in each TID if there are all completed

            this.tidList.forEach(function (element) {
              console.log("Element, " + element.TID_ID + " Certification " + element.Certification);

              if (element.Certification == true) {
                //console.log("there are false certifications yet " , element.TID_ID);
                _this32.safeToClosePackage = true;
              } else {
                unfinishedCounter = unfinishedCounter + 1; // this.safeToClosePackage = false;
              }
            }); //end foreach

            console.log(unfinishedCounter);

            if (unfinishedCounter > 0) {
              sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
                position: "top-end",
                type: "error",
                title: "Package has not been completed",
                showConfirmButton: false,
                timer: 1300
              });
            } else {
              this._packageService.updatePack(this.packageFound).subscribe(function (data) {
                _this32.audit = {
                  PID: _this32.packageFound.Package_Tracking_ID,
                  TID: "",
                  ActionName: "CERTIFICATION",
                  ActionDateTime: null,
                  BusinessUnit: _this32.packageFound.Client_Name,
                  Sticker: "",
                  User_ID: _this32.userlogged.User_Name
                }; //insert audit record

                _this32.auditService.saveAudit(_this32.audit).subscribe(function (data) {
                  console.log(data);
                });

                sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
                  position: "top-end",
                  type: "success",
                  title: "Package completed",
                  showConfirmButton: false,
                  timer: 1300
                });
              });

              this.initPackages();
              this.mainSearch.nativeElement.focus();
              this.mainSearch.nativeElement.value = "";
            }
          }
        }, {
          key: "cancelProcess",
          value: function cancelProcess() {
            this.mainSearch.nativeElement.value = "";
            this.initPackages();
            this.elonmusk = false;
          }
        }, {
          key: "exceptionCheckboxHandler",
          value: function exceptionCheckboxHandler() {
            if (this.elonmusk) {
              this.elonmusk = false;
            } else {
              this.elonmusk = true;
              this.tidSearch.nativeElement.focus();
            }
          }
        }, {
          key: "notesEnter",
          value: function notesEnter(event) {
            var _this33 = this;

            this.tidList.forEach(function (element) {
              if (_this33.tidReader == element.BarcodeSticker.toString()) {
                element.Certification = true;
                element.ExceptionNotes = _this33.notesSearch.nativeElement.value;
                element.ExceptionReason = _this33.selectSearch.nativeElement.value; //

                console.log("TID READER NTOES ENTER", _this33.tidReader);

                var tid = _this33.tidList.find(function (x) {
                  return x.BarcodeSticker == _this33.tidReader;
                }); // console.log("this elemnt table was matched " , this.tableCreationElements[index]);
                //this.tableCreationElements[index].Certification = true;


                _this33.closeTidException(tid);

                _this33.elonmusk = false; // this.selectSearch.nativeElement.focus();
              }
            }); ///end comparing
            // this._packageService.findOneByIdBC(this.packageFound.Package_Tracking_BC).subscribe( (data:Package) => {
            //   this.packageFound =data;
            //   this.tidSearch.nativeElement.focus();
            //   console.log( this.packageFound);
            //   });
          }
        }, {
          key: "sortArray",
          value: function sortArray() {
            /*this.tidList.sort((a,b) =>
            {
              return  (+a.Certification) - (+b.Certification) ||
              (a.ExceptionNotes === '' ? -1 : b.ExceptionNotes === '' ? 1 : 0)  - (b.ExceptionNotes === '' ? -1 : a.ExceptionNotes === '' ? 1 : 0)
            });  */
            this.totalItems = this.tidList.length;
            var totalCertified = this.tidList.filter(function (x) {
              return x.Certification === true;
            });
            var totalFail = this.tidList.filter(function (x) {
              return x.ExceptionReason != "";
            });
            this.totalCertified = totalCertified.length;
            this.totalFail = totalFail.length;
            this.totalPending = this.totalItems - (totalCertified.length + totalFail.length);
          }
        }, {
          key: "onExceptionReason",
          value: function onExceptionReason() {
            if (this.excBarcode === "*DND*") {
              this.selectSearch.nativeElement.value = "DND";
            } else if (this.excBarcode === "*FLD*") {
              this.selectSearch.nativeElement.value = "FAIL";
            } else if (this.excBarcode === "*NIG*") {
              this.selectSearch.nativeElement.value = "NIGO";
            } else if (this.excBarcode === "*NOS*") {
              this.selectSearch.nativeElement.value = "NOS";
            } else if (this.excBarcode === "*REJ*") {
              this.selectSearch.nativeElement.value = "REJECTS";
            } else if (this.excBarcode === "*RER*") {
              this.selectSearch.nativeElement.value = "REROUTE";
            } else if (this.excBarcode === "*RES*") {
              this.selectSearch.nativeElement.value = "RESEARCH";
            } else if (this.excBarcode === "*RTC*") {
              this.selectSearch.nativeElement.value = "RETURN TO CLIENT";
            }

            this.txtExceptionBarcode.nativeElement.value = "";
            this.notesSearch.nativeElement.focus();
          }
        }]);

        return TidCertificationComponent;
      }();

      TidCertificationComponent.ɵfac = function TidCertificationComponent_Factory(t) {
        return new (t || TidCertificationComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_packages_add_packages_service__WEBPACK_IMPORTED_MODULE_3__["AddPackagesService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_coversheet_create_coversheet_service__WEBPACK_IMPORTED_MODULE_4__["CreateCoversheetService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_packages_audit_trail_service__WEBPACK_IMPORTED_MODULE_6__["AuditTrailService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_tid_tidcreartion_service_service__WEBPACK_IMPORTED_MODULE_5__["TidcreartionServiceService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_material_dialog__WEBPACK_IMPORTED_MODULE_10__["MatDialog"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__["NgbModal"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_platform_browser__WEBPACK_IMPORTED_MODULE_8__["DomSanitizer"]));
      };

      TidCertificationComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: TidCertificationComponent,
        selectors: [["app-tid-certification"]],
        viewQuery: function TidCertificationComponent_Query(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c0, 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c1, 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c2, 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c3, 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c4, 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c5, 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c6, 1);
          }

          if (rf & 2) {
            var _t;

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.mainSearch = _t.first);
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.tidSearch = _t.first);
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.selectSearch = _t.first);
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.notesSearch = _t.first);
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.tableDesign = _t.first);
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.certifiedSelector = _t.first);
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.txtExceptionBarcode = _t.first);
          }
        },
        decls: 73,
        vars: 11,
        consts: [[1, "row"], [1, "col-md-4"], [1, "card"], [1, "card-header"], [2, "float", "right"], [3, "diameter", 4, "ngIf"], [1, "col-sm-12"], [2, "padding-left", "25px"], [1, "col-sm-12", 2, "padding-left", "5%", "padding-right", "5%"], ["type", "text", 1, "form-control", 3, "keydown.enter"], ["mainSearch", ""], ["id", "inputRow", 1, "row"], [1, "col-sm-12", 2, "padding-left", "15px", "padding-right", "15px"], ["style", "padding-left: 20px; padding-right: 20px;", 4, "ngIf"], [1, "col-sm-12", 2, "padding-left", "15px"], ["type", "checkbox", "id", "exampleCheck3", 1, "form-check-input", 2, "margin-left", "20px", 3, "ngModel", "ngModelChange", "click"], ["for", "exampleCheck3", 1, "form-check-label", 2, "padding-left", "35px"], ["barcodeSticker", ""], [4, "ngIf"], ["id", "btn1", 1, "btn", "btn-secondary", 3, "click"], ["id", "btn2", 1, "btn", "btn-secondary", 3, "click"], [1, "col-md-8"], [1, "card", 2, "padding-bottom", "1%"], [2, "float", "middle", "padding-left", "25%"], [2, "padding-left", "3%"], [1, "panel-body"], [1, "table-responsive"], ["id", "tblLoans", 1, "table", "table-striped"], [1, "thead-dark"], [4, "ngFor", "ngForOf"], [3, "id", 4, "ngFor", "ngForOf"], [3, "diameter"], [2, "padding-left", "20px", "padding-right", "20px"], [1, "form-control", 3, "change"], ["CertifiedSelector", ""], [1, "col-md-4", 2, "padding-left", "15px", "padding-right", "15px"], [1, "form-group"], ["type", "text", "placeholder", "Exception Barcode", "maxlength", "5", "name", "ExcBarcode", 1, "form-control", "txt", 3, "ngModel", "ngModelChange", "keydown.enter"], ["txtExceptionBarcode", ""], [1, "col-sm-8", 2, "padding-left", "15px", "padding-right", "15px"], ["selectSearch", ""], [3, "ngValue", 4, "ngFor", "ngForOf"], ["id", "trackinglbl", 1, "col-sm-12"], ["type", "text", "placeholder", "Notes", 1, "form-control", 2, "padding-left", "15px", "padding-right", "15px", 3, "keydown.enter"], ["notes", ""], [3, "ngValue"], [3, "id"], [3, "ngClass"]],
        template: function TidCertificationComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "h5", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "TID Certification ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "span", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](6, TidCertificationComponent_mat_spinner_6_Template, 1, 1, "mat-spinner", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "span", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "b");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Tracking # or PID-");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "input", 9, 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("keydown.enter", function TidCertificationComponent_Template_input_keydown_enter_15_listener($event) {
              return ctx.searchPackage($event);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](17, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "span", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "b");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](21, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](22, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](23, "hr");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](25, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "div", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](27, TidCertificationComponent_div_27_Template, 7, 1, "div", 13);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](28, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "div", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](31, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "input", 15);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function TidCertificationComponent_Template_input_ngModelChange_32_listener($event) {
              return ctx.elonmusk = $event;
            })("click", function TidCertificationComponent_Template_input_click_32_listener() {
              return ctx.exceptionCheckboxHandler();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "label", 16);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](34, "Record Exception");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](35, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](36, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "span", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "b");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39, "STICKER");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](40, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "div", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "input", 9, 17);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("keydown.enter", function TidCertificationComponent_Template_input_keydown_enter_42_listener($event) {
              return ctx.barcodeStickerFind($event);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](44, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](45, TidCertificationComponent_div_45_Template, 23, 2, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](46, "hr");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "button", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TidCertificationComponent_Template_button_click_47_listener() {
              return ctx.closePackage();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](48, "CLOSE PACKAGE");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "button", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TidCertificationComponent_Template_button_click_49_listener() {
              return ctx.retrieveImages();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](50, "RETRIEVE IMAGES");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "button", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TidCertificationComponent_Template_button_click_51_listener() {
              return ctx.cancelProcess();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](52, "CANCEL");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "div", 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "div", 22);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "h5", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](56, "AUDIT TABLE ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "span", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "span", 24);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](59);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "span", 24);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](61);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "span", 24);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](63);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](64, "span", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](65);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "div", 25);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](67, "div", 26);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "table", 27);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](69, "thead", 28);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](70, "tr");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](71, TidCertificationComponent_th_71_Template, 2, 1, "th", 29);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](72, TidCertificationComponent_tbody_72_Template, 10, 9, "tbody", 30);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.showSpinner);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("Business Unit: ", ctx.packageFound.Client_Name, "");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.businessUnitDetector);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.elonmusk);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](13);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.elonmusk);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" FAIL : ", ctx.totalFail, "");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" CERTIFIED : ", ctx.totalCertified, " ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" PENDING : ", ctx.totalPending, " ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" TOTAL : ", ctx.totalItems, "");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.headers);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.tidList);
          }
        },
        directives: [_angular_common__WEBPACK_IMPORTED_MODULE_11__["NgIf"], _angular_forms__WEBPACK_IMPORTED_MODULE_12__["CheckboxControlValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_12__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_12__["NgModel"], _angular_common__WEBPACK_IMPORTED_MODULE_11__["NgForOf"], _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_13__["MatSpinner"], _angular_forms__WEBPACK_IMPORTED_MODULE_12__["NgSelectOption"], _angular_forms__WEBPACK_IMPORTED_MODULE_12__["ɵangular_packages_forms_forms_z"], _angular_forms__WEBPACK_IMPORTED_MODULE_12__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_12__["MaxLengthValidator"], _angular_common__WEBPACK_IMPORTED_MODULE_11__["NgClass"]],
        styles: [".card-header[_ngcontent-%COMP%] {\r\n  background-color: #031d44;\r\n  color: white;\r\n}\r\n.card[_ngcontent-%COMP%] {\r\n  padding-bottom: 5%;\r\n  border: solid 2px #04395e;\r\n}\r\ntable[_ngcontent-%COMP%] {\r\n  table-layout: fixed;\r\n  margin: 0 0 40px 0;\r\n  width: 100%;\r\n  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.2);\r\n  display: table;\r\n  @media screen and (max-width: 580px);\r\n  display: block;\r\n}\r\n.dot[_ngcontent-%COMP%] {\r\n  height: 25px;\r\n  width: 25px;\r\n  border-radius: 50%;\r\n  display: inline-block;\r\n}\r\nth[_ngcontent-%COMP%] {\r\n  text-align: center;\r\n  vertical-align: middle;\r\n\r\n  width: 14.28%;\r\n  background-color: #031d44;\r\n  color: white;\r\n  font-family: Arial, Helvetica, sans-serif;\r\n}\r\ntd[_ngcontent-%COMP%] {\r\n  text-align: center;\r\n  vertical-align: middle;\r\n}\r\ninput[_ngcontent-%COMP%] {\r\n  border: solid 2px;\r\n}\r\ninput[_ngcontent-%COMP%]:focus {\r\n  border: 5px solid green;\r\n}\r\nselect[_ngcontent-%COMP%] {\r\n  border: solid 2px;\r\n}\r\nselect[_ngcontent-%COMP%]:focus {\r\n  border: 5px solid green;\r\n}\r\ncheckbox[_ngcontent-%COMP%]:focus {\r\n  border: 5px solid green;\r\n}\r\nhr[_ngcontent-%COMP%] {\r\n  border: 0;\r\n  height: 1px;\r\n  background-image: linear-gradient(\r\n    to right,\r\n    rgba(0, 0, 0, 0),\r\n    rgba(0, 0, 0, 0.75),\r\n    rgba(0, 0, 0, 0)\r\n  );\r\n}\r\n#checkbox1[_ngcontent-%COMP%], #checkbox2[_ngcontent-%COMP%] {\r\n  margin-top: 1%;\r\n  text-align: center;\r\n  padding-bottom: 10px;\r\n  padding-left: 10%;\r\n}\r\nbutton[_ngcontent-%COMP%] {\r\n  background-color: #023e7d;\r\n  color: white;\r\n  margin-left: 5%;\r\n  margin-bottom: 5%;\r\n  margin-right: 5%;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInRpZC1jZXJ0aWZpY2F0aW9uLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSx5QkFBeUI7RUFDekIsWUFBWTtBQUNkO0FBQ0E7RUFDRSxrQkFBa0I7RUFDbEIseUJBQXlCO0FBQzNCO0FBRUE7RUFDRSxtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLFdBQVc7RUFDWCx3Q0FBd0M7RUFDeEMsY0FBYztFQUNkLG9DQUFvQztFQUNwQyxjQUFjO0FBQ2hCO0FBQ0E7RUFDRSxZQUFZO0VBQ1osV0FBVztFQUNYLGtCQUFrQjtFQUNsQixxQkFBcUI7QUFDdkI7QUFDQTtFQUNFLGtCQUFrQjtFQUNsQixzQkFBc0I7O0VBRXRCLGFBQWE7RUFDYix5QkFBeUI7RUFDekIsWUFBWTtFQUNaLHlDQUF5QztBQUMzQztBQUNBO0VBQ0Usa0JBQWtCO0VBQ2xCLHNCQUFzQjtBQUN4QjtBQUNBO0VBQ0UsaUJBQWlCO0FBQ25CO0FBQ0E7RUFDRSx1QkFBdUI7QUFDekI7QUFDQTtFQUNFLGlCQUFpQjtBQUNuQjtBQUNBO0VBQ0UsdUJBQXVCO0FBQ3pCO0FBQ0E7RUFDRSx1QkFBdUI7QUFDekI7QUFDQTtFQUNFLFNBQVM7RUFDVCxXQUFXO0VBQ1g7Ozs7O0dBS0M7QUFDSDtBQUNBOztFQUVFLGNBQWM7RUFDZCxrQkFBa0I7RUFDbEIsb0JBQW9CO0VBQ3BCLGlCQUFpQjtBQUNuQjtBQUNBO0VBQ0UseUJBQXlCO0VBQ3pCLFlBQVk7RUFDWixlQUFlO0VBQ2YsaUJBQWlCO0VBQ2pCLGdCQUFnQjtBQUNsQiIsImZpbGUiOiJ0aWQtY2VydGlmaWNhdGlvbi5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNhcmQtaGVhZGVyIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDMxZDQ0O1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxufVxyXG4uY2FyZCB7XHJcbiAgcGFkZGluZy1ib3R0b206IDUlO1xyXG4gIGJvcmRlcjogc29saWQgMnB4ICMwNDM5NWU7XHJcbn1cclxuXHJcbnRhYmxlIHtcclxuICB0YWJsZS1sYXlvdXQ6IGZpeGVkO1xyXG4gIG1hcmdpbjogMCAwIDQwcHggMDtcclxuICB3aWR0aDogMTAwJTtcclxuICBib3gtc2hhZG93OiAwIDFweCAzcHggcmdiYSgwLCAwLCAwLCAwLjIpO1xyXG4gIGRpc3BsYXk6IHRhYmxlO1xyXG4gIEBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDU4MHB4KTtcclxuICBkaXNwbGF5OiBibG9jaztcclxufVxyXG4uZG90IHtcclxuICBoZWlnaHQ6IDI1cHg7XHJcbiAgd2lkdGg6IDI1cHg7XHJcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxufVxyXG50aCB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XHJcblxyXG4gIHdpZHRoOiAxNC4yOCU7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzAzMWQ0NDtcclxuICBjb2xvcjogd2hpdGU7XHJcbiAgZm9udC1mYW1pbHk6IEFyaWFsLCBIZWx2ZXRpY2EsIHNhbnMtc2VyaWY7XHJcbn1cclxudGQge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xyXG59XHJcbmlucHV0IHtcclxuICBib3JkZXI6IHNvbGlkIDJweDtcclxufVxyXG5pbnB1dDpmb2N1cyB7XHJcbiAgYm9yZGVyOiA1cHggc29saWQgZ3JlZW47XHJcbn1cclxuc2VsZWN0IHtcclxuICBib3JkZXI6IHNvbGlkIDJweDtcclxufVxyXG5zZWxlY3Q6Zm9jdXMge1xyXG4gIGJvcmRlcjogNXB4IHNvbGlkIGdyZWVuO1xyXG59XHJcbmNoZWNrYm94OmZvY3VzIHtcclxuICBib3JkZXI6IDVweCBzb2xpZCBncmVlbjtcclxufVxyXG5ociB7XHJcbiAgYm9yZGVyOiAwO1xyXG4gIGhlaWdodDogMXB4O1xyXG4gIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudChcclxuICAgIHRvIHJpZ2h0LFxyXG4gICAgcmdiYSgwLCAwLCAwLCAwKSxcclxuICAgIHJnYmEoMCwgMCwgMCwgMC43NSksXHJcbiAgICByZ2JhKDAsIDAsIDAsIDApXHJcbiAgKTtcclxufVxyXG4jY2hlY2tib3gxLFxyXG4jY2hlY2tib3gyIHtcclxuICBtYXJnaW4tdG9wOiAxJTtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgcGFkZGluZy1ib3R0b206IDEwcHg7XHJcbiAgcGFkZGluZy1sZWZ0OiAxMCU7XHJcbn1cclxuYnV0dG9uIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDIzZTdkO1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICBtYXJnaW4tbGVmdDogNSU7XHJcbiAgbWFyZ2luLWJvdHRvbTogNSU7XHJcbiAgbWFyZ2luLXJpZ2h0OiA1JTtcclxufVxyXG4iXX0= */"]
      });
      /***/
    },

    /***/
    "OcXF":
    /*!*********************************************************!*\
      !*** ./src/app/pages/imagelookup/imagelookup.module.ts ***!
      \*********************************************************/

    /*! exports provided: ImagelookupModule */

    /***/
    function OcXF(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ImagelookupModule", function () {
        return ImagelookupModule;
      });
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _alfresco_lookup_alfresco_lookup_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./alfresco-lookup/alfresco-lookup.component */
      "x9+C");
      /* harmony import */


      var _image_lookup_routing__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./image-lookup-routing */
      "+ngi");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var ImagelookupModule = function ImagelookupModule() {
        _classCallCheck(this, ImagelookupModule);
      };

      ImagelookupModule.ɵfac = function ImagelookupModule_Factory(t) {
        return new (t || ImagelookupModule)();
      };

      ImagelookupModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineNgModule"]({
        type: ImagelookupModule
      });
      ImagelookupModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineInjector"]({
        imports: [[_angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"], _image_lookup_routing__WEBPACK_IMPORTED_MODULE_2__["ImageLookupRouting"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"]]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵsetNgModuleScope"](ImagelookupModule, {
          declarations: [_alfresco_lookup_alfresco_lookup_component__WEBPACK_IMPORTED_MODULE_1__["AlfrescoLookupComponent"]],
          imports: [_angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"], _image_lookup_routing__WEBPACK_IMPORTED_MODULE_2__["ImageLookupRouting"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"]]
        });
      })();
      /***/

    },

    /***/
    "PCNd":
    /*!*****************************************!*\
      !*** ./src/app/shared/shared.module.ts ***!
      \*****************************************/

    /*! exports provided: SharedModule */

    /***/
    function PCNd(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SharedModule", function () {
        return SharedModule;
      });
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _header_header_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./header/header.component */
      "320Y");
      /* harmony import */


      var _sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./sidebar/sidebar.component */
      "sRhs");
      /* harmony import */


      var _footer_footer_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./footer/footer.component */
      "jQpT");
      /* harmony import */


      var _nopagefound_nopagefound_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./nopagefound/nopagefound.component */
      "3OKj");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var SharedModule = function SharedModule() {
        _classCallCheck(this, SharedModule);
      };

      SharedModule.ɵfac = function SharedModule_Factory(t) {
        return new (t || SharedModule)();
      };

      SharedModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdefineNgModule"]({
        type: SharedModule
      });
      SharedModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdefineInjector"]({
        imports: [[_angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵsetNgModuleScope"](SharedModule, {
          declarations: [_nopagefound_nopagefound_component__WEBPACK_IMPORTED_MODULE_5__["NopagefoundComponent"], _header_header_component__WEBPACK_IMPORTED_MODULE_2__["HeaderComponent"], _sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_3__["SidebarComponent"], _footer_footer_component__WEBPACK_IMPORTED_MODULE_4__["FooterComponent"]],
          imports: [_angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
          exports: [_nopagefound_nopagefound_component__WEBPACK_IMPORTED_MODULE_5__["NopagefoundComponent"], _header_header_component__WEBPACK_IMPORTED_MODULE_2__["HeaderComponent"], _sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_3__["SidebarComponent"], _footer_footer_component__WEBPACK_IMPORTED_MODULE_4__["FooterComponent"]]
        });
      })();
      /***/

    },

    /***/
    "Qxtq":
    /*!****************************************!*\
      !*** ./src/app/models/app-settings.ts ***!
      \****************************************/

    /*! exports provided: AppSettings */

    /***/
    function Qxtq(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AppSettings", function () {
        return AppSettings;
      });

      var AppSettings = function AppSettings() {
        _classCallCheck(this, AppSettings);
      }; // public static API_ENDPOINT =
      //   "https://usnusmimtwebs01.na.imtn.com/BNYM_TrackingToolAPI/api";
      // public static API_ENDPOINT =
      //   "https://USKANMIMTWEBR01.na.imtn.com/BNYM_TrackingToolAPI/api";


      AppSettings.API_ENDPOINT = "http://kcmduq04.na.imtn.com/BNYM_TrackingToolAPI/api";
      /***/
    },

    /***/
    "RUEf":
    /*!*******************************!*\
      !*** ./src/app/app.routes.ts ***!
      \*******************************/

    /*! exports provided: APP_ROUTES */

    /***/
    function RUEf(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "APP_ROUTES", function () {
        return APP_ROUTES;
      });
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _login_login_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./login/login.component */
      "vtpD");
      /* harmony import */


      var _pages_pages_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./pages/pages.component */
      "8D7W");
      /* harmony import */


      var _shared_nopagefound_nopagefound_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./shared/nopagefound/nopagefound.component */
      "3OKj");
      /* harmony import */


      var _pages_pages_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./pages/pages.module */
      "dgmN");
      /* harmony import */


      var _services_guards_login_guard_guard__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./services/guards/login-guard.guard */
      "ITgV");

      var routes = [{
        path: 'login',
        component: _login_login_component__WEBPACK_IMPORTED_MODULE_1__["LoginComponent"]
      }, {
        path: '',
        component: _pages_pages_component__WEBPACK_IMPORTED_MODULE_2__["PagesComponent"],
        canActivate: [_services_guards_login_guard_guard__WEBPACK_IMPORTED_MODULE_5__["LoginGuardGuard"]],
        loadChildren: function loadChildren() {
          return _pages_pages_module__WEBPACK_IMPORTED_MODULE_4__["PagesModule"];
        }
      }, {
        path: '**',
        component: _shared_nopagefound_nopagefound_component__WEBPACK_IMPORTED_MODULE_3__["NopagefoundComponent"]
      }];

      var APP_ROUTES = _angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"].forRoot(routes, {
        useHash: false,
        relativeLinkResolution: 'legacy'
      });
      /***/

    },

    /***/
    "SagX":
    /*!*****************************************************!*\
      !*** ./src/app/services/packages/client.service.ts ***!
      \*****************************************************/

    /*! exports provided: ClientService */

    /***/
    function SagX(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ClientService", function () {
        return ClientService;
      });
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");
      /* harmony import */


      var _shared_http_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../shared/http-rest.service */
      "vulr");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var ClientService = /*#__PURE__*/function (_shared_http_rest_ser4) {
        _inherits(ClientService, _shared_http_rest_ser4);

        var _super4 = _createSuper(ClientService);

        function ClientService(http, router) {
          var _this34;

          _classCallCheck(this, ClientService);

          _this34 = _super4.call(this, http);
          _this34.router = router;
          return _this34;
        }

        _createClass(ClientService, [{
          key: "getAllClients",
          value: function getAllClients() {
            return this.findAll("client", "FindAll");
          }
        }]);

        return ClientService;
      }(_shared_http_rest_service__WEBPACK_IMPORTED_MODULE_2__["HttpRestService"]);

      ClientService.ɵfac = function ClientService_Factory(t) {
        return new (t || ClientService)(_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵinject"](_angular_router__WEBPACK_IMPORTED_MODULE_0__["Router"]));
      };

      ClientService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineInjectable"]({
        token: ClientService,
        factory: ClientService.ɵfac,
        providedIn: 'root'
      });
      /***/
    },

    /***/
    "Sy1n":
    /*!**********************************!*\
      !*** ./src/app/app.component.ts ***!
      \**********************************/

    /*! exports provided: AppComponent */

    /***/
    function Sy1n(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AppComponent", function () {
        return AppComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");

      var AppComponent = function AppComponent() {
        _classCallCheck(this, AppComponent);

        this.title = 'Template';
      };

      AppComponent.ɵfac = function AppComponent_Factory(t) {
        return new (t || AppComponent)();
      };

      AppComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: AppComponent,
        selectors: [["app-root"]],
        decls: 2,
        vars: 0,
        consts: [["id", "wrapper"]],
        template: function AppComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "router-outlet");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }
        },
        directives: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterOutlet"]],
        styles: [".fade-scale[_ngcontent-%COMP%] {\r\n  transform: scale(0);\r\n  opacity: 0;\r\n  transition: all 0.25s linear;\r\n}\r\n\r\n.fade-scale.in[_ngcontent-%COMP%] {\r\n  opacity: 1;\r\n  transform: scale(1);\r\n}\r\n\r\n.fade-scale-custom[_ngcontent-%COMP%] {\r\n  transform: scale(0.9);\r\n  opacity: 0;\r\n  transition: all 0.1s;\r\n}\r\n\r\n.fade-scale-custom.in[_ngcontent-%COMP%] {\r\n  transform: scale(1);\r\n  opacity: 1;\r\n}\r\n\r\n.md-effect-1[_ngcontent-%COMP%]   .md-content[_ngcontent-%COMP%] {\r\n  transform: scale(0.7);\r\n  opacity: 0;\r\n  transition: all 0.3s;\r\n}\r\n\r\n.md-show.md-effect-1[_ngcontent-%COMP%]   .md-content[_ngcontent-%COMP%] {\r\n  transform: scale(1);\r\n  opacity: 1;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsbUJBQW1CO0VBQ25CLFVBQVU7RUFHViw0QkFBNEI7QUFDOUI7O0FBRUE7RUFDRSxVQUFVO0VBQ1YsbUJBQW1CO0FBQ3JCOztBQUdBO0VBSUUscUJBQXFCO0VBQ3JCLFVBQVU7RUFHVixvQkFBb0I7QUFDdEI7O0FBRUE7RUFJRSxtQkFBbUI7RUFDbkIsVUFBVTtBQUNaOztBQUVBO0VBSUUscUJBQXFCO0VBQ3JCLFVBQVU7RUFHVixvQkFBb0I7QUFDdEI7O0FBRUE7RUFJRSxtQkFBbUI7RUFDbkIsVUFBVTtBQUNaIiwiZmlsZSI6ImFwcC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmZhZGUtc2NhbGUge1xyXG4gIHRyYW5zZm9ybTogc2NhbGUoMCk7XHJcbiAgb3BhY2l0eTogMDtcclxuICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAwLjI1cyBsaW5lYXI7XHJcbiAgLW8tdHJhbnNpdGlvbjogYWxsIDAuMjVzIGxpbmVhcjtcclxuICB0cmFuc2l0aW9uOiBhbGwgMC4yNXMgbGluZWFyO1xyXG59XHJcblxyXG4uZmFkZS1zY2FsZS5pbiB7XHJcbiAgb3BhY2l0eTogMTtcclxuICB0cmFuc2Zvcm06IHNjYWxlKDEpO1xyXG59XHJcbkBpbXBvcnQgXCJ+QGFuZ3VsYXIvbWF0ZXJpYWwvcHJlYnVpbHQtdGhlbWVzL3B1cnBsZS1ncmVlbi5jc3NcIjtcclxuXHJcbi5mYWRlLXNjYWxlLWN1c3RvbSB7XHJcbiAgLXdlYmtpdC10cmFuc2Zvcm06IHNjYWxlKDAuOSk7XHJcbiAgLW1vei10cmFuc2Zvcm06IHNjYWxlKDAuOSk7XHJcbiAgLW1zLXRyYW5zZm9ybTogc2NhbGUoMC45KTtcclxuICB0cmFuc2Zvcm06IHNjYWxlKDAuOSk7XHJcbiAgb3BhY2l0eTogMDtcclxuICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAwLjFzO1xyXG4gIC1tb3otdHJhbnNpdGlvbjogYWxsIDAuMXM7XHJcbiAgdHJhbnNpdGlvbjogYWxsIDAuMXM7XHJcbn1cclxuXHJcbi5mYWRlLXNjYWxlLWN1c3RvbS5pbiB7XHJcbiAgLXdlYmtpdC10cmFuc2Zvcm06IHNjYWxlKDEpO1xyXG4gIC1tb3otdHJhbnNmb3JtOiBzY2FsZSgxKTtcclxuICAtbXMtdHJhbnNmb3JtOiBzY2FsZSgxKTtcclxuICB0cmFuc2Zvcm06IHNjYWxlKDEpO1xyXG4gIG9wYWNpdHk6IDE7XHJcbn1cclxuXHJcbi5tZC1lZmZlY3QtMSAubWQtY29udGVudCB7XHJcbiAgLXdlYmtpdC10cmFuc2Zvcm06IHNjYWxlKDAuNyk7XHJcbiAgLW1vei10cmFuc2Zvcm06IHNjYWxlKDAuNyk7XHJcbiAgLW1zLXRyYW5zZm9ybTogc2NhbGUoMC43KTtcclxuICB0cmFuc2Zvcm06IHNjYWxlKDAuNyk7XHJcbiAgb3BhY2l0eTogMDtcclxuICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAwLjNzO1xyXG4gIC1tb3otdHJhbnNpdGlvbjogYWxsIDAuM3M7XHJcbiAgdHJhbnNpdGlvbjogYWxsIDAuM3M7XHJcbn1cclxuXHJcbi5tZC1zaG93Lm1kLWVmZmVjdC0xIC5tZC1jb250ZW50IHtcclxuICAtd2Via2l0LXRyYW5zZm9ybTogc2NhbGUoMSk7XHJcbiAgLW1vei10cmFuc2Zvcm06IHNjYWxlKDEpO1xyXG4gIC1tcy10cmFuc2Zvcm06IHNjYWxlKDEpO1xyXG4gIHRyYW5zZm9ybTogc2NhbGUoMSk7XHJcbiAgb3BhY2l0eTogMTtcclxufVxyXG4iXX0= */"]
      });
      /***/
    },

    /***/
    "VAPG":
    /*!*********************************************!*\
      !*** ./src/app/models/audit-trail.model.ts ***!
      \*********************************************/

    /*! exports provided: AuditTrail */

    /***/
    function VAPG(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AuditTrail", function () {
        return AuditTrail;
      });

      var AuditTrail = function AuditTrail() {
        _classCallCheck(this, AuditTrail);
      };
      /***/

    },

    /***/
    "VZ5Q":
    /*!***************************************************************************!*\
      !*** ./src/app/pages/tid-creation/generate-tid/generate-tid.component.ts ***!
      \***************************************************************************/

    /*! exports provided: GenerateTidComponent */

    /***/
    function VZ5Q(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "GenerateTidComponent", function () {
        return GenerateTidComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var src_app_services_packages_add_packages_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! src/app/services/packages/add-packages.service */
      "wlS1");
      /* harmony import */


      var src_app_services_coversheet_create_coversheet_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! src/app/services/coversheet/create-coversheet.service */
      "44oB");
      /* harmony import */


      var src_app_services_tid_tidcreartion_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/app/services/tid/tidcreartion-service.service */
      "eHNn");
      /* harmony import */


      var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! sweetalert2/dist/sweetalert2.js */
      "PdH4");
      /* harmony import */


      var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4__);
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var src_app_models_audit_trail_model__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! src/app/models/audit-trail.model */
      "VAPG");
      /* harmony import */


      var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @ng-bootstrap/ng-bootstrap */
      "1kSV");
      /* harmony import */


      var src_app_services_packages_audit_trail_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! src/app/services/packages/audit-trail.service */
      "irGt");
      /* harmony import */


      var _cameracomponent_cameracomponent_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! ../cameracomponent/cameracomponent.component */
      "+II3");
      /* harmony import */


      var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! @angular/platform-browser */
      "jhN1");
      /* harmony import */


      var _image_display_image_display_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! ../image-display/image-display.component */
      "ev3G");
      /* harmony import */


      var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
      /*! @angular/material/dialog */
      "0IaG");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
      /*! rxjs/operators */
      "kU1M");
      /* harmony import */


      var _dialogComponents_lender_dialog_lender_dialog_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
      /*! ../dialogComponents/lender-dialog/lender-dialog.component */
      "Bd4x");
      /* harmony import */


      var src_app_services_tid_lender_names_service__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(
      /*! src/app/services/tid/lender-names.service */
      "egE0");
      /* harmony import */


      var _dialogComponents_loan_dialog_loan_dialog_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(
      /*! ../dialogComponents/loan-dialog/loan-dialog.component */
      "Hkzo");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_material_button__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(
      /*! @angular/material/button */
      "bTqV");
      /* harmony import */


      var _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(
      /*! @angular/material/progress-spinner */
      "Xa2L");
      /* harmony import */


      var _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(
      /*! @angular/material/autocomplete */
      "/1cH");
      /* harmony import */


      var _angular_material_core__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(
      /*! @angular/material/core */
      "FKr1");
      /* harmony import */


      var _bailee_transformer_pipe__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(
      /*! ./bailee-transformer.pipe */
      "NlK3");

      var _c0 = ["mainSearch"];
      var _c1 = ["barcodeStickerSearch"];
      var _c2 = ["selectSearch"];
      var _c3 = ["notes"];
      var _c4 = ["txtLoanNumber"];
      var _c5 = ["txtBorrowerName"];
      var _c6 = ["txtExceptionBarcode"];
      var _c7 = ["uploadButton"];
      var _c8 = ["LenderNameSearch"];
      var _c9 = ["FunctionBaileeButton"];
      var _c10 = ["BaileeNameSearch"];

      function GenerateTidComponent_mat_spinner_6_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "mat-spinner", 55);
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("diameter", 20);
        }
      }

      function GenerateTidComponent_div_31_Template(rf, ctx) {
        if (rf & 1) {
          var _r15 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "input", 56);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function GenerateTidComponent_div_31_Template_input_ngModelChange_1_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r15);

            var ctx_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r14.multipleLendersPresent = $event;
          })("click", function GenerateTidComponent_div_31_Template_input_click_1_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r15);

            var ctx_r16 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r16.multipleLenders($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "label", 57);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Multiple Lenders Present");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx_r2.multipleLendersPresent);
        }
      }

      function GenerateTidComponent_div_33_Template(rf, ctx) {
        if (rf & 1) {
          var _r19 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 20);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "span", 58);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "button", 59);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function GenerateTidComponent_div_33_Template_button_click_2_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r19);

            var ctx_r18 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r18.retrieveImages();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, " Retrieve Images ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "span", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "button", 59, 60);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function GenerateTidComponent_div_33_Template_button_click_5_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r19);

            var ctx_r20 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r20.openCamera();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, " Take Pic ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function GenerateTidComponent_div_62_mat_option_8_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-option", 68);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var lender_r27 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", lender_r27);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", lender_r27, " ");
        }
      }

      function GenerateTidComponent_div_62_mat_option_18_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-option", 68);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var lender_r28 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", lender_r28);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", lender_r28, " ");
        }
      }

      function GenerateTidComponent_div_62_Template(rf, ctx) {
        if (rf & 1) {
          var _r30 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 33);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "label", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Bailee Name");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "input", 61, 62);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("keydown.enter", function GenerateTidComponent_div_62_Template_input_keydown_enter_4_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r30);

            var ctx_r29 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r29.onEnterBailee($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "mat-autocomplete", null, 63);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](8, GenerateTidComponent_div_62_mat_option_8_Template, 2, 2, "mat-option", 64);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](9, "async");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "br");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "label", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "Lender Name");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "input", 65, 66);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function GenerateTidComponent_div_62_Template_input_ngModelChange_14_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r30);

            var ctx_r31 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r31.elementTid.LenderName = $event;
          })("keydown.enter", function GenerateTidComponent_div_62_Template_input_keydown_enter_14_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r30);

            var ctx_r32 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r32.onEnterLender($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "mat-autocomplete", null, 67);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](18, GenerateTidComponent_div_62_mat_option_18_Template, 2, 2, "mat-option", 64);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](19, "async");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var _r22 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](7);

          var _r25 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](17);

          var ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("matAutocomplete", _r22)("formControl", ctx_r6.myControl2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](9, 8, ctx_r6.filteredOptionsBailee));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx_r6.elementTid.LenderName)("disabled", ctx_r6.disableLenderText)("matAutocomplete", _r25)("formControl", ctx_r6.myControl);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](19, 10, ctx_r6.filteredOptions));
        }
      }

      function GenerateTidComponent_span_68_span_3_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span", 71);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r33 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("Total# ", ctx_r33.loanTotal, "");
        }
      }

      function GenerateTidComponent_span_68_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span", 69);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, GenerateTidComponent_span_68_span_3_Template, 3, 1, "span", 70);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("Expected # ", ctx_r7.loansExpected, "");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r7.multipleLendersActive);
        }
      }

      function GenerateTidComponent_div_72_option_10_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "option", 84);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ex_r38 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngValue", ex_r38);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ex_r38, " ");
        }
      }

      function GenerateTidComponent_div_72_Template(rf, ctx) {
        if (rf & 1) {
          var _r40 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 72);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 73);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "input", 74, 75);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function GenerateTidComponent_div_72_Template_input_ngModelChange_4_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r40);

            var ctx_r39 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r39.excBarcode = $event;
          })("keydown.enter", function GenerateTidComponent_div_72_Template_input_keydown_enter_4_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r40);

            var ctx_r41 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r41.onExceptionReason();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "br");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 76);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "select", 77, 78);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("change", function GenerateTidComponent_div_72_Template_select_change_8_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r40);

            var ctx_r42 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r42.sendToNotesField($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](10, GenerateTidComponent_div_72_option_10_Template, 2, 2, "option", 79);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "br");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](12, "br");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "span", 80);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "Exception Notes");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](18, "br");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 81);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "input", 82, 83);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("keydown.enter", function GenerateTidComponent_div_72_Template_input_keydown_enter_20_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r40);

            var ctx_r43 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r43.onEnterNotes($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](22, "br");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx_r9.excBarcode);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r9.exceptionArray);
        }
      }

      function GenerateTidComponent_button_75_Template(rf, ctx) {
        if (rf & 1) {
          var _r46 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 85, 86);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function GenerateTidComponent_button_75_Template_button_click_0_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r46);

            var ctx_r45 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r45.newBaileeButton();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, " NEW LENDER ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function GenerateTidComponent_button_76_Template(rf, ctx) {
        if (rf & 1) {
          var _r48 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 85);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function GenerateTidComponent_button_76_Template_button_click_0_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r48);

            var ctx_r47 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r47.changeExpected();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " CHANGE COUNT ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function GenerateTidComponent_th_103_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "th", 87);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var column_r49 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](column_r49);
        }
      }

      var _c11 = function _c11(a0) {
        return {
          "table-danger": a0
        };
      };

      function GenerateTidComponent_tr_104_Template(rf, ctx) {
        if (rf & 1) {
          var _r53 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tr", 88);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "td");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "td");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "td");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](7, "baileeTransformer");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "td");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "td");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "td");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "td");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "td");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "button", 89);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function GenerateTidComponent_tr_104_Template_button_click_17_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r53);

            var row_r50 = ctx.$implicit;
            var i_r51 = ctx.index;

            var ctx_r52 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r52.deleteRecord(row_r50, i_r51);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "X");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var row_r50 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](10, _c11, row_r50.ExceptionNotes != "" && row_r50.ExceptionNotes != ""));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("TID-", row_r50.TID_ID, "");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](row_r50.BarcodeSticker);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](7, 8, row_r50.BaileeNumber));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](row_r50.LenderName);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](row_r50.LoanOnManifest);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](row_r50.ExceptionReason);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](row_r50.ExceptionNotes);
        }
      } //import { EditRemoveComponent } from '../manifest/edit-remove/edit-remove.component';


      var GenerateTidComponent = /*#__PURE__*/function () {
        function GenerateTidComponent(_packageService, _createTIDCS, _lenderNames, _tidService, router, modalService, auditService, _sanitizer, _dialog, vc, ngRef) {
          _classCallCheck(this, GenerateTidComponent);

          this._packageService = _packageService;
          this._createTIDCS = _createTIDCS;
          this._lenderNames = _lenderNames;
          this._tidService = _tidService;
          this.router = router;
          this.modalService = modalService;
          this.auditService = auditService;
          this._sanitizer = _sanitizer;
          this._dialog = _dialog;
          this.vc = vc;
          this.ngRef = ngRef; //autocomplete variables

          this.myControl = new _angular_forms__WEBPACK_IMPORTED_MODULE_13__["FormControl"]();
          this.myControl2 = new _angular_forms__WEBPACK_IMPORTED_MODULE_13__["FormControl"]();
          this.loanTotal = 0;
          this.loansExpected = 0; //table headers

          this.headers = ["TID", "Barcode Sticker", "Bailee #", "Lender Name", "Loan on Manifest", "Exception Reason", "Exception Notes", "Remove"];
          this.exceptionArray = [" ", "DND", "FAIL", "NIGO", "NOS", "REJECTS", "REROUTE", "RESEARCH", "RETURN TO CLIENT", "REVISIONS", "MISSING NOTE", "Proposed Change"]; //for printing coversheet
          //list

          this.cslist = [];
          this.cslistTID = [];
          this.tableCreationElements = [];
          this.disableLenderText = false;
          this["boolean"] = false;
          this.excBarcode = "";
          this.multipleLendersActive = false;
          this.newBaileeActive = false;
          this.TIDList = [];
          this.loanNumber = "";
          this.borrowerName = "";
          this.currentPackageLenderList = [];
          this.lenderNameList = [];
          this.baileeNameList = [];
        }

        _createClass(GenerateTidComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this35 = this;

            this.initPackages();
            this.retrieveAllLenderNames();
            this.retrieveAllBaileeNames();
            var usertemp;
            usertemp = JSON.parse(localStorage.getItem("user"));
            this.userlogged = JSON.parse(localStorage.getItem("user")); //console.log(JSON.parse(localStorage.getItem("user")));

            if (this.userlogged == undefined) {
              sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default()("Not an admin", "This user is not logged", "error");
              this.router.navigate(["/"]);
            } else {
              console.log("User logged: " + usertemp.UserLogin);
            }

            this.display = true; //activate AutoComplete

            this.filteredOptions = this.myControl.valueChanges.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_14__["startWith"])(""), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_14__["map"])(function (value) {
              return _this35._filter(value);
            }));
            this.filteredOptionsBailee = this.myControl2.valueChanges.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_14__["startWith"])(""), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_14__["map"])(function (value) {
              return _this35._filterBailee(value);
            }));
          }
        }, {
          key: "_filter",
          value: function _filter(value) {
            var filterValue = value.toLowerCase();
            return this.baileeNameList.filter(function (option) {
              return option.toLowerCase().includes(filterValue);
            });
          }
        }, {
          key: "_filterBailee",
          value: function _filterBailee(value) {
            var filterValue = value.toLowerCase();
            return this.lenderNameList.filter(function (option) {
              return option.toLowerCase().includes(filterValue);
            });
          }
        }, {
          key: "base64toBlob",
          value: function base64toBlob(base64Data, contentType) {
            contentType = contentType || "";
            var sliceSize = 1024;
            var byteCharacters = atob(base64Data);
            var bytesLength = byteCharacters.length;
            var slicesCount = Math.ceil(bytesLength / sliceSize);
            var byteArrays = new Array(slicesCount);

            for (var sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
              var begin = sliceIndex * sliceSize;
              var end = Math.min(begin + sliceSize, bytesLength);
              var bytes = new Array(end - begin);

              for (var offset = begin, i = 0; offset < end; ++i, ++offset) {
                bytes[i] = byteCharacters[offset].charCodeAt(0);
              }

              byteArrays[sliceIndex] = new Uint8Array(bytes);
            }

            return new Blob(byteArrays, {
              type: contentType
            });
          }
        }, {
          key: "upload",
          value: function upload(files) {
            var file = files.item(0);
            console.log("file", file);

            this._tidService.upload("tid", "UploadImage", file).subscribe(function (response) {
              //console.log("success",response);
              sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
                position: "top-end",
                type: "success",
                title: "Tid has been saved",
                showConfirmButton: true,
                timer: 800
              });
            }, function (error) {
              console.log("error", error);
            });
          }
        }, {
          key: "search",
          value: function search(event) {
            //console.log(event.target.value);
            this.loanTotal = 0;
            this.loansExpected = 0;
            this.myControl.enable();
            this.myControl2.enable();
            this.multipleLendersActive = false;
            this.multipleLendersPresent = false;
            this.myControl.setValue("");
            this.myControl2.setValue("");
            this.inputReader = event.target.value;
            this.inputReader = this.inputReader.toLowerCase();
            this.inputReader = this.inputReader.replace(/\s/g, "");
            console.log(this.inputReader);
            this.loanTotal = 0;
            this.loansExpected = 0;

            if (this.inputReader.startsWith("pid-")) {
              console.log("trying to print with pid");
              this.inputReader = this.inputReader.replace("pid-", "");
              this.findOneByPID(+this.inputReader); // this.secondSearch.nativeElement.value = '';
              //this.secondSearch.nativeElement.focus();
            } else if (this.inputReader != "") {
              this.findPackage(this.inputReader);
            }

            if (this.bailee) {
              this.BaileeNameSearch.nativeElement.focus();
              this.BaileeNameSearch.nativeElement.value = "";
            }
          }
        }, {
          key: "onCheckLenders",
          value: function onCheckLenders(e) {
            var _this36 = this;

            if (this.TIDList.length > 0) {
              e.preventDefault();
              this.bailee = true;
              sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default()("Warning", String('Remove Bailee TIDs before you can remove "bailee letter present" flag'), "error");
            } else {
              if (this.bailee == false) {
                this.bailee = true;
                this.multipleLendersActive = true;

                if (this.totalLenders < 1) {
                  this.totalLenders = 1;
                  this.packageFound.totalLenders = 1;
                  this.packageFound.BaileeLetter = true;

                  this._packageService.updateAndReturnPack(this.packageFound).subscribe(function (pack) {
                    _this36.bailee = pack.BaileeLetter;
                  });
                }

                setTimeout(function () {
                  _this36.BaileeNameSearch.nativeElement.focus();
                }, 400);
              } else {
                this.multipleLendersActive = false;
                this.bailee = false;
                this.packageFound.BaileeLetter = false;
                this.packageFound.totalLenders = 0;
                this.totalLenders = 0;

                this._packageService.updateAndReturnPack(this.packageFound).subscribe(function (pack) {
                  _this36.bailee = pack.BaileeLetter;
                  console.log("Lastlee bailee atached", pack.BaileeLetter);
                });
              }
            }
          }
        }, {
          key: "clickEvt",
          value: function clickEvt(e) {
            console.log(e);
          }
        }, {
          key: "loanNumberAction",
          value: function loanNumberAction(event) {
            this.elementTid.LoanNumber = event.target.value;
            console.log("Package Loan number: " + this.elementTid.LoanNumber);
            this.barcodeStickerSearch.nativeElement.focus();
            this.barcodeStickerSearch.nativeElement.value = "";
          }
        }, {
          key: "exceptionCheckboxHandler",
          value: function exceptionCheckboxHandler() {
            this.barcodeStickerSearch.nativeElement.focus();
          }
        }, {
          key: "checkCheckBoxvalue",
          value: function checkCheckBoxvalue(event) {
            if (event.target.value == "on" && this.loanManifest == false) {
              this.loanManifest = true;
              console.log(this.loanManifest);
            }
          }
        }, {
          key: "sendToNotesField",
          value: function sendToNotesField(event) {
            this.elementTid.ExceptionReason = this.selectSearch.nativeElement.value;
            this.notesInput.nativeElement.focus();
          }
        }, {
          key: "onEnterLoanNumber",
          value: function onEnterLoanNumber(event) {
            var _this37 = this;

            var value = event.target.value;

            if (value === "*LNM*") {
              if (this.loanManifest) {
                this.loanManifest = false;
                setTimeout(function (f) {
                  _this37.barcodeStickerSearch.nativeElement.value = "";

                  _this37.txtLoanNumber.nativeElement.focus();

                  _this37.txtLoanNumber.nativeElement.value = "";
                }, 50);
                return;
              } else {
                this.loanManifest = true;
                this.barcodeStickerSearch.nativeElement.focus();
                this.barcodeStickerSearch.nativeElement.value = "";
                return;
              }
            }

            if (value === "*MNB*") {
              if (this.checked) {
                this.checked = false;
                setTimeout(function (f) {
                  _this37.barcodeStickerSearch.nativeElement.value = "";

                  _this37.txtLoanNumber.nativeElement.focus();

                  _this37.txtLoanNumber.nativeElement.value = "";
                }, 50);

                if (this.loanManifest) {
                  this.loanManifest = false;
                } else {
                  this.loanManifest = true;
                }

                return;
              } else {
                this.checked = true;
                this.barcodeStickerSearch.nativeElement.focus();
                this.barcodeStickerSearch.nativeElement.value = "";

                if (this.loanManifest) {
                  this.loanManifest = false;
                } else {
                  this.loanManifest = true;
                }

                return;
              }
            }

            if (value === "*BLP*") {
              if (this.bailee) {
                this.bailee = false;
              } else {
                this.bailee = true;
              }

              this.barcodeStickerSearch.nativeElement.focus();
              this.barcodeStickerSearch.nativeElement.value = "";
              return;
            }

            if (value === "*EXC*") {
              if (this.exceptionNeeded) {
                this.exceptionNeeded = false;
              } else {
                this.exceptionNeeded = true;
              }

              this.barcodeStickerSearch.nativeElement.focus();
              this.barcodeStickerSearch.nativeElement.value = "";
              return;
            }

            if (value === "*CNC*") {
              this.initPackages();
              return;
            }

            if (!this.checked) {
              if (this.BaileeNameSearch.nativeElement.value == "") {
                setTimeout(function () {
                  sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default()("Required", "Bailee Name is required.", "error");
                }, 0);
                this.txtLoanNumber.nativeElement.focus();
              }

              if (this.loanNumber == "") {
                setTimeout(function () {
                  sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default()("Required", "Loan Number is required.", "error");
                }, 0);
                this.txtLoanNumber.nativeElement.focus();
              } else {
                this.txtBorrowerName.nativeElement.focus();
              }
            } else if (!this.loanManifest) {
              if (this.loanNumber == "") {
                setTimeout(function () {
                  sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default()("Required", "Loan Number is required.", "error");
                }, 0);
                this.txtLoanNumber.nativeElement.focus();
              } else {
                this.txtBorrowerName.nativeElement.focus();
              }
            }
          }
        }, {
          key: "onEnterBorrowerName",
          value: function onEnterBorrowerName(event) {
            if (this.borrowerName == "") {
              setTimeout(function () {
                sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default()("Required", "Borrower name is required.", "error");
              }, 0);
              this.txtBorrowerName.nativeElement.focus();
            } else {
              this.barcodeStickerSearch.nativeElement.focus();
            }
          }
        }, {
          key: "changeLoanTrue",
          value: function changeLoanTrue() {
            var _this38 = this;

            setTimeout(function () {
              _this38.loanNumber = "";
              _this38.borrowerName = "";
              _this38.txtLoanNumber.nativeElement.value = ""; //this.txtLoanNumber.nativeElement.focus();

              if (_this38.loanManifest) {
                _this38.barcodeStickerSearch.nativeElement.focus();
              } else {
                _this38.txtLoanNumber.nativeElement.focus();
              }
            }, 0);
          }
        }, {
          key: "onCheckManifest",
          value: function onCheckManifest() {
            var _this39 = this;

            setTimeout(function () {
              if (_this39.checked && !_this39.loanManifest) {
                _this39.loanManifest = true;
                console.log("setting check on true");

                _this39.barcodeStickerSearch.nativeElement.focus();
              } else {
                _this39.loanManifest = false;

                _this39.txtLoanNumber.nativeElement.focus();
              }

              _this39.loanNumber = "";
              _this39.borrowerName = "";
            }, 10);
          }
        }, {
          key: "ifException",
          value: function ifException(event) {
            var _this40 = this;

            // console.log("LLOAN ON MANIFEST" , this.loanManifest);
            var value = event.target.value;
            value = value.toString().toUpperCase();
            value = value.trim();

            if (value === "*LNM*") {
              if (this.loanManifest) {
                this.loanManifest = false;
                setTimeout(function (f) {
                  _this40.barcodeStickerSearch.nativeElement.value = "";

                  _this40.txtLoanNumber.nativeElement.focus();

                  _this40.txtLoanNumber.nativeElement.value = "";
                }, 50);
                return;
              } else {
                this.loanManifest = true;
                this.barcodeStickerSearch.nativeElement.focus();
                this.barcodeStickerSearch.nativeElement.value = "";
                return;
              }
            }

            if (value === "*MNB*") {
              if (this.checked) {
                this.checked = false;
                setTimeout(function (f) {
                  _this40.barcodeStickerSearch.nativeElement.value = "";

                  _this40.txtLoanNumber.nativeElement.focus();

                  _this40.txtLoanNumber.nativeElement.value = "";
                }, 50);

                if (this.loanManifest) {
                  this.loanManifest = false;
                } else {
                  this.loanManifest = true;
                }

                return;
              } else {
                this.checked = true;
                this.barcodeStickerSearch.nativeElement.focus();
                this.barcodeStickerSearch.nativeElement.value = "";

                if (this.loanManifest) {
                  this.loanManifest = false;
                } else {
                  this.loanManifest = true;
                }

                return;
              }
            }

            if (value === "*BLP*") {
              if (this.bailee) {
                this.bailee = false;
              } else {
                this.bailee = true;
              }

              this.barcodeStickerSearch.nativeElement.focus();
              this.barcodeStickerSearch.nativeElement.value = "";
              return;
            }

            if (value === "*EXC*") {
              if (this.exceptionNeeded) {
                this.exceptionNeeded = false;
              } else {
                this.exceptionNeeded = true;
              }

              this.barcodeStickerSearch.nativeElement.focus();
              this.barcodeStickerSearch.nativeElement.value = "";
              return;
            }

            if (value === "*CNC*") {
              this.initPackages();
              return;
            }

            if (value === "*PNT*") {
              this.barcodeStickerSearch.nativeElement.focus();
              this.barcodeStickerSearch.nativeElement.value = "";
              this.onClickMe();
              return;
            }

            if (value === "*SVP*") {
              this.printAndSavePackage();
              return;
            }

            if (this.packageFound.Package_Tracking_ID == "") {
              sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default()("Search Error", "Please search a package using PID- or Tracking #.", "error");
              this.mainSearch.nativeElement.focus();
              return;
            } else {
              if (!this.exceptionNeeded) {
                this.elementTid = {
                  Package_Tracking_BC: this.packageFound.Package_Tracking_BC,
                  Package_Tracking_ID: this.packageFound.Package_Tracking_ID,
                  TID_ID: 1,
                  SRC_SYSTM_CD: "",
                  DCMNT_BAR_CD: "",
                  BaileeName: this.baileeNameHolder,
                  DCMNT_TYP_CD: "",
                  BaileeNumber: "",
                  LenderName: "",
                  COLLATERAL_KEY: "",
                  COLLATERAL_BAR_CODE: "",
                  POOL_SAK: "",
                  ACCOUNT_SAK: "",
                  CUSTOMER_CODE: "",
                  CUSTOMER_NAME: "",
                  CNTRLLER_NUM: "",
                  TRACK_LOCATION_CODE: "",
                  ScanDate: null,
                  DCMNT_CNDTN_CD: "",
                  FILE_NAME: "",
                  BaileeLetter: false,
                  LoanNumber: this.loanNumber,
                  BarcodeSticker: this.elementTid.BarcodeSticker,
                  LoanOnManifest: this.loanManifest,
                  Certification: this.elementTid.Certification,
                  ExceptionNotes: this.elementTid.ExceptionNotes,
                  ExceptionReason: this.elementTid.ExceptionReason,
                  UserId: this.userlogged.User_Name,
                  BorrowerName: this.borrowerName,
                  Certified_In: ""
                };

                if (!this.loanManifest || !this.checked) {
                  if (this.loanNumber == "") {
                    setTimeout(function () {
                      sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default()("Required", "Loan Number is required.", "error");
                    }, 0);
                    this.txtLoanNumber.nativeElement.focus();
                    return;
                  }

                  if (this.BaileeNameSearch.nativeElement.value == "") {
                    setTimeout(function () {
                      sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default()("Required", "Bailee Name is required.", "error");
                    }, 0);
                    this.BaileeNameSearch.nativeElement.focus();
                  }

                  if (this.borrowerName == "") {
                    setTimeout(function () {
                      sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default()("Required", "Loan Number is required.", "error");
                    }, 0);
                    this.txtBorrowerName.nativeElement.focus();
                    return;
                  }
                }

                this.elementTid.ExceptionNotes = "";
                this.elementTid.ExceptionReason = "";
                this.elementTid.BarcodeSticker = this.barcodeStickerSearch.nativeElement.value;
                this.elementTid.BarcodeSticker = this.elementTid.BarcodeSticker.toUpperCase();
                this.elementTid.BarcodeSticker = this.elementTid.BarcodeSticker.trim();

                if (this.multipleLendersActive) {
                  //check that we are not exceeding lenders
                  if (this.lenderCounter >= this.totalLenders && this.loansExpected > this.loanTotal) {
                    ////////////////////////////////////////////////////////////////////////////////////////////////
                    if (this.BaileeNameSearch.nativeElement.value != "" && this.lenderNameButton.nativeElement.value != "") {
                      this.elementTid.LenderName = this.myControl.value;
                      this.elementTid.BaileeName = this.myControl2.value;
                      var lenderTEMP = this.myControl.value;

                      if (!this.loanManifest) {
                        this.elementTid.BorrowerName = this.txtBorrowerName.nativeElement.value;
                        this.elementTid.LoanNumber = this.txtLoanNumber.nativeElement.value;
                      } else {
                        this.elementTid.BorrowerName = "";
                        this.txtBorrowerName.nativeElement.value = "";
                        this.elementTid.LoanNumber = "";
                        this.txtLoanNumber.nativeElement.value = "";
                      } //exceeding lenderCounter


                      if (this.TIDList.filter(function (e) {
                        return e.LenderName === lenderTEMP;
                      }).length > 0) {
                        this.elementTid.BaileeName = this.baileeNameHolder; //saving TID

                        this._tidService.saveOneReturnTID(this.elementTid).subscribe(function (data) {
                          _this40.tidRetured = data;
                          console.log(_this40.tidRetured);
                          _this40.elementTid.TID_ID = data.TID_ID;

                          if (_this40.bailee) {
                            _this40.loanTotal = _this40.loanTotal + 1;
                            _this40.packageFound.loanTotal = _this40.loanTotal;
                            console.log(_this40.packageFound.loanTotal, "loan total");

                            _this40._packageService.updateAndReturnPack(_this40.packageFound).subscribe(function (data) {
                              console.log(data);
                            });
                          }

                          _this40.TIDList.push(_this40.tidRetured);

                          _this40.totalItems = _this40.TIDList.length;
                          _this40.txtBorrowerName.nativeElement.value = "";
                          _this40.txtLoanNumber.nativeElement.value = "";

                          if (!_this40.checked && !_this40.loanManifest) {
                            _this40.loanNumber = "";
                            _this40.borrowerName = "";

                            _this40.txtLoanNumber.nativeElement.focus();
                          } else {
                            _this40.loanManifest = true;
                            _this40.checked = true;

                            _this40.barcodeStickerSearch.nativeElement.focus();
                          } // setTimeout(() => {
                          //   this.barcodeStickerSearch.nativeElement.focus();
                          //   this.barcodeStickerSearch.nativeElement.value = "";
                          // }, 200);


                          if (_this40.TIDList.filter(function (e) {
                            return e.LenderName === data.LenderName;
                          }).length > 0) {
                            /* vendors contains the element we're looking for */
                            console.log("Adding a lender that was previously in the package");
                          } else {
                            _this40.lenderCounter++;
                          }

                          if (_this40.loanTotal == _this40.loansExpected) {
                            var name = _this40.lenderNameButton.nativeElement.value;
                            var content = document.createElement("div");
                            content.innerHTML = " <strong>" + name + "</strong>";
                            setTimeout(function () {
                              _this40.barcodeStickerSearch.nativeElement.blur();
                            }, 800);
                            sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default()({
                              title: "Warning",
                              text: " Are you ready to print " + _this40.lenderNameButton.nativeElement.value + "  bailee cover sheet?",
                              type: "warning",
                              showCancelButton: true,
                              confirmButtonText: "Yes",
                              cancelButtonText: "No"
                            }).then(function (result) {
                              document.activeElement.blur();

                              if (result.value) {
                                //print coversheets
                                _this40.printAndSavePackage2(_this40.lenderNameButton.nativeElement.value);
                              } else if (result.dismiss === sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default.a.DismissReason.cancel) {
                                document.activeElement.blur();
                                setTimeout(function () {
                                  _this40.BaileeNameSearch.nativeElement.blur();

                                  document.activeElement.blur();
                                }, 1000); //present loan dropdown allowing them to reasign a newBaileeButton total expected

                                _this40.loansExpected = 0;

                                var dialogRef = _this40._dialog.open(_dialogComponents_loan_dialog_loan_dialog_component__WEBPACK_IMPORTED_MODULE_17__["LoanDialogComponent"], {
                                  panelClass: "custom-dialog-container",
                                  disableClose: true
                                });

                                dialogRef.afterClosed().subscribe(function (result) {
                                  if (parseInt(result) == _this40.loanTotal) {
                                    _this40.loansExpected = parseInt(result);
                                    _this40.packageFound.totalLenders = parseInt(result);
                                    _this40.packageFound.BaileeLetter = true;
                                    _this40.packageFound.multipleLenders = true;
                                    _this40.packageFound.loansExpected = _this40.loansExpected;

                                    _this40._packageService.updatePack(_this40.packageFound).subscribe(function (data) {
                                      console.log(data);
                                    });

                                    _this40.printAndSavePackage2(_this40.lenderNameButton.nativeElement.value);
                                  } else {
                                    //this.lendersRecieved = parseInt(result);
                                    _this40.loansExpected = parseInt(result);
                                    _this40.packageFound.totalLenders = parseInt(result);
                                    _this40.packageFound.BaileeLetter = true;
                                    _this40.packageFound.multipleLenders = true;
                                    _this40.packageFound.loansExpected = _this40.loansExpected;

                                    _this40._packageService.updatePack(_this40.packageFound).subscribe(function (data) {
                                      console.log(data);
                                    });

                                    console.log(_this40.loanTotal);
                                  }
                                });
                                _this40.barcodeStickerSearch.nativeElement.value = ""; // this.barcodeStickerSearch.nativeElement.focus();

                                return;
                              }
                            });
                          } // this.loanManifest = false;
                          //this.tableCreationElements.push(tableElement);

                        }, function (error) {
                          sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default()("Search Error", String(error.Message), "error");
                        });
                      } else {
                        //adding new lender
                        setTimeout(function () {
                          sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default()({
                            title: "Warning",
                            text: "Do you wish to add another lender to this package?",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonText: "Yes",
                            cancelButtonText: "No"
                          }).then(function (result) {
                            if (result.value) {
                              //this.currentPackageLenderList.push(this.lenderNameButton.nativeElement.value);
                              _this40.myControl.disable();

                              _this40.elementTid.LenderName = _this40.myControl.value;
                              _this40.lenderCounter++;
                              _this40.loanTotal = _this40.loanTotal + 1; //assign loan total to package object in db

                              _this40.packageFound.loanTotal = _this40.loanTotal;
                              _this40.packageFound.totalLenders = _this40.lenderCounter;

                              _this40._packageService.updateAndReturnPack(_this40.packageFound).subscribe(function (data) {
                                console.log(data);
                              });

                              console.log(_this40.packageFound.loanTotal, "loan total");

                              if (!_this40.loanManifest) {
                                _this40.elementTid.BorrowerName = _this40.txtBorrowerName.nativeElement.value;
                                _this40.elementTid.LoanNumber = _this40.txtLoanNumber.nativeElement.value;
                              } else {
                                _this40.elementTid.BorrowerName = "";
                                _this40.txtBorrowerName.nativeElement.value = "";
                                _this40.elementTid.LoanNumber = "";
                                _this40.txtLoanNumber.nativeElement.value = "";
                              }

                              console.log(_this40.packageFound.loanTotal, "package loan total");

                              _this40._packageService.updateAndReturnPack(_this40.packageFound).subscribe(function (data) {
                                console.log("package updated with new Lenders", data);
                                _this40.packageFound = data;
                                _this40.totalLenders = data.totalLenders;
                              });

                              _this40._tidService.saveOneReturnTID(_this40.elementTid).subscribe(function (data) {
                                _this40.tidRetured = data;
                                console.log(_this40.tidRetured);
                                _this40.elementTid.TID_ID = data.TID_ID;

                                if (_this40.TIDList.length >= 1) {
                                  _this40.multipleLendersPresent = true;
                                }

                                _this40.TIDList.push(_this40.tidRetured);

                                _this40.totalItems = _this40.TIDList.length;
                                _this40.txtBorrowerName.nativeElement.value = "";
                                _this40.txtLoanNumber.nativeElement.value = "";

                                if (_this40.TIDList.filter(function (e) {
                                  return e.LenderName === data.LenderName;
                                }).length > 0) {
                                  /* vendors contains the element we're looking for */
                                  console.log("Adding a lender that was previously in the package");
                                } else {
                                  _this40.lenderCounter++;
                                }

                                _this40.checked = true;
                                _this40.barcodeStickerSearch.nativeElement.value = "";

                                _this40.barcodeStickerSearch.nativeElement.focus(); // this.loanManifest = false;
                                //this.tableCreationElements.push(tableElement);

                              }, function (error) {
                                sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default()("Search Error", String(error.Message), "error");
                              });
                            } else if (result.dismiss === sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default.a.DismissReason.cancel) {
                              _this40.myControl.disable();

                              _this40.barcodeStickerSearch.nativeElement.value = ""; // this.barcodeStickerSearch.nativeElement.focus;

                              return;
                            }
                          });
                        }, 400);
                      }
                    } else {
                      if (this.BaileeNameSearch.nativeElement.value == "") {
                        setTimeout(function () {
                          sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default()("Required", "A bailee name and Bailee Name is required.", "error");
                        }, 300);
                      }

                      if (this.lenderNameButton.nativeElement.value == "") {
                        setTimeout(function () {
                          sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default()("Required", "A lender name and Bailee Name is required.", "error");
                        }, 300);
                      }
                    }
                  } else {
                    if (this.lenderNameButton.nativeElement.value != "" && this.BaileeNameSearch.nativeElement.value != "") {
                      this.elementTid.LenderName = this.lenderNameButton.nativeElement.value;
                      this.disableLenderText = true;
                      this.elementTid.BaileeName = this.myControl2.value;
                      this.myControl.disable(); //////////

                      this.myControl2.disable();

                      if (!this.loanManifest) {
                        this.elementTid.BorrowerName = this.txtBorrowerName.nativeElement.value;
                        this.elementTid.LoanNumber = this.txtLoanNumber.nativeElement.value;
                      } else {
                        this.elementTid.BorrowerName = "";
                        this.txtBorrowerName.nativeElement.value = "";
                        this.elementTid.LoanNumber = "";
                        this.txtLoanNumber.nativeElement.value = "";
                      } ////////////////////////////////////
                      ////////////////////////////////////
                      ////////////////////////////////////
                      ////////////////////////////////////
                      ////////////////////////////////////
                      ////////////////////////////////////


                      var saveOrNot = true;

                      if (this.loanTotal >= this.loansExpected && this.loanTotal != 0) {
                        setTimeout(function () {
                          _this40.BaileeNameSearch.nativeElement.blur();

                          document.activeElement.blur();
                          sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default()("Search Error", String("Cannot save above expected Loan count  "), "error");
                        }, 700);
                        setTimeout(function () {
                          _this40.BaileeNameSearch.nativeElement.focus();
                        }, 500);
                        saveOrNot = false;
                        this.BaileeNameSearch.nativeElement.focus();
                      } else if (saveOrNot && this.loansExpected != 0) {
                        this._tidService.saveOneReturnTID(this.elementTid).subscribe(function (data) {
                          _this40.tidRetured = data;
                          console.log(_this40.tidRetured);
                          _this40.elementTid.TID_ID = data.TID_ID;
                          _this40.barcodeStickerSearch.nativeElement.value = "";
                          _this40.totalItems++;
                          _this40.loanTotal = _this40.loanTotal + 1;
                          _this40.packageFound.loanTotal = _this40.loanTotal;
                          console.log(_this40.packageFound.loanTotal, "loan total");

                          _this40._packageService.updateAndReturnPack(_this40.packageFound).subscribe(function (data) {
                            console.log(data);
                          });

                          if (_this40.TIDList.filter(function (e) {
                            return e.LenderName === data.LenderName;
                          }).length > 0) {
                            /* vendors contains the element we're looking for */
                            console.log("Adding a lender that was previously in the package");
                          } else {
                            _this40.lenderCounter++;
                          }

                          sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
                            position: "top-end",
                            type: "success",
                            title: "Tid has been saved",
                            showConfirmButton: true,
                            timer: 800
                          });

                          _this40.TIDList.push(_this40.tidRetured); //this.TIDList.sort((a,b) => { return a.ScanDate > b.ScanDate ? -1 : b.ScanDate <  a.ScanDate ? 1 : 0 });
                          //this.TIDList.sort((a,b) => { return a.ExceptionReason == '' ? -1 : b.ExceptionReason == '' ? 1 : 0 });


                          _this40.TIDList.reverse;

                          if (!_this40.checked && !_this40.loanManifest) {
                            _this40.loanNumber = "";
                            _this40.borrowerName = "";

                            _this40.txtLoanNumber.nativeElement.focus();
                          } else {
                            _this40.loanManifest = true;
                            _this40.checked = true;

                            _this40.barcodeStickerSearch.nativeElement.focus();
                          } // setTimeout(() => {
                          //   this.barcodeStickerSearch.nativeElement.value = "";
                          //   this.barcodeStickerSearch.nativeElement.focus();
                          // }, 200);


                          console.log("PRINTING LOAN ON MANIFEST", _this40.loanManifest);
                          console.log("printing values ", _this40.txtLoanNumber.nativeElement.value, _this40.txtBorrowerName.nativeElement.value);

                          if (_this40.loanManifest) {
                            _this40.txtLoanNumber.nativeElement.value = "";
                            _this40.txtBorrowerName.nativeElement.value = "";
                          }

                          if (_this40.loanTotal == _this40.loansExpected) {
                            var name = _this40.lenderNameButton.nativeElement.value;
                            var content = document.createElement("div");
                            content.innerHTML = " <strong>" + name + "</strong>";
                            setTimeout(function () {
                              _this40.barcodeStickerSearch.nativeElement.blur();
                            }, 800);
                            document.activeElement.blur();
                            sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default()({
                              title: "Warning",
                              text: " Are you ready to print " + _this40.lenderNameButton.nativeElement.value + "  bailee cover sheet?",
                              type: "warning",
                              showCancelButton: true,
                              confirmButtonText: "Yes",
                              cancelButtonText: "No"
                            }).then(function (result) {
                              document.activeElement.blur();

                              if (result.value) {
                                //print coversheets
                                _this40.printAndSavePackage2(_this40.lenderNameButton.nativeElement.value);
                              } else if (result.dismiss === sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default.a.DismissReason.cancel) {
                                setTimeout(function () {
                                  _this40.BaileeNameSearch.nativeElement.blur();

                                  document.activeElement.blur();
                                }, 1000);
                                _this40.loansExpected = 0;

                                _this40.BaileeNameSearch.nativeElement.blur(); //present loan dropdown allowing them to reasign a new total expected


                                var dialogRef = _this40._dialog.open(_dialogComponents_loan_dialog_loan_dialog_component__WEBPACK_IMPORTED_MODULE_17__["LoanDialogComponent"], {
                                  panelClass: "custom-dialog-container",
                                  disableClose: true
                                });

                                dialogRef.afterClosed().subscribe(function (result) {
                                  //this.lendersRecieved = parseInt(result);
                                  _this40.loansExpected = parseInt(result);
                                  _this40.packageFound.totalLenders = parseInt(result);
                                  _this40.packageFound.BaileeLetter = true;
                                  _this40.packageFound.multipleLenders = true;
                                  _this40.packageFound.loansExpected = _this40.loansExpected;

                                  _this40._packageService.updatePack(_this40.packageFound).subscribe(function (data) {
                                    console.log(data);
                                  });

                                  console.log(_this40.loanTotal);
                                });
                                _this40.barcodeStickerSearch.nativeElement.value = ""; //this.barcodeStickerSearch.nativeElement.focus();

                                return;
                              }
                            });
                          }
                        }, function (error) {
                          sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default()("Search Error", String(error.Message), "error");
                        }); //assign loan total to package object in db


                        this.packageFound.loanTotal = this.loanTotal;
                        console.log(this.packageFound.loanTotal, "package loan total");

                        this._packageService.updateAndReturnPack(this.packageFound).subscribe(function (data) {
                          console.log(data);
                        });
                      } else {
                        var dialogRef = this._dialog.open(_dialogComponents_loan_dialog_loan_dialog_component__WEBPACK_IMPORTED_MODULE_17__["LoanDialogComponent"], {
                          panelClass: "custom-dialog-container",
                          disableClose: true
                        });

                        this.BaileeNameSearch.nativeElement.blur();
                        document.activeElement.blur();
                        var lessthantotal = false;
                        dialogRef.afterClosed().subscribe(function (result) {
                          //this.lendersRecieved = parseInt(result);
                          if (parseInt(result) < _this40.loanTotal) {
                            sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default()("Error", "Cannot have less expected items then current items", "error");

                            _this40.myControl.enable();

                            _this40.myControl.setValue("");

                            setTimeout(function () {
                              _this40.lenderNameButton.nativeElement.focus();
                            }, 1500);
                          } else if (parseInt(result) == _this40.loanTotal) {
                            _this40.loansExpected = parseInt(result);
                            _this40.packageFound.totalLenders = parseInt(result);
                            _this40.packageFound.BaileeLetter = true;
                            _this40.packageFound.multipleLenders = true;
                            _this40.packageFound.loansExpected = _this40.loansExpected;

                            _this40._packageService.updatePack(_this40.packageFound).subscribe(function (data) {
                              console.log(data);
                            });

                            _this40.printAndSavePackage2(_this40.lenderNameButton.nativeElement.value);
                          } else {
                            _this40.loansExpected = parseInt(result);
                            _this40.packageFound.totalLenders = parseInt(result);
                            _this40.packageFound.BaileeLetter = true;
                            _this40.packageFound.multipleLenders = true;
                            _this40.packageFound.loansExpected = _this40.loansExpected;

                            _this40._packageService.updatePack(_this40.packageFound).subscribe(function (data) {
                              console.log(data);
                            });

                            console.log(_this40.loanTotal);
                          }
                        });
                      } //about to save


                      this.barcodeStickerSearch.nativeElement.focus();
                    } else {
                      setTimeout(function () {
                        sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default()("Required", "A lender name is required.", "error");
                      }, 300);
                    }
                  }
                } else {
                  if (this.loanManifest) {
                    this.elementTid.LoanNumber = "";
                    this.elementTid.BorrowerName = "";
                  } else {
                    this.elementTid.LoanNumber = this.txtLoanNumber.nativeElement.value;
                    this.elementTid.BorrowerName = this.txtBorrowerName.nativeElement.value;
                  }

                  this._tidService.saveOneReturnTID(this.elementTid).subscribe(function (data) {
                    _this40.tidRetured = data;
                    console.log(_this40.tidRetured);
                    _this40.elementTid.TID_ID = data.TID_ID;
                    _this40.barcodeStickerSearch.nativeElement.value = "";
                    _this40.totalItems++;
                    _this40.audit = {
                      PID: _this40.packageFound.Package_Tracking_ID,
                      TID: "",
                      ActionName: "CHECK IN CREATE",
                      BusinessUnit: _this40.packageFound.Client_Name,
                      ActionDateTime: null,
                      Sticker: "",
                      User_ID: _this40.userlogged.User_Name
                    };
                    sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
                      position: "top-end",
                      type: "success",
                      title: "Tid has been saved",
                      showConfirmButton: true,
                      timer: 800
                    });

                    _this40.TIDList.push(_this40.tidRetured); //this.TIDList.sort((a,b) => { return a.ScanDate > b.ScanDate ? -1 : b.ScanDate <  a.ScanDate ? 1 : 0 });
                    //this.TIDList.sort((a,b) => { return a.ExceptionReason == '' ? -1 : b.ExceptionReason == '' ? 1 : 0 });


                    _this40.TIDList.reverse;
                    _this40.txtBorrowerName.nativeElement.value = "";
                    _this40.txtLoanNumber.nativeElement.value = "";

                    if (!_this40.checked && !_this40.loanManifest) {
                      _this40.loanNumber = "";
                      _this40.borrowerName = "";

                      _this40.txtLoanNumber.nativeElement.focus();
                    } else {
                      _this40.loanManifest = true;
                      _this40.checked = true;

                      _this40.barcodeStickerSearch.nativeElement.focus();
                    }

                    _this40.barcodeStickerSearch.nativeElement.value = ""; //               this.barcodeStickerSearch.nativeElement.focus();

                    console.log("PRINTING LOAN ON MANIFEST", _this40.loanManifest);
                    console.log("printing values ", _this40.txtLoanNumber.nativeElement.value, _this40.txtBorrowerName.nativeElement.value);

                    if (_this40.loanManifest) {
                      _this40.txtLoanNumber.nativeElement.value = "";
                      _this40.txtBorrowerName.nativeElement.value = "";
                    }
                  }, function (error) {
                    sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default()("Search Error", String(error.Message), "error");
                  });
                }
              } else {
                console.log("Exception field true");
                this.elementTid.BarcodeSticker = this.barcodeStickerSearch.nativeElement.value;
                this.elementTid.BarcodeSticker = this.elementTid.BarcodeSticker.toUpperCase();
                this.elementTid.BarcodeSticker = this.elementTid.BarcodeSticker.trim(); //this.elementTid.LenderName = this.lenderNameButton.nativeElement.value;

                this.txtExceptionBarcode.nativeElement.focus();
                this.txtExceptionBarcode.nativeElement.value = "";
                this.notesInput.nativeElement.value = "";
              }
            } // starts validations

          } //end mehtod

        }, {
          key: "onEnterNotes",
          value: function onEnterNotes(event) {
            var _this41 = this;

            console.log(event.target.value);
            this.elementTid.Package_Tracking_ID = this.packageFound.Package_Tracking_ID; //console.log("loan ON MANIFEST RECIEVED ", this.loanManifest);

            if (this.multipleLendersActive) {
              if (this.lenderCounter >= this.totalLenders) {
                this.elementTid.LenderName = this.myControl.value;
                var lenderTEMP = this.myControl.value;

                if (!this.loanManifest) {
                  this.elementTid.BorrowerName = this.txtBorrowerName.nativeElement.value;
                  this.elementTid.LoanNumber = this.txtLoanNumber.nativeElement.value;
                } else {
                  this.elementTid.BorrowerName = "";
                  this.txtBorrowerName.nativeElement.value = "";
                  this.elementTid.LoanNumber = "";
                  this.txtLoanNumber.nativeElement.value = "";
                }

                this.elementTid.ExceptionNotes = this.notesInput.nativeElement.value;
                this.elementTid.ExceptionReason = this.selectSearch.nativeElement.value;

                if (this.TIDList.filter(function (e) {
                  return e.LenderName === lenderTEMP;
                }).length > 0) {
                  //saving TID
                  this._tidService.saveOneReturnTID(this.elementTid).subscribe(function (data) {
                    _this41.tidRetured = data;
                    console.log(_this41.tidRetured);
                    _this41.elementTid.TID_ID = data.TID_ID;

                    _this41.TIDList.push(_this41.tidRetured);

                    _this41.loanTotal = _this41.loanTotal + 1;
                    _this41.packageFound.loanTotal = _this41.loanTotal;
                    console.log(_this41.packageFound.loanTotal, "loan total");

                    _this41._packageService.updateAndReturnPack(_this41.packageFound).subscribe(function (data) {
                      console.log(data);
                    });

                    _this41.totalItems = _this41.TIDList.length;
                    _this41.txtBorrowerName.nativeElement.value = "";
                    _this41.txtLoanNumber.nativeElement.value = "";
                    _this41.exceptionNeeded = false;
                    _this41.txtExceptionBarcode.nativeElement.value = "";
                    _this41.selectSearch.nativeElement.value = "";
                    _this41.notesInput.nativeElement.value = "";

                    if (!_this41.checked && !_this41.loanManifest) {
                      _this41.loanNumber = "";
                      _this41.borrowerName = "";

                      _this41.txtLoanNumber.nativeElement.focus();
                    } else {
                      _this41.loanManifest = true;
                      _this41.checked = true;

                      _this41.barcodeStickerSearch.nativeElement.focus();
                    }

                    if (_this41.TIDList.filter(function (e) {
                      return e.LenderName === data.LenderName;
                    }).length > 0) {
                      /* vendors contains the element we're looking for */
                      console.log("Adding a lender that was previously in the package");
                    } else {
                      _this41.lenderCounter++;
                    } //this.barcodeStickerSearch.nativeElement.focus();
                    // this.loanManifest = false;
                    //this.tableCreationElements.push(tableElement);

                  }, function (error) {
                    sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default()("Search Error", String(error.Message), "error");
                  });
                } else {
                  setTimeout(function () {
                    sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default()({
                      title: "Warning",
                      text: "Do you wish to add another lender to this package?",
                      type: "warning",
                      showCancelButton: true,
                      confirmButtonText: "Yes",
                      cancelButtonText: "No"
                    }).then(function (result) {
                      if (result.value) {
                        //this.currentPackageLenderList.push(this.lenderNameButton.nativeElement.value);
                        _this41.myControl.disable();

                        _this41.elementTid.LenderName = _this41.myControl.value;
                        _this41.lenderCounter++;
                        _this41.packageFound.totalLenders = _this41.lenderCounter;
                        _this41.elementTid.ExceptionNotes = _this41.notesInput.nativeElement.valunewBaileeButtone;
                        _this41.elementTid.ExceptionReason = _this41.selectSearch.nativeElement.value;

                        if (!_this41.loanManifest) {
                          _this41.elementTid.BorrowerName = _this41.txtBorrowerName.nativeElement.value;
                          _this41.elementTid.LoanNumber = _this41.txtLoanNumber.nativeElement.value;
                        } else {
                          _this41.elementTid.BorrowerName = "";
                          _this41.txtBorrowerName.nativeElement.value = "";
                          _this41.elementTid.LoanNumber = "";
                          _this41.txtLoanNumber.nativeElement.value = "";
                        }

                        _this41.loanTotal = _this41.loanTotal + 1; //assign loan total to package object in db

                        _this41.packageFound.loanTotal = _this41.loanTotal;
                        console.log(_this41.packageFound.loanTotal, "loan total");

                        _this41._packageService.updateAndReturnPack(_this41.packageFound).subscribe(function (data) {
                          console.log("package updated with new Lenders", data);
                          _this41.packageFound = data;
                          _this41.totalLenders = data.totalLenders;
                        });

                        _this41._tidService.saveOneReturnTID(_this41.elementTid).subscribe(function (data) {
                          _this41.tidRetured = data;
                          console.log(_this41.tidRetured);
                          _this41.elementTid.TID_ID = data.TID_ID;

                          _this41.TIDList.push(_this41.tidRetured);

                          _this41.totalItems = _this41.TIDList.length;
                          _this41.txtBorrowerName.nativeElement.value = "";
                          _this41.txtLoanNumber.nativeElement.value = "";
                          _this41.exceptionNeeded = false;
                          _this41.txtExceptionBarcode.nativeElement.value = "";
                          _this41.selectSearch.nativeElement.value = "";
                          _this41.notesInput.nativeElement.value = "";

                          if (_this41.TIDList.filter(function (e) {
                            return e.LenderName === data.LenderName;
                          }).length > 0) {
                            /* vendors contains the element we're looking for */
                            console.log("Adding a lender that was previously in the package");
                          } else {
                            _this41.lenderCounter++;
                          }

                          _this41.checked = true;
                          _this41.barcodeStickerSearch.nativeElement.value = "";
                          _this41.selectSearch.nativeElement.value = "";
                          _this41.txtExceptionBarcode.nativeElement = "";
                          _this41.notesInput.nativeElement.value = "";
                          setTimeout(function () {
                            _this41.exceptionNeeded = false;

                            _this41.barcodeStickerSearch.nativeElement.focus();
                          }, 100); // this.loanManifest = false;
                          //this.tableCreationElements.push(tableElement);
                        }, function (error) {
                          sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default()("Search Error", String(error.Message), "error");
                        });
                      } else if (result.dismiss === sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default.a.DismissReason.cancel) {
                        _this41.myControl.disable();

                        _this41.barcodeStickerSearch.nativeElement.value = "";
                        _this41.selectSearch.nativeElement.value = "";
                        _this41.txtExceptionBarcode.nativeElement = "";
                        _this41.notesInput.nativeElement.value = "";
                        setTimeout(function () {
                          _this41.exceptionNeeded = false;

                          _this41.barcodeStickerSearch.nativeElement.focus();
                        }, 100);
                        return;
                      }
                    });
                  }, 400);
                }
              } else {
                this.elementTid.BarcodeSticker = this.barcodeStickerSearch.nativeElement.value;
                this.elementTid.BarcodeSticker = this.elementTid.BarcodeSticker.toUpperCase();
                this.elementTid.ExceptionReason = this.selectSearch.nativeElement.value;
                this.elementTid.LenderName = this.lenderNameButton.nativeElement.value;
                this.elementTid.ExceptionNotes = event.target.value; //create TID to register

                this.elementTid = {
                  Package_Tracking_BC: this.packageFound.Package_Tracking_BC,
                  Package_Tracking_ID: this.packageFound.Package_Tracking_ID,
                  TID_ID: 1,
                  SRC_SYSTM_CD: "",
                  DCMNT_BAR_CD: "",
                  DCMNT_TYP_CD: "",
                  COLLATERAL_KEY: "",
                  COLLATERAL_BAR_CODE: "",
                  POOL_SAK: "",
                  ACCOUNT_SAK: "",
                  CUSTOMER_CODE: "",
                  CUSTOMER_NAME: "",
                  CNTRLLER_NUM: "",
                  TRACK_LOCATION_CODE: "",
                  BaileeNumber: "",
                  LenderName: this.lenderNameButton.nativeElement.value,
                  ScanDate: null,
                  DCMNT_CNDTN_CD: "",
                  FILE_NAME: "",
                  BaileeName: this.elementTid.BaileeName,
                  BaileeLetter: this.bailee,
                  LoanNumber: this.elementTid.LoanNumber,
                  BarcodeSticker: this.elementTid.BarcodeSticker,
                  LoanOnManifest: this.loanManifest,
                  Certification: this.elementTid.Certification,
                  ExceptionNotes: this.elementTid.ExceptionNotes,
                  ExceptionReason: this.elementTid.ExceptionReason,
                  UserId: this.userlogged.User_Name,
                  BorrowerName: this.borrowerName,
                  Certified_In: ""
                }; //GET CERTTIFIED FROM HTML

                if (!this.loanManifest || !this.checked) {
                  if (this.loanNumber == "") {
                    setTimeout(function () {
                      sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default()("Required", "Loan Number is required.", "error");
                    }, 0);
                    this.txtLoanNumber.nativeElement.focus();
                    return;
                  }

                  if (this.borrowerName == "") {
                    setTimeout(function () {
                      sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default()("Required", "Loan Number is required.", "error");
                    }, 0);
                    this.txtBorrowerName.nativeElement.focus();
                    return;
                  }
                }

                if (!this.loanManifest) {
                  this.elementTid.BorrowerName = this.txtBorrowerName.nativeElement.value;
                  this.elementTid.LoanNumber = this.txtLoanNumber.nativeElement.value;
                } else {
                  this.elementTid.BorrowerName = "";
                  this.txtBorrowerName.nativeElement.value = "";
                  this.elementTid.LoanNumber = "";
                  this.txtLoanNumber.nativeElement.value = "";
                }

                this._tidService.saveOneReturnTID(this.elementTid).subscribe(function (data) {
                  _this41.tidRetured = data; //console.log(this.tidRetured;

                  _this41.loanTotal = _this41.loanTotal + 1; //assign loan total to package object in db

                  _this41.packageFound.loanTotal = _this41.loanTotal;
                  console.log(_this41.packageFound.loanTotal, "loan total");

                  _this41._packageService.updateAndReturnPack(_this41.packageFound).subscribe(function (data) {
                    console.log("package updated with new Lenders", data);
                    _this41.packageFound = data;
                    _this41.totalLenders = data.totalLenders;
                  });

                  _this41.elementTid.TID_ID = data.TID_ID; //RegisterAudit

                  _this41.audit = {
                    PID: _this41.packageFound.Package_Tracking_ID,
                    TID: "",
                    ActionName: "CHECK IN CREATE",
                    BusinessUnit: _this41.packageFound.Client_Name,
                    ActionDateTime: null,
                    Sticker: "",
                    User_ID: _this41.userlogged.User_Name
                  };

                  if (_this41.TIDList.filter(function (e) {
                    return e.LenderName === data.LenderName;
                  }).length > 0) {
                    /* vendors contains the element we're looking for */
                    console.log("Adding a lender that was previously in the package");
                  } else {
                    _this41.lenderCounter++;
                  }

                  sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
                    position: "top-end",
                    type: "success",
                    title: "Tid has been saved",
                    showConfirmButton: true,
                    timer: 800
                  });

                  _this41.TIDList.push(_this41.tidRetured);

                  _this41.totalItems++; //this.TIDList.sort((a,b) => { return a.ScanDate > b.ScanDate ? -1 : b.ScanDate <  a.ScanDate ? 1 : 0 });
                  //this.TIDList.sort((a,b) => { return a.ExceptionReason == '' ? -1 : b.ExceptionReason == '' ? 1 : 0 });

                  _this41.TIDList.reverse;

                  if (!_this41.checked && !_this41.loanManifest) {
                    _this41.loanNumber = "";
                    _this41.borrowerName = "";

                    _this41.txtLoanNumber.nativeElement.focus();
                  } else {
                    _this41.loanManifest = true;
                    _this41.checked = true;

                    _this41.barcodeStickerSearch.nativeElement.focus();
                  }

                  _this41.barcodeStickerSearch.nativeElement.value = "";
                  console.log("PRINTING LOAN ON MANIFEST", _this41.loanManifest);
                  console.log("printing values ", _this41.txtLoanNumber.nativeElement.value, _this41.txtBorrowerName.nativeElement.value);

                  if (_this41.loanManifest) {
                    _this41.txtLoanNumber.nativeElement.value = "";
                    _this41.txtBorrowerName.nativeElement.value = "";
                  }

                  _this41.elementTid.ExceptionNotes = "";
                  _this41.elementTid.ExceptionReason = "";
                  _this41.notesInput.nativeElement.value = "";
                  _this41.selectSearch.nativeElement.value = "";
                  _this41.txtExceptionBarcode.nativeElement.value = "";
                  _this41.exceptionNeeded = false;
                  _this41.totalItems++;
                }, function (error) {
                  sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default()("Search Error", String(error.Message), "error");
                });

                this.exceptionNeeded = false; // this.barcodeStickerSearch.nativeElement.value = '';
                //this.barcodeStickerSearch.nativeElement.focus();

                this.selectSearch.nativeElement.value = "";
                this.notesInput.nativeElement.value = "";
              }
            } else {
              this.elementTid.BarcodeSticker = this.barcodeStickerSearch.nativeElement.value;
              this.elementTid.BarcodeSticker = this.elementTid.BarcodeSticker.toUpperCase();
              this.elementTid.ExceptionReason = this.selectSearch.nativeElement.value;
              this.elementTid.ExceptionNotes = event.target.value; //create TID to register

              this.elementTid = {
                Package_Tracking_BC: this.packageFound.Package_Tracking_BC,
                Package_Tracking_ID: this.packageFound.Package_Tracking_ID,
                TID_ID: 1,
                SRC_SYSTM_CD: "",
                DCMNT_BAR_CD: "",
                DCMNT_TYP_CD: "",
                COLLATERAL_KEY: "",
                COLLATERAL_BAR_CODE: "",
                POOL_SAK: "",
                ACCOUNT_SAK: "",
                BaileeName: "",
                CUSTOMER_CODE: "",
                CUSTOMER_NAME: "",
                CNTRLLER_NUM: "",
                TRACK_LOCATION_CODE: "",
                BaileeNumber: "",
                LenderName: "",
                ScanDate: null,
                DCMNT_CNDTN_CD: "",
                FILE_NAME: "",
                BaileeLetter: this.bailee,
                LoanNumber: this.elementTid.LoanNumber,
                BarcodeSticker: this.elementTid.BarcodeSticker,
                LoanOnManifest: this.loanManifest,
                Certification: this.elementTid.Certification,
                ExceptionNotes: this.elementTid.ExceptionNotes,
                ExceptionReason: this.elementTid.ExceptionReason,
                UserId: this.userlogged.User_Name,
                BorrowerName: this.borrowerName,
                Certified_In: ""
              }; //GET CERTTIFIED FROM HTML

              if (!this.loanManifest || !this.checked) {
                if (this.loanNumber == "") {
                  setTimeout(function () {
                    sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default()("Required", "Loan Number is required.", "error");
                  }, 0);
                  this.txtLoanNumber.nativeElement.focus();
                  return;
                }

                if (this.borrowerName == "") {
                  setTimeout(function () {
                    sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default()("Required", "Loan Number is required.", "error");
                  }, 0);
                  this.txtBorrowerName.nativeElement.focus();
                  return;
                }
              }

              if (!this.loanManifest) {
                this.elementTid.BorrowerName = this.txtBorrowerName.nativeElement.value;
                this.elementTid.LoanNumber = this.txtLoanNumber.nativeElement.value;
              } else {
                this.elementTid.BorrowerName = "";
                this.txtBorrowerName.nativeElement.value = "";
                this.elementTid.LoanNumber = "";
                this.txtLoanNumber.nativeElement.value = "";
              }

              this._tidService.saveOneReturnTID(this.elementTid).subscribe(function (data) {
                _this41.tidRetured = data; //console.log(this.tidRetured;

                _this41.elementTid.TID_ID = data.TID_ID; //RegisterAudit

                _this41.audit = {
                  PID: _this41.packageFound.Package_Tracking_ID,
                  TID: "",
                  ActionName: "CHECK IN CREATE",
                  BusinessUnit: _this41.packageFound.Client_Name,
                  ActionDateTime: null,
                  Sticker: "",
                  User_ID: _this41.userlogged.User_Name
                };
                _this41.loanTotal = _this41.loanTotal + 1; //assign loan total to package object in db

                _this41.packageFound.loanTotal = _this41.loanTotal;
                console.log(_this41.packageFound.loanTotal, "loan total");

                _this41._packageService.updateAndReturnPack(_this41.packageFound).subscribe(function (data) {
                  console.log("package updated with new Lenders", data);
                  _this41.packageFound = data;
                  _this41.totalLenders = data.totalLenders;
                });

                sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
                  position: "top-end",
                  type: "success",
                  title: "Tid has been saved",
                  showConfirmButton: true,
                  timer: 800
                });

                _this41.TIDList.push(_this41.tidRetured); //this.TIDList.sort((a,b) => { return a.ScanDate > b.ScanDate ? -1 : b.ScanDate <  a.ScanDate ? 1 : 0 });
                //this.TIDList.sort((a,b) => { return a.ExceptionReason == '' ? -1 : b.ExceptionReason == '' ? 1 : 0 });


                _this41.TIDList.reverse;

                if (!_this41.checked && !_this41.loanManifest) {
                  _this41.loanNumber = "";
                  _this41.borrowerName = "";

                  _this41.txtLoanNumber.nativeElement.focus();
                } else {
                  _this41.loanManifest = true;
                  _this41.checked = true;

                  _this41.barcodeStickerSearch.nativeElement.focus();
                }

                _this41.barcodeStickerSearch.nativeElement.value = "";
                console.log("PRINTING LOAN ON MANIFEST", _this41.loanManifest);
                console.log("printing values ", _this41.txtLoanNumber.nativeElement.value, _this41.txtBorrowerName.nativeElement.value);

                if (_this41.loanManifest) {
                  _this41.txtLoanNumber.nativeElement.value = "";
                  _this41.txtBorrowerName.nativeElement.value = "";
                }

                _this41.exceptionNeeded = false;
                _this41.elementTid.ExceptionNotes = "";
                _this41.elementTid.ExceptionReason = "";
                _this41.notesInput.nativeElement.value = "";
                _this41.selectSearch.nativeElement.value = "";
                _this41.txtExceptionBarcode.nativeElement.value = "";
              }, function (error) {
                sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default()("Search Error", String(error.Message), "error");
              });

              this.exceptionNeeded = false; // this.barcodeStickerSearch.nativeElement.value = '';
              //this.barcodeStickerSearch.nativeElement.focus();

              this.selectSearch.nativeElement.value = "";
              this.notesInput.nativeElement.value = "";
            }
          } //end createTID METHOD

        }, {
          key: "saveTID",
          value: function saveTID(s) {
            var _this42 = this;

            this._tidService.saveTID(s).subscribe(function (data) {
              console.log(data);
              _this42.audit = {
                PID: s.Package_Tracking_ID,
                TID: s.TID_ID.toString(),
                BusinessUnit: _this42.packageFound.Client_Name,
                ActionName: "CHECK IN ",
                ActionDateTime: null,
                Sticker: s.BarcodeSticker,
                User_ID: _this42.userlogged.User_Name
              }; //insert audit record

              _this42.auditService.saveAudit(_this42.audit).subscribe(function (data) {
                console.log(data);
              });
            });
          }
        }, {
          key: "findOneByPID",
          value: function findOneByPID(id) {
            var _this43 = this;

            this._packageService.findOneByPID(id).subscribe(function (packF) {
              console.log("packFFF", packF);

              if (packF.Client_Name == "" || packF.Client_Name == null) {
                sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default()("Search Error", "Business Unit not yet assigned. Please assign on Update Package module", "error");
                _this43.mainSearch.nativeElement.value = "";
              } else {
                //asigng brought values to local Package
                // this.checked = packF.ManifestInBox;
                console.log("packF baileeLetter active", packF.BaileeLetter);
                _this43.bailee = packF.BaileeLetter;
                _this43.showCamera = true;

                if (packF.BaileeLetter) {
                  _this43.multipleLendersActive = true;
                }
              } // end else


              _this43.onSearchTIDS(_this43.packageFound.Package_Tracking_ID);
            }, function (error) {
              //Swal("Search Error", String(error.Message) ,"error");
              _this43.mainSearch.nativeElement.focus();

              _this43.mainSearch.nativeElement.value = "";
            });
          } //****MODALL SERVICE *****///****MODALL SERVICE *****/

        }, {
          key: "triggerModal",
          value: function triggerModal(content) {
            console.log("content", content);
          }
        }, {
          key: "openCamera",
          value: function openCamera() {
            var _this44 = this;

            //SAving to audit
            var audit = {
              PID: this.packageFound.Package_Tracking_ID,
              TID: "N/A",
              ActionName: "Open Camera",
              ActionDateTime: null,
              BusinessUnit: this.packageFound.Client_Name,
              Sticker: "",
              User_ID: this.userlogged.User_Name
            }; //insert audit record

            this.auditService.saveAudit(audit).subscribe(function (data) {
              console.log(data);
            }); //pending to pass object information

            var dialogRef = this._dialog.open(_cameracomponent_cameracomponent_component__WEBPACK_IMPORTED_MODULE_9__["CameracomponentComponent"], {
              panelClass: "custom-dialog-container",
              data: this.packageFound
            });

            dialogRef.afterClosed().subscribe(function (result) {
              console.log("The dialog was closed");

              _this44.barcodeStickerSearch.nativeElement.focus();

              if (_this44.multipleLendersActive) {
                //this.lenderNameButton.nativeElement.focus();
                if (_this44.bailee) {
                  _this44.BaileeNameSearch.nativeElement.focus();

                  console.log("baile2");
                }
              }
            });
          } //****MODALL SERVICE *****///****MODALL SERVICE *****/

        }, {
          key: "newBaileeButton",
          value: function newBaileeButton() {
            // this.myControl.enable();
            // this.lenderNameButton.nativeElement.value = "";
            // this.lenderNameButton.nativeElement.focus();
            this.loanTotal = 0;
            this.loansExpected = 0;
            this.myControl.enable();
            this.lenderNameButton.nativeElement.value = "";
            this.lenderNameButton.nativeElement.focus(); // let dialogRef = this._dialog.open(LoanDialogComponent, {
            //   panelClass: "custom-dialog-container",
            //   disableClose: true,
            // });
            // dialogRef.afterClosed().subscribe((result: string) => {
            //   //this.lendersRecieved = parseInt(result);
            //   this.loansExpected = parseInt(result);
            //   this.packageFound.totalLenders = parseInt(result);
            //   this.packageFound.BaileeLetter = true;
            //   this.packageFound.multipleLenders = true;
            //   this.packageFound.loansExpected = this.loansExpected;
            //   this.myControl.enable();
            //   this.lenderNameButton.nativeElement.value = "";
            //   this.lenderNameButton.nativeElement.focus();
            //   this._packageService
            //     .updatePack(this.packageFound)
            //     .subscribe((data: any) => {
            //       console.log(data);
            //     });
            //   console.log(this.loanTotal);
            // });
          }
        }, {
          key: "findPackage",
          value: function findPackage(bcstring) {
            var _this45 = this;

            //console.log("Entering find pacakage with ", bcstring);
            this._packageService.findOneByIdBC(bcstring).subscribe(function (data) {
              //console.log(data, "PackageFounbd finPACKAGE");
              _this45.totalLenders = data.totalLenders;
              _this45.lendersRecieved = data.totalLenders;
              _this45.loanTotal = data.loanTotal; //console.log(data.totalLenders, "total");

              _this45.txtBorrowerName.nativeElement.value = "";
              _this45.txtLoanNumber.nativeElement.value = "";
              _this45.loansExpected = data.loansExpected;
              _this45.loanTotal = data.loanTotal;

              if (_this45.multipleLendersActive) {
                _this45.lenderNameButton.nativeElement.value = "";

                _this45.myControl.enable();
              }

              if (data.multipleLenders) {
                _this45.multipleLendersActive = true;
                _this45.multipleLendersPresent = true;
              }

              if (data.Client_Name == "" || data.Client_Name == null) {
                sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default()("Search Error", "Business Unit not yet assigned. Please assign on Update Package module", "error");
                _this45.mainSearch.nativeElement.value = "";

                _this45.mainSearch.nativeElement.focus();
              } else {
                //asigng brought values to local Package
                _this45.packageFound = data;
                _this45.showCamera = true;
                _this45.pidHolder = data.Package_Tracking_ID;
                console.log("lookings tids with this package ", _this45.packageFound);
                _this45.bailee = data.BaileeLetter;

                if (data.BaileeLetter) {
                  _this45.multipleLendersActive = true;
                }
              } // end else


              _this45.TIDList = []; //inside package brought in observable

              console.log("sending this number ", data.Package_Tracking_ID); // console.log("PID HOLDER WATCH", this.pidHolder);
              // console.log("PID HOLDER WATCH", this.packageFound.Package_Tracking_ID);

              _this45.onSearchTIDS(data.Package_Tracking_ID);
            }, function (error) {
              sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default()("Search Error", String(error.Message), "error");

              _this45.mainSearch.nativeElement.focus();

              _this45.mainSearch.nativeElement.value = "";
            }); //end find package observable
            /////////////////////////////////////////////////////
            //this.TIDList.sort((a,b) => { return a.ExceptionReason == '' ? -1 : b.ExceptionReason == '' ? 1 : 0 });


            this.TIDList.reverse;
          } //end find package
          //detect lenders.

        }, {
          key: "difLenders",
          value: function difLenders(tids) {
            var _this46 = this;

            var iCounter = 0;
            var counterArray;
            var lenderArray = [];
            tids.forEach(function (element) {
              if (element.LenderName.length > 0) {
                console.log(element.LenderName, "lendname");

                _this46.currentPackageLenderList.push(element.LenderName);

                lenderArray.push(element.LenderName);
              }
            }); //end for each

            lenderArray = _toConsumableArray(new Set(lenderArray));
            this.lenderCounter = lenderArray.length; //this.baileeCounter = lenderArray.length;
            //console.log("Number of diferent lenders found ", lenderArray.length);

            console.log("Number of diferent lenders found lender counter ", this.lenderCounter);
          }
        }, {
          key: "onSearchTIDS",
          value: function onSearchTIDS(pid) {
            var _this47 = this;

            this._tidService.findDocumentsByPID(pid).subscribe(function (data) {
              //console.log(data, "tids found");
              //this.TIDList = data;
              _this47.totalItems = _this47.TIDList.length;
              console.log("TID LIST", _this47.TIDList.length);

              _this47.difLenders(data);

              data.forEach(function (element) {
                if (element.LenderName != "") {
                  console.log("setting focus to lender field");

                  _this47.lenderNameButton.nativeElement.focus();
                }

                if (element.BaileeName != "") {
                  _this47.baileeNameHolder = element.BaileeName;
                }
              });

              if (data.length > 0) {
                console.log("Tid list above 1");
                sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default()({
                  title: "Are you sure?",
                  text: "This package has already been checked-in. Do you want to proceed?",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonText: "Yes",
                  cancelButtonText: "No"
                }).then(function (result) {
                  if (result.value) {
                    _this47.TIDList = data;
                    _this47.totalItems = _this47.TIDList.length; //this.uploadButton.nativeElement.textContent = 'Take Pic';

                    data.forEach(function (element) {
                      if (element.BaileeName != "") {
                        _this47.myControl2.setValue(element.BaileeName);

                        _this47.myControl2.disable(); ///test
                        //this.loanTotal = this.loanTotal + 1;

                      } // this.packageFound.loan

                    });

                    if (!_this47.checked || !_this47.loanManifest) {
                      _this47.txtLoanNumber.nativeElement.focus();
                    } else {
                      setTimeout(function () {
                        if (_this47.lenderCounter > 0) {//focus lendername cuz its first
                          //this.lenderNameButton.nativeElement.focus();
                        } else {
                          _this47.barcodeStickerSearch.nativeElement.focus();

                          _this47.barcodeStickerSearch.nativeElement.value = "";
                        }
                      }, 400);
                    }
                  } else if (result.dismiss === sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default.a.DismissReason.cancel) {
                    setTimeout(function () {
                      _this47.mainSearch.nativeElement.focus();

                      _this47.initPackages();
                    }, 100);
                    return;
                  }
                });
              } else {
                console.log("TID LIST BELOW 1");

                _this47.openCamera();

                _this47.BaileeNameSearch.nativeElement.focus();

                _this47.BaileeNameSearch.nativeElement.value = "";
              }
            }, function (error) {
              sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default()("Error", error.Message, "error");
              return 0;
            });
          }
        }, {
          key: "printCoverSheetTID",
          value: function printCoverSheetTID() {
            console.log("about to print this TID LIST " + this.TIDList);
            this.onCreateCoverSheetTID(this.TIDList);
          }
        }, {
          key: "printCoverSheet",
          value: function printCoverSheet() {
            var _this48 = this;

            this._packageService.findOneByPID(this.packageFound.Package_Tracking_ID).subscribe(function (res) {
              console.log("about to print cs with this package ", res);

              if (res != null) {
                _this48.cslist.push(res);

                console.log("current cslist", _this48.cslist);

                _this48.onCreateCoverSheet(_this48.cslist);

                _this48.cslist = [];
              }
            });
          }
        }, {
          key: "onCreateCoverSheetManifest",
          value: function onCreateCoverSheetManifest(items) {
            console.log(items);

            this._createTIDCS.createCoversheetManifest(items).subscribe(function (response) {
              var file = new Blob([response], {
                type: "application/pdf"
              });
              var url = URL.createObjectURL(file);
              var iframe = document.createElement("iframe");
              iframe.style.display = "none";
              iframe.src = url;
              document.body.appendChild(iframe);
              iframe.contentWindow.print();
            }, function (error) {
              sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default()("Error", String(error.Message), "error");
            }); //this.initPackages();


            this.barcodeStickerSearch.nativeElement.focus();
          }
        }, {
          key: "onCreateCoverSheetTID",
          value: function onCreateCoverSheetTID(items) {
            console.log(items);

            this._packageService.createCoversheetManifest(this.packageFound).subscribe(function (response) {
              var file = new Blob([response], {
                type: "application/pdf"
              });
              var url = URL.createObjectURL(file);
              var iframe = document.createElement("iframe");
              iframe.style.display = "none";
              iframe.src = url;
              document.body.appendChild(iframe);
              iframe.contentWindow.print();
            }, function (error) {
              sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default()("Error", String(error.Message), "error");
            });
          }
        }, {
          key: "onCreateCoverSheet",
          value: function onCreateCoverSheet(items) {
            this._packageService.createCoversheet(items).subscribe(function (response) {
              var file = new Blob([response], {
                type: "application/pdf"
              });
              var url = URL.createObjectURL(file);
              var iframe = document.createElement("iframe");
              iframe.style.display = "none";
              iframe.src = url;
              document.body.appendChild(iframe);
              iframe.contentWindow.print();
            }, function (error) {
              sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default()("Error", String(error.Message), "error");
            });
          }
        }, {
          key: "onClickMe",
          value: function onClickMe() {
            if (this.lenderCounter < this.totalLenders) {
              var pending = this.totalLenders - this.lenderCounter;
              sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default()("Warning", "Package missing " + pending.toString() + " lenders. Verify the Expected lender count matches the Total Lender count to proceed.", "error");
            } else {
              this.packageFound.BaileeLetter = this.bailee;
              this.packageFound.ManifestInBox = this.checked;
              this.onCreateCoverSheetManifest(this.TIDList);
            }
          }
        }, {
          key: "retrieveImages",
          value: function retrieveImages() {
            var _this49 = this;

            this.showSpinner = true;
            var heynow;
            var audit = {
              PID: this.packageFound.Package_Tracking_ID,
              TID: "N/A",
              ActionName: "Retrieve Images / Check In",
              ActionDateTime: null,
              BusinessUnit: this.packageFound.Client_Name,
              Sticker: "",
              User_ID: this.userlogged.User_Name
            }; //insert audit record

            this.auditService.saveAudit(audit).subscribe(function (data) {
              console.log(data);
            });

            this._tidService.retrieveDoc(this.packageFound.Package_Tracking_ID).subscribe(function (e) {
              console.log("geting images", e);
              _this49.imagesFound = e;
              e.forEach(function (element) {
                //console.log(element);
                var imageo = _this49._sanitizer.bypassSecurityTrustResourceUrl("data:image/tif;base64," + element);

                while (!element == undefined) {
                  _this49.imagesFound.push(element);
                }
              }); // const modalRef = this.modalService.open(ImageDisplayComponent, ngbModalOptions );
              // modalRef.componentInstance.images = this.imagesFound;
              // console.log("images", this.imagesFound);

              _this49._dialog.open(_image_display_image_display_component__WEBPACK_IMPORTED_MODULE_11__["ImageDisplayComponent"], {
                data: {
                  images: _this49.imagesFound,
                  pidSent: _this49.packageFound.Package_Tracking_ID
                },
                panelClass: "custom-dialog-container"
              });

              _this49.showSpinner = false;
            }, function (error) {
              console.log(error);
              _this49.showSpinner = false;
            }); //end observable

          }
        }, {
          key: "retrieveAllLenderNames",
          value: function retrieveAllLenderNames() {
            var _this50 = this;

            this._lenderNames.findAllLenderNames().subscribe(function (data) {
              //console.log("Retrieved lender names, ", data);
              data.forEach(function (tid) {
                _this50.lenderNameList.push(tid.lenderName);
              });

              _this50.lenderNameList.sort(function (a, b) {
                return a.localeCompare(b);
              });

              _this50.lenderNameList = _toConsumableArray(new Set(_this50.lenderNameList)); //console.log("Filtered Lender Name List ", this.lenderNameList);
            });
          }
        }, {
          key: "retrieveAllBaileeNames",
          value: function retrieveAllBaileeNames() {
            var _this51 = this;

            this._lenderNames.findAllBaileeNames().subscribe(function (data) {
              //console.log("Retrieved lender names, ", data);
              data.forEach(function (tid) {
                _this51.baileeNameList.push(tid.lenderName);
              }); //console.log(data);

              _this51.baileeNameList.sort(function (a, b) {
                return a.localeCompare(b);
              });

              _this51.baileeNameList = _toConsumableArray(new Set(_this51.baileeNameList)); //console.log("Filtered Lender Name List ", this.baileeNameList);
            });
          }
        }, {
          key: "onEnterLender",
          value: function onEnterLender(event) {
            var _this52 = this;

            if (this.BaileeNameSearch.nativeElement.value != "" && this.lenderNameButton.nativeElement.value != "") {
              if (this.baileeNameHolder == event.target.value) {
                setTimeout(function () {
                  sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default()("Error", "Can't use same name as Lender Name", "error");

                  _this52.myControl.setValue("");
                }, 500);
              } else {
                console.log(this.elementTid.LenderName);
                this.myControl.disable();
                this.myControl2.disable();
                this.barcodeStickerSearch.nativeElement.focus();

                var dialogRef = this._dialog.open(_dialogComponents_loan_dialog_loan_dialog_component__WEBPACK_IMPORTED_MODULE_17__["LoanDialogComponent"], {
                  panelClass: "custom-dialog-container",
                  disableClose: true
                });

                var lessthantotal = false;
                dialogRef.afterClosed().subscribe(function (result) {
                  //this.lendersRecieved = parseInt(result);
                  if (parseInt(result) < _this52.loanTotal) {
                    sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default()("Error", "Cannot have less expected items then current items", "error");

                    _this52.myControl.enable();

                    _this52.myControl.setValue("");

                    setTimeout(function () {
                      _this52.lenderNameButton.nativeElement.focus();
                    }, 1500);
                  } else if (parseInt(result) == _this52.loanTotal) {
                    _this52.loansExpected = parseInt(result);
                    _this52.packageFound.totalLenders = parseInt(result);
                    _this52.packageFound.BaileeLetter = true;
                    _this52.packageFound.multipleLenders = true;
                    _this52.packageFound.loansExpected = _this52.loansExpected;

                    _this52._packageService.updatePack(_this52.packageFound).subscribe(function (data) {
                      console.log(data);
                    });

                    _this52.printAndSavePackage2(_this52.lenderNameButton.nativeElement.value);
                  } else {
                    _this52.loansExpected = parseInt(result);
                    _this52.packageFound.totalLenders = parseInt(result);
                    _this52.packageFound.BaileeLetter = true;
                    _this52.packageFound.multipleLenders = true;
                    _this52.packageFound.loansExpected = _this52.loansExpected;

                    _this52._packageService.updatePack(_this52.packageFound).subscribe(function (data) {
                      console.log(data);
                    });

                    console.log(_this52.loanTotal);
                  }
                });
              }
            } else if (this.lenderNameButton.nativeElement.value != "") {
              setTimeout(function () {
                sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default()("Error", "Please select Lender Name", "error");

                _this52.lenderNameButton.nativeElement.focus();

                _this52.myControl.setValue("");
              }, 900);
              this.BaileeNameSearch.nativeElement.focus();
            } else {
              setTimeout(function () {
                sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default()("Error", "Please select Bailee Name and Lender Name first", "error");

                _this52.BaileeNameSearch.nativeElement.focus(); //this.myControl2.setValue("");

              }, 900);
              this.BaileeNameSearch.nativeElement.focus();
            }
          }
        }, {
          key: "onEnterBailee",
          value: function onEnterBailee(event) {
            var _this53 = this;

            if (event.target.value == this.lenderNameButton.nativeElement.value) {
              setTimeout(function () {
                sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default()("Error", "Can't use same name as Lender Name", "error");

                _this53.myControl2.setValue("");
              }, 500);
            } else {
              console.log(this.elementTid.LenderName);
              this.baileeNameHolder = event.target.value; //this.myControl.disable();

              this.myControl2.disable();
              this.lenderNameButton.nativeElement.focus();
            }
          }
        }, {
          key: "multipleLenders",
          value: function multipleLenders(e) {
            var _this54 = this;

            if (this.multipleLendersActive == true) {
              if (this.packageFound.totalLenders > 1 && this.multipleLendersActive) {
                this.multipleLendersPresent = false;
                this.packageFound.totalLenders = 1;
                this.packageFound.multipleLenders = false;
                this.packageFound.BaileeLetter = true;
                this.multipleLendersPresent = false;
                this.lenderNameButton.nativeElement.value = "";
                this.lenderNameButton.nativeElement.focus();

                this._packageService.updateAndReturnPack(this.packageFound).subscribe(function (data) {
                  _this54.totalLenders = data.totalLenders;
                  _this54.lenderNameButton.nativeElement.value = "";

                  _this54.lenderNameButton.nativeElement.focus();
                });
              } else {
                this.multipleLendersPresent = true;

                var dialogRef = this._dialog.open(_dialogComponents_lender_dialog_lender_dialog_component__WEBPACK_IMPORTED_MODULE_15__["LenderDialogComponent"], {
                  panelClass: "custom-dialog-container2",
                  disableClose: true
                });

                dialogRef.afterClosed().subscribe(function (result) {
                  _this54.lendersRecieved = parseInt(result);
                  _this54.totalLenders = parseInt(result);
                  _this54.packageFound.totalLenders = parseInt(result);
                  _this54.packageFound.BaileeLetter = true;
                  _this54.packageFound.multipleLenders = true;

                  _this54._packageService.updateAndReturnPack(_this54.packageFound).subscribe(function (data) {});

                  console.log("total rrevieced from dialog " + result);

                  _this54.myControl2.enable();

                  _this54.BaileeNameSearch.nativeElement.focus();
                });
              }
            }
          } //END MULTIPLE LENDERS

        }, {
          key: "printAndSavePackage",
          value: function printAndSavePackage() {
            var _this55 = this;

            if (this.TIDList.length > 0) {
              if (this.multipleLendersActive) {
                if (this.lenderCounter < this.totalLenders) {
                  var pending = this.totalLenders - this.lenderCounter;
                  sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default()("Warning", "Package missing " + pending.toString() + " lenders. Verify the Expected lender count matches the Total Lender count to proceed.", "error");
                } else {
                  this.packageFound.BaileeLetter = this.bailee;
                  this.packageFound.ManifestInBox = this.checked; //console.log("try to get there");

                  console.log(this.packageFound.Client_Name);

                  this._packageService.updatePackCheckIn(this.packageFound).subscribe(function (data) {
                    sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
                      position: "top-end",
                      type: "success",
                      title: "Package has been saved",
                      showConfirmButton: false,
                      timer: 800
                    });
                  });

                  var auditsave = new src_app_models_audit_trail_model__WEBPACK_IMPORTED_MODULE_6__["AuditTrail"]();
                  auditsave.PID = this.packageFound.Package_Tracking_ID;
                  auditsave.TID = "";
                  auditsave.ActionName = "CHECK IN";
                  auditsave.BusinessUnit = this.packageFound.Client_Name;
                  auditsave.ActionDateTime = null;
                  auditsave.User_ID = this.userlogged.User_Name;
                  auditsave.Sticker = "Print & Save package"; //this.audit.BusinessUnit = this.packageFound.Client_Name;
                  //insert audit record

                  this.auditService.saveAudit(auditsave).subscribe(function (data) {
                    console.log(data);
                  });
                  setTimeout(function () {
                    _this55.myControl2.setValue("");

                    _this55.myControl.setValue("");

                    _this55.baileeNameHolder = "";
                    _this55.BaileeNameSearch.nativeElement.value = "";
                  }, 200);
                  this.onCreateCoverSheetTID(this.TIDList);
                  this.initPackages();
                }
              } else {
                this.packageFound.BaileeLetter = this.bailee;
                this.packageFound.ManifestInBox = this.checked; //console.log("try to get there");

                console.log(this.packageFound.Client_Name);

                this._packageService.updatePackCheckIn(this.packageFound).subscribe(function (data) {
                  sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
                    position: "top-end",
                    type: "success",
                    title: "Package has been saved",
                    showConfirmButton: false,
                    timer: 800
                  });
                });

                var auditsave = new src_app_models_audit_trail_model__WEBPACK_IMPORTED_MODULE_6__["AuditTrail"]();
                auditsave.PID = this.packageFound.Package_Tracking_ID;
                auditsave.TID = "";
                auditsave.ActionName = "CHECK IN";
                auditsave.BusinessUnit = this.packageFound.Client_Name;
                auditsave.ActionDateTime = null;
                auditsave.User_ID = this.userlogged.User_Name;
                auditsave.Sticker = "Print & Save package"; //this.audit.BusinessUnit = this.packageFound.Client_Name;
                //insert audit record

                this.auditService.saveAudit(auditsave).subscribe(function (data) {
                  console.log(data);
                });
                this.onCreateCoverSheetTID(this.TIDList);
                this.initPackages();
                this.myControl2.enable();
                setTimeout(function () {
                  _this55.myControl2.setValue("");

                  _this55.myControl.setValue("");

                  _this55.baileeNameHolder = "";
                  _this55.BaileeNameSearch.nativeElement.value = "";
                }, 200);
              }
            } else {
              sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default()("Required", "There are 0 tids found to print ", "error");
            }
          } //end

        }, {
          key: "newBaileCounter",
          value: function newBaileCounter() {
            this.baileeCounter = 0;
          }
        }, {
          key: "newBaileeMethod",
          value: function newBaileeMethod() {
            this.baileeCounter = 0;
          }
        }, {
          key: "resetLoans",
          value: function resetLoans() {
            this.loanTotal = 0;
            this.loansExpected = 0;
            this.myControl.enable();
            this.BaileeNameSearch.nativeElement.value = "";
            this.lenderNameButton.nativeElement.value = "";
            this.myControl2.enable();
            this.multipleLendersActive = false;
            this.multipleLendersPresent = false;
          }
        }, {
          key: "initPackages",
          value: function initPackages() {
            this.baileeCounter = 0;
            this.lendersRecieved = 0;
            this.loanTotal = 0;
            this.loansExpected = 0;
            this.myControl.enable();
            this.myControl2.enable();
            this.multipleLendersActive = false;
            this.multipleLendersPresent = false;
            this.mainSearch.nativeElement.value = "";
            this.mainSearch.nativeElement.focus();
            this.excBarcode = "";
            this.lenderCounter = 0;
            this.totalItems = 0;
            this.checked = true;
            this.loanManifest = true;
            this.showCamera = false;
            this.myControl.enable();
            this.showSpinner = false;
            this.baileeCounter = 0;
            this.currentPackageLenderList = [];
            this.myControl.enable();
            this.totalLenders = 0;
            this.loansExpected = 0;
            this.loanTotal = 0;
            this.lenderCounter = 0;
            this.lendersRecieved = 0;
            this.baileeNameHolder = "";
            this.myControl2.enable(); // this.secondSearch.nativeElement.value='';

            this.exceptionNeeded = false;
            this.barcodeStickerSearch.nativeElement.value = "";
            this.TIDList = [];
            this.tableCreationElements = [];
            this.bailee = false;
            this.inputReader = "";
            this.packageFound = {
              Package_Tracking_BC: "",
              multipleLenders: false,
              Package_Tracking_ID: "",
              LoanNumber: 0,
              Sticker_ID: "",
              Status_ID: 0,
              Location_Name: "",
              totalLenders: 0,
              Client_Name: "",
              WorkFlow_ID: 0,
              Task_ID: 0,
              clearPackagesFlag: false,
              Creation_Date_Time: "",
              Available: false,
              Exception_Flag: false,
              Exception_Name: "",
              Package_Finished: false,
              ManifestInBox: false,
              BaileeLetter: false,
              Carrier: "",
              Package_Notes: "",
              Bin_Number: "",
              clearPackagesDateTime: "",
              lastModule: "",
              lastFunction: "",
              loanTotal: 0,
              loansExpected: 0
            };
            this.elementTid = {
              Package_Tracking_BC: "",
              Package_Tracking_ID: 0,
              TID_ID: 0,
              LoanNumber: "",
              BarcodeSticker: "",
              BaileeLetter: false,
              SRC_SYSTM_CD: "",
              DCMNT_BAR_CD: "",
              BaileeNumber: "",
              LenderName: "",
              DCMNT_TYP_CD: "",
              COLLATERAL_KEY: "",
              COLLATERAL_BAR_CODE: "",
              POOL_SAK: "",
              ACCOUNT_SAK: "",
              CUSTOMER_CODE: "",
              CUSTOMER_NAME: "",
              CNTRLLER_NUM: "",
              TRACK_LOCATION_CODE: "",
              ScanDate: null,
              DCMNT_CNDTN_CD: "",
              FILE_NAME: "",
              LoanOnManifest: false,
              BaileeName: "",
              Certification: false,
              ExceptionNotes: "",
              ExceptionReason: "",
              UserId: "",
              BorrowerName: "",
              Certified_In: ""
            };
          }
        }, {
          key: "deleteRecord",
          value: function deleteRecord(row, index) {
            var _this56 = this;

            sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
              title: "Are you sure?",
              text: "Do you wish to disable TID-" + row.TID_ID,
              icon: "warning",
              showCancelButton: true,
              confirmButtonColor: "#3085d6",
              cancelButtonColor: "#d33",
              confirmButtonText: "Yes, disable it!"
            }).then(function (result) {
              if (result.value == true) {
                console.log("Deleting package ", row); // this.deleteRecord(packageRow);
                //const index : number = this.tableCreationElements.indexOf(row);

                if (_this56.bailee) {
                  if (_this56.loanTotal != 0) {
                    _this56.loanTotal = _this56.loanTotal - 1;
                    _this56.packageFound.loanTotal = _this56.loanTotal;
                  }

                  _this56.packageFound.loanTotal = _this56.loanTotal;
                  console.log(_this56.packageFound.loanTotal, "loan total");

                  _this56._packageService.updateAndReturnPack(_this56.packageFound).subscribe(function (data) {
                    console.log("package updated with new Lenders", data);
                    _this56.packageFound = data;
                    _this56.totalLenders = data.totalLenders;
                  });
                }

                console.log(_this56.tableCreationElements); //let packtoDelete = this.TIDList.find(x => x.Package_Tracking_ID == this.tableCreationElements[index].PID)
                //console.log("tid to delete", packtoDelete);
                //record Audit Trail

                _this56._tidService.disableTID(row).subscribe(function (data) {
                  _this56.TIDList.filter(function (t) {
                    return t.TID_ID != row.TID_ID;
                  });
                });

                _this56.audit = {
                  PID: row.Package_Tracking_ID,
                  TID: row.TID_ID.toString(),
                  BusinessUnit: "",
                  ActionName: "CHECK IN REMOVE",
                  ActionDateTime: null,
                  Sticker: row.BarcodeSticker,
                  User_ID: _this56.userlogged.User_Name
                };
                _this56.totalItems--; //assign loan total to package object in db
                //insert audit record

                _this56.auditService.saveAudit(_this56.audit).subscribe(function (data) {
                  console.log(data);
                });

                var temporal = [];
                var temporalTIDLIST = [];

                _this56.TIDList.forEach(function (tid) {
                  temporal.push(tid.LenderName);
                });

                temporal = _toConsumableArray(new Set(temporal));

                _this56.TIDList.splice(index, 1);

                _this56.TIDList.forEach(function (tid) {
                  temporalTIDLIST.push(tid.LenderName);
                });

                temporalTIDLIST = _toConsumableArray(new Set(temporalTIDLIST));

                if (temporalTIDLIST.length == temporal.length) {
                  console.log("okcool");
                }

                if (temporal.length > temporalTIDLIST.length) {
                  if (_this56.lenderCounter > 0) {
                    _this56.lenderCounter--;
                    console.log("lendercounter minus 1");
                  }
                }
              }
            });
          }
        }, {
          key: "checkLenders",
          value: function checkLenders() {
            var temporal = [];
            var temporalTIDLIST = [];
            this.TIDList.forEach(function (tid) {
              temporal.push(tid.LenderName);
            });
            temporalTIDLIST = _toConsumableArray(new Set(temporalTIDLIST));
            temporal = _toConsumableArray(new Set(temporal));
            console.log(temporal.length + " lender names temporal");
            console.log(temporalTIDLIST.length + " lender names temporal TID");

            if (temporalTIDLIST.length == temporal.length) {
              console.log("okcool");
            }

            if (temporal.length < temporalTIDLIST.length) {
              this.lenderCounter--;
              console.log("okcool asdaksdna");
            }
          }
        }, {
          key: "onAddImages",
          value: function onAddImages() {
            sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
              title: "Image Recieved",
              text: "Sent from user " + this.userlogged.User_Name,
              imageUrl: "https://unsplash.it/400/200",
              imageWidth: 400,
              imageHeight: 200,
              imageAlt: "Webcam image",
              showDenyButton: true,
              CancelButtonText: "Retake Image",
              confirmButtonText: "Save Images",
              showCancelButton: true,
              denyButtonText: "Retake Image",
              width: 800
            });
          }
        }, {
          key: "onExceptionReason",
          value: function onExceptionReason() {
            if (this.excBarcode === "*DND*") {
              this.selectSearch.nativeElement.value = "DND";
            } else if (this.excBarcode === "*FLD*") {
              this.selectSearch.nativeElement.value = "FAIL";
            } else if (this.excBarcode === "*NIG*") {
              this.selectSearch.nativeElement.value = "NIGO";
            } else if (this.excBarcode === "*NOS*") {
              this.selectSearch.nativeElement.value = "NOS";
            } else if (this.excBarcode === "*REJ*") {
              this.selectSearch.nativeElement.value = "REJECTS";
            } else if (this.excBarcode === "*RER*") {
              this.selectSearch.nativeElement.value = "REROUTE";
            } else if (this.excBarcode === "*RES*") {
              this.selectSearch.nativeElement.value = "RESEARCH";
            } else if (this.excBarcode === "*RTC*") {
              this.selectSearch.nativeElement.value = "RETURN TO CLIENT";
            }

            this.txtExceptionBarcode.nativeElement.value = "";
            this.notesInput.nativeElement.focus();
          }
        }, {
          key: "printAndSavePackage2",
          value: function printAndSavePackage2(lenderName) {
            var newArray = [];
            this.TIDList.forEach(function (element) {
              if (element.LenderName === lenderName) {
                newArray.push(element);
              }
            });
            console.log(newArray, "kekw");
            this.onCreateCoverSheetLoans(newArray);
          }
        }, {
          key: "onCreateCoverSheetLoans",
          value: function onCreateCoverSheetLoans(items) {
            console.log(items);

            this._tidService.createCoversheetManifestLoans(items).subscribe(function (response) {
              var file = new Blob([response], {
                type: "application/pdf"
              });
              var url = URL.createObjectURL(file);
              var iframe = document.createElement("iframe");
              iframe.style.display = "none";
              iframe.src = url;
              document.body.appendChild(iframe);
              iframe.contentWindow.print();
            }, function (error) {
              sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default()("Error", String(error.Message), "error");
            });
          }
        }, {
          key: "changeExpected",
          value: function changeExpected() {
            this.myControl.enable();
            this.lenderNameButton.nativeElement.focus();
          }
        }]);

        return GenerateTidComponent;
      }();

      GenerateTidComponent.ɵfac = function GenerateTidComponent_Factory(t) {
        return new (t || GenerateTidComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_packages_add_packages_service__WEBPACK_IMPORTED_MODULE_1__["AddPackagesService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_coversheet_create_coversheet_service__WEBPACK_IMPORTED_MODULE_2__["CreateCoversheetService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_tid_lender_names_service__WEBPACK_IMPORTED_MODULE_16__["LenderNamesService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_tid_tidcreartion_service_service__WEBPACK_IMPORTED_MODULE_3__["TidcreartionServiceService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__["NgbModal"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_packages_audit_trail_service__WEBPACK_IMPORTED_MODULE_8__["AuditTrailService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_platform_browser__WEBPACK_IMPORTED_MODULE_10__["DomSanitizer"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_material_dialog__WEBPACK_IMPORTED_MODULE_12__["MatDialog"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModuleRef"]));
      };

      GenerateTidComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: GenerateTidComponent,
        selectors: [["app-generate-tid"]],
        viewQuery: function GenerateTidComponent_Query(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c0, 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c1, 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c2, 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c3, 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c4, 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c5, 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c6, 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c7, 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c8, 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c9, 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c10, 1);
          }

          if (rf & 2) {
            var _t;

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.mainSearch = _t.first);
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.barcodeStickerSearch = _t.first);
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.selectSearch = _t.first);
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.notesInput = _t.first);
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.txtLoanNumber = _t.first);
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.txtBorrowerName = _t.first);
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.txtExceptionBarcode = _t.first);
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.uploadButton = _t.first);
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.lenderNameButton = _t.first);
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.newBaileeButtonFunction = _t.first);
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.BaileeNameSearch = _t.first);
          }
        },
        decls: 106,
        vars: 22,
        consts: [[1, "row"], [1, "col-md-4"], [1, "card", 2, "border", "solid 2px #04395e"], [1, "card-header"], [2, "float", "right"], [3, "diameter", 4, "ngIf"], [1, "card-body"], ["id", "inputRow", 1, "row"], ["id", "trackinglbl", 1, "col-sm-12"], [2, "padding-left", "15px"], ["id", "inputTrackingNumber", 1, "col-sm-12"], ["type", "text", "placeholder", "Tracking # or PID-", 1, "form-control", 2, "padding-left", "15px", "padding-right", "15px", 3, "keydown.enter"], ["mainSearch", ""], ["id", "checkbox1", 1, "pull-left"], ["type", "checkbox", "readonly", "", "id", "exampleCheck1", 1, "form-check-input", 3, "ngModel", "ngModelChange", "click"], ["for", "exampleCheck1", 1, "form-check-label", 2, "margin", "5px"], ["type", "checkbox", "id", "exampleCheck2", 1, "form-check-input", 3, "ngModel", "ngModelChange", "click"], ["for", "exampleCheck2", 1, "form-check-label", 2, "margin", "5px"], [4, "ngIf"], ["class", "col-12", 4, "ngIf"], [1, "col-12"], ["id", "checkbox1", 1, "pull-right"], ["type", "checkbox", "id", "exampleCheck3", 1, "form-check-input", 3, "ngModel", "ngModelChange", "click"], ["for", "exampleCheck3", 1, "form-check-label", 2, "margin", "5px"], ["id", "checkbox2", 1, "pull-left"], ["type", "checkbox", "id", "exampleCheck4", 1, "form-check-input", 3, "ngModel", "ngModelChange", "click"], ["for", "exampleCheck4", 1, "form-check-label", 2, "margin", "5px"], [1, "col-sm-12", 3, "hidden"], ["type", "text", "placeholder", "Loan Number", 1, "form-control", 3, "ngModel", "ngModelChange", "keydown.enter"], ["txtLoanNumber", ""], ["type", "text", "placeholder", "Borrower Last Name", 1, "form-control", 3, "ngModel", "ngModelChange", "keydown.enter"], ["txtBorrowerName", ""], ["class", "col-sm-12", 4, "ngIf"], [1, "col-sm-12"], [2, "padding-left", "15px", "display", "inline"], ["style", "margin-left: 50px; display: inline; font-size: 18px", 4, "ngIf"], ["type", "text", "placeholder", "Barcode Sticker", 1, "form-control", 3, "keydown.enter"], ["barcodeStickerSearch", ""], ["mat-raised-button", "", "color", "primary", "id", "botonSplit", "class", "btn btn-secondary", 3, "click", 4, "ngIf"], ["id", "btn1", "mat-raised-button", "", "color", "primary", 1, "btn", "btn-secondary", 3, "click"], ["id", "btn2", "mat-raised-button", "", "color", "primary", 1, "btn", "btn-secondary", 3, "click"], [1, "col-md-8"], [1, "card"], [2, "float", "middle", "padding-left", "4em"], [1, "card-deck"], [1, "card", 2, "width", "20%", "display", "inline-block", "text-align", "center"], ["id", "rowstile", 1, "container-fluid"], [1, "row", 2, "text-align", "center"], [1, "panel", "panel-color", "panel-fadeDark"], [1, "panel-body"], [1, "table-responsive"], ["id", "tblLoans", 1, "table", "table-striped"], [1, "thead-dark"], ["class", "", 4, "ngFor", "ngForOf"], ["style", "text-align: center", "tr", "", 3, "ngClass", 4, "ngFor", "ngForOf"], [3, "diameter"], ["type", "checkbox", 1, "form-check-input", 3, "ngModel", "ngModelChange", "click"], [1, "form-check-label", 2, "margin", "5px"], [2, "float", "left"], [1, "btn", "btn-success", 3, "click"], ["uploadButton", ""], ["type", "text", "placeholder", "Bailee Name", "matInput", "", 1, "form-control", 3, "matAutocomplete", "formControl", "keydown.enter"], ["BaileeNameSearch", ""], ["autoBailee", "matAutocomplete"], [3, "value", 4, "ngFor", "ngForOf"], ["type", "text", "placeholder", "Lender Name", "matInput", "", 1, "form-control", 3, "ngModel", "disabled", "matAutocomplete", "formControl", "ngModelChange", "keydown.enter"], ["LenderNameSearch", ""], ["auto", "matAutocomplete"], [3, "value"], [2, "margin-left", "50px", "display", "inline", "font-size", "18px"], ["style", "margin-left: 40px; display: inline; font-size: 18px", 4, "ngIf"], [2, "margin-left", "40px", "display", "inline", "font-size", "18px"], [1, "col-md-4", 2, "padding-left", "15px", "padding-right", "15px"], [1, "form-group"], ["type", "text", "placeholder", "Exception Barcode", "maxlength", "5", "name", "ExcBarcode", 1, "form-control", "txt", 3, "ngModel", "ngModelChange", "keydown.enter"], ["txtExceptionBarcode", ""], [1, "col-sm-8", 2, "padding-left", "15px", "padding-right", "15px"], [1, "form-control", 3, "change"], ["selectSearch", ""], [3, "ngValue", 4, "ngFor", "ngForOf"], [2, "padding-left", "25px"], [1, "col-sm-12", 2, "padding-left", "15px", "padding-right", "15px"], ["type", "text", "placeholder", "Notes", 1, "form-control", 3, "keydown.enter"], ["notes", ""], [3, "ngValue"], ["mat-raised-button", "", "color", "primary", "id", "botonSplit", 1, "btn", "btn-secondary", 3, "click"], ["FunctionBaileeButton", ""], [1, ""], ["tr", "", 2, "text-align", "center", 3, "ngClass"], [3, "click"]],
        template: function GenerateTidComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "h5", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, " Check-In ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "span", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](6, GenerateTidComponent_mat_spinner_6_Template, 1, 1, "mat-spinner", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "span", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "b");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Tracking # or PID-");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "input", 11, 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("keydown.enter", function GenerateTidComponent_Template_input_keydown_enter_15_listener($event) {
              return ctx.search($event);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](17, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "span", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "b");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](21, "hr");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "div", 13);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "input", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function GenerateTidComponent_Template_input_ngModelChange_23_listener($event) {
              return ctx.checked = $event;
            })("click", function GenerateTidComponent_Template_input_click_23_listener() {
              return ctx.onCheckManifest();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "label", 15);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25, "Manifest in box");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](26, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "input", 16);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function GenerateTidComponent_Template_input_ngModelChange_27_listener($event) {
              return ctx.bailee = $event;
            })("click", function GenerateTidComponent_Template_input_click_27_listener($event) {
              return ctx.onCheckLenders($event);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "label", 17);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29, "Bailee Letter Present");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](30, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](31, GenerateTidComponent_div_31_Template, 4, 1, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](32, "hr");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](33, GenerateTidComponent_div_33_Template, 8, 0, "div", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](34, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "h5", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](37, "Generate TID");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](38, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "div", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "div", 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "input", 22);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function GenerateTidComponent_Template_input_ngModelChange_42_listener($event) {
              return ctx.exceptionNeeded = $event;
            })("click", function GenerateTidComponent_Template_input_click_42_listener() {
              return ctx.exceptionCheckboxHandler();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "label", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](44, "Record Exception");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "div", 24);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "input", 25);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function GenerateTidComponent_Template_input_ngModelChange_46_listener($event) {
              return ctx.loanManifest = $event;
            })("click", function GenerateTidComponent_Template_input_click_46_listener() {
              return ctx.changeLoanTrue();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "label", 26);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](48, "Loan in Manifest");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "div", 27);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "label", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "b");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](52, "Loan Number");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "input", 28, 29);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function GenerateTidComponent_Template_input_ngModelChange_53_listener($event) {
              return ctx.loanNumber = $event;
            })("keydown.enter", function GenerateTidComponent_Template_input_keydown_enter_53_listener($event) {
              return ctx.onEnterLoanNumber($event);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](55, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](56, "div", 27);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "label", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "b");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](59, "Borrower Last Name");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "input", 30, 31);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function GenerateTidComponent_Template_input_ngModelChange_60_listener($event) {
              return ctx.borrowerName = $event;
            })("keydown.enter", function GenerateTidComponent_Template_input_keydown_enter_60_listener($event) {
              return ctx.onEnterBorrowerName($event);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](62, GenerateTidComponent_div_62_Template, 20, 12, "div", 32);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](63, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](64, "div", 33);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "label", 34);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "b");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](67, "Barcode Sticker");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](68, GenerateTidComponent_span_68_Template, 4, 2, "span", 35);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](69, "input", 36, 37);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("keydown.enter", function GenerateTidComponent_Template_input_keydown_enter_69_listener($event) {
              return ctx.ifException($event);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](71, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](72, GenerateTidComponent_div_72_Template, 23, 2, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](73, "hr");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](74, "div");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](75, GenerateTidComponent_button_75_Template, 3, 0, "button", 38);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](76, GenerateTidComponent_button_76_Template, 2, 0, "button", 38);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](77, "button", 39);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function GenerateTidComponent_Template_button_click_77_listener() {
              return ctx.onClickMe();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](78, " PRINT TIDs ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](79, "button", 39);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function GenerateTidComponent_Template_button_click_79_listener() {
              return ctx.printAndSavePackage();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](80, " PRINT & SAVE PACKAGE MANIFEST ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](81, "button", 40);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function GenerateTidComponent_Template_button_click_81_listener() {
              return ctx.initPackages();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](82, " CANCEL ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](83, "div", 41);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](84, "div", 42);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](85, "h5", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](86, " CHECK IN AUDIT TABLE ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](87, "span", 43);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](88);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](89, "span", 43);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](90);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](91, "span", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](92);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](93, "div", 44);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](94, "div", 45);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](95, "div", 46);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](96, "div", 47);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](97, "div", 48);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](98, "div", 49);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](99, "div", 50);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](100, "table", 51);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](101, "thead", 52);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](102, "tr");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](103, GenerateTidComponent_th_103_Template, 2, 1, "th", 53);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](104, GenerateTidComponent_tr_104_Template, 19, 12, "tr", 54);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](105, "tbody");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.showSpinner);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("Business Unit: ", ctx.packageFound.Client_Name, "");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.checked);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.bailee);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.multipleLendersActive);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.showCamera);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.exceptionNeeded);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.loanManifest);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("hidden", ctx.checked && ctx.loanManifest);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.loanNumber);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("hidden", ctx.checked && ctx.loanManifest);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.borrowerName);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.multipleLendersActive);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.multipleLendersActive);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.exceptionNeeded);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.multipleLendersActive);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.multipleLendersActive);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" EXPECTED LENDERS: ", ctx.totalLenders, " ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" TOTAL LENDERS: ", ctx.lenderCounter, " ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" TOTAL ITEMS CHECKED-IN: ", ctx.totalItems, " ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.headers);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.TIDList);
          }
        },
        directives: [_angular_common__WEBPACK_IMPORTED_MODULE_18__["NgIf"], _angular_forms__WEBPACK_IMPORTED_MODULE_13__["CheckboxControlValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_13__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_13__["NgModel"], _angular_forms__WEBPACK_IMPORTED_MODULE_13__["DefaultValueAccessor"], _angular_material_button__WEBPACK_IMPORTED_MODULE_19__["MatButton"], _angular_common__WEBPACK_IMPORTED_MODULE_18__["NgForOf"], _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_20__["MatSpinner"], _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_21__["MatAutocompleteTrigger"], _angular_forms__WEBPACK_IMPORTED_MODULE_13__["FormControlDirective"], _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_21__["MatAutocomplete"], _angular_material_core__WEBPACK_IMPORTED_MODULE_22__["MatOption"], _angular_forms__WEBPACK_IMPORTED_MODULE_13__["MaxLengthValidator"], _angular_forms__WEBPACK_IMPORTED_MODULE_13__["NgSelectOption"], _angular_forms__WEBPACK_IMPORTED_MODULE_13__["ɵangular_packages_forms_forms_z"], _angular_common__WEBPACK_IMPORTED_MODULE_18__["NgClass"]],
        pipes: [_angular_common__WEBPACK_IMPORTED_MODULE_18__["AsyncPipe"], _bailee_transformer_pipe__WEBPACK_IMPORTED_MODULE_23__["BaileeTransformerPipe"]],
        styles: [".card-header[_ngcontent-%COMP%] {\r\n  background-color: #031d44;\r\n  color: white;\r\n}\r\n\r\ntable[_ngcontent-%COMP%] {\r\n  table-layout: fixed;\r\n  width: 100%;\r\n  border-style: outset;\r\n}\r\n\r\nth[_ngcontent-%COMP%] {\r\n  text-align: center;\r\n  vertical-align: middle;\r\n\r\n  width: 12.5%;\r\n  background-color: #031d44;\r\n  color: white;\r\n  font-family: Arial, Helvetica, sans-serif;\r\n}\r\n\r\n  .my-class1 .modal-dialog {\r\n  max-width: 80%;\r\n  width: 80%;\r\n}\r\n\r\nbody.swal2-shown[_ngcontent-%COMP%]    > [aria-hidden=\"true\"][_ngcontent-%COMP%] {\r\n  transition: 0.1s filter;\r\n  filter: blur(10px);\r\n}\r\n\r\n.scroll[_ngcontent-%COMP%] {\r\n  height: 100px;\r\n  overflow-y: auto;\r\n}\r\n\r\n.my-class2[_ngcontent-%COMP%] {\r\n  max-width: 80%;\r\n  width: 80%;\r\n}\r\n\r\n.custom-dialog-container[_ngcontent-%COMP%] {\r\n  \r\n  padding: 10px -10px -10px -3px;\r\n  overflow-x: hidden;\r\n  max-width: 100vw !important;\r\n  top: -3vw;\r\n  max-height: 85vw !important;\r\n  height: auto;\r\n  overflow: hidden;\r\n  position: relative;\r\n}\r\n\r\n.custom-dialog-container[_ngcontent-%COMP%] {\r\n  \r\n  padding: 10px -10px -10px -3px;\r\n  overflow-x: hidden;\r\n  max-width: 100vw !important;\r\n  width: 25vw;\r\n  top: -3vw;\r\n  max-height: 85vw !important;\r\n  height: auto;\r\n  overflow: hidden;\r\n  position: relative;\r\n}\r\n\r\ntd[_ngcontent-%COMP%] {\r\n  text-align: center;\r\n  vertical-align: middle;\r\n}\r\n\r\ninput[_ngcontent-%COMP%]:focus {\r\n  border: 5px solid green;\r\n}\r\n\r\nselect[_ngcontent-%COMP%]:focus {\r\n  border: 5px solid green;\r\n}\r\n\r\ncheckbox[_ngcontent-%COMP%]:focus {\r\n  border: 5px solid green;\r\n}\r\n\r\nhr[_ngcontent-%COMP%] {\r\n  border: 0;\r\n  height: 1px;\r\n  background-image: linear-gradient(\r\n    to right,\r\n    rgba(0, 0, 0, 0),\r\n    rgba(0, 0, 0, 0.75),\r\n    rgba(0, 0, 0, 0)\r\n  );\r\n}\r\n\r\nth[_ngcontent-%COMP%] {\r\n  width: 15%;\r\n}\r\n\r\n#checkbox1[_ngcontent-%COMP%], #checkbox2[_ngcontent-%COMP%] {\r\n  margin-top: 1%;\r\n  text-align: center;\r\n  padding-bottom: 10px;\r\n  padding-left: 10%;\r\n}\r\n\r\n#botonSplit[_ngcontent-%COMP%] {\r\n  color: white;\r\n  margin-left: 5%;\r\n  width: 40%;\r\n  margin-bottom: 5%;\r\n  margin-right: 5%;\r\n}\r\n\r\nbutton[_ngcontent-%COMP%] {\r\n  background-color: #023e7d;\r\n  color: white;\r\n  margin-left: 5%;\r\n  margin-bottom: 5%;\r\n  margin-right: 5%;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImdlbmVyYXRlLXRpZC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UseUJBQXlCO0VBQ3pCLFlBQVk7QUFDZDs7QUFFQTtFQUNFLG1CQUFtQjtFQUNuQixXQUFXO0VBQ1gsb0JBQW9CO0FBQ3RCOztBQUNBO0VBQ0Usa0JBQWtCO0VBQ2xCLHNCQUFzQjs7RUFFdEIsWUFBWTtFQUNaLHlCQUF5QjtFQUN6QixZQUFZO0VBQ1oseUNBQXlDO0FBQzNDOztBQUVBO0VBQ0UsY0FBYztFQUNkLFVBQVU7QUFDWjs7QUFFQTtFQUNFLHVCQUF1QjtFQUN2QixrQkFBa0I7QUFDcEI7O0FBRUE7RUFDRSxhQUFhO0VBQ2IsZ0JBQWdCO0FBQ2xCOztBQUVBO0VBQ0UsY0FBYztFQUNkLFVBQVU7QUFDWjs7QUFFQTtFQUNFLG9CQUFvQjtFQUNwQiw4QkFBOEI7RUFDOUIsa0JBQWtCO0VBQ2xCLDJCQUEyQjtFQUMzQixTQUFTO0VBQ1QsMkJBQTJCO0VBQzNCLFlBQVk7RUFDWixnQkFBZ0I7RUFDaEIsa0JBQWtCO0FBQ3BCOztBQUVBO0VBQ0Usb0JBQW9CO0VBQ3BCLDhCQUE4QjtFQUM5QixrQkFBa0I7RUFDbEIsMkJBQTJCO0VBQzNCLFdBQVc7RUFDWCxTQUFTO0VBQ1QsMkJBQTJCO0VBQzNCLFlBQVk7RUFDWixnQkFBZ0I7RUFDaEIsa0JBQWtCO0FBQ3BCOztBQUVBO0VBQ0Usa0JBQWtCO0VBQ2xCLHNCQUFzQjtBQUN4Qjs7QUFFQTtFQUNFLHVCQUF1QjtBQUN6Qjs7QUFDQTtFQUNFLHVCQUF1QjtBQUN6Qjs7QUFDQTtFQUNFLHVCQUF1QjtBQUN6Qjs7QUFDQTtFQUNFLFNBQVM7RUFDVCxXQUFXO0VBQ1g7Ozs7O0dBS0M7QUFDSDs7QUFDQTtFQUNFLFVBQVU7QUFDWjs7QUFDQTs7RUFFRSxjQUFjO0VBQ2Qsa0JBQWtCO0VBQ2xCLG9CQUFvQjtFQUNwQixpQkFBaUI7QUFDbkI7O0FBRUE7RUFDRSxZQUFZO0VBQ1osZUFBZTtFQUNmLFVBQVU7RUFDVixpQkFBaUI7RUFDakIsZ0JBQWdCO0FBQ2xCOztBQUVBO0VBQ0UseUJBQXlCO0VBQ3pCLFlBQVk7RUFDWixlQUFlO0VBQ2YsaUJBQWlCO0VBQ2pCLGdCQUFnQjtBQUNsQiIsImZpbGUiOiJnZW5lcmF0ZS10aWQuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jYXJkLWhlYWRlciB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzAzMWQ0NDtcclxuICBjb2xvcjogd2hpdGU7XHJcbn1cclxuXHJcbnRhYmxlIHtcclxuICB0YWJsZS1sYXlvdXQ6IGZpeGVkO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGJvcmRlci1zdHlsZTogb3V0c2V0O1xyXG59XHJcbnRoIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcclxuXHJcbiAgd2lkdGg6IDEyLjUlO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMwMzFkNDQ7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gIGZvbnQtZmFtaWx5OiBBcmlhbCwgSGVsdmV0aWNhLCBzYW5zLXNlcmlmO1xyXG59XHJcblxyXG46Om5nLWRlZXAgLm15LWNsYXNzMSAubW9kYWwtZGlhbG9nIHtcclxuICBtYXgtd2lkdGg6IDgwJTtcclxuICB3aWR0aDogODAlO1xyXG59XHJcblxyXG5ib2R5LnN3YWwyLXNob3duID4gW2FyaWEtaGlkZGVuPVwidHJ1ZVwiXSB7XHJcbiAgdHJhbnNpdGlvbjogMC4xcyBmaWx0ZXI7XHJcbiAgZmlsdGVyOiBibHVyKDEwcHgpO1xyXG59XHJcblxyXG4uc2Nyb2xsIHtcclxuICBoZWlnaHQ6IDEwMHB4O1xyXG4gIG92ZXJmbG93LXk6IGF1dG87XHJcbn1cclxuXHJcbi5teS1jbGFzczIge1xyXG4gIG1heC13aWR0aDogODAlO1xyXG4gIHdpZHRoOiA4MCU7XHJcbn1cclxuXHJcbi5jdXN0b20tZGlhbG9nLWNvbnRhaW5lciB7XHJcbiAgLyogYWRkIHlvdXIgc3R5bGVzICovXHJcbiAgcGFkZGluZzogMTBweCAtMTBweCAtMTBweCAtM3B4O1xyXG4gIG92ZXJmbG93LXg6IGhpZGRlbjtcclxuICBtYXgtd2lkdGg6IDEwMHZ3ICFpbXBvcnRhbnQ7XHJcbiAgdG9wOiAtM3Z3O1xyXG4gIG1heC1oZWlnaHQ6IDg1dncgIWltcG9ydGFudDtcclxuICBoZWlnaHQ6IGF1dG87XHJcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbn1cclxuXHJcbi5jdXN0b20tZGlhbG9nLWNvbnRhaW5lciB7XHJcbiAgLyogYWRkIHlvdXIgc3R5bGVzICovXHJcbiAgcGFkZGluZzogMTBweCAtMTBweCAtMTBweCAtM3B4O1xyXG4gIG92ZXJmbG93LXg6IGhpZGRlbjtcclxuICBtYXgtd2lkdGg6IDEwMHZ3ICFpbXBvcnRhbnQ7XHJcbiAgd2lkdGg6IDI1dnc7XHJcbiAgdG9wOiAtM3Z3O1xyXG4gIG1heC1oZWlnaHQ6IDg1dncgIWltcG9ydGFudDtcclxuICBoZWlnaHQ6IGF1dG87XHJcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbn1cclxuXHJcbnRkIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcclxufVxyXG5cclxuaW5wdXQ6Zm9jdXMge1xyXG4gIGJvcmRlcjogNXB4IHNvbGlkIGdyZWVuO1xyXG59XHJcbnNlbGVjdDpmb2N1cyB7XHJcbiAgYm9yZGVyOiA1cHggc29saWQgZ3JlZW47XHJcbn1cclxuY2hlY2tib3g6Zm9jdXMge1xyXG4gIGJvcmRlcjogNXB4IHNvbGlkIGdyZWVuO1xyXG59XHJcbmhyIHtcclxuICBib3JkZXI6IDA7XHJcbiAgaGVpZ2h0OiAxcHg7XHJcbiAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KFxyXG4gICAgdG8gcmlnaHQsXHJcbiAgICByZ2JhKDAsIDAsIDAsIDApLFxyXG4gICAgcmdiYSgwLCAwLCAwLCAwLjc1KSxcclxuICAgIHJnYmEoMCwgMCwgMCwgMClcclxuICApO1xyXG59XHJcbnRoIHtcclxuICB3aWR0aDogMTUlO1xyXG59XHJcbiNjaGVja2JveDEsXHJcbiNjaGVja2JveDIge1xyXG4gIG1hcmdpbi10b3A6IDElO1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBwYWRkaW5nLWJvdHRvbTogMTBweDtcclxuICBwYWRkaW5nLWxlZnQ6IDEwJTtcclxufVxyXG5cclxuI2JvdG9uU3BsaXQge1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICBtYXJnaW4tbGVmdDogNSU7XHJcbiAgd2lkdGg6IDQwJTtcclxuICBtYXJnaW4tYm90dG9tOiA1JTtcclxuICBtYXJnaW4tcmlnaHQ6IDUlO1xyXG59XHJcblxyXG5idXR0b24ge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMwMjNlN2Q7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gIG1hcmdpbi1sZWZ0OiA1JTtcclxuICBtYXJnaW4tYm90dG9tOiA1JTtcclxuICBtYXJnaW4tcmlnaHQ6IDUlO1xyXG59XHJcbiJdfQ== */"]
      });
      /***/
    },

    /***/
    "XIe5":
    /*!***************************************!*\
      !*** ./src/app/pages/pages.routes.ts ***!
      \***************************************/

    /*! exports provided: PAGES_ROUTES */

    /***/
    function XIe5(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PAGES_ROUTES", function () {
        return PAGES_ROUTES;
      });
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _admin_admin_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./admin/admin.module */
      "GCp2");
      /* harmony import */


      var _manifest_manifest_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./manifest/manifest.module */
      "BoU+");
      /* harmony import */


      var _workflow_workflow_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./workflow/workflow.module */
      "86tH");
      /* harmony import */


      var _reports_reports_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./reports/reports.component */
      "qzPY");
      /* harmony import */


      var _tid_creation_tid_creation_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./tid-creation/tid-creation.module */
      "z76o");
      /* harmony import */


      var _imagelookup_imagelookup_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./imagelookup/imagelookup.module */
      "OcXF");
      /* harmony import */


      var _certification_certification_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ./certification/certification.module */
      "bhVg"); // Pages
      // Module
      //import { TidCreationComponent } from './tid-creation/tid-creation.component';


      var routes = [{
        path: 'admin',
        loadChildren: function loadChildren() {
          return _admin_admin_module__WEBPACK_IMPORTED_MODULE_1__["AdminModule"];
        }
      }, {
        path: 'manifest',
        loadChildren: function loadChildren() {
          return _manifest_manifest_module__WEBPACK_IMPORTED_MODULE_2__["ManifestModule"];
        }
      }, {
        path: 'workflow',
        loadChildren: function loadChildren() {
          return _workflow_workflow_module__WEBPACK_IMPORTED_MODULE_3__["WorkflowModule"];
        }
      }, {
        path: 'reports',
        component: _reports_reports_component__WEBPACK_IMPORTED_MODULE_4__["ReportsComponent"]
      }, {
        path: 'tidCreation',
        loadChildren: function loadChildren() {
          return _tid_creation_tid_creation_module__WEBPACK_IMPORTED_MODULE_5__["TidCreationModule"];
        }
      }, {
        path: 'imageLookup',
        loadChildren: function loadChildren() {
          return _imagelookup_imagelookup_module__WEBPACK_IMPORTED_MODULE_6__["ImagelookupModule"];
        }
      }, {
        path: 'certification',
        loadChildren: function loadChildren() {
          return _certification_certification_module__WEBPACK_IMPORTED_MODULE_7__["CertificationModule"];
        }
      }];

      var PAGES_ROUTES = _angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"].forChild(routes);
      /***/

    },

    /***/
    "ZAI4":
    /*!*******************************!*\
      !*** ./src/app/app.module.ts ***!
      \*******************************/

    /*! exports provided: AppModule */

    /***/
    function ZAI4(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AppModule", function () {
        return AppModule;
      });
      /* harmony import */


      var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/platform-browser */
      "jhN1");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _app_routes__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./app.routes */
      "RUEf");
      /* harmony import */


      var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./app.component */
      "Sy1n");
      /* harmony import */


      var _login_login_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./login/login.component */
      "vtpD");
      /* harmony import */


      var _pages_pages_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./pages/pages.component */
      "8D7W");
      /* harmony import */


      var _shared_shared_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./shared/shared.module */
      "PCNd");
      /* harmony import */


      var _services_service_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ./services/service.module */
      "0Cwu");
      /* harmony import */


      var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! @ng-bootstrap/ng-bootstrap */
      "1kSV");
      /* harmony import */


      var ngx_webcam__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! ngx-webcam */
      "QKVY");
      /* harmony import */


      var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! @angular/platform-browser/animations */
      "R1ws");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
      /*! @angular/router */
      "tyNb"); // forms
      // Routes
      // Components
      // Modules


      var AppModule = function AppModule() {
        _classCallCheck(this, AppModule);
      };

      AppModule.ɵfac = function AppModule_Factory(t) {
        return new (t || AppModule)();
      };

      AppModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_11__["ɵɵdefineNgModule"]({
        type: AppModule,
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
      });
      AppModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_11__["ɵɵdefineInjector"]({
        providers: [],
        imports: [[_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"], _app_routes__WEBPACK_IMPORTED_MODULE_2__["APP_ROUTES"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ReactiveFormsModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_6__["SharedModule"], _services_service_module__WEBPACK_IMPORTED_MODULE_7__["ServiceModule"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_8__["NgbModule"], ngx_webcam__WEBPACK_IMPORTED_MODULE_9__["WebcamModule"], _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_10__["BrowserAnimationsModule"]]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_11__["ɵɵsetNgModuleScope"](AppModule, {
          declarations: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"], _login_login_component__WEBPACK_IMPORTED_MODULE_4__["LoginComponent"], _pages_pages_component__WEBPACK_IMPORTED_MODULE_5__["PagesComponent"]],
          imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"], _angular_router__WEBPACK_IMPORTED_MODULE_12__["RouterModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ReactiveFormsModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_6__["SharedModule"], _services_service_module__WEBPACK_IMPORTED_MODULE_7__["ServiceModule"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_8__["NgbModule"], ngx_webcam__WEBPACK_IMPORTED_MODULE_9__["WebcamModule"], _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_10__["BrowserAnimationsModule"]]
        });
      })();
      /***/

    },

    /***/
    "adZh":
    /*!***************************************************!*\
      !*** ./src/app/pages/workflow/workflow.routes.ts ***!
      \***************************************************/

    /*! exports provided: WORKFLOW_ROUTES */

    /***/
    function adZh(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "WORKFLOW_ROUTES", function () {
        return WORKFLOW_ROUTES;
      });
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _pidcheck_pidcheck_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./pidcheck/pidcheck.component */
      "jaMP");

      var routes = [{
        path: 'PIDCheck',
        component: _pidcheck_pidcheck_component__WEBPACK_IMPORTED_MODULE_1__["PidcheckComponent"],
        data: {
          title: 'PID Check In'
        }
      }];

      var WORKFLOW_ROUTES = _angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"].forChild(routes);
      /***/

    },

    /***/
    "bark":
    /*!*********************************************************************!*\
      !*** ./src/app/pages/manifest/edit-remove/edit-remove.component.ts ***!
      \*********************************************************************/

    /*! exports provided: EditRemoveComponent */

    /***/
    function bark(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "EditRemoveComponent", function () {
        return EditRemoveComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! sweetalert2/dist/sweetalert2.js */
      "PdH4");
      /* harmony import */


      var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1__);
      /* harmony import */


      var jquery__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! jquery */
      "EVdn");
      /* harmony import */


      var jquery__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_2__);
      /* harmony import */


      var src_app_services_packages_add_packages_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/app/services/packages/add-packages.service */
      "wlS1");
      /* harmony import */


      var src_app_services_packages_worflow_rel_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! src/app/services/packages/worflow-rel.service */
      "H7Gl");
      /* harmony import */


      var src_app_services_packages_worflow_task_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! src/app/services/packages/worflow-task.service */
      "dSTU");
      /* harmony import */


      var src_app_services_packages_worflow_status_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! src/app/services/packages/worflow-status.service */
      "/a8Q");
      /* harmony import */


      var src_app_services_packages_client_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/services/packages/client.service */
      "SagX");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var src_app_services_packages_audit_trail_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! src/app/services/packages/audit-trail.service */
      "irGt");
      /* harmony import */


      var src_app_services_tid_tidcreartion_service_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! src/app/services/tid/tidcreartion-service.service */
      "eHNn");
      /* harmony import */


      var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! @ng-bootstrap/ng-bootstrap */
      "1kSV");
      /* harmony import */


      var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
      /*! @angular/platform-browser */
      "jhN1");
      /* harmony import */


      var _tid_creation_image_display_image_display_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
      /*! ../../tid-creation/image-display/image-display.component */
      "ev3G");
      /* harmony import */


      var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
      /*! @angular/material/dialog */
      "0IaG");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(
      /*! @angular/material/progress-spinner */
      "Xa2L");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");

      var _c0 = ["tracking"];
      var _c1 = ["secondInput"];
      var _c2 = ["carrierInput"];
      var _c3 = ["notes"];
      var _c4 = ["binNumber"];
      var _c5 = ["audioOption"];

      function EditRemoveComponent_mat_spinner_6_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "mat-spinner", 36);
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("diameter", 20);
        }
      }

      function EditRemoveComponent_option_25_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "option");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ex_r11 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ex_r11);
        }
      }

      function EditRemoveComponent_option_33_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "option");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ex_r12 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ex_r12);
        }
      }

      function EditRemoveComponent_th_77_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "th", 37);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var column_r13 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](column_r13);
        }
      }

      function EditRemoveComponent_tr_78_Template(rf, ctx) {
        if (rf & 1) {
          var _r16 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tr");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "td");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "td", 38);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "td");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "td");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "td");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "td");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "button", 39);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function EditRemoveComponent_tr_78_Template_button_click_12_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r16);

            var row_r14 = ctx.$implicit;

            var ctx_r15 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r15.deleteRecordTID(row_r14, ctx_r15.index);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "X");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var row_r14 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", row_r14.TID_ID, " ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", row_r14.BarcodeSticker, " ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", row_r14.LoanOnManifest, " ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", row_r14.ExceptionReason, " ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", row_r14.ExceptionNotes, " ");
        }
      }

      var EditRemoveComponent = /*#__PURE__*/function () {
        function EditRemoveComponent(packageService, workfNamesService, auditService, taskService, statusService, clientService, _tidService, modalService, _sanitizer, _dialog, router) {
          _classCallCheck(this, EditRemoveComponent);

          this.packageService = packageService;
          this.workfNamesService = workfNamesService;
          this.auditService = auditService;
          this.taskService = taskService;
          this.statusService = statusService;
          this.clientService = clientService;
          this._tidService = _tidService;
          this.modalService = modalService;
          this._sanitizer = _sanitizer;
          this._dialog = _dialog;
          this.router = router; //static variables

          this.clientArray = ["", "Fannie", "Freddie", "Non Agency New Collateral", "Non Agency T-Docs", "NIGO", "GNMA T-docs", "Fannie T-docs", "Freddie T-docs", "Non-production", "CMC", "SLS(SL03)", "Pingora", "Reinstatements", "Resolution Team", "Recertification", "NRZM"];
          this.headers = ["TID", "Barcode Sticker", "Loan on Manifest", "Exception Reason", "Exception Notes", "Remove"];
          this.carrierArray = ["", "FEDEX", "USPS", "UPS", "DHL", "FedEx Ground", "Hand", "IM UPS", "IM FedEx"];
        }

        _createClass(EditRemoveComponent, [{
          key: "ngAfterViewInit",
          value: function ngAfterViewInit() {// this.mainInput.nativeElement.focus();
          }
        }, {
          key: "ngOnInit",
          value: function ngOnInit() {
            this.initPackFound();
            this.userlogged = JSON.parse(localStorage.getItem("user"));

            if (this.userlogged.Admin_Flag == true) {
              //console.log('User logged: ' + this.userlogged.User_Login);
              this.mainInput.nativeElement.focus();
            } else {
              sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default()("Access Denied", "User is not an admin", "error");
              this.router.navigate(["/login"]);
            }
          } // end oninit

        }, {
          key: "mainSearch",
          value: function mainSearch(event) {
            var _this57 = this;

            setTimeout(function () {
              _this57.carrierInput.nativeElement.value = "";
              _this57.binInput.nativeElement.value = "";
              _this57.notesInput.nativeElement.value = "";
              _this57.secondInput.nativeElement.value = "";
              _this57.packageArray = [];
              _this57.inputReader = event.target.value;
              _this57.inputReader = _this57.inputReader.toLowerCase();
              _this57.inputReader = _this57.inputReader.replace(/\s/g, "");

              if (_this57.inputReader.startsWith("pid-")) {
                _this57.inputReader = _this57.inputReader.replace("pid-", "");

                _this57.usingPID(_this57.inputReader);
              } else {
                _this57.inputReader = _this57.inputReader.toLowerCase();

                _this57.usingTracking(_this57.inputReader);
              }
            }, 0); //end set timeout
          }
        }, {
          key: "goToBin",
          value: function goToBin() {
            this.mainInput.nativeElement.value = "";
            this.mainInput.nativeElement.focus();
          }
        }, {
          key: "goToBusinessUnit",
          value: function goToBusinessUnit() {
            this.notesInput.nativeElement.focus(); // this.notesInput.nativeElement.value = '';
          }
        }, {
          key: "sendToNotesField",
          value: function sendToNotesField(event) {
            console.log(event.target.value);
            this.clientSelected = event.target.value;
            this.packFound.Client_Name = this.clientSelected;
            this.binInput.nativeElement.focus(); //this.binInput.nativeElement.value = '';
          }
        }, {
          key: "sendToCarrier",
          value: function sendToCarrier(event) {
            this.carrierSelected = event.target.value;
            this.packFound.Carrier = this.carrierSelected;
            this.secondInput.nativeElement.focus();
            console.log(this.carrierSelected);
          } //BACKEND CALLS

        }, {
          key: "usingTracking",
          value: function usingTracking(tracking) {
            var _this58 = this;

            this.packageService.findOneByIdBC(tracking).subscribe(function (data) {
              _this58.packFound = data;
              _this58.holdingPID = data.Package_Tracking_ID;
              var smallPack = {
                Available: data.Available,
                BaileeLetter: data.BaileeLetter,
                Client_Name: data.Client_Name,
                Creation_Date_Time: data.Creation_Date_Time,
                Exception_Flag: data.Exception_Flag,
                Exception_Name: data.Exception_Name,
                LoanNumber: data.LoanNumber,
                Location_Name: data.Location_Name,
                ManifestInBox: data.ManifestInBox,
                totalLenders: data.totalLenders,
                Package_Finished: data.Package_Finished,
                clearPackagesFlag: data.clearPackagesFlag,
                Package_Tracking_BC: data.Package_Tracking_BC,
                Package_Tracking_ID: data.Package_Tracking_ID,
                Status_ID: data.Status_ID,
                Sticker_ID: data.Sticker_ID,
                Task_ID: data.Task_ID,
                WorkFlow_ID: data.WorkFlow_ID,
                Carrier: data.Carrier,
                multipleLenders: data.multipleLenders,
                Package_Notes: data.Package_Notes,
                Bin_Number: data.Bin_Number,
                clearPackagesDateTime: data.clearPackagesDateTime,
                lastModule: data.lastModule,
                lastFunction: data.lastFunction,
                loanTotal: data.loanTotal,
                loansExpected: data.loansExpected
              };
              _this58.carrierInput.nativeElement.value = data.Carrier;
              _this58.binInput.nativeElement.value = data.Bin_Number;
              _this58.notesInput.nativeElement.value = data.Package_Notes;
              _this58.secondInput.nativeElement.value = data.Client_Name;
              _this58.holdingNotes = data.Package_Notes;
              console.log("PACAKGE NOTES", data.Package_Notes);

              if (data.Client_Name != "" && _this58.clientSelected == "") {
                console.log("clientselected empty and client found");
                smallPack = data;
                smallPack.Client_Name = data.Client_Name;
                smallPack.Bin_Number = data.Bin_Number; //this.printPackage.push(data);
              } else if (data.Client_Name != "" && _this58.clientSelected != "") {
                console.log("selected empty client brought ", data.Client_Name);
                console.log("client selected", _this58.clientSelected);
                smallPack = data;
                smallPack.Client_Name = _this58.clientSelected;
              }

              if (data.Carrier != "" && _this58.carrierSelected != "") {
                smallPack.Carrier = _this58.carrierSelected;
              } else if (data.Carrier != "" && _this58.carrierSelected == "") {
                smallPack.Carrier = data.Carrier;
              }

              _this58.printPackage.push(smallPack);

              _this58.getAllTIDbyPid(data.Package_Tracking_ID);

              _this58.carrierInput.nativeElement.focus();
            }, function (error) {
              _this58.tidList = [];

              _this58.mainInput.nativeElement.focus();

              console.log("PLAYING SOUND NOW");
              jquery__WEBPACK_IMPORTED_MODULE_2__("#audio").html('<iframe src="/BNYMTrackingTool/assets/Windows 10 Error Sound.mp3" allow="autoplay" style="display:none" id="iframeAudio"></iframe>');
              sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
                position: "top-end",
                type: "error",
                title: "",
                text: "Package not Found",
                showConfirmButton: true,
                footer: "",
                timer: 900
              });
            });
            this.mainInput.nativeElement.value = "";
            this.mainInput.nativeElement.focus();
          } // end using tracking

        }, {
          key: "usingPID",
          value: function usingPID(pid) {
            var _this59 = this;

            this.packageService.findOneByPID(+pid).subscribe(function (data) {
              console.log("package brought with pid ", data);
              _this59.packFound = data;
              var smallPack = {
                Available: data.Available,
                BaileeLetter: data.BaileeLetter,
                Client_Name: data.Client_Name,
                Creation_Date_Time: data.Creation_Date_Time,
                Exception_Flag: data.Exception_Flag,
                Exception_Name: data.Exception_Name,
                LoanNumber: data.LoanNumber,
                Location_Name: data.Location_Name,
                ManifestInBox: data.ManifestInBox,
                Package_Finished: data.Package_Finished,
                Package_Tracking_BC: data.Package_Tracking_BC,
                Package_Tracking_ID: data.Package_Tracking_ID,
                Status_ID: data.Status_ID,
                totalLenders: data.totalLenders,
                Sticker_ID: data.Sticker_ID,
                Task_ID: data.Task_ID,
                WorkFlow_ID: data.WorkFlow_ID,
                Carrier: data.Carrier,
                multipleLenders: data.multipleLenders,
                clearPackagesFlag: data.clearPackagesFlag,
                Package_Notes: data.Package_Notes,
                Bin_Number: data.Bin_Number,
                clearPackagesDateTime: data.clearPackagesDateTime,
                lastModule: data.lastModule,
                lastFunction: data.lastFunction,
                loanTotal: data.loanTotal,
                loansExpected: data.loansExpected
              };
              _this59.carrierInput.nativeElement.value = data.Carrier;
              _this59.binInput.nativeElement.value = data.Bin_Number;
              _this59.notesInput.nativeElement.value = data.Package_Notes;
              _this59.secondInput.nativeElement.value = data.Client_Name;
              _this59.holdingNotes = data.Package_Notes;

              if (data.Client_Name != "" && _this59.clientSelected == "") {
                console.log("clientselected empty and client found");
                smallPack = data;
                smallPack.Client_Name = data.Client_Name;
                smallPack.Bin_Number = data.Bin_Number; //this.printPackage.push(data);
              } else if (data.Client_Name != "" && _this59.clientSelected != "") {
                console.log("selected empty client brought ", data.Client_Name);
                console.log("client selected", _this59.clientSelected);
                smallPack = data;
                smallPack.Client_Name = _this59.clientSelected;
              }

              smallPack.Carrier = _this59.carrierSelected;

              _this59.printPackage.push(smallPack); // this.printPackage.sort((a,b) => { return a.Creation_Date_Time > b.Creation_Date_Time ? -1 : b.Creation_Date_Time <  a.Creation_Date_Time ? 1 : 0 });
              // this.printPackage.reverse();


              _this59.totalItems = _this59.printPackage.length;

              _this59.getAllTIDbyPid(data.Package_Tracking_ID);

              _this59.carrierInput.nativeElement.focus();

              _this59.mainInput.nativeElement.value = "";
            }, function (error) {
              var sound = new Audio();
              sound.src = "/BNYMTrackingTool/assets/Windows 10 Error Sound.mp3";
              sound.load();
              var promise = sound.play();

              if (promise !== undefined) {
                // On older browsers play() does not return anything, so the value would be undefined.
                promise.then(function () {// Audio is playing.
                })["catch"](function (error) {
                  console.log(error);
                });
              }

              sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
                position: "top-end",
                type: "error",
                title: "",
                text: "Package not found!",
                showConfirmButton: true,
                footer: "",
                timer: 1000
              });
            });
          } //BUTTON METHODS

        }, {
          key: "saveAndPrint",
          value: function saveAndPrint() {
            var _this60 = this;

            var sendpackage = this.packFound;
            sendpackage.Carrier = this.carrierInput.nativeElement.value;
            sendpackage.Bin_Number = this.binInput.nativeElement.value;
            sendpackage.Package_Notes = this.notesInput.nativeElement.value;
            sendpackage.Client_Name = this.secondInput.nativeElement.value;

            if (this.binInput.nativeElement.value != "") {
              sendpackage.Bin_Number = this.binInput.nativeElement.value;
            }

            if (this.notesInput.nativeElement.value != "") {
              sendpackage.Package_Notes = this.notesInput.nativeElement.value;
            }

            this.packageService.updatePackUpdateModule(sendpackage).subscribe(function (data) {
              console.log(data);
              _this60.holdingPID = data.Package_Tracking_ID; // this.onCreateCoverSheet(this.printPackage);

              sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
                position: "top-end",
                type: "success",
                title: "",
                text: "Package saved",
                showConfirmButton: true,
                footer: "",
                timer: 1100
              });
            });
            this.audit = {
              PID: this.packFound.Package_Tracking_ID,
              TID: "",
              ActionName: "Update Packages",
              ActionDateTime: null,
              BusinessUnit: this.packFound.Client_Name,
              Sticker: "",
              User_ID: this.userlogged.User_Name
            }; //insert audit record

            this.auditService.saveAudit(this.audit).subscribe(function (data) {
              console.log(data);
            });
            this.initPackFound2();
            this.mainInput.nativeElement.value = "";
            this.mainInput.nativeElement.focus(); //this.binInput.nativeElement.value= '';

            this.notesInput.nativeElement.value = "";
          } //end save n print

        }, {
          key: "enterSelect",
          value: function enterSelect() {
            console.log("SELECTED VALUE", this.secondInput.nativeElement.value);
            this.mainInput.nativeElement.value = "";
            this.mainInput.nativeElement.focus();
          }
        }, {
          key: "tryingToDisable",
          value: function tryingToDisable(packageRow) {
            var _this61 = this;

            //find all tids from that package
            this._tidService.getAllbyPid(packageRow.Package_Tracking_ID).subscribe(function (data) {
              if (data.length > 0) {
                //console.log("datas tids", data);
                //console.log(this.userlogged.Admin_Flag , "admin");
                if (_this61.userlogged.Admin_Flag == true) {
                  sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
                    title: "Are you sure?",
                    text: "Do you wish to disable package PID-" + packageRow.Package_Tracking_ID,
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    confirmButtonText: "Yes, disable it!"
                  }).then(function (result) {
                    if (result.value == true) {
                      console.log("Deleting package ", packageRow);

                      _this61.onDisable(packageRow);
                    }
                  }); //end result
                } //end admin flag
                else {
                    //meaning not an admin
                    var sound = new Audio();
                    sound.src = "src/assets/Windows 10 Error Sound.mp3";
                    sound.load();
                    var promise = sound.play();

                    if (promise !== undefined) {
                      // On older browsers play() does not return anything, so the value would be undefined.
                      promise.then(function () {// Audio is playing.
                      })["catch"](function (error) {
                        console.log(error);
                      });
                    }

                    sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default()("Error Deleting", "Package contains TIDs, can only be deleted by user with Admin rights.", "error");
                  }
              } //end if package had tids

            }, function (error) {
              var sound = new Audio();
              sound.src = "src/assets/Windows 10 Error Sound.mp3";
              sound.load();
              var promise = sound.play();

              if (promise !== undefined) {
                // On older browsers play() does not return anything, so the value would be undefined.
                promise.then(function () {// Audio is playing.
                })["catch"](function (error) {
                  console.log(error);
                });
              }

              console.log("NO TIDS FOUND", error);

              _this61.onDisable(packageRow);
            }); //end observable

          } //end trying to disable

        }, {
          key: "bulkSavePrint",
          value: function bulkSavePrint() {
            var _this62 = this;

            console.log("Saving in Bulk");
            this.printPackage.forEach(function (element) {
              _this62.packageService.updatePack(element).subscribe(function (data) {
                console.log(data);
              }, function (error) {
                sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
                  position: "top-end",
                  type: "error",
                  title: "",
                  text: "Error saving in bulk package " + element.Package_Tracking_BC,
                  showConfirmButton: true,
                  footer: "",
                  timer: 600
                });
              });
            });
            console.log("Bulk print", this.printPackage);
            this.onCreateCoverSheetBulk(this.printPackage);
            this.mainInput.nativeElement.value = "";
            this.mainInput.nativeElement.focus();
            this.secondInput.nativeElement.value = "";
          }
        }, {
          key: "onDisable",
          value: function onDisable(pack) {
            var _this63 = this;

            console.log(pack);
            console.log("DELETING");
            this.packageService.asyncDISABLERequest(pack).subscribe(function (res) {
              if (res == "Package updated Successful.") {
                sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
                  position: "top-end",
                  type: "success",
                  title: "You have disabled PID-" + pack.Package_Tracking_ID,
                  showConfirmButton: false,
                  timer: 1500
                });
                _this63.printPackage = _this63.printPackage.filter(function (item) {
                  return item.Package_Tracking_ID !== pack.Package_Tracking_ID;
                }); //this.tidList.splice(index, 1);
              } else {
                var sound = new Audio();
                sound.src = "src/assets/Windows 10 Error Sound.mp3";
                sound.load();
                var promise = sound.play();

                if (promise !== undefined) {
                  // On older browsers play() does not return anything, so the value would be undefined.
                  promise.then(function () {// Audio is playing.
                  })["catch"](function (error) {
                    console.log(error);
                  });
                }

                sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default()("Error Deleting", "Error updating package", "error");
              }
            });
            this.mainInput.nativeElement.value = "";
            this.mainInput.nativeElement.focus();
            this.secondInput.nativeElement.value = "";
          }
        }, {
          key: "retrieveImages",
          value: function retrieveImages() {
            var _this64 = this;

            this.showSpinner = true;
            var heynow; //console.log("LMAOOOOOOOOO", this.packFound.Package_Tracking_ID);

            var audit = {
              PID: this.packFound.Package_Tracking_ID,
              TID: "N/A",
              ActionName: "Retrieve Images / Update Packages",
              ActionDateTime: null,
              BusinessUnit: this.packFound.Client_Name,
              Sticker: "",
              User_ID: this.userlogged.User_Name
            }; //insert audit record

            this.auditService.saveAudit(audit).subscribe(function (data) {
              console.log(data);
            });

            this._tidService.retrieveDoc(this.packFound.Package_Tracking_ID).subscribe(function (e) {
              // console.log("geting images", e);
              _this64.showSpinner = true;
              _this64.imagesFound = e;
              e.forEach(function (element) {
                //console.log(element);
                var imageo = _this64._sanitizer.bypassSecurityTrustResourceUrl("data:image/tif;base64," + element);

                while (!element == undefined) {
                  _this64.imagesFound.push(element);
                }
              });

              _this64._dialog.open(_tid_creation_image_display_image_display_component__WEBPACK_IMPORTED_MODULE_13__["ImageDisplayComponent"], {
                data: {
                  images: _this64.imagesFound,
                  pidSent: _this64.packFound.Package_Tracking_ID
                },
                panelClass: "custom-dialog-container"
              });

              _this64.showSpinner = false;
            }, function (error) {
              console.log(error);
              _this64.showSpinner = false;
            }); //end observable

          } //end retrieve images

        }, {
          key: "getAllTIDbyPid",
          value: function getAllTIDbyPid(trackingNumber) {
            var _this65 = this;

            this._tidService.getAllbyPid(trackingNumber).subscribe(function (data) {
              console.log(data);

              if (data != undefined) {
                _this65.tidList = data;
                _this65.totalItems = data.length;
              } // this.sortArray();

            }, function (error) {
              console.log(error);
              _this65.tidList = [];
              _this65.totalItems = 0;
              var sound = new Audio();
              sound.src = "src/assets/Windows 10 Error Sound.mp3";
              sound.load();
              var promise = sound.play();

              if (promise !== undefined) {
                // On older browsers play() does not return anything, so the value would be undefined.
                promise.then(function () {// Audio is playing.
                })["catch"](function (error) {
                  console.log(error);
                });
              }

              sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
                position: "top-end",
                type: "error",
                title: "",
                text: "Tids not found for this package",
                showConfirmButton: true,
                footer: "",
                timer: 1100
              });
            }); //end subscribe

          }
        }, {
          key: "initPackFound",
          value: function initPackFound() {
            this.totalItems = 0;
            this.clientSelected = "";
            this.carrierSelected = "";
            this.mainInput.nativeElement.value = "";
            this.carrierInput.nativeElement.value = "";
            this.tidList = []; // this.mainInput.nativeElement.focus();

            this.packageArray = [];
            this.printPackage = [];
            this.packFound = {
              Package_Tracking_BC: "",
              multipleLenders: false,
              Package_Tracking_ID: "",
              LoanNumber: 0,
              Sticker_ID: "",
              clearPackagesFlag: false,
              Status_ID: 0,
              Location_Name: "",
              Client_Name: "",
              WorkFlow_ID: 0,
              Task_ID: 0,
              Creation_Date_Time: "",
              Available: false,
              Exception_Flag: false,
              totalLenders: 0,
              Exception_Name: "",
              Package_Finished: false,
              ManifestInBox: false,
              BaileeLetter: false,
              Carrier: "",
              Package_Notes: "",
              Bin_Number: "",
              clearPackagesDateTime: "",
              lastModule: "",
              lastFunction: "",
              loanTotal: 0,
              loansExpected: 0
            };
          }
        }, {
          key: "deleteRecordTID",
          value: function deleteRecordTID(row, index) {
            var _this66 = this;

            console.log("THIS PID", this.holdingPID);
            sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
              title: "Are you sure?",
              text: "Do you wish to disable TID-" + row.TID_ID,
              icon: "warning",
              showCancelButton: true,
              confirmButtonColor: "#3085d6",
              cancelButtonColor: "#d33",
              confirmButtonText: "Yes, disable it!"
            }).then(function (result) {
              if (result.value == true) {
                console.log("Deleting package ", row); // this.deleteRecord(packageRow);
                //const index : number = this.tableCreationElements.indexOf(row);
                //  console.log(this.tableCreationElements);
                //let packtoDelete = this.TIDList.find(x => x.Package_Tracking_ID == this.tableCreationElements[index].PID)
                //console.log("tid to delete", packtoDelete);
                //record Audit Trail

                _this66._tidService.disableTID(row).subscribe(function (data) {
                  _this66.tidList.filter(function (t) {
                    return t.TID_ID != row.TID_ID;
                  });
                });

                _this66.audit = {
                  PID: row.Package_Tracking_ID,
                  TID: row.TID_ID.toString(),
                  ActionName: "CHECK IN REMOVE",
                  ActionDateTime: null,
                  BusinessUnit: "",
                  Sticker: row.BarcodeSticker,
                  User_ID: _this66.userlogged.User_Name
                }; //insert audit record

                _this66.auditService.saveAudit(_this66.audit).subscribe(function (data) {
                  console.log(data);
                });

                var elementRemove = _this66.tidList.indexOf(row);

                _this66.tidList = []; // this.tidList.splice(index, elementRemove);

                setTimeout(function () {
                  _this66.getAllTIDbyPid(row.Package_Tracking_ID);
                }, 150);
              }
            });
          }
        }, {
          key: "initPackFound2",
          value: function initPackFound2() {
            this.tidList = [];
            this.totalItems = 0;
            this.clientSelected = "";
            this.binInput.nativeElement.value = "";
            this.carrierInput.nativeElement.value = "";
            this.mainInput.nativeElement.value = "";
            this.secondInput.nativeElement.value = "";
            this.mainInput.nativeElement.focus();
            this.packageArray = [];
            this.printPackage = [];
            this.packFound = {
              Package_Tracking_BC: "",
              Package_Tracking_ID: "",
              LoanNumber: 0,
              Sticker_ID: "",
              Status_ID: 0,
              Location_Name: "",
              Client_Name: "",
              WorkFlow_ID: 0,
              multipleLenders: false,
              Task_ID: 0,
              Creation_Date_Time: "",
              Available: false,
              clearPackagesFlag: false,
              Exception_Flag: false,
              Exception_Name: "",
              Package_Finished: false,
              ManifestInBox: false,
              BaileeLetter: false,
              Carrier: "",
              Package_Notes: "",
              totalLenders: 0,
              Bin_Number: "",
              clearPackagesDateTime: "",
              lastModule: "",
              lastFunction: "",
              loanTotal: 0,
              loansExpected: 0
            };
          }
        }, {
          key: "onCreateCoverSheet",
          value: function onCreateCoverSheet(items) {
            var packete = items[items.length - 1];
            console.log("On create coversheet", packete);
            this.packageArray.push(packete);
            console.log("Array falso", this.packageArray);
            this.packageService.createCoversheet(this.packageArray).subscribe(function (response) {
              var file = new Blob([response], {
                type: "application/pdf"
              });
              var url = URL.createObjectURL(file);
              var iframe = document.createElement("iframe");
              iframe.style.display = "none";
              iframe.src = url;
              document.body.appendChild(iframe);
              iframe.contentWindow.print();
            }, function (error) {
              var sound = new Audio();
              sound.src = "src/assets/Windows 10 Error Sound.mp3";
              sound.load();
              var promise = sound.play();

              if (promise !== undefined) {
                // On older browsers play() does not return anything, so the value would be undefined.
                promise.then(function () {// Audio is playing.
                })["catch"](function (error) {
                  console.log(error);
                });
              }

              sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default()("Error", String(error.Message), "error");
            });
          }
        }, {
          key: "onCreateCoverSheetBulk",
          value: function onCreateCoverSheetBulk(items) {
            this.packageService.createCoversheet(items).subscribe(function (response) {
              var file = new Blob([response], {
                type: "application/pdf"
              });
              var url = URL.createObjectURL(file);
              var iframe = document.createElement("iframe");
              iframe.style.display = "none";
              iframe.src = url;
              document.body.appendChild(iframe);
              iframe.contentWindow.print();
            }, function (error) {
              var sound = new Audio();
              sound.src = "src/assets/Windows 10 Error Sound.mp3";
              sound.load();
              var promise = sound.play();

              if (promise !== undefined) {
                // On older browsers play() does not return anything, so the value would be undefined.
                promise.then(function () {// Audio is playing.
                })["catch"](function (error) {
                  console.log(error);
                });
              }

              sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default()("Error", "Cannot print in bulk", "error");
            });
          }
        }, {
          key: "deleteRecord",
          value: function deleteRecord() {
            var _this67 = this;

            //console.log("packageIndex ",this.printPackage.indexOf(packageRow));
            if (this.packFound != null || this.packFound != undefined) {
              this.onDisable(this.packFound);
              setTimeout(function () {
                _this67.totalItems = 0;
                _this67.clientSelected = "";
                _this67.binInput.nativeElement.value = "";
                _this67.carrierInput.nativeElement.value = "";
                _this67.mainInput.nativeElement.value = "";
                _this67.secondInput.nativeElement.value = "";

                _this67.mainInput.nativeElement.focus();

                _this67.notesInput.nativeElement.value = "";
                _this67.packageArray = [];
                _this67.printPackage = [];
              }, 500);
            } else {
              sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default()("Error", "Cannot delete ", "error");
            }
          }
        }]);

        return EditRemoveComponent;
      }(); //end class


      EditRemoveComponent.ɵfac = function EditRemoveComponent_Factory(t) {
        return new (t || EditRemoveComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_packages_add_packages_service__WEBPACK_IMPORTED_MODULE_3__["AddPackagesService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_packages_worflow_rel_service__WEBPACK_IMPORTED_MODULE_4__["WorflowRelService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_packages_audit_trail_service__WEBPACK_IMPORTED_MODULE_9__["AuditTrailService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_packages_worflow_task_service__WEBPACK_IMPORTED_MODULE_5__["WorflowTaskService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_packages_worflow_status_service__WEBPACK_IMPORTED_MODULE_6__["WorflowStatusService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_packages_client_service__WEBPACK_IMPORTED_MODULE_7__["ClientService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_tid_tidcreartion_service_service__WEBPACK_IMPORTED_MODULE_10__["TidcreartionServiceService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_11__["NgbModal"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_platform_browser__WEBPACK_IMPORTED_MODULE_12__["DomSanitizer"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_material_dialog__WEBPACK_IMPORTED_MODULE_14__["MatDialog"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_8__["Router"]));
      };

      EditRemoveComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: EditRemoveComponent,
        selectors: [["app-edit-remove"]],
        viewQuery: function EditRemoveComponent_Query(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c0, 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c1, 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c2, 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c3, 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c4, 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c5, 3);
          }

          if (rf & 2) {
            var _t;

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.mainInput = _t.first);
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.secondInput = _t.first);
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.carrierInput = _t.first);
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.notesInput = _t.first);
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.binInput = _t.first);
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.audioPlayerRef = _t.first);
          }
        },
        decls: 80,
        vars: 7,
        consts: [[1, "row"], [1, "col-md-4"], [1, "card", 2, "border", "solid 2px #04395E"], [1, "card-header"], [2, "float", "right"], [3, "diameter", 4, "ngIf"], ["audioOption", ""], ["src", "/assets/Windows 10 Error Sound.mp3", "type", "audio/mp3"], [1, "col-12", 2, "padding-left", "1ex", "padding-right", "1ex", "font-size", "135%", "padding-bottom", "1ex"], [1, "col-12"], ["type", "text", "autofocus", "", 1, "form-control", 3, "keydown.enter"], ["tracking", ""], [1, "col-12", 2, "padding-left", "2ex", "padding-right", "2ex", "font-size", "135%", "padding-bottom", "1ex"], [1, "form-control", 3, "change", "keydown.enter"], ["carrierInput", ""], [4, "ngFor", "ngForOf"], ["secondInput", ""], ["type", "text", 1, "form-control", 3, "keydown.enter"], ["binNumber", ""], ["type", "text", "ng-model", "holdingNotes", 1, "form-control", 3, "keydown.enter"], ["notes", ""], ["id", "btn1", 1, "btn", "btn-secondary", 2, "margin", "10px", 3, "click"], ["id", "btn2", 1, "btn", "btn-secondary", 2, "margin", "10px", 3, "click"], [1, "col-md-8"], [1, "card"], [2, "float", "left"], [1, "card-deck"], [1, "card", 2, "width", "20%", "display", "inline-block", "text-align", "center"], ["id", "rowstile", 1, "container-fluid"], [1, "row", 2, "text-align", "center"], [1, "panel", "panel-color", "panel-fadeDark"], [1, "panel-body"], [1, "table-responsive"], ["id", "tblLoans", 1, "table", "table-striped"], [1, "thead-dark"], ["style", "width: 16.6%; text-align: center;", "class", "", 4, "ngFor", "ngForOf"], [3, "diameter"], [1, "", 2, "width", "16.6%", "text-align", "center"], [1, "w-15"], [3, "click"]],
        template: function EditRemoveComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "h5", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "Update Packages ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "span", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](6, EditRemoveComponent_mat_spinner_6_Template, 1, 1, "mat-spinner", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "audio", null, 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "source", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "span");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, " Tracking or PID ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "input", 10, 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("keydown.enter", function EditRemoveComponent_Template_input_keydown_enter_15_listener($event) {
              return ctx.mainSearch($event);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](17, "hr");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "span");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, " Carrier ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "select", 13, 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("change", function EditRemoveComponent_Template_select_change_23_listener($event) {
              return ctx.sendToCarrier($event);
            })("keydown.enter", function EditRemoveComponent_Template_select_keydown_enter_23_listener($event) {
              return ctx.sendToCarrier($event);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](25, EditRemoveComponent_option_25_Template, 2, 1, "option", 15);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](26, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "div", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "div", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "span");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](30, " Business Unit ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "select", 13, 16);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("change", function EditRemoveComponent_Template_select_change_31_listener($event) {
              return ctx.sendToNotesField($event);
            })("keydown.enter", function EditRemoveComponent_Template_select_keydown_enter_31_listener() {
              return ctx.enterSelect();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](33, EditRemoveComponent_option_33_Template, 2, 1, "option", 15);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](34, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "div", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "div");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "span");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39, "Bin Number ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "input", 17, 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("keydown.enter", function EditRemoveComponent_Template_input_keydown_enter_40_listener() {
              return ctx.goToBusinessUnit();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](42, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "div", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "div", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "div");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "span");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](47, " Notes ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "input", 19, 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("keydown.enter", function EditRemoveComponent_Template_input_keydown_enter_48_listener() {
              return ctx.goToBin();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](50, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](51, "hr");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "button", 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function EditRemoveComponent_Template_button_click_52_listener() {
              return ctx.saveAndPrint();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](53, "SAVE");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "button", 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function EditRemoveComponent_Template_button_click_54_listener() {
              return ctx.deleteRecord();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](55, "DISABLE");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](56, "button", 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function EditRemoveComponent_Template_button_click_56_listener() {
              return ctx.retrieveImages();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](57, "RETRIEVE IMAGES");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "button", 22);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function EditRemoveComponent_Template_button_click_58_listener() {
              return ctx.initPackFound2();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](59, "CANCEL");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "div", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](61, "div", 24);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "h5", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](63, "span", 25);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](64);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "span", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](66);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](67, "div", 26);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "div", 27);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](69, "div", 28);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](70, "div", 29);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](71, "div", 30);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](72, "div", 31);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](73, "div", 32);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](74, "table", 33);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](75, "thead", 34);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](76, "tr");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](77, EditRemoveComponent_th_77_Template, 2, 1, "th", 35);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](78, EditRemoveComponent_tr_78_Template, 14, 5, "tr", 15);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](79, "tbody");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.showSpinner);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](19);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.carrierArray);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.clientArray);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](31);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" TRACKING NUMBER: ", ctx.packFound.Package_Tracking_BC, " ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" TOTAL TIDS: ", ctx.totalItems, " ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.headers);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.tidList.slice().reverse());
          }
        },
        directives: [_angular_common__WEBPACK_IMPORTED_MODULE_15__["NgIf"], _angular_common__WEBPACK_IMPORTED_MODULE_15__["NgForOf"], _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_16__["MatSpinner"], _angular_forms__WEBPACK_IMPORTED_MODULE_17__["NgSelectOption"], _angular_forms__WEBPACK_IMPORTED_MODULE_17__["ɵangular_packages_forms_forms_z"]],
        styles: ["#userInfo[_ngcontent-%COMP%] {\r\n  text-align: right;\r\n  padding-top: 5%;\r\n}\r\n\r\n#trackinglbl[_ngcontent-%COMP%] {\r\n  text-align: right;\r\n  align-self: center;\r\n}\r\n\r\n.mat-radio-button[_ngcontent-%COMP%]    ~ .mat-radio-button[_ngcontent-%COMP%] {\r\n  margin-left: 16px;\r\n}\r\n\r\ninput[_ngcontent-%COMP%], select[_ngcontent-%COMP%] {\r\n  border: 2px solid;\r\n}\r\n\r\n.col-md-6[_ngcontent-%COMP%] {\r\n  border: 2px solid;\r\n}\r\n\r\nhr[_ngcontent-%COMP%] {\r\n  border: 0;\r\n  height: 1px;\r\n  background-image: linear-gradient(\r\n    to right,\r\n    rgba(0, 0, 0, 0),\r\n    rgba(0, 0, 0, 0.75),\r\n    rgba(0, 0, 0, 0)\r\n  );\r\n}\r\n\r\n.spacing[_ngcontent-%COMP%] {\r\n  margin-top: 1%;\r\n  margin-top: 1%;\r\n}\r\n\r\ninput[_ngcontent-%COMP%]:focus {\r\n  border: 5px solid green;\r\n}\r\n\r\nselect[_ngcontent-%COMP%]:focus {\r\n  border: 5px solid green;\r\n}\r\n\r\ntd[_ngcontent-%COMP%] {\r\n  text-align: center;\r\n  vertical-align: middle;\r\n}\r\n\r\n#btndiv[_ngcontent-%COMP%] {\r\n  align-self: center;\r\n  margin-left: 50px;\r\n}\r\n\r\n.card-header[_ngcontent-%COMP%] {\r\n  background-color: #031d44;\r\n  color: white;\r\n}\r\n\r\n#btn1[_ngcontent-%COMP%], #btn2[_ngcontent-%COMP%], #btn3[_ngcontent-%COMP%], #btn4[_ngcontent-%COMP%], #btn5[_ngcontent-%COMP%], #btn6[_ngcontent-%COMP%] {\r\n  background-color: #031d44;\r\n  color: white;\r\n}\r\n\r\n.btn-group[_ngcontent-%COMP%] {\r\n  margin-top: 2%;\r\n  align-content: center;\r\n  margin-left: 20%;\r\n  display: inline-block;\r\n  text-align: center;\r\n}\r\n\r\n#dropdownClient[_ngcontent-%COMP%] {\r\n  width: 90%;\r\n  display: inline-block;\r\n  align-content: center;\r\n}\r\n\r\n#selector[_ngcontent-%COMP%] {\r\n  width: 250px;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImVkaXQtcmVtb3ZlLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxpQkFBaUI7RUFDakIsZUFBZTtBQUNqQjs7QUFFQTtFQUNFLGlCQUFpQjtFQUNqQixrQkFBa0I7QUFDcEI7O0FBQ0E7RUFDRSxpQkFBaUI7QUFDbkI7O0FBQ0E7O0VBRUUsaUJBQWlCO0FBQ25COztBQUNBO0VBQ0UsaUJBQWlCO0FBQ25COztBQUNBO0VBQ0UsU0FBUztFQUNULFdBQVc7RUFDWDs7Ozs7R0FLQztBQUNIOztBQUVBO0VBQ0UsY0FBYztFQUNkLGNBQWM7QUFDaEI7O0FBQ0E7RUFDRSx1QkFBdUI7QUFDekI7O0FBQ0E7RUFDRSx1QkFBdUI7QUFDekI7O0FBQ0E7RUFDRSxrQkFBa0I7RUFDbEIsc0JBQXNCO0FBQ3hCOztBQUVBO0VBQ0Usa0JBQWtCO0VBQ2xCLGlCQUFpQjtBQUNuQjs7QUFFQTtFQUNFLHlCQUF5QjtFQUN6QixZQUFZO0FBQ2Q7O0FBRUE7Ozs7OztFQU1FLHlCQUF5QjtFQUN6QixZQUFZO0FBQ2Q7O0FBRUE7RUFDRSxjQUFjO0VBQ2QscUJBQXFCO0VBQ3JCLGdCQUFnQjtFQUNoQixxQkFBcUI7RUFDckIsa0JBQWtCO0FBQ3BCOztBQUVBO0VBQ0UsVUFBVTtFQUNWLHFCQUFxQjtFQUNyQixxQkFBcUI7QUFDdkI7O0FBRUE7RUFDRSxZQUFZO0FBQ2QiLCJmaWxlIjoiZWRpdC1yZW1vdmUuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIiN1c2VySW5mbyB7XHJcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbiAgcGFkZGluZy10b3A6IDUlO1xyXG59XHJcblxyXG4jdHJhY2tpbmdsYmwge1xyXG4gIHRleHQtYWxpZ246IHJpZ2h0O1xyXG4gIGFsaWduLXNlbGY6IGNlbnRlcjtcclxufVxyXG4ubWF0LXJhZGlvLWJ1dHRvbiB+IC5tYXQtcmFkaW8tYnV0dG9uIHtcclxuICBtYXJnaW4tbGVmdDogMTZweDtcclxufVxyXG5pbnB1dCxcclxuc2VsZWN0IHtcclxuICBib3JkZXI6IDJweCBzb2xpZDtcclxufVxyXG4uY29sLW1kLTYge1xyXG4gIGJvcmRlcjogMnB4IHNvbGlkO1xyXG59XHJcbmhyIHtcclxuICBib3JkZXI6IDA7XHJcbiAgaGVpZ2h0OiAxcHg7XHJcbiAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KFxyXG4gICAgdG8gcmlnaHQsXHJcbiAgICByZ2JhKDAsIDAsIDAsIDApLFxyXG4gICAgcmdiYSgwLCAwLCAwLCAwLjc1KSxcclxuICAgIHJnYmEoMCwgMCwgMCwgMClcclxuICApO1xyXG59XHJcblxyXG4uc3BhY2luZyB7XHJcbiAgbWFyZ2luLXRvcDogMSU7XHJcbiAgbWFyZ2luLXRvcDogMSU7XHJcbn1cclxuaW5wdXQ6Zm9jdXMge1xyXG4gIGJvcmRlcjogNXB4IHNvbGlkIGdyZWVuO1xyXG59XHJcbnNlbGVjdDpmb2N1cyB7XHJcbiAgYm9yZGVyOiA1cHggc29saWQgZ3JlZW47XHJcbn1cclxudGQge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xyXG59XHJcblxyXG4jYnRuZGl2IHtcclxuICBhbGlnbi1zZWxmOiBjZW50ZXI7XHJcbiAgbWFyZ2luLWxlZnQ6IDUwcHg7XHJcbn1cclxuXHJcbi5jYXJkLWhlYWRlciB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzAzMWQ0NDtcclxuICBjb2xvcjogd2hpdGU7XHJcbn1cclxuXHJcbiNidG4xLFxyXG4jYnRuMixcclxuI2J0bjMsXHJcbiNidG40LFxyXG4jYnRuNSxcclxuI2J0bjYge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMwMzFkNDQ7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG59XHJcblxyXG4uYnRuLWdyb3VwIHtcclxuICBtYXJnaW4tdG9wOiAyJTtcclxuICBhbGlnbi1jb250ZW50OiBjZW50ZXI7XHJcbiAgbWFyZ2luLWxlZnQ6IDIwJTtcclxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG4jZHJvcGRvd25DbGllbnQge1xyXG4gIHdpZHRoOiA5MCU7XHJcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIGFsaWduLWNvbnRlbnQ6IGNlbnRlcjtcclxufVxyXG5cclxuI3NlbGVjdG9yIHtcclxuICB3aWR0aDogMjUwcHg7XHJcbn1cclxuIl19 */"]
      });
      /***/
    },

    /***/
    "bhVg":
    /*!*************************************************************!*\
      !*** ./src/app/pages/certification/certification.module.ts ***!
      \*************************************************************/

    /*! exports provided: CertificationModule */

    /***/
    function bhVg(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CertificationModule", function () {
        return CertificationModule;
      });
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var src_app_services_packages_add_packages_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! src/app/services/packages/add-packages.service */
      "wlS1");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");
      /* harmony import */


      var _tid_certification_tid_certification_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./tid-certification/tid-certification.component */
      "OP5Q");
      /* harmony import */


      var _certification_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./certification-routing.module */
      "zCaV");
      /* harmony import */


      var src_app_services_coversheet_create_coversheet_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! src/app/services/coversheet/create-coversheet.service */
      "44oB");
      /* harmony import */


      var src_app_services_tid_tidcreartion_service_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/services/tid/tidcreartion-service.service */
      "eHNn");
      /* harmony import */


      var src_app_services_packages_audit_trail_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! src/app/services/packages/audit-trail.service */
      "irGt");
      /* harmony import */


      var _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! @angular/material/progress-spinner */
      "Xa2L");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! @angular/core */
      "fXoL"); // <== add the imports!


      var CertificationModule = function CertificationModule() {
        _classCallCheck(this, CertificationModule);
      };

      CertificationModule.ɵfac = function CertificationModule_Factory(t) {
        return new (t || CertificationModule)();
      };

      CertificationModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵdefineNgModule"]({
        type: CertificationModule
      });
      CertificationModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵdefineInjector"]({
        providers: [src_app_services_packages_add_packages_service__WEBPACK_IMPORTED_MODULE_1__["AddPackagesService"], src_app_services_coversheet_create_coversheet_service__WEBPACK_IMPORTED_MODULE_6__["CreateCoversheetService"], src_app_services_tid_tidcreartion_service_service__WEBPACK_IMPORTED_MODULE_7__["TidcreartionServiceService"], src_app_services_packages_audit_trail_service__WEBPACK_IMPORTED_MODULE_8__["AuditTrailService"]],
        imports: [[_certification_routing_module__WEBPACK_IMPORTED_MODULE_5__["CertificationRoutingModule"], _angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"], _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_9__["MatProgressSpinnerModule"]]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵsetNgModuleScope"](CertificationModule, {
          declarations: [_tid_certification_tid_certification_component__WEBPACK_IMPORTED_MODULE_4__["TidCertificationComponent"]],
          imports: [_certification_routing_module__WEBPACK_IMPORTED_MODULE_5__["CertificationRoutingModule"], _angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"], _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_9__["MatProgressSpinnerModule"]]
        });
      })();
      /***/

    },

    /***/
    "c1hE":
    /*!*********************************************!*\
      !*** ./src/app/services/tid/pdf.service.ts ***!
      \*********************************************/

    /*! exports provided: PdfService */

    /***/
    function c1hE(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PdfService", function () {
        return PdfService;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var PdfService = /*#__PURE__*/function () {
        function PdfService() {
          _classCallCheck(this, PdfService);
        }

        _createClass(PdfService, [{
          key: "getPdf",
          value: function getPdf() {
            return '/assets/pdf/front_view.pdf';
          }
        }]);

        return PdfService;
      }();

      PdfService.ɵfac = function PdfService_Factory(t) {
        return new (t || PdfService)();
      };

      PdfService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
        token: PdfService,
        factory: PdfService.ɵfac,
        providedIn: 'root'
      });
      /***/
    },

    /***/
    "dPAj":
    /*!************************************************************!*\
      !*** ./src/app/pages/admin/add-user/add-user.component.ts ***!
      \************************************************************/

    /*! exports provided: AddUserComponent */

    /***/
    function dPAj(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AddUserComponent", function () {
        return AddUserComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var src_app_services_service_index__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! src/app/services/service.index */
      "vWu4");
      /* harmony import */


      var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! sweetalert2/dist/sweetalert2.js */
      "PdH4");
      /* harmony import */


      var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_3__);
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");

      var _c0 = ["firstName"];
      var _c1 = ["lastName"];
      var _c2 = ["login"];
      var _c3 = ["password"];
      var _c4 = ["admin"];
      var _c5 = ["bulk"];

      var AddUserComponent = /*#__PURE__*/function () {
        function AddUserComponent(userService, router) {
          _classCallCheck(this, AddUserComponent);

          this.userService = userService;
          this.router = router;
        }

        _createClass(AddUserComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.resetUser();
          } //InputMethods

        }, {
          key: "firstNameInput",
          value: function firstNameInput(event) {
            this.usrMade.FirstName = event.target.value;
            console.log(this.usrMade.FirstName);
            this.lastInput.nativeElement.focus();
          }
        }, {
          key: "lasttNameInput",
          value: function lasttNameInput(event) {
            this.usrMade.LastName = event.target.value;
            console.log(this.usrMade.LastName);
          }
        }, {
          key: "saveSingleUser",
          value: function saveSingleUser() {
            var _this68 = this;

            this.usrMade.User_Login = this.loginInput.nativeElement.value;
            this.usrMade.User_Password = this.passwordInput.nativeElement.value;
            this.usrMade.Admin_Flag = this.isAdmin;
            this.usrMade.User_Name = this.usrMade.FirstName + ' ' + this.usrMade.LastName;
            console.log(this.usrMade);
            this.userService.saveUser(this.usrMade).subscribe(function (data) {
              if (data) {
                sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                  position: 'top-end',
                  type: 'success',
                  title: 'User has been saved',
                  showConfirmButton: false,
                  timer: 1500
                });
              } else {
                sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                  position: 'top-end',
                  type: 'error',
                  title: 'Error saving user ' + data,
                  showConfirmButton: false,
                  timer: 1500
                });
              }

              _this68.resetUser();
            });
          }
        }, {
          key: "onSubmitBulk",
          value: function onSubmitBulk() {
            var bulk = this.bulkInput.nativeElement.value; //console.log(this.demo);

            var arreglo = bulk.split('\n');
            console.log(arreglo);
            this.userService.bulkSave(arreglo).subscribe(function (data) {
              console.log(data);

              if (data == "Success") {
                sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                  position: 'top-end',
                  type: 'success',
                  title: 'User list has been saved',
                  showConfirmButton: false,
                  timer: 1500
                });
              } else {
                sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                  position: 'top-end',
                  type: 'error',
                  title: 'Error saving user list' + data,
                  showConfirmButton: false,
                  timer: 1500
                });
              }
            }); //this.insertUser(form);
          } //Initialize methods

        }, {
          key: "resetUser",
          value: function resetUser() {
            this.usrMade = {
              Admin_Flag: false,
              Available: true,
              FirstName: "",
              LastName: '',
              User_Login: '',
              User_Name: '',
              User_Password: ''
            };
          }
        }]);

        return AddUserComponent;
      }();

      AddUserComponent.ɵfac = function AddUserComponent_Factory(t) {
        return new (t || AddUserComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_service_index__WEBPACK_IMPORTED_MODULE_2__["UsersService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]));
      };

      AddUserComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: AddUserComponent,
        selectors: [["app-add-user"]],
        viewQuery: function AddUserComponent_Query(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c0, 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c1, 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c2, 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c3, 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c4, 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c5, 3);
          }

          if (rf & 2) {
            var _t;

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.firstInput = _t.first);
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.lastInput = _t.first);
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.loginInput = _t.first);
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.passwordInput = _t.first);
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.adminInput = _t.first);
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.bulkInput = _t.first);
          }
        },
        decls: 53,
        vars: 1,
        consts: [[1, "row"], [1, "col-md-7"], [1, "card"], [1, "card-header"], [1, "cardbody", 2, "border", "solid 2px #04395E"], [1, "col-12", 2, "padding-left", "10%", "padding-right", "10%"], ["type", "text", 1, "form-control", 3, "keydown.enter"], ["firstName", ""], ["lastName", ""], ["login", ""], ["type", "password", 1, "form-control", 3, "keydown.enter"], ["password", ""], ["type", "checkbox", "name", "checkbox", "value", "value", 3, "ngModel", "ngModelChange"], ["admin", ""], [1, "container"], [1, "btn-holder", 2, "margin-bottom", "10px"], ["type", "button", 1, "btn", "btn-info", 3, "click"], [1, "col-md-5"], [1, "card-body", 2, "border", "solid 2px #04395E"], [1, "form-group"], ["for", "bulk"], ["name", "bulk", 1, "form-control", 2, "resize", "vertical", "overflow", "auto", "border-style", "solid"], ["bulk", ""]],
        template: function AddUserComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "h5", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "Add Users");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "span");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "b");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "First Name");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "input", 6, 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("keydown.enter", function AddUserComponent_Template_input_keydown_enter_12_listener($event) {
              return ctx.firstNameInput($event);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](14, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "span");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "b");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "Last Name");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "input", 6, 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("keydown.enter", function AddUserComponent_Template_input_keydown_enter_18_listener($event) {
              return ctx.lasttNameInput($event);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](20, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "span");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "b");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, "User Login");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "input", 6, 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("keydown.enter", function AddUserComponent_Template_input_keydown_enter_24_listener($event) {
              return ctx.lasttNameInput($event);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](26, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "span");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "b");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29, "Password");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "input", 10, 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("keydown.enter", function AddUserComponent_Template_input_keydown_enter_30_listener($event) {
              return ctx.lasttNameInput($event);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](32, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "label");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "input", 12, 13);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function AddUserComponent_Template_input_ngModelChange_34_listener($event) {
              return ctx.isAdmin = $event;
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](36, " is Admin");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "div", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "div", 15);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "button", 16);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AddUserComponent_Template_button_click_39_listener() {
              return ctx.saveSingleUser();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](40, "Save User");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "div", 17);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "h5", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](44, "Bulk Add");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "div", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "label", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "button", 16);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AddUserComponent_Template_button_click_49_listener() {
              return ctx.onSubmitBulk();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](50, " Add bulk ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](51, "textarea", 21, 22);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](34);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.isAdmin);
          }
        },
        directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["CheckboxControlValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["NgModel"]],
        styles: [".card-header[_ngcontent-%COMP%] {\r\n  background-color: #031d44;\r\n  color: white;\r\n}\r\ninput[_ngcontent-%COMP%]:focus {\r\n  border: 1px solid green;\r\n}\r\n.container[_ngcontent-%COMP%] {\r\n  justify-content: space-between;\r\n  flex-direction: column;\r\n  display: flex;\r\n}\r\n.container[_ngcontent-%COMP%]   .btn-holder[_ngcontent-%COMP%] {\r\n  justify-content: flex-end;\r\n  display: flex;\r\n}\r\ntextarea[_ngcontent-%COMP%] {\r\n  width: 33em;\r\n  height: 33em;\r\n  border: 1px solid #031d44;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFkZC11c2VyLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSx5QkFBeUI7RUFDekIsWUFBWTtBQUNkO0FBQ0E7RUFDRSx1QkFBdUI7QUFDekI7QUFFQTtFQUNFLDhCQUE4QjtFQUM5QixzQkFBc0I7RUFDdEIsYUFBYTtBQUNmO0FBQ0E7RUFDRSx5QkFBeUI7RUFDekIsYUFBYTtBQUNmO0FBRUE7RUFDRSxXQUFXO0VBQ1gsWUFBWTtFQUNaLHlCQUF5QjtBQUMzQiIsImZpbGUiOiJhZGQtdXNlci5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNhcmQtaGVhZGVyIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDMxZDQ0O1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxufVxyXG5pbnB1dDpmb2N1cyB7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgZ3JlZW47XHJcbn1cclxuXHJcbi5jb250YWluZXIge1xyXG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbn1cclxuLmNvbnRhaW5lciAuYnRuLWhvbGRlciB7XHJcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG59XHJcblxyXG50ZXh0YXJlYSB7XHJcbiAgd2lkdGg6IDMzZW07XHJcbiAgaGVpZ2h0OiAzM2VtO1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkICMwMzFkNDQ7XHJcbn1cclxuIl19 */"]
      });
      /***/
    },

    /***/
    "dSTU":
    /*!***********************************************************!*\
      !*** ./src/app/services/packages/worflow-task.service.ts ***!
      \***********************************************************/

    /*! exports provided: WorflowTaskService */

    /***/
    function dSTU(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "WorflowTaskService", function () {
        return WorflowTaskService;
      });
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");
      /* harmony import */


      var _shared_http_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../shared/http-rest.service */
      "vulr");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var WorflowTaskService = /*#__PURE__*/function (_shared_http_rest_ser5) {
        _inherits(WorflowTaskService, _shared_http_rest_ser5);

        var _super5 = _createSuper(WorflowTaskService);

        function WorflowTaskService(http, router) {
          var _this69;

          _classCallCheck(this, WorflowTaskService);

          _this69 = _super5.call(this, http);
          _this69.router = router;
          _this69.rootURL = "http://localhost:53420/api/workflow_task";
          return _this69;
        }

        _createClass(WorflowTaskService, [{
          key: "getTaskbyWF",
          value: function getTaskbyWF(wfid) {
            return this.findAllById(wfid, "workflow_task", "FindAllwithWFID");
          }
        }, {
          key: "getTaskbyWF2",
          value: function getTaskbyWF2(wfid) {
            return this.findAllById(wfid, "workflow_task", "FindAllwithWFID2");
          }
        }, {
          key: "updateTaskFlag",
          value: function updateTaskFlag(pack) {
            return this.save(pack, "workflow_task", "Update");
          }
        }, {
          key: "getCurrentTask",
          value: function getCurrentTask(task) {
            return this.findAllById(task, "workflow_task", "getCurrentTask");
          }
        }]);

        return WorflowTaskService;
      }(_shared_http_rest_service__WEBPACK_IMPORTED_MODULE_2__["HttpRestService"]);

      WorflowTaskService.ɵfac = function WorflowTaskService_Factory(t) {
        return new (t || WorflowTaskService)(_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵinject"](_angular_router__WEBPACK_IMPORTED_MODULE_0__["Router"]));
      };

      WorflowTaskService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineInjectable"]({
        token: WorflowTaskService,
        factory: WorflowTaskService.ɵfac,
        providedIn: 'root'
      });
      /***/
    },

    /***/
    "dgmN":
    /*!***************************************!*\
      !*** ./src/app/pages/pages.module.ts ***!
      \***************************************/

    /*! exports provided: PagesModule */

    /***/
    function dgmN(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PagesModule", function () {
        return PagesModule;
      });
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _shared_shared_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../shared/shared.module */
      "PCNd");
      /* harmony import */


      var _pages_routes__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./pages.routes */
      "XIe5");
      /* harmony import */


      var _reports_reports_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./reports/reports.component */
      "qzPY");
      /* harmony import */


      var ngx_img_zoom__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ngx-img-zoom */
      "N0qp");
      /* harmony import */


      var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/material/dialog */
      "0IaG");
      /* harmony import */


      var _angular_material_button__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @angular/material/button */
      "bTqV");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! @angular/router */
      "tyNb"); // Modules
      // Routes


      var PagesModule = function PagesModule() {
        _classCallCheck(this, PagesModule);
      };

      PagesModule.ɵfac = function PagesModule_Factory(t) {
        return new (t || PagesModule)();
      };

      PagesModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵdefineNgModule"]({
        type: PagesModule
      });
      PagesModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵdefineInjector"]({
        imports: [[_angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_2__["SharedModule"], _pages_routes__WEBPACK_IMPORTED_MODULE_3__["PAGES_ROUTES"], ngx_img_zoom__WEBPACK_IMPORTED_MODULE_5__["NgxImgZoomModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormsModule"], _angular_material_dialog__WEBPACK_IMPORTED_MODULE_6__["MatDialogModule"]], _angular_material_button__WEBPACK_IMPORTED_MODULE_7__["MatButtonModule"], _angular_material_dialog__WEBPACK_IMPORTED_MODULE_6__["MatDialogModule"]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵsetNgModuleScope"](PagesModule, {
          declarations: [_reports_reports_component__WEBPACK_IMPORTED_MODULE_4__["ReportsComponent"]],
          imports: [_angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_2__["SharedModule"], _angular_router__WEBPACK_IMPORTED_MODULE_9__["RouterModule"], ngx_img_zoom__WEBPACK_IMPORTED_MODULE_5__["NgxImgZoomModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormsModule"], _angular_material_dialog__WEBPACK_IMPORTED_MODULE_6__["MatDialogModule"]],
          exports: [_angular_material_button__WEBPACK_IMPORTED_MODULE_7__["MatButtonModule"], _angular_material_dialog__WEBPACK_IMPORTED_MODULE_6__["MatDialogModule"]]
        });
      })();
      /***/

    },

    /***/
    "eHNn":
    /*!**************************************************************!*\
      !*** ./src/app/services/tid/tidcreartion-service.service.ts ***!
      \**************************************************************/

    /*! exports provided: TidcreartionServiceService */

    /***/
    function eHNn(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "TidcreartionServiceService", function () {
        return TidcreartionServiceService;
      });
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");
      /* harmony import */


      var _shared_http_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../shared/http-rest.service */
      "vulr");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var TidcreartionServiceService = /*#__PURE__*/function (_shared_http_rest_ser6) {
        _inherits(TidcreartionServiceService, _shared_http_rest_ser6);

        var _super6 = _createSuper(TidcreartionServiceService);

        function TidcreartionServiceService(http, router) {
          var _this70;

          _classCallCheck(this, TidcreartionServiceService);

          _this70 = _super6.call(this, http);
          _this70.router = router;
          return _this70;
        }

        _createClass(TidcreartionServiceService, [{
          key: "saveTID",
          value: function saveTID(pack) {
            return this.save(pack, "tid", "save");
          }
        }, {
          key: "createCoversheetManifestLoans",
          value: function createCoversheetManifestLoans(items) {
            return this.httpRequestBufferResponse("package", "CreateCoverSheetLenderName", null, items);
          }
        }, {
          key: "saveOneReturnTID",
          value: function saveOneReturnTID(pack) {
            return this.save(pack, "tid", "save");
          }
        }, {
          key: "saveTIDAPI",
          value: function saveTIDAPI(pack) {
            return this.save(pack, "tid", "creatingTID");
          }
        }, {
          key: "saveOneReturnTIDBailee",
          value: function saveOneReturnTIDBailee(pack) {
            return this.save(pack, "tid", "Save");
          }
        }, {
          key: "SaveSameLender",
          value: function SaveSameLender(pack) {
            return this.save(pack, "tid", "Save");
          }
        }, {
          key: "Update",
          value: function Update(pack) {
            return this.save(pack, "tid", "Update");
          }
        }, {
          key: "findTIDsById",
          value: function findTIDsById(id) {
            return this.findAllByBC(id, "tid", "findAllid");
          }
        }, {
          key: "getAllbyPid",
          value: function getAllbyPid(id) {
            return this.findAllByBC(id, "tid", "getAllbyPid");
          }
        }, {
          key: "findAllTID",
          value: function findAllTID() {
            return this.findAll("tid", "findAll");
          }
        }, {
          key: "findAllLenderNames",
          value: function findAllLenderNames() {
            return this.findAll("tid", "getAllLenders");
          }
        }, {
          key: "findbyTID",
          value: function findbyTID(id) {
            return this.findUniqueById(id, "tid", "findbyTID");
          }
        }, {
          key: "retrieveDoc",
          value: function retrieveDoc(id) {
            return this.startRetrieveSendPID(id, "tid", "SearchLicense");
          }
        }, {
          key: "deleteTid",
          value: function deleteTid(id) {
            return this.deleteForGood(id, "tid", "deleteTid");
          }
        }, {
          key: "disableTID",
          value: function disableTID(TID) {
            return this.asyncHttpRequest("POST", "tid", "deleteTid", null, TID);
          }
        }, {
          key: "findDocumentsByPID",
          value: function findDocumentsByPID(id) {
            return this.asyncHttpRequest("GET", "tid", "FindDocumentsByPID", {
              id: id
            }, null);
          }
        }]);

        return TidcreartionServiceService;
      }(_shared_http_rest_service__WEBPACK_IMPORTED_MODULE_2__["HttpRestService"]);

      TidcreartionServiceService.ɵfac = function TidcreartionServiceService_Factory(t) {
        return new (t || TidcreartionServiceService)(_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵinject"](_angular_router__WEBPACK_IMPORTED_MODULE_0__["Router"]));
      };

      TidcreartionServiceService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineInjectable"]({
        token: TidcreartionServiceService,
        factory: TidcreartionServiceService.ɵfac,
        providedIn: "root"
      });
      /***/
    },

    /***/
    "eTcO":
    /*!******************************************************!*\
      !*** ./src/app/pages/admin/users/users.component.ts ***!
      \******************************************************/

    /*! exports provided: UsersComponent */

    /***/
    function eTcO(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "UsersComponent", function () {
        return UsersComponent;
      });
      /* harmony import */


      var src_app_services_service_index__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! src/app/services/service.index */
      "vWu4");
      /* harmony import */


      var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! sweetalert2/dist/sweetalert2.js */
      "PdH4");
      /* harmony import */


      var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1__);
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");

      function UsersComponent_tr_43_Template(rf, ctx) {
        if (rf & 1) {
          var _r3 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "tr");

          _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](1, "th");

          _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](3, "th");

          _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](5, "th");

          _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](7, "th");

          _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](8);

          _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](9, "th");

          _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](10, "button", 21);

          _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("click", function UsersComponent_tr_43_Template_button_click_10_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵrestoreView"](_r3);

            var row_r1 = ctx.$implicit;

            var ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]();

            return ctx_r2.onDisable(row_r1);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](11, "X");

          _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var row_r1 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtextInterpolate"](row_r1.FirstName);

          _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtextInterpolate"](row_r1.LastName);

          _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtextInterpolate"](row_r1.User_Login);

          _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtextInterpolate"](row_r1.Admin_Flag);
        }
      }

      var _c0 = function _c0() {
        return ["/", "admin", "addUser"];
      };

      var UsersComponent = /*#__PURE__*/function () {
        function UsersComponent(userService, router) {
          _classCallCheck(this, UsersComponent);

          this.userService = userService;
          this.router = router;
        }

        _createClass(UsersComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.userlogged = JSON.parse(localStorage.getItem('user'));

            if (this.userlogged.Admin_Flag == true) {
              console.log('User logged: ' + this.userlogged.User_Login);
            } else {
              sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default()("Not an admin", "This user is not an Admin", "error");
              this.router.navigate(['/']);
            }

            this.userName = '';
            this.onLoad();
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            if (this.subscription) {
              this.subscription.unsubscribe();
            }
          }
        }, {
          key: "onLoad",
          value: function onLoad() {
            var _this71 = this;

            this.subscription = this.userService.findAllUsers().subscribe(function (users) {
              _this71.users = users;
            }, function (error) {
              sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default()("Error!", String(error.Message), "error");
            });
          }
        }, {
          key: "onEdit",
          value: function onEdit(id) {
            var _this72 = this;

            this.subscription = this.userService.findUserById(id).subscribe(function (user) {
              _this72.user = user;

              _this72.usersForm.patchValue(_this72.user);
            }, function (error) {
              sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default()("Error!", String(error), "error");
            });
          }
        }, {
          key: "onSubmit",
          value: function onSubmit() {
            var _this73 = this;

            this.subscription = this.userService.saveUser(this.usersForm.value).subscribe(function (response) {
              sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default()("Saved!", String(response), "success");

              _this73.onClearSearch();
            }, function (error) {
              sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default()("Error!", String(error.Message), "error");
            });
          }
        }, {
          key: "onSearch",
          value: function onSearch(name) {
            var _this74 = this;

            if (this.userName) {
              this.userService.findUsersByName(name).subscribe(function (users) {
                _this74.users = users; //console.log("Users found " + users[0].User_Name);

                if (users != null) {
                  console.log("success");
                } else {
                  console.log("failure");
                  sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
                    position: 'top-end',
                    type: 'error',
                    title: 'User not found',
                    showConfirmButton: false,
                    timer: 1500
                  });
                }
              }, function (error) {
                sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default()("Error!", String(error.Message), "error");
              });
            } else {
              sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default()("Error!", "User name is required for search.", "error");
            }
          }
        }, {
          key: "onDisable",
          value: function onDisable(id) {
            var _this75 = this;

            console.log(id);
            sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default()({
              title: 'Are you sure?',
              text: 'The user will be disabled from the system!',
              type: 'warning',
              showCancelButton: true,
              confirmButtonText: 'Ok',
              cancelButtonText: 'Cancel'
            }).then(function (result) {
              if (result.value) {
                console.log("Trying to disable");

                _this75.userService.asyncDELETERequest(id).subscribe(function (res) {
                  console.log(res);

                  _this75.onLoad();
                });
              } else if (result.dismiss === sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default.a.DismissReason.cancel) {
                sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default()('Cancelled!', 'The selected user is safe', 'error');
              }
            });
          } //end

        }, {
          key: "onClearSearch",
          value: function onClearSearch() {
            this.userName = "";
            this.onLoad();
          }
        }, {
          key: "initForm",
          value: function initForm() {
            var userId = '';
            var firstName = '';
            var lastName = '';
            var account = '';
            var password = '';
            var isAdmin = '';
            var available = '';
            this.usersForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
              'User_Id': new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](userId),
              'First_Name': new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](firstName, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
              'Last_Name': new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](lastName, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
              'Account': new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](account, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
              'Password': new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](password, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
              'Is_Admin': new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](isAdmin),
              'Available': new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](available)
            });
          }
        }, {
          key: "onKeydown",
          value: function onKeydown(event) {
            if (event.key === "Enter") {
              console.log(this.userName + " Searched user");
              this.onSearch(this.userName);
            }
          }
        }]);

        return UsersComponent;
      }();

      UsersComponent.ɵfac = function UsersComponent_Factory(t) {
        return new (t || UsersComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](src_app_services_service_index__WEBPACK_IMPORTED_MODULE_0__["UsersService"]), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]));
      };

      UsersComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineComponent"]({
        type: UsersComponent,
        selectors: [["app-users"]],
        decls: 44,
        vars: 4,
        consts: [[1, "row"], [1, "col-md-12"], [1, "card"], [1, "card-body"], [1, "m-t-0", "m-b-30"], [1, "col-lg-12"], ["id", "searchForm", 1, "form-inline"], [1, "form-group", "m-r-5"], ["for", "txtSearchUser", 1, "sr-only"], [1, "input-group"], ["autofocus", "", "type", "text", "placeholder", "Search user by full name", "name", "txtSearchUser", "maxlength", "50", 1, "form-control", 3, "ngModel", "ngModelChange", "keydown"], ["type", "button", 1, "btn", "btn-purple", "m-r-5", 3, "click"], [1, "fa", "fa-trash"], ["type", "button", "id", "btnAddUser", 1, "btn", "btn-success", "m-r-5", 3, "routerLink"], [1, "fa", "fa-plus"], ["type", "button", 1, "btn", "btn-danger", "m-r-5"], [1, "table-rep-plugin"], ["data-pattern", "priority-columns", 1, "table-responsive"], ["id", "tblUsers", 1, "table", "table-hover"], ["data-priority", "1", 1, "w-25"], [4, "ngFor", "ngForOf"], [3, "click"]],
        template: function UsersComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](0, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](1, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](2, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](3, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](4, "div", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](5, "h4", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](6, "USERS LIST");

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](7, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](8, "div", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](9, "form", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](10, "div", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](11, "label", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](12, "User ID");

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](13, "div", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](14, "input", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngModelChange", function UsersComponent_Template_input_ngModelChange_14_listener($event) {
              return ctx.userName = $event;
            })("keydown", function UsersComponent_Template_input_keydown_14_listener($event) {
              return ctx.onKeydown($event);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](15, "button", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("click", function UsersComponent_Template_button_click_15_listener() {
              return ctx.onClearSearch();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](16, "i", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](17, "Clear");

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](18, "button", 13);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](19, "i", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](20, "Add New");

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](21, "button", 15);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](22, "i", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](23, "Disable");

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](24, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](25, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](26, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](27, "div", 16);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](28, "div", 17);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](29, "table", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](30, "thead");

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](31, "tr");

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](32, "th", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](33, "First Name");

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](34, "th", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](35, "Last Name");

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](36, "th", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](37, "Account");

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](38, "th", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](39, "Is Admin");

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](40, "th", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](41, "Remove");

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](42, "tbody");

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](43, UsersComponent_tr_43_Template, 12, 4, "tr", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](14);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngModel", ctx.userName);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](4);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵpureFunction0"](3, _c0));

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](25);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngForOf", ctx.users);
          }
        },
        directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["ɵangular_packages_forms_forms_ba"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NgForm"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["MaxLengthValidator"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NgModel"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterLink"], _angular_common__WEBPACK_IMPORTED_MODULE_5__["NgForOf"]],
        styles: [".fade-scale[_ngcontent-%COMP%] {\r\n  transform: scale(0);\r\n  opacity: 0;\r\n  transition: all 0.25s linear;\r\n}\r\n\r\n.fade-scale.in[_ngcontent-%COMP%] {\r\n  opacity: 1;\r\n  transform: scale(1);\r\n}\r\n\r\n.fade-scale-custom[_ngcontent-%COMP%] {\r\n  transform: scale(0.9);\r\n  opacity: 0;\r\n  transition: all 0.1s;\r\n}\r\n\r\n.fade-scale-custom.in[_ngcontent-%COMP%] {\r\n  transform: scale(1);\r\n  opacity: 1;\r\n}\r\n\r\n.md-effect-1[_ngcontent-%COMP%]   .md-content[_ngcontent-%COMP%] {\r\n  transform: scale(0.7);\r\n  opacity: 0;\r\n  transition: all 0.3s;\r\n}\r\n\r\n.md-show.md-effect-1[_ngcontent-%COMP%]   .md-content[_ngcontent-%COMP%] {\r\n  transform: scale(1);\r\n  opacity: 1;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsbUJBQW1CO0VBQ25CLFVBQVU7RUFHViw0QkFBNEI7QUFDOUI7O0FBRUE7RUFDRSxVQUFVO0VBQ1YsbUJBQW1CO0FBQ3JCOztBQUdBO0VBSUUscUJBQXFCO0VBQ3JCLFVBQVU7RUFHVixvQkFBb0I7QUFDdEI7O0FBRUE7RUFJRSxtQkFBbUI7RUFDbkIsVUFBVTtBQUNaOztBQUVBO0VBSUUscUJBQXFCO0VBQ3JCLFVBQVU7RUFHVixvQkFBb0I7QUFDdEI7O0FBRUE7RUFJRSxtQkFBbUI7RUFDbkIsVUFBVTtBQUNaIiwiZmlsZSI6ImFwcC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmZhZGUtc2NhbGUge1xyXG4gIHRyYW5zZm9ybTogc2NhbGUoMCk7XHJcbiAgb3BhY2l0eTogMDtcclxuICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAwLjI1cyBsaW5lYXI7XHJcbiAgLW8tdHJhbnNpdGlvbjogYWxsIDAuMjVzIGxpbmVhcjtcclxuICB0cmFuc2l0aW9uOiBhbGwgMC4yNXMgbGluZWFyO1xyXG59XHJcblxyXG4uZmFkZS1zY2FsZS5pbiB7XHJcbiAgb3BhY2l0eTogMTtcclxuICB0cmFuc2Zvcm06IHNjYWxlKDEpO1xyXG59XHJcbkBpbXBvcnQgXCJ+QGFuZ3VsYXIvbWF0ZXJpYWwvcHJlYnVpbHQtdGhlbWVzL3B1cnBsZS1ncmVlbi5jc3NcIjtcclxuXHJcbi5mYWRlLXNjYWxlLWN1c3RvbSB7XHJcbiAgLXdlYmtpdC10cmFuc2Zvcm06IHNjYWxlKDAuOSk7XHJcbiAgLW1vei10cmFuc2Zvcm06IHNjYWxlKDAuOSk7XHJcbiAgLW1zLXRyYW5zZm9ybTogc2NhbGUoMC45KTtcclxuICB0cmFuc2Zvcm06IHNjYWxlKDAuOSk7XHJcbiAgb3BhY2l0eTogMDtcclxuICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAwLjFzO1xyXG4gIC1tb3otdHJhbnNpdGlvbjogYWxsIDAuMXM7XHJcbiAgdHJhbnNpdGlvbjogYWxsIDAuMXM7XHJcbn1cclxuXHJcbi5mYWRlLXNjYWxlLWN1c3RvbS5pbiB7XHJcbiAgLXdlYmtpdC10cmFuc2Zvcm06IHNjYWxlKDEpO1xyXG4gIC1tb3otdHJhbnNmb3JtOiBzY2FsZSgxKTtcclxuICAtbXMtdHJhbnNmb3JtOiBzY2FsZSgxKTtcclxuICB0cmFuc2Zvcm06IHNjYWxlKDEpO1xyXG4gIG9wYWNpdHk6IDE7XHJcbn1cclxuXHJcbi5tZC1lZmZlY3QtMSAubWQtY29udGVudCB7XHJcbiAgLXdlYmtpdC10cmFuc2Zvcm06IHNjYWxlKDAuNyk7XHJcbiAgLW1vei10cmFuc2Zvcm06IHNjYWxlKDAuNyk7XHJcbiAgLW1zLXRyYW5zZm9ybTogc2NhbGUoMC43KTtcclxuICB0cmFuc2Zvcm06IHNjYWxlKDAuNyk7XHJcbiAgb3BhY2l0eTogMDtcclxuICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAwLjNzO1xyXG4gIC1tb3otdHJhbnNpdGlvbjogYWxsIDAuM3M7XHJcbiAgdHJhbnNpdGlvbjogYWxsIDAuM3M7XHJcbn1cclxuXHJcbi5tZC1zaG93Lm1kLWVmZmVjdC0xIC5tZC1jb250ZW50IHtcclxuICAtd2Via2l0LXRyYW5zZm9ybTogc2NhbGUoMSk7XHJcbiAgLW1vei10cmFuc2Zvcm06IHNjYWxlKDEpO1xyXG4gIC1tcy10cmFuc2Zvcm06IHNjYWxlKDEpO1xyXG4gIHRyYW5zZm9ybTogc2NhbGUoMSk7XHJcbiAgb3BhY2l0eTogMTtcclxufVxyXG4iXX0= */"]
      });
      /***/
    },

    /***/
    "egE0":
    /*!******************************************************!*\
      !*** ./src/app/services/tid/lender-names.service.ts ***!
      \******************************************************/

    /*! exports provided: LenderNamesService */

    /***/
    function egE0(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LenderNamesService", function () {
        return LenderNamesService;
      });
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");
      /* harmony import */


      var _shared_http_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../shared/http-rest.service */
      "vulr");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var LenderNamesService = /*#__PURE__*/function (_shared_http_rest_ser7) {
        _inherits(LenderNamesService, _shared_http_rest_ser7);

        var _super7 = _createSuper(LenderNamesService);

        function LenderNamesService(http, router) {
          var _this76;

          _classCallCheck(this, LenderNamesService);

          _this76 = _super7.call(this, http);
          _this76.router = router;
          return _this76;
        }

        _createClass(LenderNamesService, [{
          key: "findAllLenderNames",
          value: function findAllLenderNames() {
            return this.findAll("tid", "getAllLenders");
          }
        }, {
          key: "findAllBaileeNames",
          value: function findAllBaileeNames() {
            return this.findAll("tid", "getAllBailee");
          }
        }]);

        return LenderNamesService;
      }(_shared_http_rest_service__WEBPACK_IMPORTED_MODULE_2__["HttpRestService"]);

      LenderNamesService.ɵfac = function LenderNamesService_Factory(t) {
        return new (t || LenderNamesService)(_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵinject"](_angular_router__WEBPACK_IMPORTED_MODULE_0__["Router"]));
      };

      LenderNamesService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineInjectable"]({
        token: LenderNamesService,
        factory: LenderNamesService.ɵfac,
        providedIn: "root"
      });
      /***/
    },

    /***/
    "ev3G":
    /*!*****************************************************************************!*\
      !*** ./src/app/pages/tid-creation/image-display/image-display.component.ts ***!
      \*****************************************************************************/

    /*! exports provided: ImageDisplayComponent */

    /***/
    function ev3G(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ImageDisplayComponent", function () {
        return ImageDisplayComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/material/dialog */
      "0IaG");
      /* harmony import */


      var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/platform-browser */
      "jhN1");
      /* harmony import */


      var jspdf__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! jspdf */
      "i680");
      /* harmony import */


      var _image_manipulation_image_manipulation_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../image-manipulation/image-manipulation.component */
      "xw10");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");

      var _c0 = ["dataX"];
      var _c1 = ["dataY"];
      var _c2 = ["dataHeight"];
      var _c3 = ["dataWidth"];
      var _c4 = ["dataRotate"];
      var _c5 = ["dataScaleX"];
      var _c6 = ["dataScaleY"];
      var _c7 = ["image"];

      function ImageDisplayComponent_div_7_Template(rf, ctx) {
        if (rf & 1) {
          var _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "img", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ImageDisplayComponent_div_7_Template_img_click_1_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r4);

            var i_r2 = ctx.index;

            var ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r3.doubleClick($event, i_r2);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var img_r1 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("src", img_r1, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
        }
      }

      var ImageDisplayComponent = /*#__PURE__*/function () {
        function ImageDisplayComponent(data, mdDialogRef, _sanitizer, _dialog) {
          _classCallCheck(this, ImageDisplayComponent);

          this.data = data;
          this.mdDialogRef = mdDialogRef;
          this._sanitizer = _sanitizer;
          this._dialog = _dialog;
          this.imageSources = [];
        }

        _createClass(ImageDisplayComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this77 = this;

            var imageSanit;
            this.images = this.data.images;
            this.imagesRaw = this.data.images;
            this.pidRecieved = this.data.pidSent; // console.log("ngONINIT display",this.images, this.data);
            //console.log("PID ON INIT ", this.pidRecieved);

            this.images.forEach(function (element) {
              _this77.currVerifiedLoanOfficerPhoto = _this77._sanitizer.bypassSecurityTrustResourceUrl('data:image/tif;base64,' + element); //this.imageSources.push(this.currVerifiedLoanOfficerPhoto);\
              // console.log(this._sanitizer.bypassSecurityTrustResourceUrl('data:image/tif;base64,' 
              // + element) as string);

              _this77.imageSources.push(_this77.currVerifiedLoanOfficerPhoto);
            });
            this.imageSources.reverse();
            this.imagesRaw.reverse();
          }
        }, {
          key: "ngAfterViewInit",
          value: function ngAfterViewInit() {}
        }, {
          key: "doubleClick",
          value: function doubleClick(event, index) {
            var _this78 = this;

            console.log(index);
            var dialogConfig = new _angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__["MatDialogConfig"]();
            dialogConfig.panelClass = 'custom-dialog-container'; //dialogConfig.data = event.target.src;

            dialogConfig.data = {
              images: event.target.src,
              sentPid: this.pidRecieved
            };

            var dialogManipulation = this._dialog.open(_image_manipulation_image_manipulation_component__WEBPACK_IMPORTED_MODULE_4__["ImageManipulationComponent"], dialogConfig);

            dialogManipulation.afterClosed().subscribe(function (res) {
              //console.log("DATA FROM IMAGE MANIPULATION", res.data);
              //console.log(res.data, "index");
              //this.imageSources.reverse();
              _this78.imageSources[index] = res.data; //this.imageSources.reverse();
              //this.imagesRaw.reverse();

              _this78.imagesRaw[index] = res.data; //this.imagesRaw.reverse();
              //  this.images[index] = res.data;
              //  this.imageSources.reverse();
              // if (res.data) {
              //   this.images.reverse();
              // }
            });
          } //end clickImage

        }, {
          key: "rotate",
          value: function rotate() {
            this.cropper.rotate(90);
          }
        }, {
          key: "createPDF",
          value: function createPDF() {
            var _this79 = this;

            // Default export is a4 paper, portrait, using millimeters for units
            var doc = new jspdf__WEBPACK_IMPORTED_MODULE_3__["jsPDF"]({
              orientation: 'p'
            }); //this.imagesRaw.reverse();

            this.imagesRaw.forEach(function (element) {
              var image = new Image();
              image.src = element;
              var heighto, widhto;
              var file, img2;
              img2 = new Image();
              var objectUrl = element;

              img2.onload = function () {
                //alert(this.width + " " + this.height);
                heighto = this.height;
                widhto = this.width;
              };

              img2.src = objectUrl;
              console.log("Comparing images  h w", image.height, image.width, _this79.imageSources[element]); //console.log("LMAOOOOOOOOOOOO",heighto, widhto);

              if (image.height != 0 && image.height != 0) {
                if (image.width > image.height) {
                  console.log("width bigger");

                  if (image.height < 900) {
                    doc.addImage(element, 'tif', 11, 15, 200, 100, null, null, 0);
                    doc.addPage("a4", "p");
                    console.log("small rectangle");
                  } else {
                    //doc.addImage(element, 'tif',11, 15, 180, 220, null,null,0);
                    console.log("width bigger and height above 900");
                    doc.addImage(element, 'tif', 2, 10, 250, 180, null, null, 0);
                    doc.addPage("a4", "p");
                  }
                } else {
                  console.log("height bigger");
                  doc.addImage(element, 'tif', 11, 15, 180, 220, null, null, 0);
                  doc.addPage("a4", "p");
                }
              } else {
                doc.addImage(element, 'tif', 5, 15, 200, 110, null, null, 0);
                doc.addPage("a4", "p");
              }
            });
            doc.deletePage(this.images.length + 1);
            doc.save("PID-" + this.pidRecieved + ".pdf");
          }
        }]);

        return ImageDisplayComponent;
      }();

      ImageDisplayComponent.ɵfac = function ImageDisplayComponent_Factory(t) {
        return new (t || ImageDisplayComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["DomSanitizer"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__["MatDialog"]));
      };

      ImageDisplayComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: ImageDisplayComponent,
        selectors: [["app-image-display"]],
        viewQuery: function ImageDisplayComponent_Query(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c0, 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c1, 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c2, 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c3, 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c4, 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c5, 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c6, 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c7, 1);
          }

          if (rf & 2) {
            var _t;

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.dataX = _t.first);
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.dataY = _t.first);
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.dataHeight = _t.first);
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.dataWidth = _t.first);
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.dataRotate = _t.first);
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.dataScaleX = _t.first);
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.dataScaleY = _t.first);
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.imageElement = _t.first);
          }
        },
        inputs: {
          images: "images"
        },
        decls: 8,
        vars: 1,
        consts: [[2, "max-height", "950px"], [1, "card"], [1, "card-body"], ["align", "center", 2, "position", "relative"], [2, "position", "absolute", "top", "-20px", "width", "30%", "left", "35%", "z-index", "1"], [1, "btn", "btn-success", 2, "border-radius", "10px", "border", "5px white", 3, "click"], [4, "ngFor", "ngForOf"], [2, "max-height", "100%", "max-width", "100%", "padding", "3em", 3, "src", "click"]],
        template: function ImageDisplayComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-dialog-content", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "span", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "button", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ImageDisplayComponent_Template_button_click_5_listener() {
              return ctx.createPDF();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Save to PDF");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, ImageDisplayComponent_div_7_Template, 2, 1, "div", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.imageSources);
          }
        },
        directives: [_angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__["MatDialogContent"], _angular_common__WEBPACK_IMPORTED_MODULE_5__["NgForOf"]],
        styles: [".btn[_ngcontent-%COMP%] {\r\n  padding-left: 0.75rem;\r\n  padding-right: 0.75rem;\r\n}\r\n\r\n  .my-class2 .modal-dialog {\r\n  max-width: 90%;\r\n  max-height: 80%;\r\n  width: 90%;\r\n  height: 80%;\r\n}\r\n\r\nlabel.btn[_ngcontent-%COMP%] {\r\n  margin-bottom: 0;\r\n}\r\n\r\n.d-flex[_ngcontent-%COMP%]    > .btn[_ngcontent-%COMP%] {\r\n  flex: 1;\r\n}\r\n\r\n.custom-dialog-container[_ngcontent-%COMP%] {\r\n  \r\n  padding: 10px -10px -10px -3px;\r\n  overflow-x: hidden;\r\n  max-width: 100vw !important;\r\n  top: -3vw;\r\n  max-height: 85vw !important;\r\n  height: 80vw;\r\n  overflow: hidden;\r\n  position: relative;\r\n}\r\n\r\n.carbonads[_ngcontent-%COMP%] {\r\n  border: 1px solid #ccc;\r\n  border-radius: 0.25rem;\r\n  font-size: 0.875rem;\r\n  overflow: hidden;\r\n  padding: 1rem;\r\n}\r\n\r\n.carbon-wrap[_ngcontent-%COMP%] {\r\n  overflow: hidden;\r\n}\r\n\r\n.carbon-img[_ngcontent-%COMP%] {\r\n  clear: left;\r\n  display: block;\r\n  float: left;\r\n}\r\n\r\n.carbon-text[_ngcontent-%COMP%], .carbon-poweredby[_ngcontent-%COMP%] {\r\n  display: block;\r\n  margin-left: 140px;\r\n}\r\n\r\n.carbon-text[_ngcontent-%COMP%], .carbon-text[_ngcontent-%COMP%]:hover, .carbon-text[_ngcontent-%COMP%]:focus {\r\n  color: #fff;\r\n  text-decoration: none;\r\n}\r\n\r\n.carbon-poweredby[_ngcontent-%COMP%], .carbon-poweredby[_ngcontent-%COMP%]:hover, .carbon-poweredby[_ngcontent-%COMP%]:focus {\r\n  color: #ddd;\r\n  text-decoration: none;\r\n}\r\n\r\n@media (min-width: 768px) {\r\n  .carbonads[_ngcontent-%COMP%] {\r\n    float: right;\r\n    margin-bottom: -1rem;\r\n    margin-top: -1rem;\r\n    max-width: 360px;\r\n  }\r\n}\r\n\r\n.footer[_ngcontent-%COMP%] {\r\n  font-size: 0.875rem;\r\n}\r\n\r\n.heart[_ngcontent-%COMP%] {\r\n  color: #ddd;\r\n  display: block;\r\n  height: 2rem;\r\n  line-height: 2rem;\r\n  margin-bottom: 0;\r\n  margin-top: 1rem;\r\n  position: relative;\r\n  text-align: center;\r\n  width: 100%;\r\n}\r\n\r\n.heart[_ngcontent-%COMP%]:hover {\r\n  color: #ff4136;\r\n}\r\n\r\n.heart[_ngcontent-%COMP%]::before {\r\n  border-top: 1px solid #eee;\r\n  content: \" \";\r\n  display: block;\r\n  height: 0;\r\n  left: 0;\r\n  position: absolute;\r\n  right: 0;\r\n  top: 50%;\r\n}\r\n\r\n.heart[_ngcontent-%COMP%]::after {\r\n  background-color: #fff;\r\n  content: \"\u2665\";\r\n  padding-left: 0.5rem;\r\n  padding-right: 0.5rem;\r\n  position: relative;\r\n  z-index: 1;\r\n}\r\n\r\n.docs-demo[_ngcontent-%COMP%] {\r\n  overflow: hidden;\r\n  padding: 2px;\r\n}\r\n\r\n.img-container[_ngcontent-%COMP%], .img-preview[_ngcontent-%COMP%] {\r\n  background-color: #f7f7f7;\r\n  text-align: center;\r\n  width: 100%;\r\n}\r\n\r\n.img-container[_ngcontent-%COMP%] {\r\n  max-height: 450px;\r\n  min-height: 350px;\r\n}\r\n\r\n@media (min-width: 768px) {\r\n  .img-container[_ngcontent-%COMP%] {\r\n    min-height: 497px;\r\n  }\r\n}\r\n\r\n.img-container[_ngcontent-%COMP%]    > img[_ngcontent-%COMP%] {\r\n  max-width: 100%;\r\n}\r\n\r\n.docs-preview[_ngcontent-%COMP%] {\r\n  margin-right: -1rem;\r\n}\r\n\r\n.img-preview[_ngcontent-%COMP%] {\r\n  float: left;\r\n  margin-bottom: 0.5rem;\r\n  margin-right: 0.5rem;\r\n  overflow: hidden;\r\n}\r\n\r\n.img-preview[_ngcontent-%COMP%]    > img[_ngcontent-%COMP%] {\r\n  max-width: 100%;\r\n}\r\n\r\n.preview-lg[_ngcontent-%COMP%] {\r\n  height: 9rem;\r\n  width: 16rem;\r\n}\r\n\r\n.preview-md[_ngcontent-%COMP%] {\r\n  height: 4.5rem;\r\n  width: 8rem;\r\n}\r\n\r\n.preview-sm[_ngcontent-%COMP%] {\r\n  height: 2.25rem;\r\n  width: 4rem;\r\n}\r\n\r\n.preview-xs[_ngcontent-%COMP%] {\r\n  height: 1.125rem;\r\n  margin-right: 0;\r\n  width: 2rem;\r\n}\r\n\r\n.docs-data[_ngcontent-%COMP%]    > .input-group[_ngcontent-%COMP%] {\r\n  margin-bottom: 0.5rem;\r\n}\r\n\r\n.docs-data[_ngcontent-%COMP%]   .input-group-prepend[_ngcontent-%COMP%]   .input-group-text[_ngcontent-%COMP%] {\r\n  min-width: 4rem;\r\n}\r\n\r\n.docs-data[_ngcontent-%COMP%]   .input-group-append[_ngcontent-%COMP%]   .input-group-text[_ngcontent-%COMP%] {\r\n  min-width: 3rem;\r\n}\r\n\r\n.docs-buttons[_ngcontent-%COMP%]    > .btn[_ngcontent-%COMP%], .docs-buttons[_ngcontent-%COMP%]    > .btn-group[_ngcontent-%COMP%], .docs-buttons[_ngcontent-%COMP%]    > .form-control[_ngcontent-%COMP%] {\r\n  margin-bottom: 0.5rem;\r\n  margin-right: 0.25rem;\r\n}\r\n\r\n.docs-toggles[_ngcontent-%COMP%]    > .btn[_ngcontent-%COMP%], .docs-toggles[_ngcontent-%COMP%]    > .btn-group[_ngcontent-%COMP%], .docs-toggles[_ngcontent-%COMP%]    > .dropdown[_ngcontent-%COMP%] {\r\n  margin-bottom: 0.5rem;\r\n}\r\n\r\n.docs-toggles[_ngcontent-%COMP%] {\r\n  margin-top: 2.5%;\r\n}\r\n\r\n.docs-tooltip[_ngcontent-%COMP%] {\r\n  display: block;\r\n  margin: -0.5rem -0.75rem;\r\n  padding: 0.5rem 0.75rem;\r\n}\r\n\r\n.docs-tooltip[_ngcontent-%COMP%]    > .icon[_ngcontent-%COMP%] {\r\n  margin: 0 -0.25rem;\r\n  vertical-align: top;\r\n}\r\n\r\n.tooltip-inner[_ngcontent-%COMP%] {\r\n  white-space: normal;\r\n}\r\n\r\n.btn-upload[_ngcontent-%COMP%]   .tooltip-inner[_ngcontent-%COMP%], .btn-toggle[_ngcontent-%COMP%]   .tooltip-inner[_ngcontent-%COMP%] {\r\n  white-space: nowrap;\r\n}\r\n\r\n.btn-toggle[_ngcontent-%COMP%] {\r\n  padding: 0.5rem;\r\n}\r\n\r\n.btn-toggle[_ngcontent-%COMP%]    > .docs-tooltip[_ngcontent-%COMP%] {\r\n  margin: -0.5rem;\r\n  padding: 0.5rem;\r\n}\r\n\r\n@media (max-width: 400px) {\r\n  .btn-group-crop[_ngcontent-%COMP%] {\r\n    margin-right: -1rem !important;\r\n  }\r\n\r\n  .btn-group-crop[_ngcontent-%COMP%]    > .btn[_ngcontent-%COMP%] {\r\n    padding-left: 0.5rem;\r\n    padding-right: 0.5rem;\r\n  }\r\n\r\n  .btn-group-crop[_ngcontent-%COMP%]   .docs-tooltip[_ngcontent-%COMP%] {\r\n    margin-left: -0.5rem;\r\n    margin-right: -0.5rem;\r\n    padding-left: 0.5rem;\r\n    padding-right: 0.5rem;\r\n  }\r\n}\r\n\r\n.docs-options[_ngcontent-%COMP%]   .dropdown-menu[_ngcontent-%COMP%] {\r\n  width: 100%;\r\n}\r\n\r\n.docs-options[_ngcontent-%COMP%]   .dropdown-menu[_ngcontent-%COMP%]    > li[_ngcontent-%COMP%] {\r\n  font-size: 0.875rem;\r\n  padding: 0.125rem 1rem;\r\n}\r\n\r\n.docs-options[_ngcontent-%COMP%]   .dropdown-menu[_ngcontent-%COMP%]   .form-check-label[_ngcontent-%COMP%] {\r\n  display: block;\r\n}\r\n\r\n.docs-cropped[_ngcontent-%COMP%]   .modal-body[_ngcontent-%COMP%] {\r\n  text-align: center;\r\n}\r\n\r\n.docs-cropped[_ngcontent-%COMP%]   .modal-body[_ngcontent-%COMP%]    > img[_ngcontent-%COMP%], .docs-cropped[_ngcontent-%COMP%]   .modal-body[_ngcontent-%COMP%]    > canvas[_ngcontent-%COMP%] {\r\n  max-width: 100%;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImltYWdlLWRpc3BsYXkuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLHFCQUFxQjtFQUNyQixzQkFBc0I7QUFDeEI7O0FBRUE7RUFDRSxjQUFjO0VBQ2QsZUFBZTtFQUNmLFVBQVU7RUFDVixXQUFXO0FBQ2I7O0FBRUE7RUFDRSxnQkFBZ0I7QUFDbEI7O0FBRUE7RUFDRSxPQUFPO0FBQ1Q7O0FBQ0E7RUFDRSxvQkFBb0I7RUFDcEIsOEJBQThCO0VBQzlCLGtCQUFrQjtFQUNsQiwyQkFBMkI7RUFDM0IsU0FBUztFQUNULDJCQUEyQjtFQUMzQixZQUFZO0VBQ1osZ0JBQWdCO0VBQ2hCLGtCQUFrQjtBQUNwQjs7QUFDQTtFQUNFLHNCQUFzQjtFQUN0QixzQkFBc0I7RUFDdEIsbUJBQW1CO0VBQ25CLGdCQUFnQjtFQUNoQixhQUFhO0FBQ2Y7O0FBRUE7RUFDRSxnQkFBZ0I7QUFDbEI7O0FBRUE7RUFDRSxXQUFXO0VBQ1gsY0FBYztFQUNkLFdBQVc7QUFDYjs7QUFFQTs7RUFFRSxjQUFjO0VBQ2Qsa0JBQWtCO0FBQ3BCOztBQUVBOzs7RUFHRSxXQUFXO0VBQ1gscUJBQXFCO0FBQ3ZCOztBQUVBOzs7RUFHRSxXQUFXO0VBQ1gscUJBQXFCO0FBQ3ZCOztBQUVBO0VBQ0U7SUFDRSxZQUFZO0lBQ1osb0JBQW9CO0lBQ3BCLGlCQUFpQjtJQUNqQixnQkFBZ0I7RUFDbEI7QUFDRjs7QUFFQTtFQUNFLG1CQUFtQjtBQUNyQjs7QUFFQTtFQUNFLFdBQVc7RUFDWCxjQUFjO0VBQ2QsWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixnQkFBZ0I7RUFDaEIsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsV0FBVztBQUNiOztBQUVBO0VBQ0UsY0FBYztBQUNoQjs7QUFFQTtFQUNFLDBCQUEwQjtFQUMxQixZQUFZO0VBQ1osY0FBYztFQUNkLFNBQVM7RUFDVCxPQUFPO0VBQ1Asa0JBQWtCO0VBQ2xCLFFBQVE7RUFDUixRQUFRO0FBQ1Y7O0FBRUE7RUFDRSxzQkFBc0I7RUFDdEIsWUFBWTtFQUNaLG9CQUFvQjtFQUNwQixxQkFBcUI7RUFDckIsa0JBQWtCO0VBQ2xCLFVBQVU7QUFDWjs7QUFFQTtFQUNFLGdCQUFnQjtFQUNoQixZQUFZO0FBQ2Q7O0FBRUE7O0VBRUUseUJBQXlCO0VBQ3pCLGtCQUFrQjtFQUNsQixXQUFXO0FBQ2I7O0FBRUE7RUFDRSxpQkFBaUI7RUFDakIsaUJBQWlCO0FBQ25COztBQUVBO0VBQ0U7SUFDRSxpQkFBaUI7RUFDbkI7QUFDRjs7QUFFQTtFQUNFLGVBQWU7QUFDakI7O0FBRUE7RUFDRSxtQkFBbUI7QUFDckI7O0FBRUE7RUFDRSxXQUFXO0VBQ1gscUJBQXFCO0VBQ3JCLG9CQUFvQjtFQUNwQixnQkFBZ0I7QUFDbEI7O0FBRUE7RUFDRSxlQUFlO0FBQ2pCOztBQUVBO0VBQ0UsWUFBWTtFQUNaLFlBQVk7QUFDZDs7QUFFQTtFQUNFLGNBQWM7RUFDZCxXQUFXO0FBQ2I7O0FBRUE7RUFDRSxlQUFlO0VBQ2YsV0FBVztBQUNiOztBQUVBO0VBQ0UsZ0JBQWdCO0VBQ2hCLGVBQWU7RUFDZixXQUFXO0FBQ2I7O0FBRUE7RUFDRSxxQkFBcUI7QUFDdkI7O0FBRUE7RUFDRSxlQUFlO0FBQ2pCOztBQUVBO0VBQ0UsZUFBZTtBQUNqQjs7QUFFQTs7O0VBR0UscUJBQXFCO0VBQ3JCLHFCQUFxQjtBQUN2Qjs7QUFFQTs7O0VBR0UscUJBQXFCO0FBQ3ZCOztBQUVBO0VBQ0UsZ0JBQWdCO0FBQ2xCOztBQUVBO0VBQ0UsY0FBYztFQUNkLHdCQUF3QjtFQUN4Qix1QkFBdUI7QUFDekI7O0FBRUE7RUFDRSxrQkFBa0I7RUFDbEIsbUJBQW1CO0FBQ3JCOztBQUVBO0VBQ0UsbUJBQW1CO0FBQ3JCOztBQUVBOztFQUVFLG1CQUFtQjtBQUNyQjs7QUFFQTtFQUNFLGVBQWU7QUFDakI7O0FBRUE7RUFDRSxlQUFlO0VBQ2YsZUFBZTtBQUNqQjs7QUFFQTtFQUNFO0lBQ0UsOEJBQThCO0VBQ2hDOztFQUVBO0lBQ0Usb0JBQW9CO0lBQ3BCLHFCQUFxQjtFQUN2Qjs7RUFFQTtJQUNFLG9CQUFvQjtJQUNwQixxQkFBcUI7SUFDckIsb0JBQW9CO0lBQ3BCLHFCQUFxQjtFQUN2QjtBQUNGOztBQUVBO0VBQ0UsV0FBVztBQUNiOztBQUVBO0VBQ0UsbUJBQW1CO0VBQ25CLHNCQUFzQjtBQUN4Qjs7QUFFQTtFQUNFLGNBQWM7QUFDaEI7O0FBRUE7RUFDRSxrQkFBa0I7QUFDcEI7O0FBRUE7O0VBRUUsZUFBZTtBQUNqQiIsImZpbGUiOiJpbWFnZS1kaXNwbGF5LmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYnRuIHtcclxuICBwYWRkaW5nLWxlZnQ6IDAuNzVyZW07XHJcbiAgcGFkZGluZy1yaWdodDogMC43NXJlbTtcclxufVxyXG5cclxuOjpuZy1kZWVwIC5teS1jbGFzczIgLm1vZGFsLWRpYWxvZyB7XHJcbiAgbWF4LXdpZHRoOiA5MCU7XHJcbiAgbWF4LWhlaWdodDogODAlO1xyXG4gIHdpZHRoOiA5MCU7XHJcbiAgaGVpZ2h0OiA4MCU7XHJcbn1cclxuXHJcbmxhYmVsLmJ0biB7XHJcbiAgbWFyZ2luLWJvdHRvbTogMDtcclxufVxyXG5cclxuLmQtZmxleCA+IC5idG4ge1xyXG4gIGZsZXg6IDE7XHJcbn1cclxuLmN1c3RvbS1kaWFsb2ctY29udGFpbmVyIHtcclxuICAvKiBhZGQgeW91ciBzdHlsZXMgKi9cclxuICBwYWRkaW5nOiAxMHB4IC0xMHB4IC0xMHB4IC0zcHg7XHJcbiAgb3ZlcmZsb3cteDogaGlkZGVuO1xyXG4gIG1heC13aWR0aDogMTAwdncgIWltcG9ydGFudDtcclxuICB0b3A6IC0zdnc7XHJcbiAgbWF4LWhlaWdodDogODV2dyAhaW1wb3J0YW50O1xyXG4gIGhlaWdodDogODB2dztcclxuICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxufVxyXG4uY2FyYm9uYWRzIHtcclxuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gIGJvcmRlci1yYWRpdXM6IDAuMjVyZW07XHJcbiAgZm9udC1zaXplOiAwLjg3NXJlbTtcclxuICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gIHBhZGRpbmc6IDFyZW07XHJcbn1cclxuXHJcbi5jYXJib24td3JhcCB7XHJcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcclxufVxyXG5cclxuLmNhcmJvbi1pbWcge1xyXG4gIGNsZWFyOiBsZWZ0O1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIGZsb2F0OiBsZWZ0O1xyXG59XHJcblxyXG4uY2FyYm9uLXRleHQsXHJcbi5jYXJib24tcG93ZXJlZGJ5IHtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICBtYXJnaW4tbGVmdDogMTQwcHg7XHJcbn1cclxuXHJcbi5jYXJib24tdGV4dCxcclxuLmNhcmJvbi10ZXh0OmhvdmVyLFxyXG4uY2FyYm9uLXRleHQ6Zm9jdXMge1xyXG4gIGNvbG9yOiAjZmZmO1xyXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxufVxyXG5cclxuLmNhcmJvbi1wb3dlcmVkYnksXHJcbi5jYXJib24tcG93ZXJlZGJ5OmhvdmVyLFxyXG4uY2FyYm9uLXBvd2VyZWRieTpmb2N1cyB7XHJcbiAgY29sb3I6ICNkZGQ7XHJcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG59XHJcblxyXG5AbWVkaWEgKG1pbi13aWR0aDogNzY4cHgpIHtcclxuICAuY2FyYm9uYWRzIHtcclxuICAgIGZsb2F0OiByaWdodDtcclxuICAgIG1hcmdpbi1ib3R0b206IC0xcmVtO1xyXG4gICAgbWFyZ2luLXRvcDogLTFyZW07XHJcbiAgICBtYXgtd2lkdGg6IDM2MHB4O1xyXG4gIH1cclxufVxyXG5cclxuLmZvb3RlciB7XHJcbiAgZm9udC1zaXplOiAwLjg3NXJlbTtcclxufVxyXG5cclxuLmhlYXJ0IHtcclxuICBjb2xvcjogI2RkZDtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICBoZWlnaHQ6IDJyZW07XHJcbiAgbGluZS1oZWlnaHQ6IDJyZW07XHJcbiAgbWFyZ2luLWJvdHRvbTogMDtcclxuICBtYXJnaW4tdG9wOiAxcmVtO1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5oZWFydDpob3ZlciB7XHJcbiAgY29sb3I6ICNmZjQxMzY7XHJcbn1cclxuXHJcbi5oZWFydDo6YmVmb3JlIHtcclxuICBib3JkZXItdG9wOiAxcHggc29saWQgI2VlZTtcclxuICBjb250ZW50OiBcIiBcIjtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICBoZWlnaHQ6IDA7XHJcbiAgbGVmdDogMDtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgcmlnaHQ6IDA7XHJcbiAgdG9wOiA1MCU7XHJcbn1cclxuXHJcbi5oZWFydDo6YWZ0ZXIge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XHJcbiAgY29udGVudDogXCLimaVcIjtcclxuICBwYWRkaW5nLWxlZnQ6IDAuNXJlbTtcclxuICBwYWRkaW5nLXJpZ2h0OiAwLjVyZW07XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIHotaW5kZXg6IDE7XHJcbn1cclxuXHJcbi5kb2NzLWRlbW8ge1xyXG4gIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgcGFkZGluZzogMnB4O1xyXG59XHJcblxyXG4uaW1nLWNvbnRhaW5lcixcclxuLmltZy1wcmV2aWV3IHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdmN2Y3O1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLmltZy1jb250YWluZXIge1xyXG4gIG1heC1oZWlnaHQ6IDQ1MHB4O1xyXG4gIG1pbi1oZWlnaHQ6IDM1MHB4O1xyXG59XHJcblxyXG5AbWVkaWEgKG1pbi13aWR0aDogNzY4cHgpIHtcclxuICAuaW1nLWNvbnRhaW5lciB7XHJcbiAgICBtaW4taGVpZ2h0OiA0OTdweDtcclxuICB9XHJcbn1cclxuXHJcbi5pbWctY29udGFpbmVyID4gaW1nIHtcclxuICBtYXgtd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5kb2NzLXByZXZpZXcge1xyXG4gIG1hcmdpbi1yaWdodDogLTFyZW07XHJcbn1cclxuXHJcbi5pbWctcHJldmlldyB7XHJcbiAgZmxvYXQ6IGxlZnQ7XHJcbiAgbWFyZ2luLWJvdHRvbTogMC41cmVtO1xyXG4gIG1hcmdpbi1yaWdodDogMC41cmVtO1xyXG4gIG92ZXJmbG93OiBoaWRkZW47XHJcbn1cclxuXHJcbi5pbWctcHJldmlldyA+IGltZyB7XHJcbiAgbWF4LXdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4ucHJldmlldy1sZyB7XHJcbiAgaGVpZ2h0OiA5cmVtO1xyXG4gIHdpZHRoOiAxNnJlbTtcclxufVxyXG5cclxuLnByZXZpZXctbWQge1xyXG4gIGhlaWdodDogNC41cmVtO1xyXG4gIHdpZHRoOiA4cmVtO1xyXG59XHJcblxyXG4ucHJldmlldy1zbSB7XHJcbiAgaGVpZ2h0OiAyLjI1cmVtO1xyXG4gIHdpZHRoOiA0cmVtO1xyXG59XHJcblxyXG4ucHJldmlldy14cyB7XHJcbiAgaGVpZ2h0OiAxLjEyNXJlbTtcclxuICBtYXJnaW4tcmlnaHQ6IDA7XHJcbiAgd2lkdGg6IDJyZW07XHJcbn1cclxuXHJcbi5kb2NzLWRhdGEgPiAuaW5wdXQtZ3JvdXAge1xyXG4gIG1hcmdpbi1ib3R0b206IDAuNXJlbTtcclxufVxyXG5cclxuLmRvY3MtZGF0YSAuaW5wdXQtZ3JvdXAtcHJlcGVuZCAuaW5wdXQtZ3JvdXAtdGV4dCB7XHJcbiAgbWluLXdpZHRoOiA0cmVtO1xyXG59XHJcblxyXG4uZG9jcy1kYXRhIC5pbnB1dC1ncm91cC1hcHBlbmQgLmlucHV0LWdyb3VwLXRleHQge1xyXG4gIG1pbi13aWR0aDogM3JlbTtcclxufVxyXG5cclxuLmRvY3MtYnV0dG9ucyA+IC5idG4sXHJcbi5kb2NzLWJ1dHRvbnMgPiAuYnRuLWdyb3VwLFxyXG4uZG9jcy1idXR0b25zID4gLmZvcm0tY29udHJvbCB7XHJcbiAgbWFyZ2luLWJvdHRvbTogMC41cmVtO1xyXG4gIG1hcmdpbi1yaWdodDogMC4yNXJlbTtcclxufVxyXG5cclxuLmRvY3MtdG9nZ2xlcyA+IC5idG4sXHJcbi5kb2NzLXRvZ2dsZXMgPiAuYnRuLWdyb3VwLFxyXG4uZG9jcy10b2dnbGVzID4gLmRyb3Bkb3duIHtcclxuICBtYXJnaW4tYm90dG9tOiAwLjVyZW07XHJcbn1cclxuXHJcbi5kb2NzLXRvZ2dsZXMge1xyXG4gIG1hcmdpbi10b3A6IDIuNSU7XHJcbn1cclxuXHJcbi5kb2NzLXRvb2x0aXAge1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIG1hcmdpbjogLTAuNXJlbSAtMC43NXJlbTtcclxuICBwYWRkaW5nOiAwLjVyZW0gMC43NXJlbTtcclxufVxyXG5cclxuLmRvY3MtdG9vbHRpcCA+IC5pY29uIHtcclxuICBtYXJnaW46IDAgLTAuMjVyZW07XHJcbiAgdmVydGljYWwtYWxpZ246IHRvcDtcclxufVxyXG5cclxuLnRvb2x0aXAtaW5uZXIge1xyXG4gIHdoaXRlLXNwYWNlOiBub3JtYWw7XHJcbn1cclxuXHJcbi5idG4tdXBsb2FkIC50b29sdGlwLWlubmVyLFxyXG4uYnRuLXRvZ2dsZSAudG9vbHRpcC1pbm5lciB7XHJcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxufVxyXG5cclxuLmJ0bi10b2dnbGUge1xyXG4gIHBhZGRpbmc6IDAuNXJlbTtcclxufVxyXG5cclxuLmJ0bi10b2dnbGUgPiAuZG9jcy10b29sdGlwIHtcclxuICBtYXJnaW46IC0wLjVyZW07XHJcbiAgcGFkZGluZzogMC41cmVtO1xyXG59XHJcblxyXG5AbWVkaWEgKG1heC13aWR0aDogNDAwcHgpIHtcclxuICAuYnRuLWdyb3VwLWNyb3Age1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAtMXJlbSAhaW1wb3J0YW50O1xyXG4gIH1cclxuXHJcbiAgLmJ0bi1ncm91cC1jcm9wID4gLmJ0biB7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDAuNXJlbTtcclxuICAgIHBhZGRpbmctcmlnaHQ6IDAuNXJlbTtcclxuICB9XHJcblxyXG4gIC5idG4tZ3JvdXAtY3JvcCAuZG9jcy10b29sdGlwIHtcclxuICAgIG1hcmdpbi1sZWZ0OiAtMC41cmVtO1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAtMC41cmVtO1xyXG4gICAgcGFkZGluZy1sZWZ0OiAwLjVyZW07XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiAwLjVyZW07XHJcbiAgfVxyXG59XHJcblxyXG4uZG9jcy1vcHRpb25zIC5kcm9wZG93bi1tZW51IHtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLmRvY3Mtb3B0aW9ucyAuZHJvcGRvd24tbWVudSA+IGxpIHtcclxuICBmb250LXNpemU6IDAuODc1cmVtO1xyXG4gIHBhZGRpbmc6IDAuMTI1cmVtIDFyZW07XHJcbn1cclxuXHJcbi5kb2NzLW9wdGlvbnMgLmRyb3Bkb3duLW1lbnUgLmZvcm0tY2hlY2stbGFiZWwge1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG59XHJcblxyXG4uZG9jcy1jcm9wcGVkIC5tb2RhbC1ib2R5IHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbi5kb2NzLWNyb3BwZWQgLm1vZGFsLWJvZHkgPiBpbWcsXHJcbi5kb2NzLWNyb3BwZWQgLm1vZGFsLWJvZHkgPiBjYW52YXMge1xyXG4gIG1heC13aWR0aDogMTAwJTtcclxufVxyXG4iXX0= */"]
      });
      /***/
    },

    /***/
    "h1ku":
    /*!*******************************************************************!*\
      !*** ./src/app/pages/tid-creation/tid-creation-routing.module.ts ***!
      \*******************************************************************/

    /*! exports provided: TidCreationRoutingModule */

    /***/
    function h1ku(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "TidCreationRoutingModule", function () {
        return TidCreationRoutingModule;
      });
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _generate_tid_generate_tid_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./generate-tid/generate-tid.component */
      "VZ5Q");
      /* harmony import */


      var src_app_services_packages_add_packages_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! src/app/services/packages/add-packages.service */
      "wlS1");
      /* harmony import */


      var src_app_services_coversheet_create_coversheet_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/app/services/coversheet/create-coversheet.service */
      "44oB");
      /* harmony import */


      var src_app_services_packages_audit_trail_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! src/app/services/packages/audit-trail.service */
      "irGt");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var routes = [{
        path: 'generateTid',
        component: _generate_tid_generate_tid_component__WEBPACK_IMPORTED_MODULE_1__["GenerateTidComponent"]
      }];

      var TidCreationRoutingModule = function TidCreationRoutingModule() {
        _classCallCheck(this, TidCreationRoutingModule);
      };

      TidCreationRoutingModule.ɵfac = function TidCreationRoutingModule_Factory(t) {
        return new (t || TidCreationRoutingModule)();
      };

      TidCreationRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineNgModule"]({
        type: TidCreationRoutingModule
      });
      TidCreationRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineInjector"]({
        providers: [src_app_services_packages_add_packages_service__WEBPACK_IMPORTED_MODULE_2__["AddPackagesService"], src_app_services_coversheet_create_coversheet_service__WEBPACK_IMPORTED_MODULE_3__["CreateCoversheetService"], src_app_services_packages_audit_trail_service__WEBPACK_IMPORTED_MODULE_4__["AuditTrailService"]],
        imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"].forChild(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵsetNgModuleScope"](TidCreationRoutingModule, {
          imports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]],
          exports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]]
        });
      })();
      /***/

    },

    /***/
    "he3H":
    /*!**********************************************************!*\
      !*** ./src/app/services/packages/table-audit.service.ts ***!
      \**********************************************************/

    /*! exports provided: TableAuditService */

    /***/
    function he3H(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "TableAuditService", function () {
        return TableAuditService;
      });
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");
      /* harmony import */


      var _shared_http_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../shared/http-rest.service */
      "vulr");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var TableAuditService = /*#__PURE__*/function (_shared_http_rest_ser8) {
        _inherits(TableAuditService, _shared_http_rest_ser8);

        var _super8 = _createSuper(TableAuditService);

        function TableAuditService(http, router) {
          var _this80;

          _classCallCheck(this, TableAuditService);

          _this80 = _super8.call(this, http);
          _this80.router = router;
          return _this80;
        }

        _createClass(TableAuditService, [{
          key: "getAllbyTN",
          value: function getAllbyTN(id) {
            return this.findAllByBC(id, "taudit", "FindAllbyTN");
          }
        }, {
          key: "getAllbyPID",
          value: function getAllbyPID(id) {
            return this.findAllById(id, "taudit", "FindAllbyPID");
          }
        }]);

        return TableAuditService;
      }(_shared_http_rest_service__WEBPACK_IMPORTED_MODULE_2__["HttpRestService"]);

      TableAuditService.ɵfac = function TableAuditService_Factory(t) {
        return new (t || TableAuditService)(_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵinject"](_angular_router__WEBPACK_IMPORTED_MODULE_0__["Router"]));
      };

      TableAuditService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineInjectable"]({
        token: TableAuditService,
        factory: TableAuditService.ɵfac,
        providedIn: 'root'
      });
      /***/
    },

    /***/
    "irGt":
    /*!**********************************************************!*\
      !*** ./src/app/services/packages/audit-trail.service.ts ***!
      \**********************************************************/

    /*! exports provided: AuditTrailService */

    /***/
    function irGt(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AuditTrailService", function () {
        return AuditTrailService;
      });
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");
      /* harmony import */


      var _shared_http_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../shared/http-rest.service */
      "vulr");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var AuditTrailService = /*#__PURE__*/function (_shared_http_rest_ser9) {
        _inherits(AuditTrailService, _shared_http_rest_ser9);

        var _super9 = _createSuper(AuditTrailService);

        function AuditTrailService(http, router) {
          var _this81;

          _classCallCheck(this, AuditTrailService);

          _this81 = _super9.call(this, http);
          _this81.router = router;
          return _this81;
        }

        _createClass(AuditTrailService, [{
          key: "saveAudit",
          value: function saveAudit(audit) {
            return this.save(audit, "audit", "save");
          }
        }, {
          key: "retrieveLatestBUSort",
          value: function retrieveLatestBUSort(audit) {
            return this.findUniqueById(audit, "audit", "searchLatestSort");
          }
        }]);

        return AuditTrailService;
      }(_shared_http_rest_service__WEBPACK_IMPORTED_MODULE_2__["HttpRestService"]); //end class


      AuditTrailService.ɵfac = function AuditTrailService_Factory(t) {
        return new (t || AuditTrailService)(_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵinject"](_angular_router__WEBPACK_IMPORTED_MODULE_0__["Router"]));
      };

      AuditTrailService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineInjectable"]({
        token: AuditTrailService,
        factory: AuditTrailService.ɵfac
      });
      /***/
    },

    /***/
    "j7lE":
    /*!*************************************************!*\
      !*** ./src/app/services/users/users.service.ts ***!
      \*************************************************/

    /*! exports provided: UsersService */

    /***/
    function j7lE(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "UsersService", function () {
        return UsersService;
      });
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");
      /* harmony import */


      var _shared_http_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../shared/http-rest.service */
      "vulr");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var UsersService = /*#__PURE__*/function (_shared_http_rest_ser10) {
        _inherits(UsersService, _shared_http_rest_ser10);

        var _super10 = _createSuper(UsersService);

        function UsersService(http, router, FormsModule) {
          var _this82;

          _classCallCheck(this, UsersService);

          _this82 = _super10.call(this, http);
          _this82.router = router;
          _this82.FormsModule = FormsModule;
          _this82.user = {
            User_Login: "",
            Admin_Flag: true,
            Available: true,
            User_Name: "",
            User_Password: "",
            FirstName: "",
            LastName: ""
          };
          return _this82;
        }

        _createClass(UsersService, [{
          key: "findAllUsers",
          value: function findAllUsers() {
            return this.findAll("users", "FindAll");
          }
        }, {
          key: "findUserById",
          value: function findUserById(id) {
            return this.findUniqueById(id, "users", "FindById");
          }
        }, {
          key: "saveUser",
          value: function saveUser(user) {
            return this.save(user, "users", "Save");
          }
        }, {
          key: "bulkSave",
          value: function bulkSave(list) {
            return this.saveBulk(list, "users", "bulkSave");
          }
        }, {
          key: "disableUser",
          value: function disableUser(id) {
            return this.save(id, "users", "disable");
          }
        }, {
          key: "asyncDELETERequest",
          value: function asyncDELETERequest(pack) {
            return this.asyncHttpRequest("POST", "users", "deletePackage", "", pack);
          }
        }, {
          key: "findUsersByName",
          value: function findUsersByName(name) {
            return this.asyncHttpRequest("GET", "Users", "FindByFilters", {
              name: name
            }, null);
          }
        }, {
          key: "login",
          value: function login(user) {
            return this.save(user, "users", "Login");
          }
        }, {
          key: "saveStorage",
          value: function saveStorage(user) {
            localStorage.setItem('user', JSON.stringify(user));
            this.user = user;
          }
        }, {
          key: "isLogged",
          value: function isLogged() {
            return this.user !== null ? true : false;
          }
        }, {
          key: "logout",
          value: function logout() {
            this.user = null;
            localStorage.removeItem('user');
            this.router.navigate(['/login']);
          }
        }]);

        return UsersService;
      }(_shared_http_rest_service__WEBPACK_IMPORTED_MODULE_2__["HttpRestService"]);

      UsersService.ɵfac = function UsersService_Factory(t) {
        return new (t || UsersService)(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](_angular_router__WEBPACK_IMPORTED_MODULE_0__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"]));
      };

      UsersService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineInjectable"]({
        token: UsersService,
        factory: UsersService.ɵfac
      });
      /***/
    },

    /***/
    "jQpT":
    /*!***************************************************!*\
      !*** ./src/app/shared/footer/footer.component.ts ***!
      \***************************************************/

    /*! exports provided: FooterComponent */

    /***/
    function jQpT(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "FooterComponent", function () {
        return FooterComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var FooterComponent = /*#__PURE__*/function () {
        function FooterComponent() {
          _classCallCheck(this, FooterComponent);
        }

        _createClass(FooterComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return FooterComponent;
      }();

      FooterComponent.ɵfac = function FooterComponent_Factory(t) {
        return new (t || FooterComponent)();
      };

      FooterComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: FooterComponent,
        selectors: [["app-footer"]],
        decls: 2,
        vars: 0,
        consts: [[1, "footer", "text-right"]],
        template: function FooterComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "footer", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " 2021 \xA9 Iron Mountain - Version: 12092021\n");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }
        },
        encapsulation: 2
      });
      /***/
    },

    /***/
    "jaMP":
    /*!***************************************************************!*\
      !*** ./src/app/pages/workflow/pidcheck/pidcheck.component.ts ***!
      \***************************************************************/

    /*! exports provided: PidcheckComponent */

    /***/
    function jaMP(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PidcheckComponent", function () {
        return PidcheckComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var src_app_services_packages_add_packages_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! src/app/services/packages/add-packages.service */
      "wlS1");
      /* harmony import */


      var src_app_services_packages_taudit_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/app/services/packages/taudit.service */
      "r/Lp");
      /* harmony import */


      var src_app_services_packages_worflow_task_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! src/app/services/packages/worflow-task.service */
      "dSTU");
      /* harmony import */


      var src_app_services_packages_worflow_status_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! src/app/services/packages/worflow-status.service */
      "/a8Q");
      /* harmony import */


      var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! sweetalert2/dist/sweetalert2.js */
      "PdH4");
      /* harmony import */


      var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_6__);
      /* harmony import */


      var jquery__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! jquery */
      "EVdn");
      /* harmony import */


      var jquery__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_7__);
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var src_app_services_packages_worflow_rel_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! src/app/services/packages/worflow-rel.service */
      "H7Gl");
      /* harmony import */


      var src_app_services_packages_table_audit_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! src/app/services/packages/table-audit.service */
      "he3H");
      /* harmony import */


      var src_app_services_packages_exception_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! src/app/services/packages/exception.service */
      "y9jz");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");

      var _c0 = ["TaskSelector"];

      function PidcheckComponent_option_24_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "option", 35);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var client_r7 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngValue", client_r7.Workflow_Task_ID);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](client_r7.Workflow_Task_Name);
        }
      }

      function PidcheckComponent_option_32_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "option", 35);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var client_r8 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngValue", client_r8.Workflow_Status_ID);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](client_r8.Workflow_Status_NAME);
        }
      }

      function PidcheckComponent_th_77_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "th", 36);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var column_r9 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](column_r9);
        }
      }

      function PidcheckComponent_tr_78_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "tr");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "td");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](3, "date");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "td");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](5);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "td");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](7);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "td");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](9);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "td");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](11);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "td");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](13);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](14, "td");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](15);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](16, "td");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](17);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var row_r10 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](3, 8, row_r10.DateTimeCreated, "dd/MM/yyyy hh:mm:ss"));

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("", row_r10.TrackingNumber, " ");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("", row_r10.PID, " ");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("", row_r10.Workflow_Name, " ");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("", row_r10.Task_Name, " ");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("", row_r10.Status_Name, " ");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("", row_r10.Exception_Name, " ");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("", row_r10.User_Name, " ");
        }
      }

      var PidcheckComponent = /*#__PURE__*/function () {
        //Methods
        function PidcheckComponent(packageService, auditService, taskService, workfNamesService, statusService, tableService, exceptionService, router) {
          _classCallCheck(this, PidcheckComponent);

          this.packageService = packageService;
          this.auditService = auditService;
          this.taskService = taskService;
          this.workfNamesService = workfNamesService;
          this.statusService = statusService;
          this.tableService = tableService;
          this.exceptionService = exceptionService;
          this.router = router; //Variable Declarations

          this.rows = [];
          this.clients = [];
          this.status = ["In Progress", "Ready", "Suspend", "Complete", "Exception"];
          this.headers = ["Date Time", "Tracking", "PID", "Workflow", "Task", "Status", "Optional Task", "User"];
          this.displaySelectors = false;
          this.message = "";
          this.exceptionList = [];
          this.taskList = [];
          this.workflowList = [];
          this.auditList = [];
          this.statusList = [];
          this.tableList = [];
          this.completeStatusList = [];
          this.TlistComplete = [];
          this.dataTable = [];
        }

        _createClass(PidcheckComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.workflowDisplayName = "";
            this.checkboxTrue = true;
            this.userlogged = JSON.parse(localStorage.getItem("user"));

            if (this.userlogged != null) {
              console.log("User logged: " + this.userlogged.User_Login);
            } else {
              this.router.navigate(["/login"]);
            }

            this.fillform();
            this.displaySelectors = false;
            this.taskSelected = ""; //this.getWorkflows();

            this.getStatus();
            this.getAllStatus();
            this.enableFields = false;
            this.resetFound();
          }
        }, {
          key: "onSubmit",
          value: function onSubmit() {
            this.packFound.ManifestInBox = true;
            this.idPIDorTN();
            this.resetFound();
            this.enableFields = false;
            this.auditList = []; //SEARCH WHICH STATUS HAS BEEN COMPLETED

            this.refreshTable();
            this.getStatus();
            this.onOptionsSelected(this.packFound.Status_ID); //setTimeout(() => this.taskSelector.nativeElement.focus() , .5);
          }
        }, {
          key: "checkCompletedTask",
          value: function checkCompletedTask(list) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var _this83 = this;

              var filterComplete, orderList, listaCompleta, lastOrder, ultimoTask;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      filterComplete = list;
                      console.log("lista para filtrar", filterComplete);
                      orderList = [];
                      listaCompleta = [];
                      filterComplete = filterComplete.filter(function (x) {
                        return x.Status_Name == "Complete";
                      });
                      console.log("all completed task", filterComplete);
                      console.log("all  task", this.taskList);
                      this.taskList.forEach(function (elementTask) {
                        _this83.message += "<div class='container p-3 my-3 bg-secondary text-white border border-secondary rounded'> " + elementTask.Workflow_Task_Name + "</div>";
                        listaCompleta.push(elementTask.Stage_Order);
                        filterComplete.forEach(function (elementCompletedTask) {
                          //PAINT COMPLETED STATUS
                          if (elementTask.Workflow_Task_Name == elementCompletedTask.Task_Name) {
                            orderList.push(elementTask.Stage_Order);
                            console.log("TASK COMPLETED: ", elementTask.Workflow_Task_Name);

                            if (_this83.message.includes("<div class='container p-3 my-3 bg-secondary text-white border border-secondary rounded'> " + elementTask.Workflow_Task_Name + "</div>")) {
                              _this83.message = _this83.message.replace("<div class='container p-3 my-3 bg-secondary text-white border border-secondary rounded'> " + elementTask.Workflow_Task_Name + "</div>", "<div class='container p-3 my-3 bg-success text-white border border-success rounded'> " + elementTask.Workflow_Task_Name + "</div>");
                            }
                          }
                        });
                      });
                      lastOrder = Math.max.apply(Math, orderList);
                      console.log("last task completed ", lastOrder);
                      ultimoTask = Math.max.apply(Math, listaCompleta);
                      console.log("last task in workflow", ultimoTask);
                      this.taskList.forEach(function (element) {
                        filterComplete.forEach(function (elementComplete) {
                          if (element.Workflow_Task_Name == elementComplete.Task_Name) {
                            if (lastOrder == ultimoTask) {
                              console.log("CHAOS BORN");
                              _this83.packFound.Package_Finished = true;

                              _this83.updatePackageNoSwal();

                              _this83.enableFields = true;
                            }
                          }
                        });
                      });

                    case 13:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "getCurrentTask",
          value: function getCurrentTask(task) {
            var _this84 = this;

            this.taskService.getCurrentTask(task).subscribe(function (res) {
              console.log("Current task", res);
              _this84.currentTask = res;
            });
          }
        }, {
          key: "onOptionsSelected",
          value: function onOptionsSelected(value) {
            console.log("the selected value is " + value.valueOf());
            this.getStatus1(value);
          }
        }, {
          key: "onStatusSelected",
          value: function onStatusSelected(value) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
              var _this85 = this;

              var statusIDSelected;
              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                while (1) {
                  switch (_context3.prev = _context3.next) {
                    case 0:
                      console.log("the selected value of status is " + value);
                      this.getStatus();
                      statusIDSelected = value;
                      this.statusService.findById(statusIDSelected).subscribe(function (res) {
                        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this85, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
                          var _this86 = this;

                          var options, _yield$sweetalert2_di, exceptionSelected, exceptionFiltered;

                          return regeneratorRuntime.wrap(function _callee2$(_context2) {
                            while (1) {
                              switch (_context2.prev = _context2.next) {
                                case 0:
                                  //statusName = res.Workflow_Status_NAME;
                                  console.log("STATUSPADREEEE", res.Workflow_Status_NAME);

                                  if (!(res.Workflow_Status_NAME == "Optional Task")) {
                                    _context2.next = 20;
                                    break;
                                  }

                                  console.log("HE PICKED AN EXCEPTIOOOOON DO SOMETHING!!!!");
                                  this.exceptionList = this.exceptionList.filter(function (x) {
                                    return x.workflow_ID == _this86.packFound.WorkFlow_ID;
                                  });
                                  options = {};
                                  jquery__WEBPACK_IMPORTED_MODULE_7__["map"](this.exceptionList, function (o) {
                                    options[o.Exception_ID] = o.Exception_Name;
                                  });
                                  console.log("LISTA DE EXCEPTIONS FILTRADA", this.exceptionList);
                                  console.log(options);
                                  _context2.next = 10;
                                  return sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_6___default.a.fire({
                                    title: "Select an optional task",
                                    input: "select",
                                    inputOptions: options,
                                    inputPlaceholder: "Select task",
                                    showCancelButton: true
                                  });

                                case 10:
                                  _yield$sweetalert2_di = _context2.sent;
                                  exceptionSelected = _yield$sweetalert2_di.value;
                                  console.log("Exception selected ", exceptionSelected);
                                  exceptionFiltered = this.exceptionList.find(function (x) {
                                    return x.Exception_ID == exceptionSelected;
                                  });
                                  console.log("exceptionFiltered was ", exceptionFiltered);
                                  this.packFound.Exception_Name = exceptionFiltered.Exception_Name;
                                  this.packFound.Exception_Flag = true;
                                  this.updatePackage();
                                  this.insertTaudit();
                                  this.onSubmit();

                                case 20:
                                case "end":
                                  return _context2.stop();
                              }
                            }
                          }, _callee2, this);
                        }));
                      }, function (error) {//Swal("Can't find", String(error.Message) ,"error");
                      });

                    case 4:
                    case "end":
                      return _context3.stop();
                  }
                }
              }, _callee3, this);
            }));
          }
        }, {
          key: "idPIDorTN",
          value: function idPIDorTN() {
            console.log(this.inputReader);
            this.inputReader = this.inputReader.toLowerCase();
            console.log(this.inputReader);

            if (this.inputReader.startsWith("pid-")) {
              var res = this.inputReader.replace("pid-", "");
              var y = +res; //console.log("numero modificado sin pid " + y);

              this.packageService.formData.Package_Tracking_ID = y;
              this.findOneByPID(this.packageService.formData.Package_Tracking_ID);
            } else {
              //Seach item does not start with PID, searched with Tracking number
              this.packageService.formData.Package_Tracking_BC = this.inputReader;
              this.findOneByTrackingNumber(this.packageService.formData.Package_Tracking_BC);
            }
          }
        }, {
          key: "checkIfException",
          value: function checkIfException() {
            if (this.packFound.Exception_Flag == true) {
              console.log("THIS PACKAGE HAS AN EXCEPTION");
              this.showNextTask();
            }
          }
        }, {
          key: "showNextTask",
          value: function showNextTask() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
              var _this87 = this;

              var options;
              return regeneratorRuntime.wrap(function _callee5$(_context5) {
                while (1) {
                  switch (_context5.prev = _context5.next) {
                    case 0:
                      options = {};
                      jquery__WEBPACK_IMPORTED_MODULE_7__["map"](this.taskList, function (o) {
                        options[o.Workflow_Task_ID] = o.Workflow_Task_Name;
                      });
                      console.log("LISTA DE TASK FILTRADA", this.taskList);
                      console.log(options);
                      sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_6___default.a.fire({
                        title: "This package is in optional task " + this.packFound.Exception_Name,
                        text: "Do you want to change to Workflow task?",
                        showCancelButton: true,
                        confirmButtonColor: "#3085d6",
                        cancelButtonColor: "#d33",
                        confirmButtonText: "Change Task"
                      }).then(function (result) {
                        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this87, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
                          var _yield$sweetalert2_di2, nextTask, newTaskToSet;

                          return regeneratorRuntime.wrap(function _callee4$(_context4) {
                            while (1) {
                              switch (_context4.prev = _context4.next) {
                                case 0:
                                  if (!result.value) {
                                    _context4.next = 12;
                                    break;
                                  }

                                  _context4.next = 3;
                                  return sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_6___default.a.fire({
                                    title: "Select next Task for this package",
                                    input: "select",
                                    inputOptions: options,
                                    inputPlaceholder: "Select a task",
                                    showCancelButton: true
                                  });

                                case 3:
                                  _yield$sweetalert2_di2 = _context4.sent;
                                  nextTask = _yield$sweetalert2_di2.value;
                                  //console.log("Exception selected " , nextTask);
                                  newTaskToSet = this.taskList.find(function (x) {
                                    return x.Workflow_Task_ID == nextTask;
                                  }); //console.log('the following task id is about to be saved',newTaskToSet);

                                  this.packFound.Task_ID = newTaskToSet.Workflow_Task_ID;
                                  this.packFound.Exception_Flag = false;
                                  this.packFound.Exception_Name = ""; //falta setear el status id al paquete
                                  //console.log('NO OLVIDES QUE TE FALTA SETEAR EL STATUS ID AL PAQUETE ');

                                  this.setNewStatus(newTaskToSet);
                                  this.updatePackage();
                                  this.insertTaudit();

                                case 12:
                                case "end":
                                  return _context4.stop();
                              }
                            }
                          }, _callee4, this);
                        }));
                      });

                    case 5:
                    case "end":
                      return _context5.stop();
                  }
                }
              }, _callee5, this);
            }));
          }
        }, {
          key: "setNewStatus",
          value: function setNewStatus(newTaskToSet) {
            this.completeStatusList = this.completeStatusList.filter(function (x) {
              return x.Workflow_Task_ID == newTaskToSet.Workflow_Task_ID;
            }); //console.log('newComplete Status list filtered');

            var filteredStatus = this.completeStatusList.find(function (x) {
              return x.Step_Order == 1;
            }); //console.log('filtered status',filteredStatus);

            this.packFound.Status_ID = filteredStatus.Workflow_Status_ID;
          }
        }, {
          key: "getAllStatus",
          value: function getAllStatus() {
            var _this88 = this;

            this.statusService.getAll().subscribe(function (response) {
              // console.log('This is the complete status list', response );
              _this88.completeStatusList = response; // console.log('Asinged value to status list : ', this.completeStatusList);
            }); //end observable for get all status
          }
        }, {
          key: "fillform",
          value: function fillform() {
            this.packageService.formData = {
              Package_Tracking_BC: "",
              Package_Tracking_ID: "",
              totalLenders: 0,
              LoanNumber: 0,
              Sticker_ID: "",
              Location_Name: "",
              Client_Name: "",
              clearPackagesFlag: false,
              multipleLenders: false,
              WorkFlow_ID: 0,
              Task_ID: 0,
              Status_ID: 0,
              Creation_Date_Time: "",
              Available: false,
              Exception_Flag: false,
              Exception_Name: "",
              Package_Finished: false,
              BaileeLetter: false,
              ManifestInBox: false,
              Carrier: "",
              Package_Notes: "",
              Bin_Number: "",
              clearPackagesDateTime: "",
              lastModule: "",
              lastFunction: "",
              loanTotal: 0,
              loansExpected: 0
            };
          }
        }, {
          key: "getWorkflows",
          value: function getWorkflows() {
            var _this89 = this;

            this.workfNamesService.getAll().subscribe(function (res) {
              _this89.workflowList = res; // console.log('Response Workflow names: ', res);
            });
          }
        }, {
          key: "getStatus1",
          value: function getStatus1(value) {
            var _this90 = this;

            this.statusService.getAll().subscribe(function (res) {
              _this90.statusList = res; //console.log('statusService' ,this.statusList);
            }); //console.log("this.taskID" , this.packFound.Task_ID);
            //console.log(this.statusList);

            this.filteredStatus = this.statusList.filter(function (x) {
              return x.Workflow_Task_ID == +_this90.packFound.Task_ID;
            }); //ahora con este task que agarrate busca el status filtrandolo en su array
            //logic
            //console.log("filtered status ", this.filteredStatus);
          }
        }, {
          key: "getStatus",
          value: function getStatus() {
            var _this91 = this;

            this.statusService.getAll().subscribe(function (res) {
              _this91.statusList = res; //console.log('statusService' ,this.statusList);
            }); //getall exceptions

            this.exceptionService.getAll().subscribe(function (res) {
              _this91.exceptionList = res; // console.log("All exceptions ",this.exceptionList);
            });
            var filteredStatus = this.statusList.filter(function (state) {
              return +state.Workflow_Task_ID == _this91.packFound.Task_ID;
            }); //ahora con este task que agarrate busca el status filtrandolo en su array
            // console.log(filteredStatus);
          }
        }, {
          key: "transformStatus",
          value: function transformStatus(taskID, statusName) {
            var _this92 = this;

            this.statusService.getTransformStatus(statusName, taskID).subscribe(function (res) {
              var dis = res;
              _this92.packFound.Status_ID = +dis.Workflow_Status_ID;
            });
          }
        }, {
          key: "getStatusName",
          value: function getStatusName(taskID, statusName) {
            var _this93 = this;

            this.statusService.getTransformStatus(statusName, taskID).subscribe(function (res) {
              var dis = res;
              _this93.packFound.Status_ID = +dis.Workflow_Status_ID;
              return res;
            });
            return null;
          }
        }, {
          key: "errorPickStatus",
          value: function errorPickStatus(value) {
            if (value != 0) {} else {
              sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_6___default()("Status", "Pick a status", "error");
            }
          }
        }, {
          key: "onClickMe",
          value: function onClickMe() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
              var statusName;
              return regeneratorRuntime.wrap(function _callee6$(_context6) {
                while (1) {
                  switch (_context6.prev = _context6.next) {
                    case 0:
                      //console.log("Pack Found ",this.packFound);
                      //console.log("Status selected ",this.statusSelected);
                      if (this.statusSelected == "") {
                        sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_6___default()("Status", "Pick a status", "error");
                      }

                      //Check if status selected is exception
                      //var statusName : WorkflowStatus = this.getStatusName(+this.packFound.Task_ID, this.statusSelected);
                      this.updatePackage();
                      this.insertTaudit();
                      this.onSubmit();

                    case 4:
                    case "end":
                      return _context6.stop();
                  }
                }
              }, _callee6, this);
            }));
          }
        }, {
          key: "refreshTable",
          value: function refreshTable() {
            var _this94 = this;

            this.tableService.getAllbyPID(this.packFound.Package_Tracking_ID).subscribe(function (res) {
              _this94.tableList = res; //console.log(this.auditList);
            }, function (error) {//console.log("No table data found");
            });
          }
        }, {
          key: "insertTaudit",
          value: function insertTaudit() {
            var _this95 = this;

            this.auditCreated = {
              auditID: 0,
              Package_Tracking_ID: this.packFound.Package_Tracking_ID,
              Package_Tracking_Number: this.packFound.Package_Tracking_BC,
              Workflow_ID: this.packFound.WorkFlow_ID,
              Task_ID: this.packFound.Task_ID,
              Status_ID: this.packFound.Status_ID,
              Action_Date_Time: this.packFound.Creation_Date_Time,
              Available: true,
              Exception_Name: this.packFound.Exception_Name,
              User_Login: this.userlogged.User_Name
            }; //console.log("Creating audit ", this.auditCreated);

            this.auditService.savePackage(this.auditCreated).subscribe(function (res) {
              //console.log("Sucess");
              _this95.refreshTable();
            }, function (error) {
              sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_6___default()("Cannot save in TAudit !", String(error.Message), "error");
            });
          }
        }, {
          key: "ChangeWorkflowtoString",
          value: function ChangeWorkflowtoString(statusId) {
            if (statusId == 1) {
              return "NEW COLLATERAL WORKFLOW";
            }

            if (statusId == 2) {
              return "RECERTIFICATION WORKFLOW";
            }

            if (statusId == 3) {
              return "TRAILING WORKFLOW";
            }

            if (statusId == 4) {
              return "CRITICAL TRAILING WORKFLOW";
            }

            if (statusId == 5) {
              return "NIGO WORKFLOW";
            }

            if (statusId == 6) {
              return "REINSTATEMENTS WORKFLOW";
            }

            if (statusId == 7) {
              return "BAU IMAGING WORKFLOW";
            }

            if (statusId == 8) {
              return "RELEASE WORKFLOW";
            }

            return "";
          }
        }, {
          key: "resetFound",
          value: function resetFound() {
            this.checkboxTrue = false;
            this.inputReader = "";
            this.packFound = {
              Package_Tracking_BC: "",
              Package_Tracking_ID: "",
              LoanNumber: 0,
              Sticker_ID: "",
              Status_ID: 0,
              totalLenders: 0,
              Location_Name: "",
              Client_Name: "",
              clearPackagesFlag: false,
              WorkFlow_ID: 0,
              Task_ID: 0,
              Creation_Date_Time: "",
              Available: false,
              Exception_Flag: false,
              Exception_Name: "",
              Package_Finished: false,
              multipleLenders: false,
              ManifestInBox: false,
              BaileeLetter: false,
              Carrier: "",
              Package_Notes: "",
              Bin_Number: "",
              clearPackagesDateTime: "",
              lastModule: "",
              lastFunction: "",
              loanTotal: 0,
              loansExpected: 0
            }, this.workflowDisplayName = "";
            this.message = "";
            this.dataTable = []; //this.workflowPicked= 0;

            this.auditList = [];
            this.taskList = [];
            this.tableList = [];
          }
        }, {
          key: "updatePackage",
          value: function updatePackage() {
            //console.log("status found " + this.statusSelected);
            console.log("UPDATING PACKAGE WITH THIS INFO ", this.packFound);
            this.packageService.updatePack(this.packFound).subscribe(function (res) {
              if (res == "Package updated Successful.") {
                //console.log('Updated Succesfully: ', res);
                sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_6___default()("Success", "Updated Successful", "success");
              } else {
                sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_6___default()("Error updating", "Error updating package", "error");
              }
            });
          }
        }, {
          key: "updatePackageNoSwal",
          value: function updatePackageNoSwal() {
            //console.log("status found " + this.statusSelected);
            console.log("UPDATING PACKAGE WITH THIS INFO ", this.packFound);
            this.packageService.updatePack(this.packFound).subscribe(function (res) {
              if (res == "Package updated Successful.") {//console.log('Updated Succesfully: ', res);
                //Swal("Success", "Updated Successful", "success")
              } else {
                //Swal("Error updating", "Error updating package", "error")
                console.log("Error updating finished package");
              }
            });
          }
        }, {
          key: "findOneByPID",
          value: function findOneByPID(id) {
            var _this96 = this;

            this.packageService.findOneByPID(id).subscribe(function (packF) {
              //console.log(packF);
              //console.log('Response find by pid object: ', packF );
              _this96.displaySelectors = true;
              _this96.packFound = {
                Package_Tracking_BC: packF.Package_Tracking_BC,
                Package_Tracking_ID: packF.Package_Tracking_ID,
                LoanNumber: packF.LoanNumber,
                Sticker_ID: packF.Sticker_ID,
                Status_ID: packF.Status_ID,
                Location_Name: packF.Location_Name,
                Client_Name: packF.Client_Name,
                WorkFlow_ID: packF.WorkFlow_ID,
                Task_ID: packF.Task_ID,
                clearPackagesFlag: packF.clearPackagesFlag,
                totalLenders: packF.totalLenders,
                Creation_Date_Time: packF.Creation_Date_Time,
                Available: packF.Available,
                multipleLenders: packF.multipleLenders,
                Exception_Flag: packF.Exception_Flag,
                Exception_Name: packF.Exception_Name,
                Package_Finished: false,
                ManifestInBox: packF.ManifestInBox,
                BaileeLetter: packF.BaileeLetter,
                Carrier: packF.Carrier,
                Package_Notes: packF.Package_Notes,
                Bin_Number: packF.Bin_Number,
                clearPackagesDateTime: packF.clearPackagesDateTime,
                lastModule: packF.lastModule,
                lastFunction: packF.lastFunction,
                loanTotal: packF.loanTotal,
                loansExpected: packF.loansExpected
              };

              if (_this96.packFound.ManifestInBox == true) {
                _this96.checkboxTrue = true;
              }

              _this96.workflowPicked = packF.WorkFlow_ID;

              _this96.getWorkflowName(_this96.workflowPicked);

              _this96.getCurrentTask(_this96.packFound.Task_ID);

              _this96.taskService.getTaskbyWF(_this96.workflowPicked).subscribe(function (res) {
                _this96.taskList = res; //console.log('taskService' ,this.taskList);

                _this96.checkIfException();
              });

              _this96.tableService.getAllbyPID(_this96.packFound.Package_Tracking_ID).subscribe(function (res) {
                _this96.tableList = res;
                _this96.TlistComplete = res;

                _this96.checkCompletedTask(res);
              }, function (error) {
                console.log("Error retrieving audits using PID");
              }); //if result is error

            }, function (error) {//Swal("Can't find", String(error.Message) ,"error");
            });
          }
        }, {
          key: "findOneByTrackingNumber",
          value: function findOneByTrackingNumber(id) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee7() {
              var _this97 = this;

              return regeneratorRuntime.wrap(function _callee7$(_context7) {
                while (1) {
                  switch (_context7.prev = _context7.next) {
                    case 0:
                      this.packageService.findOneByIdBC(id).subscribe(function (packF) {
                        console.log(packF); // console.log('Response find by tracking number object: ', packF );

                        _this97.displaySelectors = true;
                        _this97.packFound = {
                          Package_Tracking_BC: packF.Package_Tracking_BC,
                          Package_Tracking_ID: packF.Package_Tracking_ID,
                          Sticker_ID: packF.Sticker_ID,
                          LoanNumber: packF.LoanNumber,
                          totalLenders: packF.totalLenders,
                          Status_ID: packF.Status_ID,
                          Location_Name: packF.Location_Name,
                          multipleLenders: packF.multipleLenders,
                          Client_Name: packF.Client_Name,
                          WorkFlow_ID: packF.WorkFlow_ID,
                          Task_ID: packF.Task_ID,
                          Creation_Date_Time: packF.Creation_Date_Time,
                          Available: packF.Available,
                          clearPackagesFlag: packF.clearPackagesFlag,
                          Exception_Flag: packF.Exception_Flag,
                          Exception_Name: packF.Exception_Name,
                          Package_Finished: false,
                          ManifestInBox: packF.ManifestInBox,
                          BaileeLetter: packF.BaileeLetter,
                          Carrier: packF.Carrier,
                          Package_Notes: packF.Package_Notes,
                          Bin_Number: packF.Bin_Number,
                          clearPackagesDateTime: packF.clearPackagesDateTime,
                          lastModule: packF.lastModule,
                          lastFunction: packF.lastFunction,
                          loanTotal: packF.loanTotal,
                          loansExpected: packF.loansExpected
                        };
                        _this97.workflowPicked = packF.WorkFlow_ID;

                        _this97.getWorkflowName(_this97.workflowPicked);

                        _this97.getCurrentTask(_this97.packFound.Task_ID);

                        _this97.taskService.getTaskbyWF(_this97.workflowPicked).subscribe(function (res) {
                          _this97.taskList = res;
                          console.log("taskService", _this97.taskList);

                          _this97.checkIfException();
                        });

                        _this97.tableService.getAllbyTN(_this97.packFound.Package_Tracking_BC).subscribe(function (res) {
                          _this97.tableList = res;
                          _this97.TlistComplete = res;

                          _this97.checkCompletedTask(res);
                        }, function (error) {
                          console.log("Audits not found");
                        });
                      }, function (error) {//Swal("Can't find", String(error.Message) ,"error");
                      });

                    case 1:
                    case "end":
                      return _context7.stop();
                  }
                }
              }, _callee7, this);
            }));
          }
        }, {
          key: "getWorkflowName",
          value: function getWorkflowName(workflowId) {
            if (workflowId == 1) {
              this.workflowDisplayName = "RECERTIFICATION WORKFLOW";
            }

            if (workflowId == 2) {
              this.workflowDisplayName = "NEW COLLATERAL WORKFLOW";
            }

            if (workflowId == 3) {
              this.workflowDisplayName = "TRAILING WORKFLOW";
            }

            if (workflowId == 4) {
              this.workflowDisplayName = "CRITICAL TRAILING WORKFLOW";
            }

            if (workflowId == 5) {
              this.workflowDisplayName = "NIGO (PREP/RESEARCH)";
            }

            if (workflowId == 6) {
              this.workflowDisplayName = "NIGO (SHIPPING)";
            }

            if (workflowId == 7) {
              this.workflowDisplayName = "REINSTATEMENTS WORKFLOW";
            }

            if (workflowId == 8) {
              this.workflowDisplayName = "BAU IMAGING WORKFLOW";
            }

            if (workflowId == 9) {
              this.workflowDisplayName = "RELEASE WORKFLOW";
            }
          }
        }]);

        return PidcheckComponent;
      }();

      PidcheckComponent.ɵfac = function PidcheckComponent_Factory(t) {
        return new (t || PidcheckComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](src_app_services_packages_add_packages_service__WEBPACK_IMPORTED_MODULE_2__["AddPackagesService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](src_app_services_packages_taudit_service__WEBPACK_IMPORTED_MODULE_3__["TauditService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](src_app_services_packages_worflow_task_service__WEBPACK_IMPORTED_MODULE_4__["WorflowTaskService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](src_app_services_packages_worflow_rel_service__WEBPACK_IMPORTED_MODULE_9__["WorflowRelService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](src_app_services_packages_worflow_status_service__WEBPACK_IMPORTED_MODULE_5__["WorflowStatusService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](src_app_services_packages_table_audit_service__WEBPACK_IMPORTED_MODULE_10__["TableAuditService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](src_app_services_packages_exception_service__WEBPACK_IMPORTED_MODULE_11__["ExceptionService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_8__["Router"]));
      };

      PidcheckComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({
        type: PidcheckComponent,
        selectors: [["app-pidcheck"]],
        viewQuery: function PidcheckComponent_Query(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵviewQuery"](_c0, 3);
          }

          if (rf & 2) {
            var _t;

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵloadQuery"]()) && (ctx.taskSelector = _t.first);
          }
        },
        decls: 80,
        vars: 14,
        consts: [[1, "row"], [1, "col-md-4"], [1, "card", 2, "border", "solid 2px #04395E"], [1, "card-header"], ["id", "inputRow", 1, "row"], ["id", "trackinglbl", 1, "col-sm-12"], ["id", "inputTrackingNumber", 1, "col-sm-12"], ["id", "mainInput", "name", "Package_Tracking_ID", "autofocus", "", "type", "text", "placeholder", "Tracking # or PID-", 1, "form-control", 3, "ngModel", "ngModelChange", "keydown.enter"], ["Package_Tracking_ID", "ngModel"], [1, "col-sm-12", 2, "padding", "10px"], [1, "col-sm-12"], ["id", "selector", "name", "TaskSelector", 1, "form-control", 3, "disabled", "ngModel", "selectionChange", "focus", "ngModelChange"], ["TaskSelector", "ngModel"], [3, "ngValue", 4, "ngFor", "ngForOf"], ["id", "selector", "name", "StatusSelector", 1, "form-control", 3, "disabled", "ngModel", "change", "ngModelChange"], ["StatusSelector", "ngModel"], [1, "list-group"], [1, "list-group-item", "list-group-item-action", "flex-column", "align-items-start"], [1, "d-flex", "w-100", "justify-content-between"], [1, "mb-1", "font-weight-bold"], [1, "mb-1"], ["id", "btn1", 1, "btn", "btn-secondary", 3, "click"], ["id", "btn2", 1, "btn", "btn-secondary", 3, "click"], [1, "col-md-8"], [1, "card"], [1, "card-deck"], [1, "card", 2, "width", "20%", "display", "inline-block", "text-align", "center"], ["id", "rowstile", 1, "container-fluid"], [1, "row", 2, "text-align", "center", 3, "innerHTML"], [1, "panel", "panel-color", "panel-fadeDark"], [1, "panel-body"], ["id", "tblLoans", 1, "table", "table-striped"], [1, "thead-dark"], ["class", "", 4, "ngFor", "ngForOf"], [4, "ngFor", "ngForOf"], [3, "ngValue"], [1, ""]],
        template: function PidcheckComponent_Template(rf, ctx) {
          if (rf & 1) {
            var _r11 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "h5", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, "Check in");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](5, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "div", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "div", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "span");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "b");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](10, "Tracking # or PID-");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](11, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "div", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](13, "input", 7, 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function PidcheckComponent_Template_input_ngModelChange_13_listener($event) {
              return ctx.inputReader = $event;
            })("keydown.enter", function PidcheckComponent_Template_input_keydown_enter_13_listener() {
              return ctx.onSubmit();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](15, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](16, "hr");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](17, "div", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](18, "span");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](19, "b");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](20, "Task:");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](21, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](22, "select", 11, 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("selectionChange", function PidcheckComponent_Template_select_selectionChange_22_listener() {
              _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r11);

              var _r1 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](23);

              return ctx.errorPickStatus(_r1.value);
            })("focus", function PidcheckComponent_Template_select_focus_22_listener() {
              _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r11);

              var _r1 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](23);

              return ctx.onOptionsSelected(_r1.value);
            })("ngModelChange", function PidcheckComponent_Template_select_ngModelChange_22_listener($event) {
              return ctx.packFound.Task_ID = $event;
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](24, PidcheckComponent_option_24_Template, 2, 2, "option", 13);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](25, "div", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](26, "span");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](27, "b");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](28, "Status:");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](29, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](30, "select", 14, 15);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("change", function PidcheckComponent_Template_select_change_30_listener() {
              _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r11);

              var _r3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](31);

              return ctx.onStatusSelected(_r3.value);
            })("ngModelChange", function PidcheckComponent_Template_select_ngModelChange_30_listener($event) {
              return ctx.packFound.Status_ID = $event;
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](32, PidcheckComponent_option_32_Template, 2, 2, "option", 13);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](33, "hr");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](34, "div", 16);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](35, "a", 17);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](36, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](37, "h5", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](38, "Sticker #:");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](39, "p", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](40);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](41, "a", 17);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](42, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](43, "h5", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](44, "Location:");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](45, "p", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](46);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](47, "a", 17);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](48, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](49, "h5", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](50, "Client:");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](51, "p", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](52);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](53, "a", 17);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](54, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](55, "h5", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](56, "Workflow:");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](57, "p", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](58);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](59, "hr");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](60, "button", 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function PidcheckComponent_Template_button_click_60_listener() {
              return ctx.onClickMe();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](61, "Save");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](62, "button", 22);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function PidcheckComponent_Template_button_click_62_listener() {
              return ctx.resetFound();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](63, "Cancel");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](64, "div", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](65, "div", 24);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](66, "h5", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](67, "PID Check");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](68, "div", 25);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](69, "div", 26);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](70, "div", 27);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](71, "div", 28);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](72, "div", 29);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](73, "div", 30);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](74, "table", 31);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](75, "thead", 32);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](76, "tr");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](77, PidcheckComponent_th_77_Template, 2, 1, "th", 33);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](78, PidcheckComponent_tr_78_Template, 18, 11, "tr", 34);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](79, "tbody");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](13);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx.inputReader);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](9);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("disabled", ctx.enableFields)("ngModel", ctx.packFound.Task_ID);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx.taskList);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("disabled", ctx.enableFields)("ngModel", ctx.packFound.Status_ID);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx.filteredStatus);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](8);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", ctx.packFound.Sticker_ID, " ");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("", ctx.packFound.Location_Name, " ");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("", ctx.packFound.Client_Name, " ");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx.workflowDisplayName);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](13);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("innerHTML", ctx.message, _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsanitizeHtml"]);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx.headers);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx.tableList);
          }
        },
        directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_12__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_12__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_12__["NgModel"], _angular_forms__WEBPACK_IMPORTED_MODULE_12__["SelectControlValueAccessor"], _angular_common__WEBPACK_IMPORTED_MODULE_13__["NgForOf"], _angular_forms__WEBPACK_IMPORTED_MODULE_12__["NgSelectOption"], _angular_forms__WEBPACK_IMPORTED_MODULE_12__["ɵangular_packages_forms_forms_z"]],
        pipes: [_angular_common__WEBPACK_IMPORTED_MODULE_13__["DatePipe"]],
        styles: ["#userInfo[_ngcontent-%COMP%]{\r\n    text-align: right;\r\n    padding-top: 1%;\r\n}\r\n\r\n#trackinglbl[_ngcontent-%COMP%]{\r\n    padding-left: 20px;\r\n    text-align: left;\r\n    align-self: center;\r\n    \r\n\r\n\r\n}\r\n\r\nhr[_ngcontent-%COMP%]{\r\n    border: 0;\r\n    height: 1px;\r\n    background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));\r\n}\r\n\r\n#btnSearch[_ngcontent-%COMP%]{\r\n    width: 40%;\r\n    background-color:  #031d44\t\t;\r\n    color: white;\r\n}\r\n\r\n.form-control[_ngcontent-%COMP%]{\r\n    color:grey;\r\n\r\n}\r\n\r\n#infoContainer[_ngcontent-%COMP%]{\r\n    position: relative; \r\n    align-self: center;\r\n    border-radius:  10px;\r\n    margin-left: 5%;\r\n    width: 90%;\r\n    padding-right: 10%;\r\n    height: 11.5vw;  \r\n    border: 3px solid darkgray;\r\n}\r\n\r\n#inputTrackingNumber[_ngcontent-%COMP%]{\r\n    padding-top: 10px;\r\n}\r\n\r\n#inputRow[_ngcontent-%COMP%]{\r\n    position: relative;\r\n\r\n}\r\n\r\n.card[_ngcontent-%COMP%]{\r\n    padding-bottom: 100px;\r\n}\r\n\r\n#infoContainer2[_ngcontent-%COMP%]{\r\n    position: relative; \r\n    align-self: center;\r\n    border-radius:  10px;\r\n    margin-left: 5%;\r\n    width: 90%;;\r\n    padding-right: 10%;\r\n    height: 5.5vw;  \r\n    border: 3px solid darkgray;\r\n}\r\n\r\n#col1[_ngcontent-%COMP%], #col2[_ngcontent-%COMP%], #col3[_ngcontent-%COMP%], #col4[_ngcontent-%COMP%] {\r\n    text-align: center;\r\n    display: inline-block   ;\r\n}\r\n\r\n#col1[_ngcontent-%COMP%]{\r\n    margin-top: 2%;\r\n    text-align: center; \r\n    padding-top: 10px;\r\n    padding-bottom: 10px;\r\n    font-weight: bold;\r\n}\r\n\r\n#col2[_ngcontent-%COMP%]{\r\n    margin-top: 1.25%;\r\n    text-align: center; \r\n    padding-top: 15px;\r\n    padding-bottom: 2px;\r\n \r\n}\r\n\r\n#col3[_ngcontent-%COMP%]{\r\n    margin-top: 2%;\r\n    text-align: center; \r\n    padding-top: 10px;\r\n    padding-bottom: 10px;\r\n    font-weight: bold;\r\n\r\n}\r\n\r\n#col4[_ngcontent-%COMP%]{\r\n    margin-top: 1.25%;\r\n    text-align: center; \r\n    padding-top: 10px;\r\n    padding-bottom: 10px;\r\n}\r\n\r\n#col5[_ngcontent-%COMP%]{\r\n    margin-top: 2%;\r\n    text-align: center; \r\n    padding-top: 10px;\r\n    padding-bottom: 10px;\r\n    font-weight: bold;\r\n\r\n}\r\n\r\n#col6[_ngcontent-%COMP%]{\r\n    margin-top: 1.25%;\r\n\r\n    text-align: center; \r\n    padding-top: 15px;\r\n    padding-bottom: 2px;\r\n \r\n}\r\n\r\n#col7[_ngcontent-%COMP%]{\r\n    margin-top: 2%;\r\n    text-align: center; \r\n    padding-top: 10px;\r\n    font-weight: bold;\r\n\r\n    padding-bottom: 10px;\r\n}\r\n\r\n#col8[_ngcontent-%COMP%]{\r\n    margin-top: 1.25%;\r\n    text-align: center; \r\n    padding-top: 10px;\r\n    padding-bottom: 10px;\r\n}\r\n\r\n#checkbox1[_ngcontent-%COMP%]{\r\n    margin-top: 1%;\r\n    text-align: center; \r\n    padding-bottom: 10px;\r\n    padding-left: 10%;\r\n\r\n}\r\n\r\n.col-sm[_ngcontent-%COMP%]{\r\n    margin-top: 1.25%;\r\n    text-align: center; \r\n    padding-top: 10px;\r\n    padding-bottom: 10px;\r\n    font-weight: bold;\r\n}\r\n\r\n.btn-group[_ngcontent-%COMP%] {\r\n    margin-top:2%;\r\n    position: center ;   \r\n    margin-left: 42%;\r\n  }\r\n\r\n#dropdownClient[_ngcontent-%COMP%]{\r\n      width: 90%;\r\n      display:inline-block;\r\n      align-content: center ;   \r\n  }\r\n\r\n#selector[_ngcontent-%COMP%]{\r\n    width: 500px ;\r\n    align-content: center ;   \r\n    border: solid 2px #04395E;\r\n  }\r\n\r\n#mainInput[_ngcontent-%COMP%]:focus {\r\n    border: 4px solid green;\r\n}\r\n\r\n#selector2[_ngcontent-%COMP%]{\r\n  border: solid 2px #04395E;\r\n  width: 500px ;\r\n  margin-bottom: 25px;\r\n  align-content: center ;   \r\n\r\n}\r\n\r\n.card-deck[_ngcontent-%COMP%]{\r\nmargin: 0;\r\n}\r\n\r\n#btn1[_ngcontent-%COMP%], #btn2[_ngcontent-%COMP%]{\r\n    margin-left: 15%;\r\n    margin-right: 15%;\r\n    margin-bottom: 5%;\r\n\r\n    background-color:  #04395E\t\t;\r\n    color: white;\r\n}\r\n\r\n.greenLight[_ngcontent-%COMP%]{\r\n      background: green;\r\n  }\r\n\r\n.card-header[_ngcontent-%COMP%]{\r\n    background-color:  #031d44\t\t;\r\n    color: white;\r\n}\r\n\r\n#editRemoveWrapper[_ngcontent-%COMP%] {\r\n      height: auto;\r\n      width: auto;\r\n  }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInBpZGNoZWNrLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxpQkFBaUI7SUFDakIsZUFBZTtBQUNuQjs7QUFFQTtJQUNJLGtCQUFrQjtJQUNsQixnQkFBZ0I7SUFDaEIsa0JBQWtCOzs7O0FBSXRCOztBQUNBO0lBQ0ksU0FBUztJQUNULFdBQVc7SUFDWCxvR0FBb0c7QUFDeEc7O0FBQ0E7SUFDSSxVQUFVO0lBQ1YsNEJBQTRCO0lBQzVCLFlBQVk7QUFDaEI7O0FBR0E7SUFDSSxVQUFVOztBQUVkOztBQUVBO0lBQ0ksa0JBQWtCO0lBQ2xCLGtCQUFrQjtJQUNsQixvQkFBb0I7SUFDcEIsZUFBZTtJQUNmLFVBQVU7SUFDVixrQkFBa0I7SUFDbEIsY0FBYztJQUNkLDBCQUEwQjtBQUM5Qjs7QUFFQTtJQUNJLGlCQUFpQjtBQUNyQjs7QUFDQTtJQUNJLGtCQUFrQjs7QUFFdEI7O0FBQ0E7SUFDSSxxQkFBcUI7QUFDekI7O0FBRUE7SUFDSSxrQkFBa0I7SUFDbEIsa0JBQWtCO0lBQ2xCLG9CQUFvQjtJQUNwQixlQUFlO0lBQ2YsVUFBVTtJQUNWLGtCQUFrQjtJQUNsQixhQUFhO0lBQ2IsMEJBQTBCO0FBQzlCOztBQUVBO0lBQ0ksa0JBQWtCO0lBQ2xCLHdCQUF3QjtBQUM1Qjs7QUFDQTtJQUNJLGNBQWM7SUFDZCxrQkFBa0I7SUFDbEIsaUJBQWlCO0lBQ2pCLG9CQUFvQjtJQUNwQixpQkFBaUI7QUFDckI7O0FBQ0E7SUFDSSxpQkFBaUI7SUFDakIsa0JBQWtCO0lBQ2xCLGlCQUFpQjtJQUNqQixtQkFBbUI7O0FBRXZCOztBQUNBO0lBQ0ksY0FBYztJQUNkLGtCQUFrQjtJQUNsQixpQkFBaUI7SUFDakIsb0JBQW9CO0lBQ3BCLGlCQUFpQjs7QUFFckI7O0FBQ0E7SUFDSSxpQkFBaUI7SUFDakIsa0JBQWtCO0lBQ2xCLGlCQUFpQjtJQUNqQixvQkFBb0I7QUFDeEI7O0FBRUE7SUFDSSxjQUFjO0lBQ2Qsa0JBQWtCO0lBQ2xCLGlCQUFpQjtJQUNqQixvQkFBb0I7SUFDcEIsaUJBQWlCOztBQUVyQjs7QUFFQTtJQUNJLGlCQUFpQjs7SUFFakIsa0JBQWtCO0lBQ2xCLGlCQUFpQjtJQUNqQixtQkFBbUI7O0FBRXZCOztBQUNBO0lBQ0ksY0FBYztJQUNkLGtCQUFrQjtJQUNsQixpQkFBaUI7SUFDakIsaUJBQWlCOztJQUVqQixvQkFBb0I7QUFDeEI7O0FBQ0E7SUFDSSxpQkFBaUI7SUFDakIsa0JBQWtCO0lBQ2xCLGlCQUFpQjtJQUNqQixvQkFBb0I7QUFDeEI7O0FBRUE7SUFDSSxjQUFjO0lBQ2Qsa0JBQWtCO0lBQ2xCLG9CQUFvQjtJQUNwQixpQkFBaUI7O0FBRXJCOztBQUNBO0lBQ0ksaUJBQWlCO0lBQ2pCLGtCQUFrQjtJQUNsQixpQkFBaUI7SUFDakIsb0JBQW9CO0lBQ3BCLGlCQUFpQjtBQUNyQjs7QUFFQTtJQUNJLGFBQWE7SUFDYixpQkFBaUI7SUFDakIsZ0JBQWdCO0VBQ2xCOztBQUVBO01BQ0ksVUFBVTtNQUNWLG9CQUFvQjtNQUNwQixzQkFBc0I7RUFDMUI7O0FBQ0E7SUFDRSxhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHlCQUF5QjtFQUMzQjs7QUFDQTtJQUNFLHVCQUF1QjtBQUMzQjs7QUFFRTtFQUNBLHlCQUF5QjtFQUN6QixhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLHNCQUFzQjs7QUFFeEI7O0FBRUE7QUFDQSxTQUFTO0FBQ1Q7O0FBR0E7SUFDSSxnQkFBZ0I7SUFDaEIsaUJBQWlCO0lBQ2pCLGlCQUFpQjs7SUFFakIsNEJBQTRCO0lBQzVCLFlBQVk7QUFDaEI7O0FBRUU7TUFDSSxpQkFBaUI7RUFDckI7O0FBRUE7SUFDRSw0QkFBNEI7SUFDNUIsWUFBWTtBQUNoQjs7QUFHRTtNQUNJLFlBQVk7TUFDWixXQUFXO0VBQ2YiLCJmaWxlIjoicGlkY2hlY2suY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIiN1c2VySW5mb3tcclxuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xyXG4gICAgcGFkZGluZy10b3A6IDElO1xyXG59XHJcblxyXG4jdHJhY2tpbmdsYmx7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDIwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gICAgYWxpZ24tc2VsZjogY2VudGVyO1xyXG4gICAgXHJcblxyXG5cclxufVxyXG5ocntcclxuICAgIGJvcmRlcjogMDtcclxuICAgIGhlaWdodDogMXB4O1xyXG4gICAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCByZ2JhKDAsIDAsIDAsIDApLCByZ2JhKDAsIDAsIDAsIDAuNzUpLCByZ2JhKDAsIDAsIDAsIDApKTtcclxufVxyXG4jYnRuU2VhcmNoe1xyXG4gICAgd2lkdGg6IDQwJTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICAjMDMxZDQ0XHRcdDtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuXHJcbi5mb3JtLWNvbnRyb2x7XHJcbiAgICBjb2xvcjpncmV5O1xyXG5cclxufVxyXG5cclxuI2luZm9Db250YWluZXJ7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7IFxyXG4gICAgYWxpZ24tc2VsZjogY2VudGVyO1xyXG4gICAgYm9yZGVyLXJhZGl1czogIDEwcHg7XHJcbiAgICBtYXJnaW4tbGVmdDogNSU7XHJcbiAgICB3aWR0aDogOTAlO1xyXG4gICAgcGFkZGluZy1yaWdodDogMTAlO1xyXG4gICAgaGVpZ2h0OiAxMS41dnc7ICBcclxuICAgIGJvcmRlcjogM3B4IHNvbGlkIGRhcmtncmF5O1xyXG59XHJcblxyXG4jaW5wdXRUcmFja2luZ051bWJlcntcclxuICAgIHBhZGRpbmctdG9wOiAxMHB4O1xyXG59XHJcbiNpbnB1dFJvd3tcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuXHJcbn1cclxuLmNhcmR7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMTAwcHg7XHJcbn1cclxuXHJcbiNpbmZvQ29udGFpbmVyMntcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTsgXHJcbiAgICBhbGlnbi1zZWxmOiBjZW50ZXI7XHJcbiAgICBib3JkZXItcmFkaXVzOiAgMTBweDtcclxuICAgIG1hcmdpbi1sZWZ0OiA1JTtcclxuICAgIHdpZHRoOiA5MCU7O1xyXG4gICAgcGFkZGluZy1yaWdodDogMTAlO1xyXG4gICAgaGVpZ2h0OiA1LjV2dzsgIFxyXG4gICAgYm9yZGVyOiAzcHggc29saWQgZGFya2dyYXk7XHJcbn1cclxuXHJcbiNjb2wxLCAjY29sMiwgI2NvbDMsICNjb2w0IHtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jayAgIDtcclxufVxyXG4jY29sMXtcclxuICAgIG1hcmdpbi10b3A6IDIlO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyOyBcclxuICAgIHBhZGRpbmctdG9wOiAxMHB4O1xyXG4gICAgcGFkZGluZy1ib3R0b206IDEwcHg7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxufVxyXG4jY29sMntcclxuICAgIG1hcmdpbi10b3A6IDEuMjUlO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyOyBcclxuICAgIHBhZGRpbmctdG9wOiAxNXB4O1xyXG4gICAgcGFkZGluZy1ib3R0b206IDJweDtcclxuIFxyXG59XHJcbiNjb2wze1xyXG4gICAgbWFyZ2luLXRvcDogMiU7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7IFxyXG4gICAgcGFkZGluZy10b3A6IDEwcHg7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMTBweDtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG5cclxufVxyXG4jY29sNHtcclxuICAgIG1hcmdpbi10b3A6IDEuMjUlO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyOyBcclxuICAgIHBhZGRpbmctdG9wOiAxMHB4O1xyXG4gICAgcGFkZGluZy1ib3R0b206IDEwcHg7XHJcbn1cclxuXHJcbiNjb2w1e1xyXG4gICAgbWFyZ2luLXRvcDogMiU7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7IFxyXG4gICAgcGFkZGluZy10b3A6IDEwcHg7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMTBweDtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG5cclxufVxyXG5cclxuI2NvbDZ7XHJcbiAgICBtYXJnaW4tdG9wOiAxLjI1JTtcclxuXHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7IFxyXG4gICAgcGFkZGluZy10b3A6IDE1cHg7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMnB4O1xyXG4gXHJcbn1cclxuI2NvbDd7XHJcbiAgICBtYXJnaW4tdG9wOiAyJTtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjsgXHJcbiAgICBwYWRkaW5nLXRvcDogMTBweDtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG5cclxuICAgIHBhZGRpbmctYm90dG9tOiAxMHB4O1xyXG59XHJcbiNjb2w4e1xyXG4gICAgbWFyZ2luLXRvcDogMS4yNSU7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7IFxyXG4gICAgcGFkZGluZy10b3A6IDEwcHg7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMTBweDtcclxufVxyXG5cclxuI2NoZWNrYm94MXtcclxuICAgIG1hcmdpbi10b3A6IDElO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyOyBcclxuICAgIHBhZGRpbmctYm90dG9tOiAxMHB4O1xyXG4gICAgcGFkZGluZy1sZWZ0OiAxMCU7XHJcblxyXG59XHJcbi5jb2wtc217XHJcbiAgICBtYXJnaW4tdG9wOiAxLjI1JTtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjsgXHJcbiAgICBwYWRkaW5nLXRvcDogMTBweDtcclxuICAgIHBhZGRpbmctYm90dG9tOiAxMHB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbn1cclxuXHJcbi5idG4tZ3JvdXAge1xyXG4gICAgbWFyZ2luLXRvcDoyJTtcclxuICAgIHBvc2l0aW9uOiBjZW50ZXIgOyAgIFxyXG4gICAgbWFyZ2luLWxlZnQ6IDQyJTtcclxuICB9XHJcblxyXG4gICNkcm9wZG93bkNsaWVudHtcclxuICAgICAgd2lkdGg6IDkwJTtcclxuICAgICAgZGlzcGxheTppbmxpbmUtYmxvY2s7XHJcbiAgICAgIGFsaWduLWNvbnRlbnQ6IGNlbnRlciA7ICAgXHJcbiAgfVxyXG4gICNzZWxlY3RvcntcclxuICAgIHdpZHRoOiA1MDBweCA7XHJcbiAgICBhbGlnbi1jb250ZW50OiBjZW50ZXIgOyAgIFxyXG4gICAgYm9yZGVyOiBzb2xpZCAycHggIzA0Mzk1RTtcclxuICB9XHJcbiAgI21haW5JbnB1dDpmb2N1cyB7XHJcbiAgICBib3JkZXI6IDRweCBzb2xpZCBncmVlbjtcclxufVxyXG5cclxuICAjc2VsZWN0b3Iye1xyXG4gIGJvcmRlcjogc29saWQgMnB4ICMwNDM5NUU7XHJcbiAgd2lkdGg6IDUwMHB4IDtcclxuICBtYXJnaW4tYm90dG9tOiAyNXB4O1xyXG4gIGFsaWduLWNvbnRlbnQ6IGNlbnRlciA7ICAgXHJcblxyXG59XHJcblxyXG4uY2FyZC1kZWNre1xyXG5tYXJnaW46IDA7XHJcbn1cclxuXHJcblxyXG4jYnRuMSwjYnRuMntcclxuICAgIG1hcmdpbi1sZWZ0OiAxNSU7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDE1JTtcclxuICAgIG1hcmdpbi1ib3R0b206IDUlO1xyXG5cclxuICAgIGJhY2tncm91bmQtY29sb3I6ICAjMDQzOTVFXHRcdDtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuICAuZ3JlZW5MaWdodHtcclxuICAgICAgYmFja2dyb3VuZDogZ3JlZW47XHJcbiAgfVxyXG4gIFxyXG4gIC5jYXJkLWhlYWRlcntcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICAjMDMxZDQ0XHRcdDtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuXHJcbiAgI2VkaXRSZW1vdmVXcmFwcGVyIHtcclxuICAgICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgICB3aWR0aDogYXV0bztcclxuICB9XHJcbiAgXHJcbiAgIl19 */"]
      });
      /***/
    },

    /***/
    "paas":
    /*!**********************************************************!*\
      !*** ./src/app/pages/admin/reprint/reprint.component.ts ***!
      \**********************************************************/

    /*! exports provided: ReprintComponent */

    /***/
    function paas(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ReprintComponent", function () {
        return ReprintComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var src_app_services_packages_add_packages_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! src/app/services/packages/add-packages.service */
      "wlS1");
      /* harmony import */


      var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! sweetalert2/dist/sweetalert2.js */
      "PdH4");
      /* harmony import */


      var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_3__);
      /* harmony import */


      var src_app_services_tid_tidcreartion_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! src/app/services/tid/tidcreartion-service.service */
      "eHNn");
      /* harmony import */


      var src_app_services_coversheet_create_coversheet_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! src/app/services/coversheet/create-coversheet.service */
      "44oB");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");

      var _c0 = ["mainSearch"];

      var _c1 = function _c1() {
        return {
          standalone: true
        };
      };

      function ReprintComponent_tr_45_Template(rf, ctx) {
        if (rf & 1) {
          var _r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tr");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "td");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "td");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "td");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "td");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "td");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "td");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](13, "date");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "td");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 24);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "input", 25);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function ReprintComponent_tr_45_Template_input_ngModelChange_16_listener($event) {
            var item_r2 = ctx.$implicit;
            return item_r2.Selected = $event;
          })("change", function ReprintComponent_tr_45_Template_input_change_16_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r6);

            var item_r2 = ctx.$implicit;
            var i_r3 = ctx.index;

            var ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r5.onCheckSelect(item_r2.Selected, i_r3);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](17, "label", 26);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var item_r2 = ctx.$implicit;
          var i_r3 = ctx.index;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r2.Package_Tracking_BC);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("PID-", item_r2.Package_Tracking_ID, "");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("TID-", item_r2.TID_ID, "");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r2.ScanDate);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r2.TID);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind2"](13, 10, item_r2.Check_DateTime, "dd/MM/yyyy"));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("id", "chkPrint" + i_r3)("ngModel", item_r2.Selected)("ngModelOptions", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](13, _c1));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("for", "chkPrint" + i_r3);
        }
      }

      var ReprintComponent = /*#__PURE__*/function () {
        function ReprintComponent(router, _packageService, _tidService, _createTIDCS) {
          _classCallCheck(this, ReprintComponent);

          this.router = router;
          this._packageService = _packageService;
          this._tidService = _tidService;
          this._createTIDCS = _createTIDCS;
          this.headers = ["Tracking Number", "Assigned PID", "Assigned TID", "TID CREATION DATE", "Select"];
          this.itemSelected = false;
        }

        _createClass(ReprintComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.printPackages = [];
            this.rePrintTIDList = [];
            this.userlogged = JSON.parse(localStorage.getItem('user'));

            if (this.userlogged.Admin_Flag == true) {
              console.log('User logged: ' + this.userlogged.User_Login);
            } else {
              sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_3___default()("Not an admin", "This user is not an Admin", "error");
              this.router.navigate(['/']);
            }
          }
        }, {
          key: "onClearSearch",
          value: function onClearSearch() {
            this.mainSearch.nativeElement.value = '';
            this.mainSearch.nativeElement.focus();
            this.tidListFound = [];
            this.rePrintTIDList = [];
          }
        }, {
          key: "searchBtn",
          value: function searchBtn() {
            var _this98 = this;

            this.tidListFound = [];
            this.rePrintTIDList = [];
            this.inputReader = this.mainSearch.nativeElement.value;
            this.inputReader.toLowerCase();

            if (this.inputReader.startsWith('pid-')) {
              console.log("Started with pid");
            } else {
              //console.log("Started without pid");      
              this._packageService.findOneByIdBC(this.inputReader).subscribe(function (data) {
                _this98.packageFound = data; //console.log(this.packageFound);

                sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                  position: 'top-end',
                  type: 'success',
                  title: 'Package Found',
                  showConfirmButton: false,
                  timer: 800
                });

                _this98.findAllbyID(_this98.inputReader);
              });
            }
          }
        }, {
          key: "search",
          value: function search(event) {
            var _this99 = this;

            this.tidListFound = [];
            this.rePrintTIDList = [];
            console.log(event.target.value);
            this.inputReader = event.target.value;
            this.inputReader = this.inputReader.toLowerCase();
            console.log(this.inputReader);

            if (this.inputReader.startsWith('pid-')) {
              console.log("Started with pid");
              this.inputReader = this.inputReader.replace("pid-", "");

              this._packageService.findOneByPID(+this.inputReader).subscribe(function (data) {
                console.log(data);
                _this99.packageFound = data;
                sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                  position: 'top-end',
                  type: 'success',
                  title: 'Package Found',
                  showConfirmButton: false,
                  timer: 800
                });

                _this99.findAllbyID(_this99.packageFound.Package_Tracking_BC);
              });
            } else {
              console.log("Started without pid or tid");

              this._packageService.findOneByIdBC(this.inputReader).subscribe(function (data) {
                _this99.packageFound = data; //console.log(this.packageFound);

                sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                  position: 'top-end',
                  type: 'success',
                  title: 'Package Found',
                  showConfirmButton: false,
                  timer: 800
                });

                _this99.findAllbyID(_this99.inputReader);
              });
            }
          }
        }, {
          key: "findAllTID",
          value: function findAllTID() {
            this._tidService.findAllTID().subscribe(function (data) {
              console.log(data);
            });
          }
        }, {
          key: "findAllbyID",
          value: function findAllbyID(id) {
            var _this100 = this;

            this._tidService.findTIDsById(id).subscribe(function (data) {
              _this100.tidListFound = data;
              console.log(data);
            });
          }
        }, {
          key: "selectedRecord",
          value: function selectedRecord(event, id) {
            var index = this.tidListFound.indexOf(id);
            console.log("index number " + index);
            var elem = this.tidListFound[index]; //console.log(elem.TID_ID);

            this.rePrintTIDList.push(elem);
            console.log(this.rePrintTIDList);
          }
        }, {
          key: "ReprintPID",
          value: function ReprintPID() {
            console.log(this.packageFound);
            this.printPackages.push(this.packageFound);
            console.log(this.printPackages);

            if (this.tidListFound != null) {
              console.log("Printing ALL PID LIST");
              this.onCreateCoverSheetTID(this.printPackages);
            }
          }
        }, {
          key: "printTidList",
          value: function printTidList() {
            if (typeof this.rePrintTIDList !== 'undefined' && this.rePrintTIDList.length > 0) {
              console.log("Printing TID LIST");
              this.onCreateCoverSheetTIDReal(this.rePrintTIDList);
            } else {
              sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                position: 'top-end',
                type: 'error',
                title: 'Select a TID',
                showConfirmButton: false,
                timer: 800
              });
            }
          }
        }, {
          key: "onCreateCoverSheetTID",
          value: function onCreateCoverSheetTID(items) {
            this._packageService.createCoversheet(items).subscribe(function (response) {
              var file = new Blob([response], {
                type: 'application/pdf'
              });
              var url = URL.createObjectURL(file);
              var iframe = document.createElement('iframe');
              iframe.style.display = 'none';
              iframe.src = url;
              document.body.appendChild(iframe);
              iframe.contentWindow.print();
            }, function (error) {
              sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_3___default()("Error", String(error.Message), "error");
            });
          }
        }, {
          key: "onCreateCoverSheetTIDReal",
          value: function onCreateCoverSheetTIDReal(items) {
            this._createTIDCS.createCoversheetManifest(items).subscribe(function (response) {
              var file = new Blob([response], {
                type: 'application/pdf'
              });
              var url = URL.createObjectURL(file);
              var iframe = document.createElement('iframe');
              iframe.style.display = 'none';
              iframe.src = url;
              document.body.appendChild(iframe);
              iframe.contentWindow.print();
            }, function (error) {
              sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_3___default()("Error", String(error.Message), "error");
            });
          }
        }, {
          key: "onCheckSelect",
          value: function onCheckSelect(item, index) {
            console.log(item, index);
            var tidselected = this.tidListFound[index];
            this.rePrintTIDList.push(tidselected);
            console.log(this.rePrintTIDList);
          }
        }]);

        return ReprintComponent;
      }();

      ReprintComponent.ɵfac = function ReprintComponent_Factory(t) {
        return new (t || ReprintComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_packages_add_packages_service__WEBPACK_IMPORTED_MODULE_2__["AddPackagesService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_tid_tidcreartion_service_service__WEBPACK_IMPORTED_MODULE_4__["TidcreartionServiceService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_coversheet_create_coversheet_service__WEBPACK_IMPORTED_MODULE_5__["CreateCoversheetService"]));
      };

      ReprintComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: ReprintComponent,
        selectors: [["app-reprint"]],
        viewQuery: function ReprintComponent_Query(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c0, 3);
          }

          if (rf & 2) {
            var _t;

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.mainSearch = _t.first);
          }
        },
        decls: 46,
        vars: 1,
        consts: [[1, "row"], [1, "col-md-12"], [1, "card"], [1, "card-body"], [1, "m-t-0", "m-b-30"], [1, "col-lg-12"], ["id", "searchForm", 1, "form-inline"], [1, "form-group", "m-r-5"], ["for", "txtSearch", 1, "sr-only"], [1, "input-group"], ["type", "text", "placeholder", "Search: TN/PID/TID", "name", "txtSearch", "maxlength", "50", "autofocus", "", 1, "form-control", 3, "keydown.enter"], ["mainSearch", ""], [1, "button-group"], ["type", "button", 1, "btn", "btn-secondary", "waves-effect", "waves-light", "mr-2", 3, "click"], [1, "fa", "fa-trash"], ["type", "button", 1, "btn", "btn-primary", "waves-effect", "waves-light", "mr-2", 3, "click"], [1, "fa", "fa-print"], ["type", "button", 1, "btn", "btn-info", "waves-effect", "waves-light", "mr-2", 3, "click"], [1, "table-rep-plugin"], ["data-pattern", "priority-columns", 1, "table-responsive"], ["id", "tblItems", 1, "table", "table-hover"], [1, "thead-dark"], [1, "w-25"], [4, "ngFor", "ngForOf"], [1, "custom-control", "custom-checkbox"], ["type", "checkbox", 1, "custom-control-input", 3, "id", "ngModel", "ngModelOptions", "ngModelChange", "change"], [1, "custom-control-label", 3, "for"]],
        template: function ReprintComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "h4", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "REPRINT MANAGEMENT");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "form", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "label", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Search: TN/PID/TID");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "input", 10, 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("keydown.enter", function ReprintComponent_Template_input_keydown_enter_14_listener($event) {
              return ctx.search($event);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "button", 13);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ReprintComponent_Template_button_click_17_listener() {
              return ctx.onClearSearch();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](18, "i", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "Clear Search");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "button", 15);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ReprintComponent_Template_button_click_20_listener() {
              return ctx.printTidList();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](21, "i", 16);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, "Reprint TID");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "button", 17);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ReprintComponent_Template_button_click_23_listener() {
              return ctx.ReprintPID();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](24, "i", 16);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25, "Reprint PID");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](26, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "div", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "table", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "thead", 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "tr");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "th", 22);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](35, "Tracking Number");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "th", 22);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](37, "PID");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "th", 22);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39, "TID");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "th", 22);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](41, "Check-In Date Time");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "th", 22);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](43, "Select");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "tbody");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](45, ReprintComponent_tr_45_Template, 18, 14, "tr", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](45);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.tidListFound);
          }
        },
        directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["ɵangular_packages_forms_forms_ba"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["NgForm"], _angular_common__WEBPACK_IMPORTED_MODULE_7__["NgForOf"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["CheckboxControlValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["NgModel"]],
        pipes: [_angular_common__WEBPACK_IMPORTED_MODULE_7__["DatePipe"]],
        styles: [".card-header[_ngcontent-%COMP%]{\r\n    background-color:  #031d44\t\t;\r\n    color: white;\r\n}\r\n\r\n#customers[_ngcontent-%COMP%]   tr[_ngcontent-%COMP%]:nth-child(even)  {\r\n    background-color: #f2f2f2;\r\n  }\r\n\r\n#customers[_ngcontent-%COMP%]   tr[_ngcontent-%COMP%]:hover {\r\n    background-color: #ddd;\r\n  }\r\n\r\n#customers[_ngcontent-%COMP%]   th[_ngcontent-%COMP%] {\r\n    padding-top: 12px;\r\n    padding-bottom: 12px;\r\n    text-align: left;\r\n    background-color:  #023e7d\t\t;\r\n    color: white;\r\n  }\r\n\r\n#customers[_ngcontent-%COMP%] {\r\n    font-family: \"Trebuchet MS\", Arial, Helvetica, sans-serif;\r\n    border-collapse: collapse;\r\n    width: 100%;\r\n    \r\n  }\r\n\r\n#customers[_ngcontent-%COMP%], #customers[_ngcontent-%COMP%]   thead[_ngcontent-%COMP%]    > th[_ngcontent-%COMP%] {\r\n    border: 1px solid #ddd;\r\n    padding: 8px;\r\n  }\r\n\r\n.table[_ngcontent-%COMP%]   tr.active[_ngcontent-%COMP%]   td[_ngcontent-%COMP%] {  \r\n    background-color:#48da24 !important;  \r\n    color: white;  \r\n  }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInJlcHJpbnQuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLDRCQUE0QjtJQUM1QixZQUFZO0FBQ2hCOztBQUVBO0lBQ0kseUJBQXlCO0VBQzNCOztBQUVBO0lBQ0Usc0JBQXNCO0VBQ3hCOztBQUVBO0lBQ0UsaUJBQWlCO0lBQ2pCLG9CQUFvQjtJQUNwQixnQkFBZ0I7SUFDaEIsNEJBQTRCO0lBQzVCLFlBQVk7RUFDZDs7QUFFQTtJQUNFLHlEQUF5RDtJQUN6RCx5QkFBeUI7SUFDekIsV0FBVzs7RUFFYjs7QUFFQTtJQUNFLHNCQUFzQjtJQUN0QixZQUFZO0VBQ2Q7O0FBRUE7SUFDRSxtQ0FBbUM7SUFDbkMsWUFBWTtFQUNkIiwiZmlsZSI6InJlcHJpbnQuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jYXJkLWhlYWRlcntcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICAjMDMxZDQ0XHRcdDtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuI2N1c3RvbWVycyB0cjpudGgtY2hpbGQoZXZlbikgIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmMmYyZjI7XHJcbiAgfVxyXG4gICAgXHJcbiAgI2N1c3RvbWVycyB0cjpob3ZlciB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZGRkO1xyXG4gIH1cclxuICBcclxuICAjY3VzdG9tZXJzIHRoIHtcclxuICAgIHBhZGRpbmctdG9wOiAxMnB4O1xyXG4gICAgcGFkZGluZy1ib3R0b206IDEycHg7XHJcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogICMwMjNlN2RcdFx0O1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gIH1cclxuXHJcbiAgI2N1c3RvbWVycyB7XHJcbiAgICBmb250LWZhbWlseTogXCJUcmVidWNoZXQgTVNcIiwgQXJpYWwsIEhlbHZldGljYSwgc2Fucy1zZXJpZjtcclxuICAgIGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIFxyXG4gIH1cclxuICBcclxuICAjY3VzdG9tZXJzICwgI2N1c3RvbWVycyB0aGVhZCA+IHRoIHtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkICNkZGQ7XHJcbiAgICBwYWRkaW5nOiA4cHg7XHJcbiAgfVxyXG5cclxuICAudGFibGUgdHIuYWN0aXZlIHRkIHsgIFxyXG4gICAgYmFja2dyb3VuZC1jb2xvcjojNDhkYTI0ICFpbXBvcnRhbnQ7ICBcclxuICAgIGNvbG9yOiB3aGl0ZTsgIFxyXG4gIH0gIl19 */"]
      });
      /***/
    },

    /***/
    "qzPY":
    /*!****************************************************!*\
      !*** ./src/app/pages/reports/reports.component.ts ***!
      \****************************************************/

    /*! exports provided: ReportsComponent */

    /***/
    function qzPY(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ReportsComponent", function () {
        return ReportsComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var ReportsComponent = /*#__PURE__*/function () {
        function ReportsComponent() {
          _classCallCheck(this, ReportsComponent);
        }

        _createClass(ReportsComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "goToLink",
          value: function goToLink(url) {
            window.open(url, "_blank");
          }
        }]);

        return ReportsComponent;
      }();

      ReportsComponent.ɵfac = function ReportsComponent_Factory(t) {
        return new (t || ReportsComponent)();
      };

      ReportsComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: ReportsComponent,
        selectors: [["app-reports"]],
        decls: 9,
        vars: 0,
        consts: [[1, "container"], [1, "card"], [1, "card-header"], [1, "card-body"], ["href", "", 3, "click"]],
        template: function ReportsComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "h2");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h5", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Reports");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "a", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ReportsComponent_Template_a_click_7_listener() {
              return ctx.goToLink("http://usnusmimtwebp01.na.imtn.com/IMReports/");
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Reporting");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }
        },
        styles: [".card-header[_ngcontent-%COMP%]{\r\n    background-color:  #031d44\t\t;\r\n    color: white;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInJlcG9ydHMuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLDRCQUE0QjtJQUM1QixZQUFZO0FBQ2hCIiwiZmlsZSI6InJlcG9ydHMuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jYXJkLWhlYWRlcntcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICAjMDMxZDQ0XHRcdDtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxufSJdfQ== */"]
      });
      /***/
    },

    /***/
    "r/Lp":
    /*!*****************************************************!*\
      !*** ./src/app/services/packages/taudit.service.ts ***!
      \*****************************************************/

    /*! exports provided: TauditService */

    /***/
    function rLp(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "TauditService", function () {
        return TauditService;
      });
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");
      /* harmony import */


      var _shared_http_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../shared/http-rest.service */
      "vulr");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var TauditService = /*#__PURE__*/function (_shared_http_rest_ser11) {
        _inherits(TauditService, _shared_http_rest_ser11);

        var _super11 = _createSuper(TauditService);

        function TauditService(http, router) {
          var _this101;

          _classCallCheck(this, TauditService);

          _this101 = _super11.call(this, http);
          _this101.router = router;
          _this101.rootURL = "http://localhost:53420/api/taudit";
          return _this101;
        }

        _createClass(TauditService, [{
          key: "getTaskbyWF",
          value: function getTaskbyWF(wfid) {
            return this.findAllById(wfid, "taudit", "FindAllwithTaskID");
          }
        }, {
          key: "getTransformStatus",
          value: function getTransformStatus(name, id) {
            return this.findUniqueByIdStatus(name, id, "taudit", "FindAllwithTaskID");
          }
        }, {
          key: "savePackage",
          value: function savePackage(pack) {
            return this.save(pack, "taudit", "Save");
          }
        }, {
          key: "getAllbyTN",
          value: function getAllbyTN(id) {
            return this.findAllById(id, "taudit", "FindAllbyTN");
          }
        }, {
          key: "getAllbyPID",
          value: function getAllbyPID(id) {
            return this.findAllById(id, "taudit", "FindAllbyPID");
          }
        }]);

        return TauditService;
      }(_shared_http_rest_service__WEBPACK_IMPORTED_MODULE_2__["HttpRestService"]);

      TauditService.ɵfac = function TauditService_Factory(t) {
        return new (t || TauditService)(_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵinject"](_angular_router__WEBPACK_IMPORTED_MODULE_0__["Router"]));
      };

      TauditService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineInjectable"]({
        token: TauditService,
        factory: TauditService.ɵfac,
        providedIn: 'root'
      });
      /***/
    },

    /***/
    "sRhs":
    /*!*****************************************************!*\
      !*** ./src/app/shared/sidebar/sidebar.component.ts ***!
      \*****************************************************/

    /*! exports provided: SidebarComponent */

    /***/
    function sRhs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SidebarComponent", function () {
        return SidebarComponent;
      });
      /* harmony import */


      var _services_shared_sidebar_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ../../services/shared/sidebar.service */
      "F/wz");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");

      var _c0 = function _c0() {
        return ["/admin"];
      };

      var _c1 = function _c1() {
        return ["/admin/users"];
      };

      var _c2 = function _c2() {
        return ["/admin/reprint"];
      };

      var _c3 = function _c3() {
        return ["/manifest/editRemove"];
      };

      var _c4 = function _c4() {
        return ["/manifest"];
      };

      var _c5 = function _c5() {
        return ["/manifest/addPackages"];
      };

      var _c6 = function _c6() {
        return ["/manifest/sortPackages"];
      };

      var _c7 = function _c7() {
        return ["/tidCreation/generateTid"];
      };

      var _c8 = function _c8() {
        return ["/certification/tidCertification"];
      };

      var _c9 = function _c9() {
        return ["/imageLookup"];
      };

      var _c10 = function _c10() {
        return ["/imageLookup/alfrescoLookup"];
      };

      var _c11 = function _c11() {
        return ["/reports"];
      };

      var SidebarComponent = /*#__PURE__*/function () {
        function SidebarComponent(_sidebarservice) {
          _classCallCheck(this, SidebarComponent);

          this._sidebarservice = _sidebarservice;
        }

        _createClass(SidebarComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return SidebarComponent;
      }();

      SidebarComponent.ɵfac = function SidebarComponent_Factory(t) {
        return new (t || SidebarComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_services_shared_sidebar_service__WEBPACK_IMPORTED_MODULE_0__["SidebarService"]));
      };

      SidebarComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({
        type: SidebarComponent,
        selectors: [["app-sidebar"]],
        decls: 59,
        vars: 24,
        consts: [[1, "left", "side-menu"], ["id", "remove-scroll", 1, "slimscroll-menu"], ["id", "sidebar-menu"], ["id", "side-menu", 1, "metismenu"], ["href", "javascript: void(0);", "routerLinkActive", "active", 3, "routerLink"], [1, "ion-md-person"], ["aria-expanded", "false", 1, "nav-second-level"], ["routerLinkActive", "active", 3, "routerLink"], [1, "nav-second-level"], [1, "ion-md-document"], [1, "icon", "ion-md-paper"], [1, "ion-md-clipboard"], [1, "icon", "ion-md-images"], ["aria-expanded", "true", 1, "nav-second-level"], [1, "ion-ios-list-box"], [1, "clearfix"]],
        template: function SidebarComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "ul", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "li");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "a", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](6, "i", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "span");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](8, " Admin ");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "ul", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "li");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](11, "a", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](12, "Users");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](13, "ul", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](14, "li");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](15, "a", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](16, "Reprint");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](17, "ul", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](18, "li");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](19, "a", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](20, "Update Packages");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](21, "li");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](22, "a", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](23, "i", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](24, "span");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](25, " Manifest ");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](26, "ul", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](27, "li");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](28, "a", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](29, "Add Packages");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](30, "ul", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](31, "li");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](32, "a", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](33, "Sort Packages");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](34, "li");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](35, "a", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](36, "i", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](37, "span");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](38, "Check-IN ");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](39, "li");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](40, "a", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](41, "i", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](42, "span");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](43, " Certification ");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](44, "li");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](45, "a", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](46, "i", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](47, "span");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](48, " Image Look Up ");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](49, "ul", 13);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](50, "li");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](51, "a", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](52, "Alfresco Image Lookup");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](53, "li");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](54, "a", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](55, "i", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](56, "span");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](57, " Reports ");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](58, "div", 15);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](12, _c0));

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](13, _c1));

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](14, _c2));

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](15, _c3));

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](16, _c4));

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](17, _c5));

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](18, _c6));

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](19, _c7));

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](20, _c8));

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](21, _c9));

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](22, _c10));

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](23, _c11));
          }
        },
        directives: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterLinkWithHref"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterLinkActive"]],
        encapsulation: 2
      });
      /***/
    },

    /***/
    "vWu4":
    /*!*******************************************!*\
      !*** ./src/app/services/service.index.ts ***!
      \*******************************************/

    /*! exports provided: LoginGuardGuard, SidebarService, UsersService */

    /***/
    function vWu4(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony import */


      var _guards_login_guard_guard__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./guards/login-guard.guard */
      "ITgV");
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "LoginGuardGuard", function () {
        return _guards_login_guard_guard__WEBPACK_IMPORTED_MODULE_0__["LoginGuardGuard"];
      });
      /* harmony import */


      var _shared_sidebar_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./shared/sidebar.service */
      "F/wz");
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "SidebarService", function () {
        return _shared_sidebar_service__WEBPACK_IMPORTED_MODULE_1__["SidebarService"];
      });
      /* harmony import */


      var _users_users_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./users/users.service */
      "j7lE");
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "UsersService", function () {
        return _users_users_service__WEBPACK_IMPORTED_MODULE_2__["UsersService"];
      }); // Guards
      // Services

      /***/

    },

    /***/
    "vmXk":
    /*!********************************!*\
      !*** ./src/app/models/User.ts ***!
      \********************************/

    /*! exports provided: User */

    /***/
    function vmXk(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "User", function () {
        return User;
      });

      var User = function User() {
        _classCallCheck(this, User);
      };
      /***/

    },

    /***/
    "vtpD":
    /*!******************************************!*\
      !*** ./src/app/login/login.component.ts ***!
      \******************************************/

    /*! exports provided: LoginComponent */

    /***/
    function vtpD(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginComponent", function () {
        return LoginComponent;
      });
      /* harmony import */


      var _models_User__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ../models/User */
      "vmXk");
      /* harmony import */


      var _services_users_users_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ../services/users/users.service */
      "j7lE");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var sweetalert2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! sweetalert2 */
      "PSD3");
      /* harmony import */


      var sweetalert2__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_3__);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");

      function LoginComponent_div_18_span_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "span", 23);

          _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](1, "small", 24);

          _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](2, "User name is required.");

          _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        }
      }

      function LoginComponent_div_18_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "div", 21);

          _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](1, LoginComponent_div_18_span_1_Template, 3, 0, "span", 22);

          _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]();

          var _r1 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵreference"](17);

          _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", _r1 == null ? null : _r1.errors.required);
        }
      }

      function LoginComponent_div_25_span_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "span", 23);

          _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](1, "small", 24);

          _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](2, "Password is required.");

          _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        }
      }

      function LoginComponent_div_25_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "div", 21);

          _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](1, LoginComponent_div_25_span_1_Template, 3, 0, "span", 22);

          _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]();

          var _r3 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵreference"](24);

          _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", _r3 == null ? null : _r3.errors.required);
        }
      }

      var LoginComponent = /*#__PURE__*/function () {
        function LoginComponent(_userService, router) {
          _classCallCheck(this, LoginComponent);

          this._userService = _userService;
          this.router = router;
          this.user = new _models_User__WEBPACK_IMPORTED_MODULE_0__["User"]();
        }

        _createClass(LoginComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            init_plugins();
            this.cleanUser();
            this.userlogged = JSON.parse(localStorage.getItem("user")); //console.log('User logged: ' + this.userlogged.User_Login);

            if (this.userlogged != null) {
              this.router.navigate(["/"]);
            } else {
              this.router.navigate(["/login"]);
            }
          }
        }, {
          key: "onSubmit",
          value: function onSubmit() {
            var _this102 = this;

            console.log(this.user);

            this._userService.login(this.user).subscribe(function (res) {
              console.log("Login Response object: ", res);
              sweetalert2__WEBPACK_IMPORTED_MODULE_3___default()("Login Successful", "", "success");
              localStorage.setItem("user", JSON.stringify(res));

              _this102.router.navigate(["/"]);
            }, function (error) {
              sweetalert2__WEBPACK_IMPORTED_MODULE_3___default()("Login Error", String(error.Message), "error");
            });
          }
        }, {
          key: "cleanUser",
          value: function cleanUser() {
            this.user = {
              Admin_Flag: null,
              Available: null,
              User_Login: "",
              User_Name: "",
              User_Password: "",
              FirstName: "",
              LastName: ""
            };
          }
        }]);

        return LoginComponent;
      }();

      LoginComponent.ɵfac = function LoginComponent_Factory(t) {
        return new (t || LoginComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_services_users_users_service__WEBPACK_IMPORTED_MODULE_1__["UsersService"]), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]));
      };

      LoginComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineComponent"]({
        type: LoginComponent,
        selectors: [["app-login"]],
        decls: 30,
        vars: 4,
        consts: [[1, "pb-0"], [1, "wrapper-page"], [1, "account-pages"], [1, "account-box"], [1, "account-logo-box", "p-4", 2, "background-color", "#031d44"], [1, "m-0", "text-center", "text-white"], [1, "account-content"], [1, "form-horizontal", 3, "ngSubmit"], ["loginForm", "ngForm"], [1, "form-group", "mb-4", "row"], [1, "col-12"], ["for", "emailaddress"], ["autocomplete", "off", "type", "text", "name", "UserName", "placeholder", "Username", "required", "", 1, "form-control", 3, "ngModel", "ngModelChange"], ["UserName", "ngModel"], ["class", "col-xs-12", "style", "margin-top: -4px", 4, "ngIf"], [1, "form-group", "row", "mb-4"], ["for", "password"], ["autocomplete", "off", "type", "password", "name", "Password", "placeholder", "Password", "required", "", 1, "form-control", 3, "ngModel", "ngModelChange"], ["Password", "ngModel"], [1, "form-group", "row", "text-center", "m-t-10"], ["type", "submit", "id", "btnLogin", 1, "btn", "btn-md", "btn-block", "text-white", "waves-effect", "waves-light", 2, "background-color", "#04395e"], [1, "col-xs-12", 2, "margin-top", "-4px"], ["class", "messages", 4, "ngIf"], [1, "messages"], [1, "text-danger", "error"]],
        template: function LoginComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](2, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](3, "div", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](4, "div", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](5, "h3", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](6, "Bank of New York Mellon");

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](7, "h3", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](8, "Tracking Tool");

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](9, "div", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](10, "form", 7, 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngSubmit", function LoginComponent_Template_form_ngSubmit_10_listener() {
              return ctx.onSubmit();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](12, "div", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](13, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](14, "label", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](15, "User Name :");

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](16, "input", 12, 13);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngModelChange", function LoginComponent_Template_input_ngModelChange_16_listener($event) {
              return ctx.user.User_Name = $event;
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](18, LoginComponent_div_18_Template, 2, 1, "div", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](19, "div", 15);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](20, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](21, "label", 16);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](22, "Password :");

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](23, "input", 17, 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngModelChange", function LoginComponent_Template_input_ngModelChange_23_listener($event) {
              return ctx.user.User_Password = $event;
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](25, LoginComponent_div_25_Template, 2, 1, "div", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](26, "div", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](27, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](28, "button", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](29, " Sign In ");

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            var _r0 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵreference"](11);

            var _r1 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵreference"](17);

            var _r3 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵreference"](24);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](16);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngModel", ctx.user.User_Name);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", _r1.invalid && (_r1.dirty || _r1.touched || _r0.submitted));

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](5);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngModel", ctx.user.User_Password);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", _r3.invalid && (_r3.dirty || _r3.touched || _r0.submitted));
          }
        },
        directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["ɵangular_packages_forms_forms_ba"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["NgForm"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["RequiredValidator"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["NgModel"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["NgIf"]],
        encapsulation: 2
      });
      /***/
    },

    /***/
    "vulr":
    /*!******************************************************!*\
      !*** ./src/app/services/shared/http-rest.service.ts ***!
      \******************************************************/

    /*! exports provided: HttpRestService */

    /***/
    function vulr(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HttpRestService", function () {
        return HttpRestService;
      });
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! rxjs/operators */
      "kU1M");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! rxjs */
      "qCKp");
      /* harmony import */


      var src_app_models_app_settings__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! src/app/models/app-settings */
      "Qxtq");
      /**
       * @Author: Humberto Ibarra.
       * @Description: Provides the http methods for the requests to the API.
       * @Date: 06-12-2018
       * @TModel: type of the object to work with.
       **/


      var HttpRestService = /*#__PURE__*/function () {
        function HttpRestService(http) {
          _classCallCheck(this, HttpRestService);

          this.http = http;
          this.hostURL = "".concat(src_app_models_app_settings__WEBPACK_IMPORTED_MODULE_2__["AppSettings"].API_ENDPOINT);
        }

        _createClass(HttpRestService, [{
          key: "findAll",
          value: function findAll(controllerName, actionName) {
            return this.http.get("".concat(this.hostURL, "/").concat(controllerName, "/").concat(actionName)).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["tap"])(function (model) {
              return model;
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["catchError"])(this.handleError));
          }
        }, {
          key: "findAllById",
          value: function findAllById(id, controllerName, actionName) {
            return this.http.get("".concat(this.hostURL, "/").concat(controllerName, "/").concat(actionName, "/").concat(id)).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["tap"])(function (model) {
              return model;
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["catchError"])(this.handleError));
          }
        }, {
          key: "findAllByName",
          value: function findAllByName(name, pass, controllerName, actionName) {
            return this.http.get("".concat(this.hostURL, "/").concat(controllerName, "/").concat(actionName, "/").concat(name, "/").concat(pass)).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["tap"])(function (model) {
              return model;
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["catchError"])(this.handleError));
          }
        }, {
          key: "upload",
          value: function upload(controllerName, actionName, file) {
            var formData = new FormData();
            formData.append('Image', file, file.name);
            console.log(formData); // return this.asyncHttpRequest("POST", "FileUpload", "UploadImage", { formData: formData}, null);

            return this.http.post("".concat(this.hostURL, "/").concat(controllerName, "/").concat(actionName), formData);
          }
        }, {
          key: "uploadBulk",
          value: function uploadBulk(controllerName, actionName, fileList) {
            var formData = new FormData();
            fileList.forEach(function (file) {
              formData.append('Image', file, file.name);
            }); //console.log(formData)
            // return this.asyncHttpRequest("POST", "FileUpload", "UploadImage", { formData: formData}, null);

            return this.http.post("".concat(this.hostURL, "/").concat(controllerName, "/").concat(actionName), formData);
          }
        }, {
          key: "uploadSingle",
          value: function uploadSingle(controllerName, actionName, file) {
            var formData = new FormData();
            formData.append('Image', file, file.name);
            console.log(formData); // return this.asyncHttpRequest("POST", "FileUpload", "UploadImage", { formData: formData}, null);

            return this.http.post("".concat(this.hostURL, "/").concat(controllerName, "/").concat(actionName), formData);
          }
        }, {
          key: "findAllByBC",
          value: function findAllByBC(id, controllerName, actionName) {
            return this.http.get("".concat(this.hostURL, "/").concat(controllerName, "/").concat(actionName, "/").concat(id)).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["tap"])(function (model) {
              return model;
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["catchError"])(this.handleError));
          }
        }, {
          key: "httpRequestBufferResponse",
          value: function httpRequestBufferResponse(controllerName, actionName, params, model) {
            return this.http.post("".concat(this.hostURL, "/").concat(controllerName, "/").concat(actionName), model, {
              responseType: 'arraybuffer'
            }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["tap"])(function (data) {
              return data;
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["catchError"])(this.handleError));
          }
        }, {
          key: "httpRequestBufferResponseManifest",
          value: function httpRequestBufferResponseManifest(controllerName, actionName, params, model) {
            return this.http.post("".concat(this.hostURL, "/").concat(controllerName, "/").concat(actionName), model, {
              responseType: 'arraybuffer'
            }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["tap"])(function (data) {
              return data;
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["catchError"])(this.handleError));
          }
        }, {
          key: "findUniqueByIdStatus",
          value: function findUniqueByIdStatus(name, id, controllerName, actionName) {
            return this.http.get("".concat(this.hostURL, "/").concat(controllerName, "/").concat(actionName, "/").concat(id, "/").concat(name)).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["tap"])(function (model) {
              return model;
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["catchError"])(this.handleError));
          }
        }, {
          key: "findUniqueById",
          value: function findUniqueById(id, controllerName, actionName) {
            return this.http.get("".concat(this.hostURL, "/").concat(controllerName, "/").concat(actionName, "/").concat(id)).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["tap"])(function (model) {
              return model;
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["catchError"])(this.handleError));
          }
        }, {
          key: "findUniqueByBC",
          value: function findUniqueByBC(id, controllerName, actionName) {
            return this.http.get("".concat(this.hostURL, "/").concat(controllerName, "/").concat(actionName, "/").concat(id)).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["tap"])(function (model) {
              return model;
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["catchError"])(this.handleError));
          }
        }, {
          key: "startRetrieveSendPID",
          value: function startRetrieveSendPID(id, controllerName, actionName) {
            return this.http.get("".concat(this.hostURL, "/").concat(controllerName, "/").concat(actionName, "/").concat(id)).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["tap"])(function (model) {
              return model;
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["catchError"])(this.handleError));
          }
        }, {
          key: "startRetrieve",
          value: function startRetrieve(controllerName, actionName) {
            return this.http.get("".concat(this.hostURL, "/").concat(controllerName, "/").concat(actionName)).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["tap"])(function (model) {
              return model;
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["catchError"])(this.handleError));
          }
        }, {
          key: "saveMultiple",
          value: function saveMultiple(model, controllerName, actionName) {
            return this.http.post("".concat(this.hostURL, "/").concat(controllerName, "/").concat(actionName), model).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["tap"])(function (data) {
              return data;
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["catchError"])(this.handleError));
          }
        }, {
          key: "save",
          value: function save(model, controllerName, actionName) {
            return this.http.post("".concat(this.hostURL, "/").concat(controllerName, "/").concat(actionName), model).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["tap"])(function (data) {
              return data;
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["catchError"])(this.handleError));
          }
        }, {
          key: "saveBulk",
          value: function saveBulk(model, controllerName, actionName) {
            return this.http.post("".concat(this.hostURL, "/").concat(controllerName, "/").concat(actionName), model).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["tap"])(function (data) {
              return data;
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["catchError"])(this.handleError));
          }
        }, {
          key: "update",
          value: function update(model, controllerName, actionName) {
            return this.http.put("".concat(this.hostURL, "/").concat(controllerName, "/").concat(actionName), model).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["tap"])(function (data) {
              return data;
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["catchError"])(this.handleError));
          }
        }, {
          key: "delete",
          value: function _delete(id, controllerName, actionName) {
            return this.http["delete"]("".concat(this.hostURL, "/").concat(controllerName, "/").concat(actionName, "/").concat(id)).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["tap"])(function (data) {
              return data;
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["catchError"])(this.handleError));
          }
        }, {
          key: "disable",
          value: function disable(id, controllerName, actionName) {
            return this.http["delete"]("".concat(this.hostURL, "/").concat(controllerName, "/").concat(actionName, "/").concat(id)).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["tap"])(function (data) {
              return data;
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["catchError"])(this.handleError));
          }
        }, {
          key: "deleteForGood",
          value: function deleteForGood(p, controllerName, actionName) {
            return this.http["delete"]("".concat(p, "/").concat(this.hostURL, "/").concat(controllerName, "/").concat(actionName)).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["tap"])(function (data) {
              return data;
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["catchError"])(this.handleError));
          }
        }, {
          key: "asyncHttpRequest",
          value: function asyncHttpRequest(requestType, controllerName, actionName, params, model) {
            switch (requestType) {
              case "GET":
                {
                  return this.http.get("".concat(this.hostURL, "/").concat(controllerName, "/").concat(actionName, "/").concat(this.resolveRequestParameters(params))).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["tap"])(function (model) {
                    return model;
                  }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["catchError"])(this.handleError));
                }

              case "POST":
                {
                  if (model) {
                    return this.http.post("".concat(this.hostURL, "/").concat(controllerName, "/").concat(actionName), model).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["tap"])(function (data) {
                      return data;
                    }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["catchError"])(this.handleError));
                  }
                }
            }
          }
        }, {
          key: "handleError",
          value: function handleError(err) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["throwError"])(err.error);
          }
        }, {
          key: "resolveRequestParameters",
          value: function resolveRequestParameters(obj) {
            var params = "";
            var values = Object.values(obj);

            for (var _i = 0, _values = values; _i < _values.length; _i++) {
              var value = _values[_i];
              params += "/".concat(value);
            }

            return params;
          }
        }]);

        return HttpRestService;
      }();
      /***/

    },

    /***/
    "wlS1":
    /*!***********************************************************!*\
      !*** ./src/app/services/packages/add-packages.service.ts ***!
      \***********************************************************/

    /*! exports provided: AddPackagesService */

    /***/
    function wlS1(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AddPackagesService", function () {
        return AddPackagesService;
      });
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");
      /* harmony import */


      var _shared_http_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../shared/http-rest.service */
      "vulr");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var AddPackagesService = /*#__PURE__*/function (_shared_http_rest_ser12) {
        _inherits(AddPackagesService, _shared_http_rest_ser12);

        var _super12 = _createSuper(AddPackagesService);

        function AddPackagesService(http, router) {
          var _this103;

          _classCallCheck(this, AddPackagesService);

          _this103 = _super12.call(this, http);
          _this103.router = router;
          _this103.rootURL = "http://localhost:53420/api/package";
          return _this103;
        }

        _createClass(AddPackagesService, [{
          key: "createCoversheet",
          value: function createCoversheet(items) {
            return this.httpRequestBufferResponse("package", "CreateCoverSheet", null, items);
          }
        }, {
          key: "createCoversheetManifest",
          value: function createCoversheetManifest(items) {
            return this.httpRequestBufferResponseManifest("package", "CreateCoverSheetManifest", null, items);
          }
        }, {
          key: "createCoversheetLoans",
          value: function createCoversheetLoans(items) {
            return this.httpRequestBufferResponse("package", "CreateCoverSheetLoans", null, items);
          } // POST: api/PID_TBL

          /*
          postPackage(formData : Package){
            return this.http.post(this.rootURL+'/save', formData)
          }*/

        }, {
          key: "savePackage",
          value: function savePackage(pack) {
            return this.save(pack, "package", "save");
          }
        }, {
          key: "savePackageSortModule",
          value: function savePackageSortModule(pack) {
            return this.save(pack, "package", "saveSort");
          }
        }, {
          key: "createPackage",
          value: function createPackage(pack) {
            return this.save(pack, "package", "createAddPackages");
          }
        }, {
          key: "createPackageOriginal",
          value: function createPackageOriginal(pack) {
            return this.save(pack, "package", "create");
          }
        }, {
          key: "findOneByIdBC",
          value: function findOneByIdBC(id) {
            return this.findUniqueByBC(id, "package", "FindById");
          }
        }, {
          key: "findOneByPID",
          value: function findOneByPID(id) {
            return this.findUniqueById(id, "package", "FindByPID");
          }
        }, {
          key: "findPackLatestBU",
          value: function findPackLatestBU(id) {
            return this.findUniqueByBC(id, "package", "FindBySortAudit");
          }
        }, {
          key: "asyncPOSTRequest",
          value: function asyncPOSTRequest(pack) {
            return this.asyncHttpRequest("POST", "package", "Save", "", pack);
          }
        }, {
          key: "asyncDELETERequest",
          value: function asyncDELETERequest(pack) {
            return this.asyncHttpRequest("POST", "package", "deletePackage", "", pack);
          }
        }, {
          key: "asyncDISABLERequest",
          value: function asyncDISABLERequest(pack) {
            return this.asyncHttpRequest("POST", "package", "UpdateToDisable", "", pack);
          }
        }, {
          key: "updatePack",
          value: function updatePack(pack) {
            return this.save(pack, "package", "Update");
          }
        }, {
          key: "updateSort",
          value: function updateSort(pack) {
            return this.save(pack, "package", "UpdateSort");
          }
        }, {
          key: "updatePackCheckIn",
          value: function updatePackCheckIn(pack) {
            return this.save(pack, "package", "updatePackCheckIn");
          }
        }, {
          key: "updatePackUpdateModule",
          value: function updatePackUpdateModule(pack) {
            return this.save(pack, "package", "UpdatePackages");
          }
        }, {
          key: "updateAndReturnPack",
          value: function updateAndReturnPack(pack) {
            return this.save(pack, "package", "UpdateAndReturn");
          } //clear packages flag only not full object

        }, {
          key: "clearPackagesFlag",
          value: function clearPackagesFlag(pack) {
            return this.save(pack, "package", "ClearPackageFlag");
          }
        }, {
          key: "disablePack",
          value: function disablePack(id) {
            return this["delete"](id, "package", "delete");
          }
        }, {
          key: "deletePack",
          value: function deletePack(p) {
            return this.deleteForGood(p, "package", "deletePackage");
          }
        }, {
          key: "getAll",
          value: function getAll(id) {
            return this.findAllById(id, "package", "FindAll");
          }
        }, {
          key: "getByUserDate",
          value: function getByUserDate(name, pass) {
            return this.findAllByName(name, pass, "package", "findByUserDate");
          }
        }, {
          key: "getbyWildcard",
          value: function getbyWildcard(id) {
            return this.findAllByBC(id, "package", "wildcard");
          }
        }]);

        return AddPackagesService;
      }(_shared_http_rest_service__WEBPACK_IMPORTED_MODULE_2__["HttpRestService"]);

      AddPackagesService.ɵfac = function AddPackagesService_Factory(t) {
        return new (t || AddPackagesService)(_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵinject"](_angular_router__WEBPACK_IMPORTED_MODULE_0__["Router"]));
      };

      AddPackagesService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineInjectable"]({
        token: AddPackagesService,
        factory: AddPackagesService.ɵfac
      });
      /***/
    },

    /***/
    "x9+C":
    /*!********************************************************************************!*\
      !*** ./src/app/pages/imagelookup/alfresco-lookup/alfresco-lookup.component.ts ***!
      \********************************************************************************/

    /*! exports provided: AlfrescoLookupComponent */

    /***/
    function x9C(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AlfrescoLookupComponent", function () {
        return AlfrescoLookupComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var src_app_services_packages_add_packages_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! src/app/services/packages/add-packages.service */
      "wlS1");
      /* harmony import */


      var src_app_services_tid_tidcreartion_service_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! src/app/services/tid/tidcreartion-service.service */
      "eHNn");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! sweetalert2/dist/sweetalert2.js */
      "PdH4");
      /* harmony import */


      var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4__);
      /* harmony import */


      var src_app_services_tid_pdf_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! src/app/services/tid/pdf.service */
      "c1hE");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");

      var _c0 = ["viewer"];

      var AlfrescoLookupComponent = /*#__PURE__*/function () {
        function AlfrescoLookupComponent(_packageService, _tidService, router, _pdfService) {
          _classCallCheck(this, AlfrescoLookupComponent);

          this._packageService = _packageService;
          this._tidService = _tidService;
          this.router = router;
          this._pdfService = _pdfService;
          this.searchData = "";
          this.pdfSrc = "";
        } //pdfSrc :  string = 'http://www.africau.edu/images/default/sample.pdf';
        //pdfSrc :  string = 'C:\IronMountain\BNYM Tracking Tool\BNYMTT\im5034412375.pdf';


        _createClass(AlfrescoLookupComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.initializeTid();
            this.userlogged = JSON.parse(localStorage.getItem("user"));

            if (this.userlogged != null) {
              console.log("User logged: " + this.userlogged.User_Login);
            } else {
              this.router.navigate(["/login"]);
            }
          }
        }, {
          key: "ngAfterViewInit",
          value: function ngAfterViewInit() {}
        }, {
          key: "retrieveDocument",
          value: function retrieveDocument() {// this._tidService.retrieveDoc().subscribe((e : any) => {
            //   console.log(e);
            // });
          }
        }, {
          key: "wvDocumentLoadedHandler",
          value: function wvDocumentLoadedHandler() {
            // you can access docViewer object for low-level APIs
            var docViewer = this.wvInstance;
            var annotManager = this.wvInstance.annotManager; // and access classes defined in the WebViewer iframe

            var Annotations = this.wvInstance.Annotations;
            var rectangle = new Annotations.RectangleAnnotation();
            rectangle.PageNumber = 1;
            rectangle.X = 100;
            rectangle.Y = 100;
            rectangle.Width = 250;
            rectangle.Height = 250;
            rectangle.StrokeThickness = 5;
            rectangle.Author = annotManager.getCurrentUser();
            annotManager.addAnnotation(rectangle);
            annotManager.drawAnnotations(rectangle.PageNumber); // see https://www.pdftron.com/api/web/WebViewer.html for the full list of low-level APIs
          } //end method

        }, {
          key: "search",
          value: function search(event) {
            var _this104 = this;

            if (this.searchData !== "") {
              this.searchData = this.searchData.toLowerCase();

              if (this.searchData.startsWith("t")) {
                console.log("Started with TID");
                var tidNumber = this.searchData.replace("tid-", "");
                var sent = +tidNumber;

                this._tidService.findbyTID(sent).subscribe(function (data) {
                  console.log(data);
                  _this104.tidFound = data; //start retrieving doc
                  // this._tidService.retrieveDoc().subscribe((e : any) => {
                  //   console.log(e);
                  // });
                  //display doc

                  _this104.pdfSrc = _this104._pdfService.getPdf();
                }, function (error) {
                  sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
                    position: "top-end",
                    type: "error",
                    title: "No TIDs found",
                    showConfirmButton: false,
                    timer: 1500
                  });
                });
              } else {
                sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
                  position: "top-end",
                  type: "error",
                  title: "No TIDs found",
                  showConfirmButton: false,
                  timer: 1500
                });
              }
            }
          } //end searchMethod

        }, {
          key: "initializeTid",
          value: function initializeTid() {
            console.log(this.pdfSrc);
            this.pdfSrc = "";
            this.searchData = "";
            this.tidFound = {
              Package_Tracking_BC: "",
              Package_Tracking_ID: "",
              TID_ID: undefined,
              SRC_SYSTM_CD: "",
              DCMNT_BAR_CD: "",
              DCMNT_TYP_CD: "",
              COLLATERAL_KEY: "",
              BaileeNumber: "",
              LenderName: "",
              COLLATERAL_BAR_CODE: "",
              POOL_SAK: "",
              ACCOUNT_SAK: "",
              CUSTOMER_CODE: "",
              CUSTOMER_NAME: "",
              CNTRLLER_NUM: "",
              TRACK_LOCATION_CODE: "",
              ScanDate: null,
              DCMNT_CNDTN_CD: "",
              FILE_NAME: "",
              BaileeLetter: false,
              LoanNumber: "",
              BarcodeSticker: "",
              BaileeName: "",
              LoanOnManifest: false,
              Certification: false,
              ExceptionNotes: "",
              ExceptionReason: "",
              UserId: "",
              BorrowerName: "",
              Certified_In: ""
            };
          }
        }]);

        return AlfrescoLookupComponent;
      }();

      AlfrescoLookupComponent.ɵfac = function AlfrescoLookupComponent_Factory(t) {
        return new (t || AlfrescoLookupComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_packages_add_packages_service__WEBPACK_IMPORTED_MODULE_1__["AddPackagesService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_tid_tidcreartion_service_service__WEBPACK_IMPORTED_MODULE_2__["TidcreartionServiceService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_tid_pdf_service__WEBPACK_IMPORTED_MODULE_5__["PdfService"]));
      };

      AlfrescoLookupComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: AlfrescoLookupComponent,
        selectors: [["app-alfresco-lookup"]],
        viewQuery: function AlfrescoLookupComponent_Query(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c0, 1);
          }

          if (rf & 2) {
            var _t;

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.viewerRef = _t.first);
          }
        },
        decls: 52,
        vars: 5,
        consts: [[1, "row"], [1, "col-md-12"], [1, "card"], [1, "card-body"], [1, "m-t-0", "m-b-30"], [1, "col-lg-12"], [1, "form-inline"], [1, "form-group", "m-r-5"], ["for", "txtSearch", 1, "sr-only"], [1, "input-group"], ["autofocus", "", "type", "text", "placeholder", "Search: TN/PID/TID", "name", "txtSearch", "maxlength", "50", 1, "form-control", 3, "ngModel", "ngModelChange", "keydown.enter"], [1, "button-group"], ["id", "btnSearch", "type", "button", 1, "btn", "btn-effect-ripple", "btn-success", "waves-effect", "waves-light", "mr-2"], [1, "fa", "fa-search"], ["type", "button", 1, "btn", "btn-secondary", "waves-effect", "waves-light", "mr-2", 3, "click"], [1, "fa", "fa-trash"], [1, "col-lg-8"], [1, "container", 2, "overflow", "scroll", "height", "1000px"], [1, "col-lg-4"], [1, "list-group"], [1, "list-group-item", "list-group-item-action", "flex-column", "align-items-start"], [1, "d-flex", "w-100", "justify-content-between"], [1, "mb-1", "font-weight-bold"], [1, "mb-1"]],
        template: function AlfrescoLookupComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "h4", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "ALFRESCO IMAGE LOOKUP");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "label", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Search: TN/PID/TID");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "input", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function AlfrescoLookupComponent_Template_input_ngModelChange_14_listener($event) {
              return ctx.searchData = $event;
            })("keydown.enter", function AlfrescoLookupComponent_Template_input_keydown_enter_14_listener($event) {
              return ctx.search($event);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "button", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](17, "i", 13);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, " Search ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "button", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AlfrescoLookupComponent_Template_button_click_19_listener() {
              return ctx.initializeTid();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](20, "i", 15);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "Clear Search");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](22, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 16);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](25, "div", 17);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "div", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "a", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "div", 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "h5", 22);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](31, "TRACKING:");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "p", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "a", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "div", 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "h5", 22);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](37, "PID:");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "p", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "a", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "div", 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "h5", 22);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](43, "TID:");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "p", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](45);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "a", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "div", 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "h5", 22);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](49, "COLLATERAL ID:");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "p", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](51);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.searchData);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](19);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" TRACKING NUMBER: ", ctx.tidFound.Package_Tracking_BC, " ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("PID-", ctx.tidFound.Package_Tracking_ID, " ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("TID-", ctx.tidFound.TID_ID, " ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("COLLATERALID-", ctx.tidFound.COLLATERAL_KEY, "");
          }
        },
        directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["MaxLengthValidator"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["NgModel"]],
        styles: ["app-webviewer[_ngcontent-%COMP%] {\r\n    flex: 1;\r\n    height: 100%;\r\n    margin: 8px;\r\n    box-shadow: 1px 1px 10px #999;\r\n  }\r\n  \r\n  .viewer[_ngcontent-%COMP%] { width: 100%; height: 100%; }\r\n  \r\n  .webviewer[_ngcontent-%COMP%]{\r\n    width: 100%; height: 100%;\r\n  }\r\n  \r\n  #viewer[_ngcontent-%COMP%] {\r\n    overflow: visible;\r\n    height: 100%;\r\n  }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFsZnJlc2NvLWxvb2t1cC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0VBRUU7SUFDRSxPQUFPO0lBQ1AsWUFBWTtJQUNaLFdBQVc7SUFFSCw2QkFBNkI7RUFDdkM7O0VBRUEsVUFBVSxXQUFXLEVBQUUsWUFBWSxFQUFFOztFQUVyQztJQUNFLFdBQVcsRUFBRSxZQUFZO0VBQzNCOztFQUVBO0lBQ0UsaUJBQWlCO0lBQ2pCLFlBQVk7RUFDZCIsImZpbGUiOiJhbGZyZXNjby1sb29rdXAuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG4gIFxyXG4gIGFwcC13ZWJ2aWV3ZXIge1xyXG4gICAgZmxleDogMTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIG1hcmdpbjogOHB4O1xyXG4gICAgLXdlYmtpdC1ib3gtc2hhZG93OiAxcHggMXB4IDEwcHggIzk5OTtcclxuICAgICAgICAgICAgYm94LXNoYWRvdzogMXB4IDFweCAxMHB4ICM5OTk7XHJcbiAgfVxyXG4gIFxyXG4gIC52aWV3ZXIgeyB3aWR0aDogMTAwJTsgaGVpZ2h0OiAxMDAlOyB9XHJcbiAgXHJcbiAgLndlYnZpZXdlcntcclxuICAgIHdpZHRoOiAxMDAlOyBoZWlnaHQ6IDEwMCU7XHJcbiAgfVxyXG5cclxuICAjdmlld2VyIHtcclxuICAgIG92ZXJmbG93OiB2aXNpYmxlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gIH0iXX0= */"]
      });
      /***/
    },

    /***/
    "xw10":
    /*!***************************************************************************************!*\
      !*** ./src/app/pages/tid-creation/image-manipulation/image-manipulation.component.ts ***!
      \***************************************************************************************/

    /*! exports provided: ImageManipulationComponent */

    /***/
    function xw10(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ImageManipulationComponent", function () {
        return ImageManipulationComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/material/dialog */
      "0IaG");
      /* harmony import */


      var cropperjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! cropperjs */
      "urRO");
      /* harmony import */


      var cropperjs__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(cropperjs__WEBPACK_IMPORTED_MODULE_2__);
      /* harmony import */


      var file_saver__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! file-saver */
      "Iab2");
      /* harmony import */


      var file_saver__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(file_saver__WEBPACK_IMPORTED_MODULE_3__);
      /* harmony import */


      var _angular_material_button__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/material/button */
      "bTqV");

      var _c0 = ["image"];

      var ImageManipulationComponent = /*#__PURE__*/function () {
        function ImageManipulationComponent(_dialogRef, data) {
          _classCallCheck(this, ImageManipulationComponent);

          this._dialogRef = _dialogRef;
          this.data = data;
          this.rotated = false;
        }

        _createClass(ImageManipulationComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            //console.log(this.imageSource , "FICKAsd");
            console.log("DATA image source", this.data);
            this.imageSource = this.data.images;
            this.pidRecieved = this.data.sentPid;
          }
        }, {
          key: "ngAfterViewInit",
          value: function ngAfterViewInit() {
            this.imageElement.nativeElement.src = this.imageSource;
            this.options = {
              preview: '.img-preview',
              viewMode: 0,
              scalable: true,
              modal: true,
              movable: true,
              zoomOnWheel: true,
              responsive: true,
              checkOrientation: true,
              dragMode: 'none',
              autocrop: true,
              rotatable: true,
              ready: function ready(e) {
                console.log(e.type);
              },
              cropstart: function cropstart(e) {},
              cropmove: function cropmove(e) {},
              cropend: function cropend(e) {},
              crop: function crop(e) {
                var data = e.detail;
              },
              zoom: function zoom(e) {}
            };
            this.cropper = new cropperjs__WEBPACK_IMPORTED_MODULE_2___default.a(this.imageElement.nativeElement, this.options);
          }
        }, {
          key: "rotate",
          value: function rotate() {
            console.log(this.rotated);

            if (this.rotated == false) {
              this.rotated = true;
              this.cropper.rotate(90); //this.cropper = this.cropper.setAspectRatio(9/16);

              this.options.viewMode = 0; //this.cropper = this.cropper.setAspectRatio(3/2);
              //this.cropper = this.cropper.move(0, 0);

              this.cropper.zoom(-0.5);
            } else {
              this.rotated = false;
              this.cropper.reset();
            }
          }
        }, {
          key: "rotateLeft",
          value: function rotateLeft() {
            console.log(this.rotated);

            if (this.rotated == false) {
              this.rotated = true;
              this.cropper.rotate(-90);
              this.options.viewMode = 0; //this.cropper = this.cropper.setAspectRatio(3/2);

              this.cropper.zoom(-0.5);
            } else {
              this.rotated = false;
              this.cropper.reset();
            } //    this.cropper.zoom(-0.6);

          }
        }, {
          key: "getImageCropped",
          value: function getImageCropped() {
            var canvas = this.cropper.getCroppedCanvas({
              width: 4000,
              height: 4000
            });
            var image = canvas.toDataURL("image/png");
            console.log(image);
            this.editImage = image;
            var blob = new Blob([image], {
              "type": "image/png"
            });

            var blobToFile = function blobToFile(theBlob, fileName) {
              var b = theBlob; //A Blob() is almost a File() - it's just missing the two properties below which we will add

              b.lastModifiedDate = new Date();
              b.name = fileName; //Cast to a File() type

              return theBlob;
            }; //var blob = new Blob(["Hello, world!"], {type: "text/plain;charset=utf-8"});


            Object(file_saver__WEBPACK_IMPORTED_MODULE_3__["saveAs"])(image, "PID-" + this.pidRecieved + ".png");
          }
        }, {
          key: "replaceImage",
          value: function replaceImage() {
            var canvas = this.cropper.getCroppedCanvas({
              width: 3840,
              height: 2160
            });
            var image = canvas.toDataURL("image/tif"); //console.log(image);

            this.editImage = image;
            this.cropper.setCanvasData;
          }
        }, {
          key: "dataURItoBlob",
          value: function dataURItoBlob(dataURI) {
            var byteString = window.atob(dataURI);
            var arrayBuffer = new ArrayBuffer(byteString.length);
            var int8Array = new Uint8Array(arrayBuffer);

            for (var i = 0; i < byteString.length; i++) {
              int8Array[i] = byteString.charCodeAt(i);
            }

            var blob = new Blob([int8Array], {
              type: 'image/png'
            });
            return blob;
          }
        }, {
          key: "sendToImageManipulation",
          value: function sendToImageManipulation() {
            if (this.rotated) {
              var canvas = this.cropper.getCroppedCanvas({
                width: 3840,
                height: 2160
              });
              console.log("canvas rotated");
            } else {
              var canvas = this.cropper.getCroppedCanvas({
                width: 3840,
                height: 2160
              });
            } //console.log(canvas);


            var image = canvas.toDataURL("image/png"); //console.log(image);

            this.editImage = image; // closing itself and sending data to parent component

            this._dialogRef.close({
              data: image
            });
          }
        }]);

        return ImageManipulationComponent;
      }(); //end class


      ImageManipulationComponent.ɵfac = function ImageManipulationComponent_Factory(t) {
        return new (t || ImageManipulationComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"]));
      };

      ImageManipulationComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: ImageManipulationComponent,
        selectors: [["app-image-manipulation"]],
        viewQuery: function ImageManipulationComponent_Query(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c0, 1);
          }

          if (rf & 2) {
            var _t;

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.imageElement = _t.first);
          }
        },
        inputs: {
          imageSource: "imageSource"
        },
        decls: 22,
        vars: 0,
        consts: [[1, "container"], [1, "row"], [1, "col-md-9"], [1, "docs-demo"], [1, "img-container"], [2, "max-height", "100%", "max-width", "100%"], ["image", ""], [1, "col-md-3"], [1, "docs-preview", "clearfix"], [1, "img-preview", "preview-lg"], [1, "col-md-3", "docs-toggles"], ["title", "Rotate Image", "mat-button", "", "color", "primary", 1, "btn", "btn-outline-primary", "mr-1", 3, "click"], ["title", "getImageCropped", "mat-button", "", "color", "primary", "href", "editImage", "download", "image.png", 1, "btn", "btn-outline-primary", "mr-1", 3, "click"], ["title", "getImageCropped", "mat-button", "", "color", "primary", "href", "editImage", 1, "btn", "btn-outline-primary", "mr-1", 3, "click"]],
        template: function ImageManipulationComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "h3");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "Edit Image:");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "img", 5, 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "div", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "button", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ImageManipulationComponent_Template_button_click_13_listener() {
              return ctx.rotate();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "Rotate Right ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "button", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ImageManipulationComponent_Template_button_click_15_listener() {
              return ctx.rotateLeft();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "Rotate Left");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "button", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ImageManipulationComponent_Template_button_click_17_listener() {
              return ctx.getImageCropped();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "Download");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](19, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "button", 13);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ImageManipulationComponent_Template_button_click_20_listener() {
              return ctx.sendToImageManipulation();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "Replace with selected");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }
        },
        directives: [_angular_material_button__WEBPACK_IMPORTED_MODULE_4__["MatButton"]],
        styles: [".btn[_ngcontent-%COMP%] {\r\n  padding-left: 0.75rem;\r\n  padding-right: 0.75rem;\r\n}\r\n\r\nlabel.btn[_ngcontent-%COMP%] {\r\n  margin-bottom: 0;\r\n}\r\n\r\n.d-flex[_ngcontent-%COMP%]    > .btn[_ngcontent-%COMP%] {\r\n  flex: 1;\r\n}\r\n\r\n.carbonads[_ngcontent-%COMP%] {\r\n  border: 1px solid #ccc;\r\n  border-radius: 0.25rem;\r\n  font-size: 0.875rem;\r\n  overflow: hidden;\r\n  padding: 1rem;\r\n}\r\n\r\nimg[_ngcontent-%COMP%] {\r\n  display: block;\r\n\r\n  \r\n  max-width: 100%;\r\n}\r\n\r\n.carbon-wrap[_ngcontent-%COMP%] {\r\n  overflow: hidden;\r\n}\r\n\r\n.carbon-img[_ngcontent-%COMP%] {\r\n  clear: left;\r\n  display: block;\r\n  float: left;\r\n}\r\n\r\n.carbon-text[_ngcontent-%COMP%], .carbon-poweredby[_ngcontent-%COMP%] {\r\n  display: block;\r\n  margin-left: 140px;\r\n}\r\n\r\n.carbon-text[_ngcontent-%COMP%], .carbon-text[_ngcontent-%COMP%]:hover, .carbon-text[_ngcontent-%COMP%]:focus {\r\n  color: #fff;\r\n  text-decoration: none;\r\n}\r\n\r\n.carbon-poweredby[_ngcontent-%COMP%], .carbon-poweredby[_ngcontent-%COMP%]:hover, .carbon-poweredby[_ngcontent-%COMP%]:focus {\r\n  color: #ddd;\r\n  text-decoration: none;\r\n}\r\n\r\n@media (min-width: 768px) {\r\n  .carbonads[_ngcontent-%COMP%] {\r\n    float: right;\r\n    margin-bottom: -1rem;\r\n    margin-top: -1rem;\r\n    max-width: 360px;\r\n  }\r\n}\r\n\r\n.footer[_ngcontent-%COMP%] {\r\n  font-size: 0.875rem;\r\n}\r\n\r\n.heart[_ngcontent-%COMP%] {\r\n  color: #ddd;\r\n  display: block;\r\n  height: 2rem;\r\n  line-height: 2rem;\r\n  margin-bottom: 0;\r\n  margin-top: 1rem;\r\n  position: relative;\r\n  text-align: center;\r\n  width: 100%;\r\n}\r\n\r\n.heart[_ngcontent-%COMP%]:hover {\r\n  color: #ff4136;\r\n}\r\n\r\n.heart[_ngcontent-%COMP%]::before {\r\n  border-top: 1px solid #eee;\r\n  content: \" \";\r\n  display: block;\r\n  height: 0;\r\n  left: 0;\r\n  position: absolute;\r\n  right: 0;\r\n  top: 50%;\r\n}\r\n\r\n.heart[_ngcontent-%COMP%]::after {\r\n  background-color: #fff;\r\n  content: \"\u2665\";\r\n  padding-left: 0.5rem;\r\n  padding-right: 0.5rem;\r\n  position: relative;\r\n  z-index: 1;\r\n}\r\n\r\n.docs-demo[_ngcontent-%COMP%] {\r\n  margin-bottom: 1rem;\r\n  overflow: hidden;\r\n  padding: 2px;\r\n}\r\n\r\n.img-container[_ngcontent-%COMP%], .img-preview[_ngcontent-%COMP%] {\r\n  background-color: #f7f7f7;\r\n  text-align: center;\r\n  width: 100%;\r\n}\r\n\r\n.img-container[_ngcontent-%COMP%] {\r\n  max-height: 497px;\r\n  min-height: 200px;\r\n}\r\n\r\n@media (min-width: 768px) {\r\n  .img-container[_ngcontent-%COMP%] {\r\n    min-height: 497px;\r\n  }\r\n}\r\n\r\n.img-container[_ngcontent-%COMP%]    > img[_ngcontent-%COMP%] {\r\n  max-width: 100%;\r\n}\r\n\r\n.docs-preview[_ngcontent-%COMP%] {\r\n  margin-right: -1rem;\r\n}\r\n\r\n.img-preview[_ngcontent-%COMP%] {\r\n  float: left;\r\n  margin-bottom: 0.5rem;\r\n  margin-right: 0.5rem;\r\n  margin-top: 5vw;\r\n\r\n  overflow: hidden;\r\n}\r\n\r\n.img-preview[_ngcontent-%COMP%]    > img[_ngcontent-%COMP%] {\r\n  max-width: 100%;\r\n}\r\n\r\n.preview-lg[_ngcontent-%COMP%] {\r\n  height: 12rem;\r\n  width: 19rem;\r\n}\r\n\r\n.preview-md[_ngcontent-%COMP%] {\r\n  height: 4.5rem;\r\n  width: 8rem;\r\n}\r\n\r\n.preview-sm[_ngcontent-%COMP%] {\r\n  height: 2.25rem;\r\n  width: 4rem;\r\n}\r\n\r\n.preview-xs[_ngcontent-%COMP%] {\r\n  height: 1.125rem;\r\n  margin-right: 0;\r\n  width: 2rem;\r\n}\r\n\r\n.docs-data[_ngcontent-%COMP%]    > .input-group[_ngcontent-%COMP%] {\r\n  margin-bottom: 0.5rem;\r\n}\r\n\r\n.docs-data[_ngcontent-%COMP%]   .input-group-prepend[_ngcontent-%COMP%]   .input-group-text[_ngcontent-%COMP%] {\r\n  min-width: 4rem;\r\n}\r\n\r\n.docs-data[_ngcontent-%COMP%]   .input-group-append[_ngcontent-%COMP%]   .input-group-text[_ngcontent-%COMP%] {\r\n  min-width: 3rem;\r\n}\r\n\r\n.docs-buttons[_ngcontent-%COMP%]    > .btn[_ngcontent-%COMP%], .docs-buttons[_ngcontent-%COMP%]    > .btn-group[_ngcontent-%COMP%], .docs-buttons[_ngcontent-%COMP%]    > .form-control[_ngcontent-%COMP%] {\r\n  margin-bottom: 0.5rem;\r\n  margin-right: 0.25rem;\r\n}\r\n\r\n.docs-toggles[_ngcontent-%COMP%]    > .btn[_ngcontent-%COMP%], .docs-toggles[_ngcontent-%COMP%]    > .btn-group[_ngcontent-%COMP%], .docs-toggles[_ngcontent-%COMP%]    > .dropdown[_ngcontent-%COMP%] {\r\n  margin-bottom: 0.5rem;\r\n}\r\n\r\n.docs-tooltip[_ngcontent-%COMP%] {\r\n  display: block;\r\n  margin: -0.5rem -0.75rem;\r\n  padding: 0.5rem 0.75rem;\r\n}\r\n\r\n.docs-tooltip[_ngcontent-%COMP%]    > .icon[_ngcontent-%COMP%] {\r\n  margin: 0 -0.25rem;\r\n  vertical-align: top;\r\n}\r\n\r\n.tooltip-inner[_ngcontent-%COMP%] {\r\n  white-space: normal;\r\n}\r\n\r\n.btn-upload[_ngcontent-%COMP%]   .tooltip-inner[_ngcontent-%COMP%], .btn-toggle[_ngcontent-%COMP%]   .tooltip-inner[_ngcontent-%COMP%] {\r\n  white-space: nowrap;\r\n}\r\n\r\n.btn-toggle[_ngcontent-%COMP%] {\r\n  padding: 0.5rem;\r\n}\r\n\r\n.btn-toggle[_ngcontent-%COMP%]    > .docs-tooltip[_ngcontent-%COMP%] {\r\n  margin: -0.5rem;\r\n  padding: 0.5rem;\r\n}\r\n\r\n@media (max-width: 400px) {\r\n  .btn-group-crop[_ngcontent-%COMP%] {\r\n    margin-right: -1rem !important;\r\n  }\r\n\r\n  .btn-group-crop[_ngcontent-%COMP%]    > .btn[_ngcontent-%COMP%] {\r\n    padding-left: 0.5rem;\r\n    padding-right: 0.5rem;\r\n  }\r\n\r\n  .btn-group-crop[_ngcontent-%COMP%]   .docs-tooltip[_ngcontent-%COMP%] {\r\n    margin-left: -0.5rem;\r\n    margin-right: -0.5rem;\r\n    padding-left: 0.5rem;\r\n    padding-right: 0.5rem;\r\n  }\r\n}\r\n\r\n.docs-options[_ngcontent-%COMP%]   .dropdown-menu[_ngcontent-%COMP%] {\r\n  width: 100%;\r\n}\r\n\r\n.docs-options[_ngcontent-%COMP%]   .dropdown-menu[_ngcontent-%COMP%]    > li[_ngcontent-%COMP%] {\r\n  font-size: 0.875rem;\r\n  padding: 0.125rem 1rem;\r\n}\r\n\r\n.docs-options[_ngcontent-%COMP%]   .dropdown-menu[_ngcontent-%COMP%]   .form-check-label[_ngcontent-%COMP%] {\r\n  display: block;\r\n}\r\n\r\n.docs-cropped[_ngcontent-%COMP%]   .modal-body[_ngcontent-%COMP%] {\r\n  text-align: center;\r\n}\r\n\r\n.docs-cropped[_ngcontent-%COMP%]   .modal-body[_ngcontent-%COMP%]    > img[_ngcontent-%COMP%], .docs-cropped[_ngcontent-%COMP%]   .modal-body[_ngcontent-%COMP%]    > canvas[_ngcontent-%COMP%] {\r\n  max-width: 100%;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImltYWdlLW1hbmlwdWxhdGlvbi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UscUJBQXFCO0VBQ3JCLHNCQUFzQjtBQUN4Qjs7QUFFQTtFQUNFLGdCQUFnQjtBQUNsQjs7QUFFQTtFQUNFLE9BQU87QUFDVDs7QUFFQTtFQUNFLHNCQUFzQjtFQUN0QixzQkFBc0I7RUFDdEIsbUJBQW1CO0VBQ25CLGdCQUFnQjtFQUNoQixhQUFhO0FBQ2Y7O0FBRUE7RUFDRSxjQUFjOztFQUVkLDBEQUEwRDtFQUMxRCxlQUFlO0FBQ2pCOztBQUVBO0VBQ0UsZ0JBQWdCO0FBQ2xCOztBQUVBO0VBQ0UsV0FBVztFQUNYLGNBQWM7RUFDZCxXQUFXO0FBQ2I7O0FBRUE7O0VBRUUsY0FBYztFQUNkLGtCQUFrQjtBQUNwQjs7QUFFQTs7O0VBR0UsV0FBVztFQUNYLHFCQUFxQjtBQUN2Qjs7QUFFQTs7O0VBR0UsV0FBVztFQUNYLHFCQUFxQjtBQUN2Qjs7QUFFQTtFQUNFO0lBQ0UsWUFBWTtJQUNaLG9CQUFvQjtJQUNwQixpQkFBaUI7SUFDakIsZ0JBQWdCO0VBQ2xCO0FBQ0Y7O0FBRUE7RUFDRSxtQkFBbUI7QUFDckI7O0FBRUE7RUFDRSxXQUFXO0VBQ1gsY0FBYztFQUNkLFlBQVk7RUFDWixpQkFBaUI7RUFDakIsZ0JBQWdCO0VBQ2hCLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLFdBQVc7QUFDYjs7QUFFQTtFQUNFLGNBQWM7QUFDaEI7O0FBRUE7RUFDRSwwQkFBMEI7RUFDMUIsWUFBWTtFQUNaLGNBQWM7RUFDZCxTQUFTO0VBQ1QsT0FBTztFQUNQLGtCQUFrQjtFQUNsQixRQUFRO0VBQ1IsUUFBUTtBQUNWOztBQUVBO0VBQ0Usc0JBQXNCO0VBQ3RCLFlBQVk7RUFDWixvQkFBb0I7RUFDcEIscUJBQXFCO0VBQ3JCLGtCQUFrQjtFQUNsQixVQUFVO0FBQ1o7O0FBRUE7RUFDRSxtQkFBbUI7RUFDbkIsZ0JBQWdCO0VBQ2hCLFlBQVk7QUFDZDs7QUFFQTs7RUFFRSx5QkFBeUI7RUFDekIsa0JBQWtCO0VBQ2xCLFdBQVc7QUFDYjs7QUFFQTtFQUNFLGlCQUFpQjtFQUNqQixpQkFBaUI7QUFDbkI7O0FBRUE7RUFDRTtJQUNFLGlCQUFpQjtFQUNuQjtBQUNGOztBQUVBO0VBQ0UsZUFBZTtBQUNqQjs7QUFFQTtFQUNFLG1CQUFtQjtBQUNyQjs7QUFFQTtFQUNFLFdBQVc7RUFDWCxxQkFBcUI7RUFDckIsb0JBQW9CO0VBQ3BCLGVBQWU7O0VBRWYsZ0JBQWdCO0FBQ2xCOztBQUVBO0VBQ0UsZUFBZTtBQUNqQjs7QUFFQTtFQUNFLGFBQWE7RUFDYixZQUFZO0FBQ2Q7O0FBRUE7RUFDRSxjQUFjO0VBQ2QsV0FBVztBQUNiOztBQUVBO0VBQ0UsZUFBZTtFQUNmLFdBQVc7QUFDYjs7QUFFQTtFQUNFLGdCQUFnQjtFQUNoQixlQUFlO0VBQ2YsV0FBVztBQUNiOztBQUVBO0VBQ0UscUJBQXFCO0FBQ3ZCOztBQUVBO0VBQ0UsZUFBZTtBQUNqQjs7QUFFQTtFQUNFLGVBQWU7QUFDakI7O0FBRUE7OztFQUdFLHFCQUFxQjtFQUNyQixxQkFBcUI7QUFDdkI7O0FBRUE7OztFQUdFLHFCQUFxQjtBQUN2Qjs7QUFFQTtFQUNFLGNBQWM7RUFDZCx3QkFBd0I7RUFDeEIsdUJBQXVCO0FBQ3pCOztBQUVBO0VBQ0Usa0JBQWtCO0VBQ2xCLG1CQUFtQjtBQUNyQjs7QUFFQTtFQUNFLG1CQUFtQjtBQUNyQjs7QUFFQTs7RUFFRSxtQkFBbUI7QUFDckI7O0FBRUE7RUFDRSxlQUFlO0FBQ2pCOztBQUVBO0VBQ0UsZUFBZTtFQUNmLGVBQWU7QUFDakI7O0FBRUE7RUFDRTtJQUNFLDhCQUE4QjtFQUNoQzs7RUFFQTtJQUNFLG9CQUFvQjtJQUNwQixxQkFBcUI7RUFDdkI7O0VBRUE7SUFDRSxvQkFBb0I7SUFDcEIscUJBQXFCO0lBQ3JCLG9CQUFvQjtJQUNwQixxQkFBcUI7RUFDdkI7QUFDRjs7QUFFQTtFQUNFLFdBQVc7QUFDYjs7QUFFQTtFQUNFLG1CQUFtQjtFQUNuQixzQkFBc0I7QUFDeEI7O0FBRUE7RUFDRSxjQUFjO0FBQ2hCOztBQUVBO0VBQ0Usa0JBQWtCO0FBQ3BCOztBQUVBOztFQUVFLGVBQWU7QUFDakIiLCJmaWxlIjoiaW1hZ2UtbWFuaXB1bGF0aW9uLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYnRuIHtcclxuICBwYWRkaW5nLWxlZnQ6IDAuNzVyZW07XHJcbiAgcGFkZGluZy1yaWdodDogMC43NXJlbTtcclxufVxyXG5cclxubGFiZWwuYnRuIHtcclxuICBtYXJnaW4tYm90dG9tOiAwO1xyXG59XHJcblxyXG4uZC1mbGV4ID4gLmJ0biB7XHJcbiAgZmxleDogMTtcclxufVxyXG5cclxuLmNhcmJvbmFkcyB7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICBib3JkZXItcmFkaXVzOiAwLjI1cmVtO1xyXG4gIGZvbnQtc2l6ZTogMC44NzVyZW07XHJcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICBwYWRkaW5nOiAxcmVtO1xyXG59XHJcblxyXG5pbWcge1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG5cclxuICAvKiBUaGlzIHJ1bGUgaXMgdmVyeSBpbXBvcnRhbnQsIHBsZWFzZSBkb24ndCBpZ25vcmUgdGhpcyAqL1xyXG4gIG1heC13aWR0aDogMTAwJTtcclxufVxyXG5cclxuLmNhcmJvbi13cmFwIHtcclxuICBvdmVyZmxvdzogaGlkZGVuO1xyXG59XHJcblxyXG4uY2FyYm9uLWltZyB7XHJcbiAgY2xlYXI6IGxlZnQ7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgZmxvYXQ6IGxlZnQ7XHJcbn1cclxuXHJcbi5jYXJib24tdGV4dCxcclxuLmNhcmJvbi1wb3dlcmVkYnkge1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIG1hcmdpbi1sZWZ0OiAxNDBweDtcclxufVxyXG5cclxuLmNhcmJvbi10ZXh0LFxyXG4uY2FyYm9uLXRleHQ6aG92ZXIsXHJcbi5jYXJib24tdGV4dDpmb2N1cyB7XHJcbiAgY29sb3I6ICNmZmY7XHJcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG59XHJcblxyXG4uY2FyYm9uLXBvd2VyZWRieSxcclxuLmNhcmJvbi1wb3dlcmVkYnk6aG92ZXIsXHJcbi5jYXJib24tcG93ZXJlZGJ5OmZvY3VzIHtcclxuICBjb2xvcjogI2RkZDtcclxuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbn1cclxuXHJcbkBtZWRpYSAobWluLXdpZHRoOiA3NjhweCkge1xyXG4gIC5jYXJib25hZHMge1xyXG4gICAgZmxvYXQ6IHJpZ2h0O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogLTFyZW07XHJcbiAgICBtYXJnaW4tdG9wOiAtMXJlbTtcclxuICAgIG1heC13aWR0aDogMzYwcHg7XHJcbiAgfVxyXG59XHJcblxyXG4uZm9vdGVyIHtcclxuICBmb250LXNpemU6IDAuODc1cmVtO1xyXG59XHJcblxyXG4uaGVhcnQge1xyXG4gIGNvbG9yOiAjZGRkO1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIGhlaWdodDogMnJlbTtcclxuICBsaW5lLWhlaWdodDogMnJlbTtcclxuICBtYXJnaW4tYm90dG9tOiAwO1xyXG4gIG1hcmdpbi10b3A6IDFyZW07XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLmhlYXJ0OmhvdmVyIHtcclxuICBjb2xvcjogI2ZmNDEzNjtcclxufVxyXG5cclxuLmhlYXJ0OjpiZWZvcmUge1xyXG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCAjZWVlO1xyXG4gIGNvbnRlbnQ6IFwiIFwiO1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIGhlaWdodDogMDtcclxuICBsZWZ0OiAwO1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICByaWdodDogMDtcclxuICB0b3A6IDUwJTtcclxufVxyXG5cclxuLmhlYXJ0OjphZnRlciB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcclxuICBjb250ZW50OiBcIuKZpVwiO1xyXG4gIHBhZGRpbmctbGVmdDogMC41cmVtO1xyXG4gIHBhZGRpbmctcmlnaHQ6IDAuNXJlbTtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgei1pbmRleDogMTtcclxufVxyXG5cclxuLmRvY3MtZGVtbyB7XHJcbiAgbWFyZ2luLWJvdHRvbTogMXJlbTtcclxuICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gIHBhZGRpbmc6IDJweDtcclxufVxyXG5cclxuLmltZy1jb250YWluZXIsXHJcbi5pbWctcHJldmlldyB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Y3ZjdmNztcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5pbWctY29udGFpbmVyIHtcclxuICBtYXgtaGVpZ2h0OiA0OTdweDtcclxuICBtaW4taGVpZ2h0OiAyMDBweDtcclxufVxyXG5cclxuQG1lZGlhIChtaW4td2lkdGg6IDc2OHB4KSB7XHJcbiAgLmltZy1jb250YWluZXIge1xyXG4gICAgbWluLWhlaWdodDogNDk3cHg7XHJcbiAgfVxyXG59XHJcblxyXG4uaW1nLWNvbnRhaW5lciA+IGltZyB7XHJcbiAgbWF4LXdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4uZG9jcy1wcmV2aWV3IHtcclxuICBtYXJnaW4tcmlnaHQ6IC0xcmVtO1xyXG59XHJcblxyXG4uaW1nLXByZXZpZXcge1xyXG4gIGZsb2F0OiBsZWZ0O1xyXG4gIG1hcmdpbi1ib3R0b206IDAuNXJlbTtcclxuICBtYXJnaW4tcmlnaHQ6IDAuNXJlbTtcclxuICBtYXJnaW4tdG9wOiA1dnc7XHJcblxyXG4gIG92ZXJmbG93OiBoaWRkZW47XHJcbn1cclxuXHJcbi5pbWctcHJldmlldyA+IGltZyB7XHJcbiAgbWF4LXdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4ucHJldmlldy1sZyB7XHJcbiAgaGVpZ2h0OiAxMnJlbTtcclxuICB3aWR0aDogMTlyZW07XHJcbn1cclxuXHJcbi5wcmV2aWV3LW1kIHtcclxuICBoZWlnaHQ6IDQuNXJlbTtcclxuICB3aWR0aDogOHJlbTtcclxufVxyXG5cclxuLnByZXZpZXctc20ge1xyXG4gIGhlaWdodDogMi4yNXJlbTtcclxuICB3aWR0aDogNHJlbTtcclxufVxyXG5cclxuLnByZXZpZXcteHMge1xyXG4gIGhlaWdodDogMS4xMjVyZW07XHJcbiAgbWFyZ2luLXJpZ2h0OiAwO1xyXG4gIHdpZHRoOiAycmVtO1xyXG59XHJcblxyXG4uZG9jcy1kYXRhID4gLmlucHV0LWdyb3VwIHtcclxuICBtYXJnaW4tYm90dG9tOiAwLjVyZW07XHJcbn1cclxuXHJcbi5kb2NzLWRhdGEgLmlucHV0LWdyb3VwLXByZXBlbmQgLmlucHV0LWdyb3VwLXRleHQge1xyXG4gIG1pbi13aWR0aDogNHJlbTtcclxufVxyXG5cclxuLmRvY3MtZGF0YSAuaW5wdXQtZ3JvdXAtYXBwZW5kIC5pbnB1dC1ncm91cC10ZXh0IHtcclxuICBtaW4td2lkdGg6IDNyZW07XHJcbn1cclxuXHJcbi5kb2NzLWJ1dHRvbnMgPiAuYnRuLFxyXG4uZG9jcy1idXR0b25zID4gLmJ0bi1ncm91cCxcclxuLmRvY3MtYnV0dG9ucyA+IC5mb3JtLWNvbnRyb2wge1xyXG4gIG1hcmdpbi1ib3R0b206IDAuNXJlbTtcclxuICBtYXJnaW4tcmlnaHQ6IDAuMjVyZW07XHJcbn1cclxuXHJcbi5kb2NzLXRvZ2dsZXMgPiAuYnRuLFxyXG4uZG9jcy10b2dnbGVzID4gLmJ0bi1ncm91cCxcclxuLmRvY3MtdG9nZ2xlcyA+IC5kcm9wZG93biB7XHJcbiAgbWFyZ2luLWJvdHRvbTogMC41cmVtO1xyXG59XHJcblxyXG4uZG9jcy10b29sdGlwIHtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICBtYXJnaW46IC0wLjVyZW0gLTAuNzVyZW07XHJcbiAgcGFkZGluZzogMC41cmVtIDAuNzVyZW07XHJcbn1cclxuXHJcbi5kb2NzLXRvb2x0aXAgPiAuaWNvbiB7XHJcbiAgbWFyZ2luOiAwIC0wLjI1cmVtO1xyXG4gIHZlcnRpY2FsLWFsaWduOiB0b3A7XHJcbn1cclxuXHJcbi50b29sdGlwLWlubmVyIHtcclxuICB3aGl0ZS1zcGFjZTogbm9ybWFsO1xyXG59XHJcblxyXG4uYnRuLXVwbG9hZCAudG9vbHRpcC1pbm5lcixcclxuLmJ0bi10b2dnbGUgLnRvb2x0aXAtaW5uZXIge1xyXG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbn1cclxuXHJcbi5idG4tdG9nZ2xlIHtcclxuICBwYWRkaW5nOiAwLjVyZW07XHJcbn1cclxuXHJcbi5idG4tdG9nZ2xlID4gLmRvY3MtdG9vbHRpcCB7XHJcbiAgbWFyZ2luOiAtMC41cmVtO1xyXG4gIHBhZGRpbmc6IDAuNXJlbTtcclxufVxyXG5cclxuQG1lZGlhIChtYXgtd2lkdGg6IDQwMHB4KSB7XHJcbiAgLmJ0bi1ncm91cC1jcm9wIHtcclxuICAgIG1hcmdpbi1yaWdodDogLTFyZW0gIWltcG9ydGFudDtcclxuICB9XHJcblxyXG4gIC5idG4tZ3JvdXAtY3JvcCA+IC5idG4ge1xyXG4gICAgcGFkZGluZy1sZWZ0OiAwLjVyZW07XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiAwLjVyZW07XHJcbiAgfVxyXG5cclxuICAuYnRuLWdyb3VwLWNyb3AgLmRvY3MtdG9vbHRpcCB7XHJcbiAgICBtYXJnaW4tbGVmdDogLTAuNXJlbTtcclxuICAgIG1hcmdpbi1yaWdodDogLTAuNXJlbTtcclxuICAgIHBhZGRpbmctbGVmdDogMC41cmVtO1xyXG4gICAgcGFkZGluZy1yaWdodDogMC41cmVtO1xyXG4gIH1cclxufVxyXG5cclxuLmRvY3Mtb3B0aW9ucyAuZHJvcGRvd24tbWVudSB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5kb2NzLW9wdGlvbnMgLmRyb3Bkb3duLW1lbnUgPiBsaSB7XHJcbiAgZm9udC1zaXplOiAwLjg3NXJlbTtcclxuICBwYWRkaW5nOiAwLjEyNXJlbSAxcmVtO1xyXG59XHJcblxyXG4uZG9jcy1vcHRpb25zIC5kcm9wZG93bi1tZW51IC5mb3JtLWNoZWNrLWxhYmVsIHtcclxuICBkaXNwbGF5OiBibG9jaztcclxufVxyXG5cclxuLmRvY3MtY3JvcHBlZCAubW9kYWwtYm9keSB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG4uZG9jcy1jcm9wcGVkIC5tb2RhbC1ib2R5ID4gaW1nLFxyXG4uZG9jcy1jcm9wcGVkIC5tb2RhbC1ib2R5ID4gY2FudmFzIHtcclxuICBtYXgtd2lkdGg6IDEwMCU7XHJcbn1cclxuIl19 */"]
      });
      /***/
    },

    /***/
    "y9jz":
    /*!********************************************************!*\
      !*** ./src/app/services/packages/exception.service.ts ***!
      \********************************************************/

    /*! exports provided: ExceptionService */

    /***/
    function y9jz(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ExceptionService", function () {
        return ExceptionService;
      });
      /* harmony import */


      var _shared_http_rest_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ../shared/http-rest.service */
      "vulr");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var ExceptionService = /*#__PURE__*/function (_shared_http_rest_ser13) {
        _inherits(ExceptionService, _shared_http_rest_ser13);

        var _super13 = _createSuper(ExceptionService);

        function ExceptionService(http, router) {
          var _this105;

          _classCallCheck(this, ExceptionService);

          _this105 = _super13.call(this, http);
          _this105.router = router;
          _this105.rootURL = "http://localhost:53420/api/exception";
          return _this105;
        }

        _createClass(ExceptionService, [{
          key: "saveException",
          value: function saveException(pack) {
            return this.save(pack, "exception", "Save");
          }
        }, {
          key: "FindByTaskID",
          value: function FindByTaskID(id, name) {
            return this.findUniqueByIdStatus(name, id, "exception", "Update");
          }
        }, {
          key: "getAll",
          value: function getAll() {
            return this.findAll("exception", "FindAll");
          }
        }, {
          key: "updateException",
          value: function updateException(pack) {
            return this.save(pack, "exception", "Update");
          }
        }]);

        return ExceptionService;
      }(_shared_http_rest_service__WEBPACK_IMPORTED_MODULE_0__["HttpRestService"]);

      ExceptionService.ɵfac = function ExceptionService_Factory(t) {
        return new (t || ExceptionService)(_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵinject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]));
      };

      ExceptionService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineInjectable"]({
        token: ExceptionService,
        factory: ExceptionService.ɵfac,
        providedIn: 'root'
      });
      /***/
    },

    /***/
    "z76o":
    /*!***********************************************************!*\
      !*** ./src/app/pages/tid-creation/tid-creation.module.ts ***!
      \***********************************************************/

    /*! exports provided: TidCreationModule */

    /***/
    function z76o(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "TidCreationModule", function () {
        return TidCreationModule;
      });
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _generate_tid_generate_tid_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./generate-tid/generate-tid.component */
      "VZ5Q");
      /* harmony import */


      var _tid_creation_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./tid-creation-routing.module */
      "h1ku");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");
      /* harmony import */


      var src_app_services_coversheet_create_coversheet_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! src/app/services/coversheet/create-coversheet.service */
      "44oB");
      /* harmony import */


      var _cameracomponent_cameracomponent_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./cameracomponent/cameracomponent.component */
      "+II3");
      /* harmony import */


      var ngx_webcam__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ngx-webcam */
      "QKVY");
      /* harmony import */


      var _image_display_image_display_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ./image-display/image-display.component */
      "ev3G");
      /* harmony import */


      var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! @angular/material/dialog */
      "0IaG");
      /* harmony import */


      var _angular_material_button__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! @angular/material/button */
      "bTqV");
      /* harmony import */


      var _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! @angular/material/progress-spinner */
      "Xa2L");
      /* harmony import */


      var _image_manipulation_image_manipulation_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
      /*! ./image-manipulation/image-manipulation.component */
      "xw10");
      /* harmony import */


      var _dialogComponents_lender_dialog_lender_dialog_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
      /*! ./dialogComponents/lender-dialog/lender-dialog.component */
      "Bd4x");
      /* harmony import */


      var _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
      /*! @angular/material/autocomplete */
      "/1cH");
      /* harmony import */


      var _generate_tid_bailee_transformer_pipe__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
      /*! ./generate-tid/bailee-transformer.pipe */
      "NlK3");
      /* harmony import */


      var _dialogComponents_loan_dialog_loan_dialog_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(
      /*! ./dialogComponents/loan-dialog/loan-dialog.component */
      "Hkzo");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var TidCreationModule = function TidCreationModule() {
        _classCallCheck(this, TidCreationModule);
      };

      TidCreationModule.ɵfac = function TidCreationModule_Factory(t) {
        return new (t || TidCreationModule)();
      };

      TidCreationModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_17__["ɵɵdefineNgModule"]({
        type: TidCreationModule
      });
      TidCreationModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_17__["ɵɵdefineInjector"]({
        providers: [src_app_services_coversheet_create_coversheet_service__WEBPACK_IMPORTED_MODULE_5__["CreateCoversheetService"], _angular_material_dialog__WEBPACK_IMPORTED_MODULE_9__["MatDialogModule"], _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_14__["MatAutocompleteModule"]],
        imports: [[_angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"], ngx_webcam__WEBPACK_IMPORTED_MODULE_7__["WebcamModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"], _tid_creation_routing_module__WEBPACK_IMPORTED_MODULE_2__["TidCreationRoutingModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"], _angular_material_dialog__WEBPACK_IMPORTED_MODULE_9__["MatDialogModule"], _angular_material_button__WEBPACK_IMPORTED_MODULE_10__["MatButtonModule"], _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_11__["MatProgressSpinnerModule"], _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_14__["MatAutocompleteModule"]], _angular_material_dialog__WEBPACK_IMPORTED_MODULE_9__["MatDialogModule"]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_17__["ɵɵsetNgModuleScope"](TidCreationModule, {
          declarations: [_generate_tid_generate_tid_component__WEBPACK_IMPORTED_MODULE_1__["GenerateTidComponent"], _cameracomponent_cameracomponent_component__WEBPACK_IMPORTED_MODULE_6__["CameracomponentComponent"], _image_display_image_display_component__WEBPACK_IMPORTED_MODULE_8__["ImageDisplayComponent"], _image_manipulation_image_manipulation_component__WEBPACK_IMPORTED_MODULE_12__["ImageManipulationComponent"], _dialogComponents_lender_dialog_lender_dialog_component__WEBPACK_IMPORTED_MODULE_13__["LenderDialogComponent"], _generate_tid_bailee_transformer_pipe__WEBPACK_IMPORTED_MODULE_15__["BaileeTransformerPipe"], _dialogComponents_loan_dialog_loan_dialog_component__WEBPACK_IMPORTED_MODULE_16__["LoanDialogComponent"]],
          imports: [_angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"], ngx_webcam__WEBPACK_IMPORTED_MODULE_7__["WebcamModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"], _tid_creation_routing_module__WEBPACK_IMPORTED_MODULE_2__["TidCreationRoutingModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"], _angular_material_dialog__WEBPACK_IMPORTED_MODULE_9__["MatDialogModule"], _angular_material_button__WEBPACK_IMPORTED_MODULE_10__["MatButtonModule"], _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_11__["MatProgressSpinnerModule"], _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_14__["MatAutocompleteModule"]],
          exports: [_generate_tid_generate_tid_component__WEBPACK_IMPORTED_MODULE_1__["GenerateTidComponent"], _angular_material_dialog__WEBPACK_IMPORTED_MODULE_9__["MatDialogModule"]]
        });
      })();
      /***/

    },

    /***/
    "zCaV":
    /*!*********************************************************************!*\
      !*** ./src/app/pages/certification/certification-routing.module.ts ***!
      \*********************************************************************/

    /*! exports provided: CertificationRoutingModule */

    /***/
    function zCaV(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CertificationRoutingModule", function () {
        return CertificationRoutingModule;
      });
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _tid_certification_tid_certification_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./tid-certification/tid-certification.component */
      "OP5Q");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var routes = [{
        path: 'tidCertification',
        component: _tid_certification_tid_certification_component__WEBPACK_IMPORTED_MODULE_1__["TidCertificationComponent"]
      }];

      var CertificationRoutingModule = function CertificationRoutingModule() {
        _classCallCheck(this, CertificationRoutingModule);
      };

      CertificationRoutingModule.ɵfac = function CertificationRoutingModule_Factory(t) {
        return new (t || CertificationRoutingModule)();
      };

      CertificationRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineNgModule"]({
        type: CertificationRoutingModule
      });
      CertificationRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjector"]({
        imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"].forChild(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵsetNgModuleScope"](CertificationRoutingModule, {
          imports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]],
          exports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]]
        });
      })();
      /***/

    },

    /***/
    "zUnb":
    /*!*********************!*\
      !*** ./src/main.ts ***!
      \*********************/

    /*! no exports provided */

    /***/
    function zUnb(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony import */


      var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/platform-browser */
      "jhN1");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./app/app.module */
      "ZAI4");
      /* harmony import */


      var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./environments/environment */
      "AytR");

      if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
      }

      _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["platformBrowser"]().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])["catch"](function (err) {
        return console.error(err);
      });
      /***/

    },

    /***/
    "zn8P":
    /*!******************************************************!*\
      !*** ./$$_lazy_route_resource lazy namespace object ***!
      \******************************************************/

    /*! no static exports found */

    /***/
    function zn8P(module, exports) {
      function webpackEmptyAsyncContext(req) {
        // Here Promise.resolve().then() is used instead of new Promise() to prevent
        // uncaught exception popping up in devtools
        return Promise.resolve().then(function () {
          var e = new Error("Cannot find module '" + req + "'");
          e.code = 'MODULE_NOT_FOUND';
          throw e;
        });
      }

      webpackEmptyAsyncContext.keys = function () {
        return [];
      };

      webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
      module.exports = webpackEmptyAsyncContext;
      webpackEmptyAsyncContext.id = "zn8P";
      /***/
    }
  }, [[0, "runtime", "vendor"]]]);
})();
//# sourceMappingURL=main-es5.js.map